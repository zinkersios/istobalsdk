# IstobalSDK

## Repository

[Homepage](https://bitbucket.org/zinkersios/istobalsdk/src/develop/
)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

IstobalSDK is compatible with Swift 4.x+ and the following platforms:

- iOS 10+

## Installation

IstobalSDK is available through [ZinkersPods](https://HDVillamil@bitbucket.org/zinkersios/zinkerspods.git). To install
it, simply add the following line to your Podfile:

```ruby
source 'https://HDVillamil@bitbucket.org/zinkersios/zinkerspods.git'

target 'your_project_target' do
use_frameworks!

pod 'IstobalSDK'
```

## Implementation
```swift
import IstobalSDK

```

### 1. Set the app configuration
Enter the data provided by Istobal using  `SDKConfiguration.shared`


```swift
setConfiguration(clientId: String, clientSecret: String, companyId: String)

```

You can also configure the views color scheme using  `SDKAppearance.shared`

```swift
setAppearance(primaryButtonBackground: UIColor? = nil, primaryButtonLabel: UIColor? = nil, primarTitle: UIColor? = nil, primaryLabel: UIColor? = nil, secondaryLabel: UIColor? = nil, specialLabel: UIColor? = nil, appBackground: UIColor? = nil, navigatioBarBackground: UIColor? = nil, navigationBarTint: UIColor? = nil, cellBackground: UIColor? = nil, cellLabelPrimary: UIColor? = nil, cellLabelSecondary: UIColor? = nil, hideByIstobal: Bool?, font: SDKTypography?)

```

Where the colors are all optional (the basic scheme will be applied for each color that is not specified ).

`hideByIstobal` will hide the istobal logo if true.

`hideByIstobal` will set the font for the UI. You can see the available fonts using `SDKAppearance.shared`

```swift
getAvailabeFonts()->[SDKTypography]

```

### 2. Register
Register a new user with an id and a hash provided by your company using  `        APIManager.sharedInstance`.


```swift
fetchRegister(with userId: String, hash: String, completionHandler: @escaping (Result<Bool>) -> Void)

```

If the registration is correct, the server's response will be 201. if the response is 409, then the user already exist and you should proceed to do a login

### 3. Login
Log the user un the system so the UI can show it's information using  `APIManager.sharedInstance`.


```swift
fetchLogIn(with userId: String, hash: String, completionHandler: @escaping (Result<Bool>) -> Void)

```

### 3. Use the User Interface
You can use the UIViewControllers provided by the SDK to display the info of the user using  `SDKNavigation.shared`

if you need to display the user vehicles:

```swift
perfomSegueToMyVehicles(_ caller: UIViewController)

```

if you need to display the user tickets:

```swift
perfomSegueToMyTickets(_ caller: UIViewController)

```

if you need to display an installation services:

```swift
openInstallationServicesWith(_ caller: UIViewController, installation: Installation, delegate: ServicesViewDelegate)

```

### 4. Services Delegate
The UI will only show the services and addons of a certain installation, but the purchase must be handled by your app. That's why you need to implment the ServicesViewDelegate


```swift
onServiceReadyToBuy(installationId: String, selectedService: Service, selectedAddons: [Addon])

```

### 5. Buy a service
the delegate will provide the selected information through the UI. With this you must proceed to make the payment. Once done, you must provide the information to the server so that the server can return the desired ticket using `APIManager.sharedInstance`.


```swift
payServices(totalPaid: Double, installationId: String, selectedService: Service, selectedAddons: [Addon], completionHandler: @escaping (Result<[String: Any]>) -> Void)

```

the `totalPaid` property has to be provided in cents


###### PROPRIETARY
[![Istobal](https://www.istobal.com/es/wp-content/uploads/2017/12/logo-istobal.png)](http://istobal.com)


###### SUPPORT:
[smartwash@istobal.com](smartwash@istobal.com)

###### AUTHOR
[![Zinkers](http://zinkers.net/istobal/zinkers.png)](http://zinkers.io)


## License

IstobalSDK is available under the MIT license. See the LICENSE file for more info.
