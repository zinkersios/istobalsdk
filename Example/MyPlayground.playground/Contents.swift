//: Playground - noun: a place where people can play

import UIKit

class Vehicle{
    
    var numberPlate: String = ""
    var doorsnumber: Int = 0
    var speed: Double = 0
    
    init(numberPlate: String, doorsnumber: Int) {
        self.numberPlate = numberPlate
        self.doorsnumber = doorsnumber
    }
    
    func accelerate(_ speed: Double){
        self.speed = speed
    }
    
    func stop(){
        while self.speed != 0 {
            self.speed = self.speed - 1
        }
    }
    
    func isStoped()->Bool{
        return self.speed == 0
    }
}

class Bike: Vehicle{
    
     init(numberPlate: String){
        super.init(numberPlate: numberPlate, doorsnumber: 0)
    }
    
    override func accelerate(_ speed: Double) {
        self.speed = speed/2
    }
}


let coche = Vehicle(numberPlate: "0000 XXX", doorsnumber: 5)
let moto = Bike(numberPlate: "0000 123")

coche.accelerate(50)
moto.accelerate(50)



