//
//  textFieldCell.swift
//  IstobalSDK_Example
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//  Copyright © 2018 Zinkers. All rights reserved.
//

import UIKit

protocol textFieldCellDelegate{
    func onTextUpdated(_ indexPath: IndexPath, newText: String)
}

class textFieldCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var dataTF: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    var delegate: textFieldCellDelegate!
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dataTF.addTarget(self, action:  #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.selectionStyle = .none
        self.dataTF.delegate = self
    }

    func setCell(delegate: textFieldCellDelegate, title: String, data: String, placeholder: String, indexPath: IndexPath){
        self.delegate = delegate
        self.indexPath = indexPath
        self.titleLabel.text = title
        self.setTextField(data: data, placeholder: placeholder)
    }
    
    func setTextField(data: String, placeholder: String){
        self.dataTF.placeholder = placeholder
        self.dataTF.text = data
         self.delegate.onTextUpdated(self.indexPath, newText: data)
    }
    
     @objc func  textFieldDidChange(_ textField: UITextField) {
        self.delegate.onTextUpdated(self.indexPath, newText: textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}
