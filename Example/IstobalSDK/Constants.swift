//
//  Constants.swift
//  IstobalSDK_Example
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//  Copyright © 2018 Zinkers. All rights reserved.
//

import Foundation
import UIKit

enum commons{
    static let accept = NSLocalizedString("accept", comment: "")
    static let cancel = NSLocalizedString("cancel", comment: "")
    static let activate = NSLocalizedString("activate", comment: "")
}

enum sideMenuTitle{
    static let toVehicles = NSLocalizedString("side_menu_to_cars", comment: "")
    static let toTickets = NSLocalizedString("side_menu_to_tickets", comment: "")
    static let toServices = NSLocalizedString("side_menu_to_services", comment: "")
}

enum sideMenuId{
    static let toVehicles = 1
    static let toTickets = 2
    static let toServices = 3
}

enum configuration{
    static let clientId = NSLocalizedString("client_id", comment: "")
    static let secretId = NSLocalizedString("secret_id", comment: "")
    static let user = NSLocalizedString("user", comment: "")
    static let userId = NSLocalizedString("user_id", comment: "")
    static let hideByIstobal = NSLocalizedString("hide_by_istobal", comment: "")
    static let typography = NSLocalizedString("typography", comment: "")
    static let bgColor = NSLocalizedString("background_color", comment: "")
    static let buttonsColor = NSLocalizedString("button_color", comment: "")
    static let navBacgroundColor = NSLocalizedString("nav_background_color", comment: "")
    static let choiceFont = NSLocalizedString("choice_a_font", comment: "")
    static let companyId = NSLocalizedString("company_id", comment: "")
    static let installationId = NSLocalizedString("installation_id", comment: "")
}

enum demoData{
    static let clientId = "4_ftoerk7kphws48soogocg0w0sk4ow488sk8g0kgwgk4ck8g0k"
    static let secretId = "487wxn9whq68os0c8ow88o48gog84cc80gcw00ks0g44ksgokw"
    static let user = "kjfsahfkjsahkfjhsakjfhsakljfhasdlkjhfasdlkjhfaskjhfadskjh"
    static let userId = "5"
    static let installationId = "29"
    static let companyId = "14"
}

enum cellId{
    static let textFieldCell = "textFieldCell"
    static let colorCell = "colorCell"
    static let hideByIstobalCell = "hideByIstobalCell"
}


enum cellHeight{
    static let textFieldCell:CGFloat = 60
    static let colorCell:CGFloat = 230
    static let hideByIstobalCell:CGFloat = 60
}
