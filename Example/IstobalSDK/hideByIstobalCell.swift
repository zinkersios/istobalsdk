//
//  hideByIstobalCell.swift
//  IstobalSDK_Example
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//  Copyright © 2018 Zinkers. All rights reserved.
//

import UIKit

protocol hydeByIstobalCellDelegate {
    func onDataChanged(_ indexPath: IndexPath, data: Bool)
}

class hideByIstobalCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dataSwitch: UISwitch!
    
    var delegate: hydeByIstobalCellDelegate!
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.titleLabel.text = configuration.hideByIstobal
    }
    
    func setCell(delegate: hydeByIstobalCellDelegate, indexPath: IndexPath){
        self.indexPath = indexPath
        self.delegate = delegate
        self.delegate.onDataChanged(self.indexPath, data: self.dataSwitch.isOn)
    }

    @IBAction func onDataChanged(_ sender: UISwitch) {
        self.delegate.onDataChanged(self.indexPath, data: sender.isOn)
    }
    
    
}
