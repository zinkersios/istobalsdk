//
//  menuItem.swift
//  haulap
//
//  Created by Hernán Darío Villamil on 30/5/18.
//  Copyright © 2018 Hernán Darío Villamil. All rights reserved.
//

import Foundation

enum menuItems{
    static let toVehicles = menuItem(sideMenuId.toVehicles, title: sideMenuTitle.toVehicles, icon: "ic_star")
    static let toTickets = menuItem(sideMenuId.toTickets, title: sideMenuTitle.toTickets, icon: "ic_star")
    static let toServices = menuItem(sideMenuId.toServices, title: sideMenuTitle.toServices, icon: "ic_star")
}


class menuItem: Codable{
    enum codingKeys: String, CodingKey{
        case id, title, icon
    }
    
    var id:Int?
    var title: String?
    var icon: String?
    
    func getId()->Int{
        return self.id != nil ? self.id! : -1
    }
    
    func getTitle()->String{
        return self.title != nil ? self.title! : ""
    }
    
    func getIcon()->String{
        return self.icon != nil ? self.icon! : ""
    }
    
    init(_ id: Int, title: String, icon: String){
        self.id = id
        self.icon = icon
        self.title = title
    }
    
}
