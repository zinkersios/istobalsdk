//
//  ViewController.swift
//  IstobalSDK
//
//  Created by Hernan on 07/24/2018.
//  Copyright (c) 2018 Hernan. All rights reserved.
//

import UIKit
import IstobalSDK
import SideMenu

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, textFieldCellDelegate, colorCellDeletage, typographySelectorDelegate, hydeByIstobalCellDelegate, ServicesViewDelegate{
    @IBOutlet weak var sideMenuButton: UIBarButtonItem!
    @IBOutlet weak var setDataButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var userMail: String?
    var clientId: String?
    var secretId: String?
    var companyId: String?
    var installationId: String?
    var userId: String?
    var backgroundColor: SDKColor?
    var navBarBackgroundColor: SDKColor?
    var buttonColor: SDKColor?
    var typography: SDKTypography?
    var hideByIstobal: Bool = false
    var indicator: UIActivityIndicatorView!
    var isLoged: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSideMenu()
        self.setTableView()
        self.setDataButton.title = commons.activate
        self.setActivateButton()
        self.setMenuButton()
    }
    
    func setActivateButton(){
        self.setDataButton.isEnabled = self.checkData()
    }
    
    func setTableView(){
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()

        let textFieldCell = UINib(nibName: cellId.textFieldCell, bundle: nil)
        self.tableView.register(textFieldCell, forCellReuseIdentifier: cellId.textFieldCell)
        
        let colorCell = UINib(nibName: cellId.colorCell, bundle: nil)
        self.tableView.register(colorCell, forCellReuseIdentifier: cellId.colorCell)
        
        let hideCell = UINib(nibName: cellId.hideByIstobalCell, bundle: nil)
        self.tableView.register(hideCell, forCellReuseIdentifier: cellId.hideByIstobalCell)
//        self.loadData()
    }
    
    func setSideMenu(){
        let vc = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
        vc.parentVC = self
        
        let sideMenuManager = SideMenuManager.default
        sideMenuManager.menuLeftNavigationController = UISideMenuNavigationController(rootViewController: vc)
        sideMenuManager.menuPresentMode = .menuSlideIn
        sideMenuManager.menuWidth = max(round(min((UIScreen.main.bounds.width) - 32, (UIScreen.main.bounds.height))), 240)
        sideMenuManager.menuFadeStatusBar = false
        sideMenuManager.menuEnableSwipeGestures = true
    }
    
    func checkData()->Bool{
        if self.userMail == nil || self.userMail == ""{return false}
        if self.userId == nil || self.userId == ""{return false}
        if self.clientId == nil || self.clientId == ""{return false}
        if self.secretId == nil || self.secretId == ""{return false}
        if self.installationId == nil || self.installationId == ""{return false}
        if self.companyId == nil || self.companyId == ""{return false}
        
        return true
    }
    
    func register(){
        self.indicator.startAnimating()
        APIManager.sharedInstance.fetchRegister(with: self.userId ?? "", hash: self.userMail ?? "") { (result) in
            if result.error == nil{
                self.login()
            }else{
                self.indicator.stopAnimating()
                self.isLoged = false
                self.setMenuButton()

            }
        }
    }
    
    func login(){
        APIManager.sharedInstance.fetchLogIn(with: self.userId ?? "", hash: self.userMail ?? "") { (result) in
            self.indicator.removeFromSuperview()
            debugPrint("logeado \(String(describing: result.value))")
//            debugPrint("acces token: \(String(describing: APIManager.sharedInstance.accessToken))")
            
            if result.error != nil{
                self.indicator.stopAnimating()
                self.isLoged = false
                self.setMenuButton()
            }else{
                self.isLoged = true
                self.setMenuButton()
            }
           
        }
    }

    
    func setConfiguration(){
        SDKConfiguration.shared.setConfiguration(clientId: self.clientId ?? "", clientSecret: self.secretId ?? "", companyId: self.companyId ?? "")
    }
    
    func setMenuButton(){
        self.sideMenuButton.isEnabled = self.isLoged
    }
    
    func setApparence(){
        SDKAppearance.shared.setAppearance(hideByIstobal: self.hideByIstobal, font: self.typography != nil ? self.typography : nil)
    }
    
    @IBAction func onSetDataClicked(_ sender: Any) {
        self.indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.indicator.frame = self.view.frame
        self.view.addSubview(indicator)
       
        self.setApparence()
        self.setConfiguration()
        self.register()
    }
    
    @IBAction func onSideMenuClicked(_ sender: Any) {
        self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    
    
    // MARK: - TableView Delegate and datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0,1,2,3,4,5: return tableView.dequeueReusableCell(withIdentifier: cellId.textFieldCell, for: indexPath)
        case 6,7,8: return tableView.dequeueReusableCell(withIdentifier: cellId.colorCell, for: indexPath)
        case 9: return UITableViewCell(style: .subtitle, reuseIdentifier: "typography")
        case 10: return tableView.dequeueReusableCell(withIdentifier: cellId.hideByIstobalCell, for: indexPath)
        
        default: return UITableViewCell()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: (cell as! textFieldCell).setCell(delegate: self, title: configuration.user, data: demoData.user, placeholder: configuration.user, indexPath: indexPath)
        case 1: (cell as! textFieldCell).setCell(delegate: self, title: configuration.userId, data: demoData.userId, placeholder: configuration.userId, indexPath: indexPath)
        case 2: (cell as! textFieldCell).setCell(delegate: self, title: configuration.clientId, data: demoData.clientId, placeholder: configuration.clientId, indexPath: indexPath)
        case 3: (cell as! textFieldCell).setCell(delegate: self, title: configuration.secretId, data: demoData.secretId, placeholder: configuration.secretId, indexPath: indexPath)
        case 4: (cell as! textFieldCell).setCell(delegate: self, title: configuration.companyId, data: demoData.companyId, placeholder: configuration.companyId, indexPath: indexPath)
        case 5: (cell as! textFieldCell).setCell(delegate: self, title: configuration.installationId, data: demoData.installationId, placeholder: configuration.installationId, indexPath: indexPath)
        case 6: (cell as! colorCell).setCell(delegate: self, title: configuration.bgColor, color: SDKColor(), indexPath: indexPath)
        case 7: (cell as! colorCell).setCell(delegate: self, title: configuration.buttonsColor, color: SDKColor(), indexPath: indexPath)
        case 8: (cell as! colorCell).setCell(delegate: self, title: configuration.navBacgroundColor, color: SDKColor(), indexPath: indexPath)
        case 9:
            cell.textLabel?.text = configuration.typography
            cell.detailTextLabel?.text = ""
            cell.selectionStyle = .none
        case 10: (cell as! hideByIstobalCell).setCell(delegate: self, indexPath: indexPath)
            
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0,1,2,3,4,5: return cellHeight.textFieldCell
        case 6,7,8: return cellHeight.colorCell
        case 9: return 50
        case 10: return cellHeight.hideByIstobalCell
            
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 9:
           let sb = UIStoryboard(name: "TypographySelector", bundle: nil)
           if let vc = sb.instantiateViewController(withIdentifier: "TypographySelectorController") as? TypographySelectorController{
                vc.indexPath = indexPath
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
            
            
        default: break
        }
    }
    
    // MARK: - Text Field Cell Delegate
    func onTextUpdated(_ indexPath: IndexPath, newText: String) {
        debugPrint("new text \(newText)")
        debugPrint("from \(indexPath.row)")
        
        switch indexPath.row {
        case 0: self.userMail = newText
        case 1: self.userId = newText
        case 2: self.clientId = newText
        case 3: self.secretId = newText
        case 4: self.companyId = newText
        case 5: self.installationId = newText
            
        default: break
        }
        self.setActivateButton()
        
    }
    
    // MARK: - Color Cell Delegate
    func onColorUpdated(_ indexPath: IndexPath, color: SDKColor) {
        debugPrint("new color")
        switch indexPath.row {
        case 6: self.backgroundColor = color
        case 7: self.buttonColor = color
        case 8: self.navBarBackgroundColor = color
            
        default: break
        }
        self.setActivateButton()
    }
    
    // MARK: - Hide By Istobal Delegate
    func onDataChanged(_ indexPath: IndexPath, data: Bool) {
        debugPrint("hide by istobal: \(data)")
        self.hideByIstobal = data
        self.setActivateButton()
    }
    
    // MARK: - Typography Selector Delegate
    func onFontSelected(_ indexPath: IndexPath, font: SDKTypography) {
        debugPrint("newFont")
        self.typography = font
        self.setActivateButton()
        if let cell = tableView.cellForRow(at: indexPath){
            cell.detailTextLabel?.text = font.getFamilyName()
            if let thisFont = font.getFonts().first{
                 cell.detailTextLabel?.font = UIFont(name: thisFont, size: 14)
            }
        }
    }
    
    // MARK: - Service View Delegate
    func onServiceReadyToBuy(installationId: String, selectedService: Service, selectedAddons: [Addon]) {
        debugPrint("installtion id: \(installationId)")
        debugPrint("selected service: \(selectedService.name)")
        
        for addon in selectedAddons{
            debugPrint("selected service: \(addon.name )")
        }
        
        self.afterPay(installationId: installationId, selectedService: selectedService, selectedAddons: selectedAddons)
    }
    
    func afterPay(installationId: String, selectedService: Service, selectedAddons: [Addon]){
        let totalPaid = self.getTotalPaid(selectedService: selectedService, selectedAddons: selectedAddons)
        APIManager.sharedInstance.payServices(totalPaid: totalPaid, installationId: installationId, selectedService: selectedService, selectedAddons: selectedAddons) { (result) in
            if result.error != nil{
                debugPrint("ha ocurrod un error")
                return
            }else{
                debugPrint("resultado: \(result)")
            }
        }

    }

    func getTotalPaid(selectedService: Service, selectedAddons: [Addon])->Double{
        var totalPaid:Double = 0
        totalPaid += selectedService.price
        
        for addon in selectedAddons{
            totalPaid += addon.price
        }

        return totalPaid * 100 // convertir a centimos
    }
}

