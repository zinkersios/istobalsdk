//
//  colorCell.swift
//  IstobalSDK_Example
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//  Copyright © 2018 Zinkers. All rights reserved.
//

import UIKit

protocol colorCellDeletage{
    func onColorUpdated(_ indexPath: IndexPath, color: SDKColor)
}

class colorCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var preView: UIView!
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    var color: SDKColor!
    var delegate: colorCellDeletage!
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.preView.layer.borderColor = UIColor.black.cgColor
        self.preView.layer.borderWidth = 1
        self.selectionStyle = .none
    }
    
    func setCell(delegate: colorCellDeletage, title: String, color: SDKColor, indexPath: IndexPath){
        self.delegate = delegate
        self.color = color
        self.indexPath = indexPath
        self.titleLabel.text = title
        self.setUI()
    }

    
    func setUI(){
       self.setColorSliders()
    }
    
    func setColorSliders(){
        self.redSlider.tintColor = UIColor.red
        self.redSlider.thumbTintColor = UIColor.red
        self.redSlider.value = self.color.red!
        
        self.greenSlider.tintColor = UIColor.green
        self.greenSlider.thumbTintColor = UIColor.green
        self.greenSlider.value = self.color.green!
        
        self.blueSlider.tintColor = UIColor.blue
        self.blueSlider.thumbTintColor = UIColor.blue
        self.blueSlider.value = self.color.blue!
        
        self.setPreView()
    }
    
    func setPreView(){
        self.preView.backgroundColor = self.color.getColor()
        self.delegate.onColorUpdated(self.indexPath, color: self.color)
    }
    

    @IBAction func onRedUpdated(_ sender: UISlider) {
        self.color.red = sender.value
        self.setPreView()
    }
    
    @IBAction func onGreenUpdated(_ sender: UISlider) {
        self.color.green = sender.value
        self.setPreView()
    }
    
    @IBAction func onBlueUpdated(_ sender: UISlider) {
        self.color.blue = sender.value
        self.setPreView()
    }
    
}
