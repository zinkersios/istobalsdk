//
//  TypographySelectorController.swift
//  IstobalSDK_Example
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//  Copyright © 2018 Zinkers. All rights reserved.
//

import UIKit
import IstobalSDK

protocol typographySelectorDelegate{
    func onFontSelected(_ indexPath: IndexPath, font: SDKTypography)
}

class TypographySelectorController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var fonts:[SDKTypography] = []
    var delegate: typographySelectorDelegate!
    var indexPath: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.setTableView()
        
    }
    
    func setUI(){
        self.titleLabel.text = configuration.choiceFont
        self.acceptButton.setTitle(commons.accept, for: .normal)
        self.acceptButton.isHidden = true
        self.cancelButton.setTitle(commons.cancel, for: .normal)
    }
    
    func setTableView(){
        self.tableView.dataSource = self
        self.tableView.delegate = self
        tableView.tableFooterView = UIView()

        self.loadData()
    }
    
    func loadData(){
        self.fonts = SDKAppearance.shared.getAvailabeFonts()
        self.tableView.reloadData()
    }

    @IBAction func onCancelTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onAcceptTap(_ sender: Any) {
    }
    
    // MARK: - TableView Delegate and datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fonts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let font = self.fonts[indexPath.row]
        cell.textLabel?.text = font.getFamilyName()
        if let fontExample = font.getFonts().first{
            cell.textLabel?.font = UIFont(name: fontExample, size: 17)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.onFontSelected(self.indexPath, font: self.fonts[indexPath.row])
        self.onCancelTap(self)
        
    }
    
    
}
