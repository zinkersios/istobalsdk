//
//  SideMenuController.swift
//  IstobalSDK_Example
//
//  Created by Hernán Darío Villamil on 24/07/2018.
//  Copyright © 2018 Zinkers. All rights reserved.
//

import UIKit
import IstobalSDK

class SideMenuController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var items:[menuItem] = []
    var parentVC:ViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTableView()

    }
    
    func setTableView(){
        self.loadData()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
//        let cell = UINib(nibName: cellId.sideMenuCell, bundle: nil)
//        self.tableView.register(cell, forCellReuseIdentifier: cellId.sideMenuCell)
//        self.tableView.reloadData()
//
//        let infoCell = UINib(nibName: cellId.sideMenuInfoCell, bundle: nil)
//        self.tableView.register(infoCell, forCellReuseIdentifier: cellId.sideMenuInfoCell)
        self.tableView.reloadData()
    }
    
    func loadData(){
        self.items.append(menuItems.toVehicles)
        self.items.append(menuItems.toTickets)
        self.items.append(menuItems.toServices)
        
        self.tableView.reloadData()
    }
    
    // MARK: - UITable
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.textLabel?.text = self.items[indexPath.row].getTitle()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.items[indexPath.row].getId() {
        case menuItems.toVehicles.getId(): SDKNavigation.shared.perfomSegueToMyVehicles(self)
        case menuItems.toTickets.getId(): SDKNavigation.shared.perfomSegueToMyTickets(self)
        case menuItems.toServices.getId(): SDKNavigation.shared.perfomSegueToInstallationServices(self, installationId: (self.parentVC?.installationId)!, delegate: self.parentVC!)
            
        default: break
        }
    }
}
