//
//  SDKColor.swift
//  IstobalSDK_Example
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//  Copyright © 2018 Zinkers. All rights reserved.
//

import Foundation
import UIKit

public class SDKColor{
    
    var red: Float?
    var green: Float?
    var blue: Float?
    
    init(){
        self.red = 0
        self.green = 0
        self.blue = 0
    }
    
    func getCGRed()->CGFloat{
        return self.red != nil ? CGFloat(self.red!) : 0
    }
    
    func getCGGreen()->CGFloat{
        return self.green != nil ? CGFloat(self.green!) : 0
    }
    
    func getCGBlue()->CGFloat{
        return self.blue != nil ? CGFloat(self.blue!) : 0
    }
    
    func getColor()->UIColor{
        return UIColor(red: self.getCGRed()/255.0, green: self.getCGGreen()/255.0, blue: self.getCGBlue()/255.0, alpha: 1.0)
    }
    
}
