#
# Be sure to run `pod lib lint IstobalSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'IstobalSDK'
  s.version          = '0.1.3'
  s.summary          = 'This is a SDK for Istobal and SmartWash'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://HDVillamil@bitbucket.org/zinkersios/istobalsdk.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Hernan' => 'hvillamil@zinkers.io' }
  s.source           = { :git => 'https://HDVillamil@bitbucket.org/zinkersios/istobalsdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'IstobalSDK/Classes/**/*'
  
   s.resource_bundles = {
     'IstobalSDK' => ['IstobalSDK/Assets/**/*']
   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'Alamofire'
   s.dependency 'Locksmith'
   s.dependency 'Device'
   s.dependency 'KVNProgress'
   s.dependency 'ETNavBarTransparent'
   s.dependency 'Goya'
   s.dependency 'NotificationBannerSwift'
   s.dependency 'ReachabilitySwift'
   s.dependency 'Kingfisher'
   s.dependency 'IQKeyboardManagerSwift'
   s.dependency 'Lightbox'
   s.dependency 'SwiftWebViewProgress'

end
