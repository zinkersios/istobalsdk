//
//  Instruction.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Instruction {
    let step: String
    let title: String
    let body: String
}

extension Instruction {
    static func getInstructionsInside() -> [Instruction] {
        var items = [Instruction]()
        
        var step = Instruction(step: "1", title: .localized(.instruction1), body: .localized(.instruction1Body))
        items.append(step)
        
        step = Instruction(step: "2", title: .localized(.instruction2), body: .localized(.instruction2Body))
        items.append(step)
        
        step = Instruction(step: "3", title: .localized(.instruction3), body: .localized(.instruction3Body))
        items.append(step)
        
        return items
    }
    
    static func getInstructionsOutside() -> [Instruction] {
        var items = [Instruction]()
        
        var step = Instruction(step: "1", title: .localized(.instruction1), body: .localized(.instruction1Body))
        items.append(step)
        
        step = Instruction(step: "2", title: .localized(.instruction2), body: .localized(.instruction2Body))
        items.append(step)
        
        step = Instruction(step: "3", title: .localized(.instruction3Outside), body: .localized(.instruction3OutsideBody))
        items.append(step)
        
        return items
    }
    
    static func getTicketInstructions() -> [Instruction] {
        var items = [Instruction]()
        
        var step = Instruction(step: "a", title: .localized(.ticketInstruction1Title), body: .localized(.ticketInstruction1Body))
        items.append(step)
        
        step = Instruction(step: "b", title: .localized(.ticketInstruction2Title), body: .localized(.ticketInstruction2Body))
        items.append(step)
        
        return items
    }
    
    static func getNoVehicleInstructions() -> [Instruction] {
        var items = [Instruction]()
        
        let step = Instruction(step: "a",
                               title: .localized(.ticketInstruction2Title),
                               body: .localized(.ticketInstruction2Body))
        items.append(step)
        
        return items
    }
}
