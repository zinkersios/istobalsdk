//
//  TerminalInstruction.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct TerminalInstruction {
    let title: String
    let body: String
    let image: String
    let showQR: Bool
}

extension TerminalInstruction {
    static func getInstructions() -> [TerminalInstruction] {
        var items = [TerminalInstruction]()
        
        var step = TerminalInstruction(title: .localized(.useTheTerminal),
                                       body: .localized(.useTheTerminalDescrip),
                                       image: "ic_lector",
                                       showQR: true)
        items.append(step)
        
        step = TerminalInstruction(title: .localized(.terminalTryAgain),
                                   body: .localized(.terminalTryAgainDescrip),
                                   image: "ic_intentalo",
                                   showQR: false)
        items.append(step)
        
        return items
    }
}
