//
//  FieldsTranslation.swift
//  istobal
//
//  Created by Shanti Rodríguez on 6/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct FieldsTranslation: Codable, JSONParselable {
    let field: String
    let value: String
}

extension FieldsTranslation {
    init?(json: JSONObject) {
        guard let field = json["field"] as? String else { return nil }
        guard let value = json["value"] as? String else { return nil }
        self.init(field: field, value: value)
    }
}
