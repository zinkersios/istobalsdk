//
//  Translation.swift
//  istobal
//
//  Created by Shanti Rodríguez on 6/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct Translation: Codable, JSONParselable {
    let fields: [FieldsTranslation]
    let language: String
}

extension Translation {
    init?(json: JSONObject) {
        guard let fields = FieldsTranslation.createRequiredInstances(from: json, arrayKey: "fields") else { return nil }
        guard let language = json["language"] as? String else { return nil }
        self.init(fields: fields, language: language)
    }
}
