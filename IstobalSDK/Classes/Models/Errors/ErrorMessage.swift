//
//  ErrorMessage.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 5/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct ErrorMessage: JSONParselable {
    let message: String
    let propertyPath: String
    let description: String
    
    init(message: String, propertyPath: String) {
        self.message = message
        self.propertyPath = propertyPath
        self.description = propertyPath + ": " + message
    }
    
    init?(json: [String: Any]) {
        guard let message = json["message"] as? String else { return nil }
        guard let propertyPath = json["propertyPath"] as? String else { return nil }
        self.init(message: message, propertyPath: propertyPath)
    }
}
