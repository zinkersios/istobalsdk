//
//  TicketInstallation.swift
//  istobal
//
//  Created by Shanti Rodríguez on 30/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct TicketInstallation: JSONParselable {
    let id: Int
    let companyName: String
    let image: String?
    let latitude: String
    let longitude: String
    let gtt: Bool
}

extension TicketInstallation {
    init?(json: [String: Any]) {
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let companyName = json[Constants.JSONKey.companyName] as? String else { return nil }
        guard let latitude = json["latitude"] as? String else { return nil }
        guard let longitude = json["longitude"] as? String else { return nil }
        guard let gtt = json["gtt"] as? Bool else { return nil }
        
        let image = json["image"] as? String
        self.init(id: id, companyName: companyName, image: image, latitude: latitude, longitude: longitude, gtt: gtt)
    }
}
