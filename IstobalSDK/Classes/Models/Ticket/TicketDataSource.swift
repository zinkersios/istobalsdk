//
//  TicketDataSource.swift
//  istobal
//
//  Created by Shanti Rodríguez on 3/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct TicketDataSource {
    let all: TicketsDataSource
    let active: TicketsDataSource
    let comsumed: TicketsDataSource
}
