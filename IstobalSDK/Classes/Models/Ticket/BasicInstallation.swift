//
//  BasicInstallation.swift
//  istobal
//
//  Created by Shanti Rodríguez on 30/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct BasicInstallation: JSONParselable {
    let id: Int
    let companyName: String
    let image: URL?
    let latitude: String
    let longitude: String
    let gtt: Bool
    let favourite: Bool
    let address: String?
    let valoration: Double?
    let valorations: Int?
    let allowFidelity: Bool
    let city: String
    let postalCode: String
    let state: String
}

extension BasicInstallation {
    init?(json: [String: Any]) {
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let companyName = json[Constants.JSONKey.companyName] as? String else { return nil }
        guard let latitude = json["latitude"] as? String else { return nil }
        guard let longitude = json["longitude"] as? String else { return nil }
        guard let gtt = json["gtt"] as? Bool else { return nil }
        guard let favourite = json["favourite"] as? Bool else { return nil }
        guard let city = json[Constants.JSONKey.city] as? String else { return nil }
        guard let postalCode = json[Constants.JSONKey.postalCode] as? String else { return nil }
        guard let state = json[Constants.JSONKey.state] as? String else { return nil }
        
        let image = URL(json: json, key: "image")
        let address = json["address"] as? String
        let valoration = json["valoration"] as? Double
        let valorations = json["valorations"] as? Int
        let allowFidelity = json["allowFidelity"] as? Bool ?? false
        
        self.init(id: id,
                  companyName: companyName,
                  image: image,
                  latitude: latitude,
                  longitude: longitude,
                  gtt: gtt,
                  favourite: favourite,
                  address: address,
                  valoration: valoration,
                  valorations: valorations,
                  allowFidelity: allowFidelity,
                  city: city,
                  postalCode: postalCode,
                  state: state)
    }
}
