//
//  TicketLine.swift
//  istobal
//
//  Created by Shanti Rodríguez on 26/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct TicketLine: JSONParselable {
    let created: String
    let expirationDate: String
    let name: String
    var serviceType: TypeOfService
    let status: TicketStatus
    let washesLeft: Int
    let codeId: String?
    let code: String?
}

extension TicketLine {
    init?(json: [String: Any]) {
        guard let created = json["created"] as? String else { return nil }
        guard let expirationDate = json["expirationDate"] as? String else { return nil }
        guard let name = json["name"] as? String else { return nil }
        guard let serviceType = TypeOfService(json: json, key: "serviceType") else { return nil }
        guard let washesLeft = json["washesLeft"] as? Int else { return nil }
        guard let status = json["status"] as? String else { return nil }
        guard let ticketStatus = TicketStatus(rawValue: status) else { return nil }
        
        let codeId = json["codeId"] as? String
        let code = json["code"] as? String
        
        self.init(created: created,
                  expirationDate: expirationDate,
                  name: name,
                  serviceType: serviceType,
                  status: ticketStatus,
                  washesLeft: washesLeft,
                  codeId: codeId,
                  code: code)
    }
}
