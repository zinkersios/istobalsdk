//
//  Ticket.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 10/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

struct Ticket: JSONParselable {
    let date: String
    let expirationDate: String
    let installation: BasicInstallation?
    let name: String
    let ticketId: String
    let totalPrice: Double
    var vehicleID: Int?
    let vehicleName: String?
    let status: TicketStatus
    let giftStatus: GiftStatus
    let id: Int
    let ticketLines: [TicketLine]
    let fidelityCardName: String?
    let fidelityCardLogo: URL?
    let totalPaid: Double
}

extension Ticket {
    init?(json: [String: Any]) {
        guard let date = json["date"] as? String else { return nil }
        guard let name = json["name"] as? String else { return nil }
        guard let ticketId = json["ticketId"] as? String else { return nil }
        guard let totalPrice = Double(json: json, key: "totalPrice") else { return nil }
        guard let status = json["status"] as? String else { return nil }
        guard let ticketStatus = TicketStatus(rawValue: status) else { return nil }
        guard let giftStatusString = json["giftStatus"] as? String else { return nil }
        guard let giftStatus = GiftStatus(rawValue: giftStatusString) else { return nil }
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let serviceTypes = TypeOfService.createRequiredInstances(from: json, arrayKey: "serviceTypes") else { return nil }
        guard let arrayTicketLines = TicketLine.createRequiredInstances(from: json, arrayKey: "ticketLines") else { return nil }
        guard let totalPaid = Double(json: json, key: "totalPaid") else { return nil }
        
        let vehicleId = json["vehicleId"] as? Int
        let vehicleName = json["vehicleName"] as? String
        let expirationDate = json["expirationDate"] as? String ?? ""
        let installation = BasicInstallation(json: json, key: "installation")
        let fidelityCardName = json["fidelityCardName"] as? String
        let fidelityCardLogo = URL(json: json, key: "fidelityCardLogo")
        let ticketLines = Ticket.setUpServiceIdentifiers(ticketLines: arrayTicketLines, serviceTypes: serviceTypes)
        
        self.init(date: date,
                  expirationDate: expirationDate,
                  installation: installation,
                  name: name,
                  ticketId: ticketId,
                  totalPrice: totalPrice,
                  vehicleID: vehicleId,
                  vehicleName: vehicleName,
                  status: ticketStatus,
                  giftStatus: giftStatus,
                  id: id,
                  ticketLines: ticketLines,
                  fidelityCardName: fidelityCardName,
                  fidelityCardLogo: fidelityCardLogo,
                  totalPaid: totalPaid)
    }
    
    /// last ticket purchased
    static func getLastTicketPurchased() -> Ticket? {
        guard let data = UserDefaults.User.data(forKey: .lastTicketPurchased),
            let json = NSKeyedUnarchiver.unarchiveObject(with: data) as? JSONObject
            else {
                return nil
        }
        
        guard let ticket = Ticket(json: json) else {
            return nil
        }
        
        return ticket
    }
}

extension Ticket {
    func paramatersForEditVehicle() -> JSONObject {
        var dic = JSONObject()
        
        if let vehicleID = self.vehicleID {
            dic["vehicleId"] = vehicleID
        }
        return dic
    }
    
    /*
     Lo que yo hago es buscar dentro de serviceTYpe de fuera de ticketLines, el ID del "service"
     entonces busco ese mismo dentro del ticketLine
     es decir miro "serviceType" de siempre, cojo su "service" y cojo su ID
     voy dentro de ticketLine
     y busco el mismo ID, para saber con cual trabajo
     */
    private static func setUpServiceIdentifiers(ticketLines: [TicketLine], serviceTypes: [TypeOfService]) -> [TicketLine] {
        var array = [TicketLine]()
        
        for serviceType in serviceTypes where serviceType.service?.id != nil {
            
            for ticketLine in ticketLines where ((ticketLine.serviceType.services?.filter({$0.id == serviceType.service?.id}).first) != nil) {
                var mutableItem = ticketLine
                mutableItem.serviceType.code = ticketLine.code
                mutableItem.serviceType.codeId = ticketLine.codeId
                mutableItem.serviceType.service = ticketLine.serviceType.services?.filter({$0.id == serviceType.service?.id}).first
                array.append(mutableItem)
            }
        }
        
        return array
    }
}
    
