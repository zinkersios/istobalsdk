//
//  InstallationContactData.swift
//  istobal
//
//  Created by Shanti Rodríguez on 7/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct InstallationContactData {
    let name: String
    let type: ContactDataType
}

extension InstallationContactData {
    static func getData(installation: Installation) -> [InstallationContactData] {
        let viewModel = InstallationViewModel(model: installation)
        
        let name = InstallationContactData(name: installation.companyName, type: .name)
        let address = InstallationContactData(name: viewModel.address, type: .address)
        let phone = InstallationContactData(name: installation.phone, type: .phone)
        
        var array = [name, address, phone]
        
        if let mail = installation.email {
            let email = InstallationContactData(name: mail, type: .email)
            array.append(email)
        }
        
        return array
    }
}
