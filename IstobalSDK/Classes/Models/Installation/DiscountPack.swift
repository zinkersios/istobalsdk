//
//  DiscountPack.swift
//  istobal
//
//  Created by Shanti Rodríguez on 24/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

public struct DiscountPack: Codable, JSONParselable {
    public let discount: Int
    public let number: Int
    public let initialRange: Int
    public var finalRange: Int
}

extension DiscountPack {
    init?(json: [String: Any]) {
        guard let discount = json["discount"] as? Int else { return nil }
        guard let number = json["number"] as? Int else { return nil }
        
        self.init(discount: discount,
                  number: number,
                  initialRange: number,
                  finalRange: number)
    }
}
