//
//  Combination.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/12/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

public struct Combination: Codable, JSONParselable {
    public let addons: [Int]
    public let duration: Int
}

extension Combination {
    init?(json: [String: Any]) {
        guard let addons = json["addons"] as? [Int] else { return nil }
        guard let duration = json["duration"] as? Int else { return nil }
        self.init(addons: addons, duration: duration)
    }
}
