//
//  Program.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Program: JSONParselable {
    let description: String
    let id: Int
    let name: String
    let price: Int
}

extension Program: Equatable {
    init?(json: [String: Any]) {
        guard let description = json["description"] as? String else { return nil }
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let name = json["name"] as? String else { return nil }
        guard let price = json["price"] as? Int else { return nil }
        self.init(description: description, id: id, name: name, price: price)
    }
    
    init?(json: Any?) {
        guard let json = json as? JSONObject else { return nil }
        guard let description = json["description"] as? String else { return nil }
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let name = json["name"] as? String else { return nil }
        guard let price = json["price"] as? Int else { return nil }
        self.init(description: description, id: id, name: name, price: price)
    }
}

//swiftlint:disable:next operator_whitespace
func ==(lhs: Program, rhs: Program) -> Bool {
    return lhs.id == rhs.id
}
