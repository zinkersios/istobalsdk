//
//  Report.swift
//  istobal
//
//  Created by Shanti Rodríguez on 9/3/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct Report {
    let installationID: Int
    let type: String
    let message: String
}

extension Report {
    func paramaters() -> JSONObject {
        var dic = JSONObject()
        dic["message"] = self.message
        dic["type"] = self.type
        
        return dic
    }
}
