//
//  InstallationFilter.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 26/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import MapKit

class InstallationFilter {
    private var latitude = "0"
    private var longitude = "0"
    var limit = "10"
    var offset: String?
    var location: CLLocation?
    var favourite: Bool?
    var certified: Bool?
    var promotion: Bool?
    
    func convertToDic() -> JSONObject {
        var dic = [String: String]()
        
        // set location
        if let location = location {
            latitude = "\(location.coordinate.latitude)"
            longitude = "\(location.coordinate.longitude)"
        }
        
        // next page
        if let offset = offset {
            dic[Constants.JSONKey.offset] = offset
        }
        
        /// get my favorites
        if let fav = favourite {
            dic["favourite"] = fav ? "true" : "false"
        }
        
        /// get centers with promotions
        if let promotion = promotion {
            dic["promotion"] = promotion ? "true" : "false"
        }
        
        /// get certified centers
        if let certified = certified {
            dic["certified"] = certified ? "true" : "false"
        }
        
        dic["lat"] = latitude
        dic["long"] = longitude
        //dic["limit"] = limit
        return dic
    }
}
