//
//  Service.swift
//  istobal
//
//  Created by Shanti Rodríguez on 29/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

public struct Service: Codable, JSONParselable {
    public let id: Int
    public let duration: Int
    public let name: String
    public var price: Double
    public let description: String?
    public let longDescription: String?
    public var addons: [Addon]
    public let combinations: [Combination]
    public var typeOfServiceName: String?
    public var hasBalance: Bool
    public var multiplier: Int // Indica cuantas veces se quiere comprar este servicio
    public var discountPack: DiscountPack? // Indica si el usuario selecciono un pack
}

extension Service: Equatable {
    init?(json: [String: Any]) {
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let translations = Translation.createRequiredInstances(from: json, arrayKey: Constants.JSONKey.translations) else { return nil }
        // addons?
        var addons = [Addon]()
        if let fixAddons = Addon.createRequiredInstances(from: json, arrayKey: "addons") {
            addons = fixAddons
        }
        
        // Combinations?
        var combinations = [Combination]()
        if let fixCombinations = Combination.createRequiredInstances(from: json, arrayKey: "combinations") {
            combinations = fixCombinations
        }
        
        let name = json["name"] as? String
        let translationVM = TranslationViewModel(translations: translations, name: name)
        let duration = json["duration"] as? Int ?? 0
        let price = Double(json: json, key: "price") ?? 0
        
        self.init(id: id,
                  duration: duration,
                  name: translationVM.name,
                  price: price,
                  description: translationVM.description,
                  longDescription: translationVM.longDescription,
                  addons: addons,
                  combinations: combinations,
                  typeOfServiceName: nil,
                  hasBalance: false,
                  multiplier: 1,
                  discountPack: nil)
    }
    
    init?(json: Any?) {
        guard let json = json as? JSONObject else { return nil }
        self.init(json: json)
    }
}

extension Service {
    var totalPriceWithAddons: Double {
        if self.addons.count > 0 {
            let totalAddons = self.addons.map({$0.price}).reduce(0, +)
            return self.price + totalAddons
        }
        return self.price
    }
    
    var totalPriceWithAddonsWithMultiplier: Double {
        return self.totalPriceWithAddons * Double(self.multiplier)
    }
    
    var totalPriceWithAddonsAndPacks: Double {
        if let pack = self.discountPack, self.multiplier >= pack.initialRange && self.multiplier <= pack.finalRange {
            return PriceUtils.applyPriceDiscount(discount: Double(pack.discount), price: self.totalPriceWithAddonsWithMultiplier)
        } else {
            return self.totalPriceWithAddonsWithMultiplier
        }
    }
}

//swiftlint:disable:next operator_whitespace
public func ==(lhs: Service, rhs: Service) -> Bool {
    return lhs.id == rhs.id
}
