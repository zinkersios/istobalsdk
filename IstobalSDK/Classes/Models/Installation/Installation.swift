//
//  Installation.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 26/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Installation: JSONParselable {
    let address: String
    let city: String
    let companyName: String
    let country: String
    var distance: Double?
    var favourite: Bool
    let id: Int
    let image: String?
    let latitude: String
    let logo: String?
    let longitude: String
    let postalCode: String
    let state: String
    let location: Location
    let lastValorations: [Valoration]?
    let valoration: Double?
    let valorations: Int?
    let credit: Float?
    let programs: [Program]?
    let lastWash: Program?
    let typeOfServices: [TypeOfService]?
    let timetable: String?
    let images: [URL]
    let phone: String
    var offers: [Offer]
    var email: String?
}

extension Installation {
    init?(json: [String: Any]) {
        guard let address = json[Constants.JSONKey.address] as? String else { return nil }
        guard let city = json[Constants.JSONKey.city] as? String else { return nil }
        guard let companyName = json[Constants.JSONKey.companyName] as? String else { return nil }
        guard let country = json[Constants.JSONKey.country] as? String else { return nil }
        guard let favourite = json["favourite"] as? Bool else { return nil }
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let latitude = json["latitude"] as? String else { return nil }
        guard let longitude = json["longitude"] as? String else { return nil }
        guard let postalCode = json[Constants.JSONKey.postalCode] as? String else { return nil }
        guard let state = json[Constants.JSONKey.state] as? String else { return nil }
        guard let images = (json["images"] as? [String]).flatMap({ $0.toURLArray() }) else { return nil }
        guard let phone = json["phone"] as? String else { return nil }
        guard let offers = Offer.createRequiredInstances(from: json, arrayKey: "offers") else { return nil }
        
        let image = json["image"] as? String
        let logo = json["logo"] as? String
        let distance = Double(json: json, key: "distance")
        let location = Location(latitude: latitude, longitude: longitude)
        let valoration = json["valoration"] as? Double
        let valorations = json["valorations"] as? Int
        let credit = json["credit"] as? Float
        let lastValorations = Valoration.createRequiredInstances(from: json, arrayKey: "lastValorations")
        let programs = Program.createRequiredInstances(from: json, arrayKey: "programs")
        let lastWash = Program.init(json: json["lastWash"])
        let typeOfServices = TypeOfService.createRequiredInstances(from: json, arrayKey: "serviceTypes")
        let timetable = json["timetable"] as? String
        let email = json["emailNotifications"] as? String
        
        self.init(address: address,
                  city: city,
                  companyName: companyName,
                  country: country,
                  distance: distance,
                  favourite: favourite,
                  id: id,
                  image: image,
                  latitude: latitude,
                  logo: logo,
                  longitude: longitude,
                  postalCode: postalCode,
                  state: state,
                  location: location,
                  lastValorations: lastValorations,
                  valoration: valoration,
                  valorations: valorations,
                  credit: credit,
                  programs: programs,
                  lastWash: lastWash,
                  typeOfServices: typeOfServices,
                  timetable: timetable,
                  images: images,
                  phone: phone,
                  offers: offers,
                  email: email)
    }
    
    init(id: Int) {
        self.init(address: "",
                  city: "",
                  companyName: "",
                  country: "",
                  distance: nil,
                  favourite: false,
                  id: id,
                  image: nil,
                  latitude: "",
                  logo: nil,
                  longitude: "",
                  postalCode: "",
                  state: "",
                  location: Location(latitude: 0, longitude: 0),
                  lastValorations: nil,
                  valoration: nil,
                  valorations: nil,
                  credit: nil,
                  programs: nil,
                  lastWash: nil,
                  typeOfServices: nil,
                  timetable: nil,
                  images: [URL](),
                  phone: "",
                  offers: [Offer](),
                  email: nil)
    }
    
    init(id: Int, typeOfServices: [TypeOfService]) {
        self.init(address: "",
                  city: "",
                  companyName: "",
                  country: "",
                  distance: nil,
                  favourite: false,
                  id: id,
                  image: nil,
                  latitude: "",
                  logo: nil,
                  longitude: "",
                  postalCode: "",
                  state: "",
                  location: Location(latitude: 0, longitude: 0),
                  lastValorations: nil,
                  valoration: nil,
                  valorations: nil,
                  credit: nil,
                  programs: nil,
                  lastWash: nil,
                  typeOfServices: typeOfServices,
                  timetable: nil,
                  images: [URL](),
                  phone: "",
                  offers: [Offer](),
                  email: nil)
    }
}
