//
//  Addon.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

public struct Addon: Codable, JSONParselable {
    public let id: Int
    public let name: String
    public let price: Double
    public let description: String
    public let longDescription: String?
}

extension Addon: Equatable {
    init?(json: [String: Any]) {
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let price = Double(json: json, key: "price") else { return nil }
        guard let translations = Translation.createRequiredInstances(from: json, arrayKey: Constants.JSONKey.translations) else { return nil }
        let name = json["name"] as? String
        let translationVM = TranslationViewModel(translations: translations, name: name)
        
        guard let description = translationVM.description else { return nil }
        
        self.init(id: id,
                  name: translationVM.name,
                  price: price,
                  description: description,
                  longDescription: translationVM.longDescription)
    }
}

//swiftlint:disable:next operator_whitespace
public func ==(lhs: Addon, rhs: Addon) -> Bool {
    return lhs.id == rhs.id
}
