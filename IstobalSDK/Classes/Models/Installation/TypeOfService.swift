//
//  TypeOfService.swift
//  istobal
//
//  Created by Shanti Rodríguez on 29/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct TypeOfService: Codable, JSONParselable {
    let id: Int
    let image: URL?
    let name: String
    let description: String?
    let services: [Service]?
    var service: Service?
    let status: TicketStatus
    var codeId: String?
    var code: String?
    let hasBalance: Bool
    
    /// Comprobar si el nuevo ticket, obtenido en el enpoint getTicket, ha cambiado su estado a comsumido
    func checkIfConsumed(with updatedTicket: Ticket) -> Bool {
        
        for ticketLine in updatedTicket.ticketLines {
            if self.codeId == ticketLine.codeId && ticketLine.status == .consumed {
                return true
            }
        }
        return false
    }
}

extension TypeOfService {
    init?(json: JSONObject) {
        guard let hasBalance = json["hasBalance"] as? Bool else { return nil }
        guard let translations = Translation.createRequiredInstances(from: json, arrayKey: Constants.JSONKey.translations) else { return nil }
        let name = json["name"] as? String
        let translationVM = TranslationViewModel(translations: translations, name: name)
        
        let services = TypeOfService.servicesWithName(json: json, name: translationVM.name, hasBalance: hasBalance)
        var service = Service.init(json: json["service"])
        service?.hasBalance = hasBalance
        
        let codeId = json["codeId"] as? String
        let code = json["code"] as? String
        let image = URL(json: json, key: "image")
        let id = json["id"] as? Int ?? 0
        
        var ticketStatus = TicketStatus.pending
        
        if let status = json["status"] as? String, let tStatus = TicketStatus(rawValue: status) {
            ticketStatus = tStatus
        }
        
        self.init(id: id,
                  image: image,
                  name: translationVM.name,
                  description: translationVM.description,
                  services: services,
                  service: service,
                  status: ticketStatus,
                  codeId: codeId,
                  code: code,
                  hasBalance: hasBalance)
    }
    
    private static func servicesWithName(json: JSONObject, name: String, hasBalance: Bool) -> [Service]? {
        guard let services = Service.createRequiredInstances(from: json, arrayKey: "services") else {
            return nil
        }
        
        var array = [Service]()
        
        for service in services {
            var mutableService = service
            mutableService.typeOfServiceName = name
            mutableService.hasBalance = hasBalance
            array.append(mutableService)
        }
        return array
    }
}

