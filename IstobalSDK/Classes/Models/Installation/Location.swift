//
//  Location.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 26/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Location: JSONParselable {
    let latitude: Double
    let longitude: Double
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init(latitude: String, longitude: String) {
        guard let lat = Double(latitude), let long = Double(longitude) else {
            self.latitude = Double(0)
            self.longitude = Double(0)
            return
        }
        
        self.latitude = lat
        self.longitude = long
    }
    
    init?(json: JSONObject) {
        guard let latitude = Double(json: json, key: "latitude") else { return nil }
        guard let longitude = Double(json: json, key: "longitude") else { return nil }
        self.init(latitude: latitude, longitude: longitude)
    }
}
