//
//  Vehicle.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Vehicle: JSONParselable {
    var brand: String?
    let dirtDays: Int
    let dirtIndex: Int
    let frameNumber: String?
    let id: Int
    let licenseNumber: String
    var model: String?
    var name: String
    let owners: [Owners]
    let washes: Int
    let tickets: [Ticket]
}

extension Vehicle {
    init?(json: [String: Any]) {
        guard let dirtDays = json["dirtDays"] as? Int else { return nil }
        guard let dirtIndex = json["dirtIndex"] as? Int else { return nil }
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let licenseNumber = json["licenseNumber"] as? String else { return nil }
        guard let owners = Owners.createRequiredInstances(from: json, arrayKey: "owners") else { return nil }
        guard let washes = json["washes"] as? Int else { return nil }
        guard let tickets = Ticket.createRequiredInstances(from: json, arrayKey: "tickets") else { return nil }
        
        let brand = json["brand"] as? String
        let model = json["model"] as? String
        let frameNumber = json["frameNumber"] as? String
        var name = " "
        
        if let vehicleName = json["name"] as? String {
            name = vehicleName
        }
        
        self.init(brand: brand,
                  dirtDays: dirtDays,
                  dirtIndex: dirtIndex,
                  frameNumber: frameNumber,
                  id: id,
                  licenseNumber: licenseNumber,
                  model: model,
                  name: name,
                  owners: owners,
                  washes: washes,
                  tickets: tickets)
    }
}

// Parameters
extension Vehicle {
    func convertToParamaters() -> JSONObject {
        var dic = JSONObject()
        dic["name"] = self.name
        
        if let model = self.model {
            dic["model"] = model
        }
        
        if let brand = self.brand {
            dic["brand"] = brand
        }
        
        return dic
    }
}
