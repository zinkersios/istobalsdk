//
//  Owners.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 6/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Owners: JSONParselable {
    let name: String?
    let surname: String?
    
    init(name: String?, surname: String?) {
        self.name = name
        self.surname = surname
    }
    
    init?(json: [String: Any]) {
        let name = json["name"] as? String
        let surname = json["surname"] as? String

        self.init(name: name, surname: surname)
    }
}
