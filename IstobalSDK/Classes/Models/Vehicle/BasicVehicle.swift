//
//  BasicVehicle.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct BasicVehicle: JSONParselable {
    let id: Int
    let name: String
    let isAssignedVehicle: Bool
    
    func convertToDictionary() -> JSONObject {
        let dic: JSONObject = [
            Constants.JSONKey.id: self.id,
            "name": self.name
        ]
        return dic
    }
}

extension BasicVehicle: Equatable {
    init?(json: [String: Any]) {
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        var vehicleName: String = .localized(.unnamedVehicle)
        
        if let name = json["name"] as? String {
            vehicleName = name
        }
        
        self.init(id: id, name: vehicleName, isAssignedVehicle: false)
    }
    
    /// get user vehicles
    static func getMyVehicles() -> [BasicVehicle] {
        var items = [BasicVehicle]()
        
        guard let data = UserDefaults.User.data(forKey: .myVehicles), let json = NSKeyedUnarchiver.unarchiveObject(with: data) as? JSONArray else {
            return items
        }
        
        items = json.compactMap { BasicVehicle(json: $0) }
        return items
    }
    
    // get my vehicles with no assigned car
    static func getMyVehiclesWithNoAssignedCar() -> [BasicVehicle]? {
        var vehicles = [BasicVehicle]()
        vehicles.append(BasicVehicle.withoutAssignedVehicle())
        vehicles += BasicVehicle.getMyVehicles()
        return vehicles
    }
    
    static func withoutAssignedVehicle() -> BasicVehicle {
        return BasicVehicle(id: 0, name: .localized(.noCarAssigned), isAssignedVehicle: true)
    }
}

//swiftlint:disable:next operator_whitespace
func ==(lhs: BasicVehicle, rhs: BasicVehicle) -> Bool {
    return lhs.id == rhs.id && lhs.name == rhs.name && lhs.isAssignedVehicle == rhs.isAssignedVehicle
}
