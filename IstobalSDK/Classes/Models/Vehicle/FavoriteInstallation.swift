//
//  FavoriteInstallation.swift
//  istobal
//
//  Created by Shanti Rodríguez on 29/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct FavoriteInstallation: JSONParselable {
    let id: Int
    let companyName: String
    
    func convertToDictionary() -> JSONObject {
        let dic: JSONObject = [
            Constants.JSONKey.id: self.id,
            Constants.JSONKey.companyName: self.companyName
        ]
        return dic
    }
    
    /// get my favorites
    static func getMyFavorites() -> [FavoriteInstallation] {
        var items = [FavoriteInstallation]()
        
        guard let data = UserDefaults.User.data(forKey: .favorites), let json = NSKeyedUnarchiver.unarchiveObject(with: data) as? JSONArray else {
            return items
        }
        
        items = json.compactMap { FavoriteInstallation(json: $0) }
        return items
    }
    
    /// check if it's favorite
    static func checkIfItsFavorite(with installation: Installation) -> Bool {
        let fav = FavoriteInstallation(id: installation.id, companyName: installation.companyName)
        let favorites = self.getMyFavorites()
        
        return favorites.contains(fav) ? true : false
    }
    
    /// save favorite
    static func addFavorite(with installation: Installation) {
        var favorites = self.getMyFavorites()
        let newFavorite = FavoriteInstallation(id: installation.id, companyName: installation.companyName)
        
        if !checkIfItsFavorite(with: installation) {
            favorites.append(newFavorite)
            
            /// convert favorites to dic
            let dicArray = favorites.map { $0.convertToDictionary() }
            APIHelper.saveMyFavorites(with: dicArray)
            
//            log.info("favorito añadido en local. dicArray: \(dicArray)")
            debugPrint("favorito añadido en local. dicArray: \(dicArray)")
        }
    }
    
    /// remove favorite
    static func removeFavorite(with installation: Installation) {
        let favoriteToDelete = FavoriteInstallation(id: installation.id, companyName: installation.companyName)
        var favorites = self.getMyFavorites()
        favorites.removeObject(obj: favoriteToDelete)
        
        /// convert remaining favorites to dic
        let dicArray = favorites.map { $0.convertToDictionary() }
        APIHelper.saveMyFavorites(with: dicArray)
        
//        log.info("favorito eliminado en local. dicArray: \(dicArray)")
        debugPrint("favorito eliminado en local. dicArray: \(dicArray)")
    }
}

extension FavoriteInstallation: Equatable {
    init?(json: [String: Any]) {
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let companyName = json[Constants.JSONKey.companyName] as? String else { return nil }
        
        self.init(id: id, companyName: companyName)
    }
}

//swiftlint:disable:next operator_whitespace
func ==(lhs: FavoriteInstallation, rhs: FavoriteInstallation) -> Bool {
    return lhs.id == rhs.id
}
