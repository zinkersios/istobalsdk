//
//  ValidateBilling.swift
//  istobal
//
//  Created by Shanti Rodríguez on 4/5/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct ValidateBilling {
    // MARK: - Properties
    private let billing: InvoiceCompany
    private var type: BillingData
    
    // Methods
    init(billing: InvoiceCompany, type: BillingData = .taxID) {
        self.billing = billing
        self.type = type
    }
    
    func brokenRule() -> BrokenRule? {
        switch type {
        case .taxID:
            return validate(string: billing.taxId)
        case .companyName:
            return validate(string: billing.companyName)
        case .address:
            return validate(string: billing.address)
        case .city:
            return validate(string: billing.city)
        case .state:
            return validate(string: billing.state)
        case .country:
            return validate(string: billing.country)
        case .postalCode:
            return validate(string: billing.postalCode)
        case .email:
            return validateEmail(billing.email)
        }
    }
    
    mutating func validateAll() -> BrokenRule? {
        for type in BillingData.data {
            self.type = type
            
            if let rule = brokenRule() {
                return rule
            }
        }
        return nil
    }
    
    // MARK: - Private Methods
    
    func validate(string: String) -> BrokenRule? {
        if containsWhiteSpaces(string: string) {
            return BrokenRule(propertyName: "\(type)", message: type.brokenRuleMessage)
        }
        return nil
    }
    
    func validateEmail(_ email: String) -> BrokenRule? {
        if containsWhiteSpaces(string: email) {
            return BrokenRule(propertyName: "\(type)", message: type.brokenRuleMessage)
        }
        
        if !email.isEmail {
            return BrokenRule(propertyName: "\(type)", message: .localized(.wrongFormatEmail))
        }
        return nil
    }
    
    func containsWhiteSpaces(string: String) -> Bool {
        let whitespaceSet = CharacterSet.whitespaces
        return string.trimmingCharacters(in: whitespaceSet).isEmpty
    }
}
