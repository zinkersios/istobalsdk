//
//  BrokenRule.swift
//  istobal
//
//  Created by Shanti Rodríguez on 4/5/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

struct BrokenRule {
    var propertyName: String
    var message: String
}
