//
//  SRNotification.swift
//  istobal
//
//  Created by Shanti Rodríguez on 6/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct SRNotification: JSONParselable {
    let screen: ScreenType
    let screenId: String
    let text: String
    let title: String
    let date: String
}

extension SRNotification {
    init?(json: JSONObject) {
        guard let screenId = json["screenId"] as? String else { return nil }
        guard let text = json["text"] as? String else { return nil }
        guard let title = json["title"] as? String else { return nil }
        
        // Get screen type
        var type = ScreenType.none
        
        if let screen = json["screen"] as? String, let screenType = ScreenType(rawValue: screen) {
            type = screenType
        }
        
        // Get date
        var customDate = " "
        if let strDate = json["date"] as? String, let date = Date.date(rfc8601Date: strDate) {
            customDate = date.notificationFormatted
        }
        
        self.init(screen: type,
                  screenId: screenId,
                  text: text,
                  title: title,
                  date: customDate)
    }
}
