//
//  TableSection.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 30/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct TableSection: Hashable {
    let title: String
    let date: Date
    
    var hashValue: Int {
        return date.hashValue
    }
    
    static func == (lhs: TableSection, rhs: TableSection) -> Bool {
        return lhs.title == rhs.title && lhs.date == rhs.date
    }
}
