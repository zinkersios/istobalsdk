//
//  ScreenType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 6/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum ScreenType: String {
    case finished = "wash_finished" // Lavado finalizado correctamente. Ir a la pantalla de valoración
    case stoped = "wash_stoped" // Lavado detenido externamente.
    case installation = "welcome_installation" // Abrir instalación
    case ticket = "gift_redeem" // Abrir ticket
    case none
}
