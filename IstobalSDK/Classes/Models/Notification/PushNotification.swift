//
//  PushNotification.swift
//  istobal
//
//  Created by Shanti Rodríguez on 8/11/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct PushNotification: JSONParselable {
    let id: String
    let title: String
    let body: String
    let text: String
    let screen: ScreenType
    let idSecondary: String? // es el id del ServiceType elegido por el usuario, que hace falta para realizar la votación
}

extension PushNotification {
    init?(json: [String: Any]) {
        guard  let aps = json["aps"] as? [String: Any] else { return nil }
        guard let alert = aps["alert"] as? [String: Any] else { return nil }
        guard let title = alert["title"] as? String else { return nil }
        guard let body = alert["body"] as? String else { return nil }
        guard let text = json["text"] as? String else { return nil }
        guard let screen = json["screen"] as? String else { return nil }
        guard var screenType = ScreenType(rawValue: screen) else { return nil }
        guard let id = json[Constants.JSONKey.id] as? String else { return nil }
        
        let idSecondary = json["id_secondary"] as? String
        
        if screenType == .installation, id.count == 0 {
            screenType = .ticket
        }
        
        self.init(id: id,
                  title: title,
                  body: body,
                  text: text,
                  screen: screenType,
                  idSecondary: idSecondary)
    }
}
