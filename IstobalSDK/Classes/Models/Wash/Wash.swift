//
//  Wash.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 7/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

enum WashType {
    case last, withPrograms, smartwash
}

struct Wash {
    let name: String
    let description: String
    let price: String
    let imageName: String
    let type: WashType
}

extension Wash {
    init(program: Program, type: WashType) {
        self.init(name: .localized(.repeatLastWash),
                  description: program.name,
                  price: "\(program.price) €",
            imageName: "ic_repetir_46",
            type: type)
    }
    
    static func getWashWithPrograms() -> [Wash] {
        var items = [Wash]()
        
        let item = Wash(name: .localized(.services),
                        description: .localized(.servicesBody),
                        price: "",
                        imageName: "ic_programas_46",
                        type: .withPrograms)
        items.append(item)
        
        return items
    }
    
    static func getWashesKept() -> [Wash] {
        var items = [Wash]()
        
        var item = Wash(name: "Mi lavado semanal",
                        description: "M’Joy Alzira", price: "6,50",
                        imageName: "ic_repetir_46", type: .withPrograms)
        items.append(item)
        
        item = Wash(name: "Después del viaje…",
                    description: "Carwash Valencia", price: "6,00",
                    imageName: "ic_asistente_46", type: .withPrograms)
        items.append(item)
        
        item = Wash(name: "Light touch",
                    description: "Lavarápido Alicante", price: "4,50",
                    imageName: "ic_programas_46", type: .withPrograms)
        items.append(item)
        
        return items
    }
}
