//
//  Price.swift
//  istobal
//
//  Created by Shanti Rodríguez on 26/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct Price {
    let value: Double
}

extension Price {
    var asString: String {
        if isOtherValue {
            return .localized(.otherValue)
        }
        return PriceUtils.priceToCurrency(with: value)
    }
    
    var isOtherValue: Bool {
        return value == 0
    }
}

extension Price {
    static func toChargeBalance() -> [Price] {
        return [Price(value: 2),
                Price(value: 5),
                Price(value: 7.5),
                Price(value: 10),
                Price(value: 20),
                Price(value: 50),
                Price(value: 100),
                Price(value: 0)]
    }
    
    static func toBuyService() -> [Price] {
        return [Price(value: 0.5),
                Price(value: 1),
                Price(value: 1.5),
                Price(value: 2),
                Price(value: 2.5),
                Price(value: 0)]
    }
}
