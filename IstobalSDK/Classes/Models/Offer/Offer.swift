//
//  Offer.swift
//  istobal
//
//  Created by Shanti Rodríguez on 11/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct Offer: Codable, JSONParselable, Equatable, Comparable {
    let description: String
    let discount: Int
    let id: Int
    let image: URL?
    let name: String
    let type: OfferType
    let service: Service?
    
    static func < (lhs: Offer, rhs: Offer) -> Bool {
        return lhs.discount < rhs.discount
    }
}

extension Offer {
    init?(json: JSONObject) {
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        
        var description = " "
        if let value = json["description"] as? String {
            description = value
        }
        
        var discount = 0
        if let value = json["discount"] as? Int {
           discount = value
        }
        
        var name = " "
        if let value = json["name"] as? String {
            name = value
        }
        
        let image = URL(json: json, key: "image")
        let service = Service.init(json: json["service"])
        
        self.init(description: description,
                  discount: discount,
                  id: id,
                  image: image,
                  name: name,
                  type: .coupon,
                  service: service)
    }
}

extension Offer {
    static func noUseCoupon() -> Offer {
        return Offer(description: "",
                     discount: 0,
                     id: -9,
                     image: nil,
                     name: .localized(.noDiscountCoupon),
                     type: .noCoupon,
                     service: nil)
    }
    
    static func addCoupon() -> Offer {
        return Offer(description: "",
                     discount: 0,
                     id: -9,
                     image: nil,
                     name: .localized(.addDiscountCoupon),
                     type: .addCoupon,
                     service: nil)
    }
}

//swiftlint:disable:next operator_whitespace
func ==(lhs: Offer, rhs: Offer) -> Bool {
    return lhs.id == rhs.id && lhs.type == rhs.type
}
