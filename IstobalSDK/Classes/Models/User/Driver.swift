//
//  Driver.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 15/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Driver {
    let name: String
    let type: String
}
