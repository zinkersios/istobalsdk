//
//  InvoiceCompany.swift
//  istobal
//
//  Created by Shanti Rodríguez on 3/5/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct InvoiceCompany: JSONParselable {
    var address: String
    var city: String
    var companyName: String
    var country: String
    var email: String
    let id: Int
    var postalCode: String
    var state: String
    var taxId: String
}

extension InvoiceCompany {
    init?(json: JSONObject) {
        guard let address = json[Constants.JSONKey.address] as? String else { return nil }
        guard let city = json[Constants.JSONKey.city] as? String else { return nil }
        guard let companyName = json[Constants.JSONKey.companyName] as? String else { return nil }
        guard let country = json[Constants.JSONKey.country] as? String else { return nil }
        guard let email = json[Constants.JSONKey.email] as? String else { return nil }
        guard let id = json[Constants.JSONKey.id] as? Int else { return nil }
        guard let postalCode = json[Constants.JSONKey.postalCode] as? String else { return nil }
        guard let state = json[Constants.JSONKey.state] as? String else { return nil }
        guard let taxId = json[Constants.JSONKey.taxID] as? String else { return nil }
        
        self.init(address: address,
                  city: city,
                  companyName: companyName,
                  country: country,
                  email: email,
                  id: id,
                  postalCode: postalCode,
                  state: state,
                  taxId: taxId)
    }
    
    static func billingWithoutData(taxId: String) -> InvoiceCompany {
        return InvoiceCompany(address: "",
                              city: "",
                              companyName: "",
                              country: "",
                              email: "",
                              id: 0,
                              postalCode: "",
                              state: "",
                              taxId: taxId)
    }
}

// MARK: - API
extension InvoiceCompany {
    // add existing billing to the user
    func parametersToAdd() -> JSONObject {
        var dic = JSONObject()
        dic[Constants.JSONKey.invoiceCompany] = [Constants.JSONKey.id: self.id]
        return dic
    }
    
    // Create billing
    func parametersToCreate() -> JSONObject {
        var dic = JSONObject()
        dic[Constants.JSONKey.invoiceCompany] = [
            Constants.JSONKey.taxID: self.taxId,
            Constants.JSONKey.companyName: self.companyName,
            Constants.JSONKey.address: self.address,
            Constants.JSONKey.email: self.email,
            Constants.JSONKey.city: self.city,
            Constants.JSONKey.postalCode: self.postalCode,
            Constants.JSONKey.state: self.state,
            Constants.JSONKey.country: self.country
        ]
        return dic
    }
    
    func parametersToRemove() -> JSONObject {
        var dic = JSONObject()
        dic[Constants.JSONKey.invoiceCompany] = NSNull()
        return dic
    }
}
