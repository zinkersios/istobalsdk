//
//  User.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 20/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

enum UserType: String {
    case email, social
}

struct User: JSONParselable {
    let email: String
    var firstName: String?
    var lastName: String?
    let washes: Int
    let level: String?
    let vehicle: Bool
    var phone: String?
    let type: UserType
    let image: String?
    var password: String?
    var pin: String?
    let invoiceCompany: InvoiceCompany?
}

extension User {
    init?(json: JSONObject) {
        guard let email = json[Constants.JSONKey.email] as? String else { return nil }
        guard let washes = json["washes"] as? Int else { return nil }
        guard let vehicle = json["vehicle"] as? Bool else { return nil }
        guard let loginType = json["loginType"] as? String else { return nil }
        guard let type = UserType(rawValue: loginType) else { return nil }
        
        let firstName = json["name"] as? String
        let lastName = json["surname"] as? String
        let level = json["level"] as? String
        let phone = json["phone"] as? String
        let image = json["image"] as? String
        let invoiceCompany = InvoiceCompany(json: json, key: Constants.JSONKey.invoiceCompany)
        
        self.init(email: email,
                  firstName: firstName,
                  lastName: lastName,
                  washes: washes,
                  level: level,
                  vehicle: vehicle,
                  phone: phone,
                  type: type,
                  image: image,
                  password: nil,
                  pin: nil,
                  invoiceCompany: invoiceCompany)
    }
}

extension User {
    func parameters() -> JSONObject {
        var params = [String: String]()
        
        /// firstname
        if let name = self.firstName {
            params["name"] = name
        }
        /// lastname
        if let lastname = self.lastName {
            params["surname"] = lastname
        }
        /// phone
        if let phone = self.phone {
            params["phone"] = phone
        }
        /// password
        if let password = self.password {
            params["plainPassword"] = password
        }
        /// PIN
        if let pin = self.pin {
            params["plainPin"] = pin
        }
        
        return params
    }
}
