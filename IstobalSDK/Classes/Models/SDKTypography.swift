//
//  SDKTypography.swift
//  Alamofire
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//

import Foundation



public class SDKTypography{
    
    var familyName: String?
    var fonts: [String]?
    var fontExtension: String?
    
    init(){
        self.familyName = ""
        self.fonts = []
        self.fontExtension = ""
    }
    
    public func getFonts()->[String]{
        return self.fonts != nil ? self.fonts! : []
    }
    
    public func appendNewFont(_ fontName: String){
        if fonts == nil{
             fonts = []
        }
        
        fonts?.append(fontName)
    }
    
    public func getFamilyName()->String{
        return self.familyName != nil ? self.familyName! : ""
    }
    
    
    public func getFontExtension()->String{
        return self.fontExtension != nil ? self.fontExtension! : ""
    }
    
    public func getFontFor(_ fontSufix: String)->String{
        for font in self.getFonts(){
            if let sufix = font.split(separator: "-").last{
                if sufix == fontSufix{
                    return font
                }
            }
        }
        return ""
    }
}
