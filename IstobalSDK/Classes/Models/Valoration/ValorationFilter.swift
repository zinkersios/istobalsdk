//
//  ValorationFilter.swift
//  istobal
//
//  Created by Shanti Rodríguez on 26/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct ValorationFilter {
    var installationID: Int
    var page: Int
    
    func convertToDictionary() -> JSONObject {        
        let dic: JSONObject = ["installation": self.installationID,
                               "page": self.page]
        return dic
    }
}
