//
//  Valoration.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 13/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Valoration: JSONParselable {
    let comment: String
    let image: String?
    let name: String?
    let rating: Int
    let surname: String?
    let codeId: String
    let installationID: String
    
    func convertToDictionary() -> JSONObject {
        let dic: JSONObject = ["rating": self.rating,
                               "comment": self.comment,
                               "codeId": self.codeId,
                               "installationID": self.installationID]
        return dic
    }
}

extension Valoration {
    init?(json: [String: Any]) {
        guard let comment = json["comment"] as? String else { return nil }
        guard let rating = json["rating"] as? Int else { return nil }
        let image = json["image"] as? String
        let surname = json["surname"] as? String
        let codeId = ""
        let installationID = ""
        let name = json["name"] as? String
        
        self.init(comment: comment,
                  image: image,
                  name: name,
                  rating: rating,
                  surname: surname,
                  codeId: codeId,
                  installationID: installationID)
    }
}
