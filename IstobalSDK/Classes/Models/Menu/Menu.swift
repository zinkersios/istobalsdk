//
//  Menu.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 20/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

struct Menu {
    let title: String
    let imageName: String
    let type: TypeController
}

extension Menu {
    static func getForLoggedInUser() -> [Menu] {
        let home = Menu(title: .localized(.washingCenters), imageName: "ic_menu_centros_46", type: .home)
        //let smartwash = Menu(title: .localized(.smartWash), imageName: "ic_menu_asistente", type: .smartwash)
        let myVehicles = Menu(title: .localized(.myVehicles), imageName: "ic_menu_vehiculos_46", type: .myVehicles)
        let myTickets = Menu(title: .localized(.myTickets), imageName: "ic_menu_tickets", type: .myTickets)
        let myCards = Menu(title: .localized(.loyaltyCard), imageName: "ic_menu_mistarjetas", type: .myCards)
        let notifications = Menu(title: .localized(.notifications), imageName: "ic_menu_notificaciones", type: .notifications)
        let ticketReading = Menu(title: .localized(.ticketReading), imageName: "ic_menu_lectura", type: .ticketReading)
       
        return [home, myVehicles, myTickets, myCards, notifications, ticketReading]
    }
    
    static func getForGuestUser() -> [Menu] {
        let home = Menu(title: .localized(.washingCenters), imageName: "ic_menu_centros_46", type: .home)
        //let smartwash = Menu(title: .localized(.smartWash), imageName: "ic_menu_asistente", type: .smartwash)
        let logIn = Menu(title: .localized(.logIn), imageName: "ic_menu_login", type: .logIn)
        let registry = Menu(title: .localized(.registry), imageName: "ic_menu_registro", type: .registry)
        //let ticketReading = Menu(title: .localized(.ticketReading), imageName: "ic_menu_lectura", type: .ticketReading)
        
        return [home, logIn, registry]
    }
    
    static func getSecondSection() -> [Menu] {
        let aboutUs = Menu(title: .localized(.aboutUs), imageName: "", type: .aboutUs)
        let support = Menu(title: .localized(.support), imageName: "", type: .support)
        
        return [aboutUs, support]
    }
}
