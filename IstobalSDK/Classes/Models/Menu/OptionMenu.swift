//
//  OptionMenu.swift
//  istobal
//
//  Created by Shanti Rodríguez on 14/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct OptionMenu {
    let name: String
    let cell: OptionMenuCellType
    let action: OptionMenuAction
}

extension OptionMenu {
    init(name: String, action: OptionMenuAction) {
        self.name = name
        self.cell = .normal
        self.action = action
    }
}

enum OptionMenuCellType {
    case big, normal
}

enum OptionMenuAction {
    case installationName
    case installationAddress
    case installationPhone
    case installationEmail
    case installationGallery
    case installationReportProblem
    case installationContactDetails
    case deleteLoyaltyCard
    case giftTicket
}
