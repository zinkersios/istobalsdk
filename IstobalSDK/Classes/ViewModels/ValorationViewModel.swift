//
//  ValorationViewModel.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 13/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct ValorationViewModel {
    let fullName: String
    let rating: Double
    var valoration: Valoration
    
    init(model: Valoration) {
        /// set fullName
        if let name = model.name, let surname = model.surname {
            self.fullName = name + " " + surname
        } else {
            self.fullName = .localized(.valuationWithoutName)
        }
        
        /// valoration
        let num = Double(model.rating)
        self.rating = num.floor(nearest: 0.5)
        
        self.valoration = model
    }
}
