//
//  InstallationViewModel.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 27/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct InstallationViewModel {
    var installation: Installation
    let basicInstallation: BasicInstallation?
    let distanceHumanReadable: String
    let distance: Float
    let valoration: Double
    let valorations: String
    let schedule: String
    let address: String
    let phone: String
    
    init(model: Installation) {
        self.installation = model
        
        /// set distance
        if let meters = model.distance {
            let meter: Meter = Meter(meters)
            self.distanceHumanReadable = meter.toString
            self.distance = meter.toFloat
        } else {
            self.distanceHumanReadable = ""
            self.distance = 0
        }
        
        /// valoration
        self.valoration = InstallationViewModel.setUpValoration(model.valoration)
        self.valorations = InstallationViewModel.setUpValorations(model.valorations)
        
        /// schedule
        if let timetable = model.timetable {
            self.schedule = timetable
        } else {
            self.schedule = .localized(.timetableNotAvailable)
        }
        
        // address
        self.address = model.address + ", " + model.postalCode + ", " + model.city + " (\(model.state))"
        self.phone = "  " + model.phone
        self.basicInstallation = nil
    }
    
    init(model: BasicInstallation) {
        self.distanceHumanReadable = ""
        self.distance = 0
        self.valoration = InstallationViewModel.setUpValoration(model.valoration)
        self.valorations = InstallationViewModel.setUpValorations(model.valorations)
        self.schedule = ""
        self.phone = ""
        self.basicInstallation = model
        self.installation = Installation(id: model.id)
        
        let string = model.address ?? " "
        self.address = string + ", " + model.postalCode + ", " + model.city + " (\(model.state))"
    }
}

extension InstallationViewModel {
    private static func setUpValoration(_ valoration: Double?) -> Double {
        if let val = valoration {
            return val.floor(nearest: 0.5)
        }
        return 0
    }
    
    private static func setUpValorations(_ valorations: Int?) -> String {
        if let total = valorations {
            let string: String = (total == 1) ? .localized(.valuation) : .localized(.valuations)
            return "\(total) " + string.lowercased()
        }
        return "0 " + .localized(.valuations)
    }
}
