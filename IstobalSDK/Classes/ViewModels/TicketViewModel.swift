//
//  TicketViewModel.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 16/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct TicketViewModel {
    let title: String
    let purchasedOn: String
    let validUntil: String
    let totalPaid: String
    let totalPrice: String?
    let income: String
    let translatedStatus: String
    let navTitle: String
    let ticket: Ticket

    init(model: Ticket) {
        // Set title       
        self.title = model.name
        
        /// set purchase On
        if let date = Date.date(rfc8601Date: model.date) {
            self.purchasedOn = date.ticketFormatted.uppercased()
        } else {
            self.purchasedOn = ""
        }
        
        /// set valid until
        if let date = Date.date(rfc8601Date: model.expirationDate) {
            self.validUntil = date.ticketFormatted.uppercased()
        } else {
            self.validUntil = ""
        }
        
        if model.ticketId.count > 0 {
            self.navTitle =  Utils.capitalisedText(with: .localized(.ticket)) + " #" + model.ticketId
        } else {
            self.navTitle =  Utils.capitalisedText(with: .localized(.ticket))
        }
        
        self.translatedStatus = model.status.humanReadable
        self.income = model.ticketId
        self.totalPrice = (model.totalPrice == model.totalPaid) ? nil : PriceUtils.priceToCurrency(with: model.totalPrice)
        self.totalPaid = PriceUtils.priceToCurrency(with: model.totalPaid)
        self.ticket = model
    }
}
