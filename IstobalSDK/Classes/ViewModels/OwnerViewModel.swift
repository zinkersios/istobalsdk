//
//  OwnerViewModel.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 6/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct OwnerViewModel {
    var owner: Owners
    let fullName: String
    
    init(model: Owners) {
        self.owner = model
        
        /// set full names
        if let name = model.name, let surname = model.surname {
            self.fullName = name + " " + surname
        } else if let name = model.name {
            self.fullName = name + " "
        } else {
            self.fullName = ""
        }
    }
}
