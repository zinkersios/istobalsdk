//
//  UserViewModel.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 20/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct UserViewModel {
    let fullName: String
    let level: String
    let avatarURL: URL?
    let washes: String
    var user: User
    
    init(model: User) {
        // set full name
        if let firstName = model.firstName, let lastName = model.lastName, !firstName.isEmpty, !lastName.isEmpty {
            self.fullName = firstName + " " + lastName
        } else if let firstName = model.firstName, !firstName.isEmpty {
            self.fullName = firstName
        } else {
            self.fullName = model.email
        }
        
        /// set washes
        if model.washes == 1 {
            self.washes = "1 " + .localized(.aWash)
        } else {
            self.washes = "\(model.washes) " + .localized(.variousWashes)
        }
        
        /// set Level
        if let level = model.level {
            self.level = level + " · " + self.washes.lowercased()
        } else {
            self.level = self.washes.lowercased()
        }
        
        /// set avatar url
        if let image = model.image {
            self.avatarURL = URL(string: image)
        } else {
            self.avatarURL = nil
        }
        
        self.user = model
    }
}

extension UserViewModel {
    static func getUser() -> UserViewModel? {
        guard let data = UserDefaults.User.data(forKey: .user),
            let json = NSKeyedUnarchiver.unarchiveObject(with: data) as? JSONObject
            else {
                return nil
        }
        
        guard let user = User.init(json: json) else {
            return nil
        }
        
        return UserViewModel.init(model: user)
    }
}
