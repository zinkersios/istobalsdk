//
//  VehicleViewModel.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 6/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct VehicleViewModel {
    var vehicle: Vehicle
    let dirtDays: String
    let dirtIndex: String
    let washes: String
    let washesText: String
    let users: String
    
    init(model: Vehicle) {
        self.vehicle = model
        
        /// set dirtDays
        if model.dirtDays == 1 {
            self.dirtDays = "1 " + .localized(.day)
        } else {
            self.dirtDays = "\(model.dirtDays) " + .localized(.days)
        }
        
        /// set dirtIndex
        self.dirtIndex = "\(model.dirtIndex)%"
        
        /// set washes
        if model.washes == 1 {
            self.washes = "1 "
            self.washesText = .localized(.aWash)
        } else {
            self.washes = "\(model.washes) "
            self.washesText = .localized(.variousWashes)
        }
        
        /// set users
        if model.owners.count == 1 {
            self.users = "    1 " + .localized(.aUser)
        } else {
            self.users = "    \(model.owners.count) " + .localized(.variousUsers)
        }
    }
}
