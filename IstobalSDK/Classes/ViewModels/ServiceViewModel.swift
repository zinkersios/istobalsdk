//
//  ServiceViewModel.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct ServiceViewModel {
    let price: String
    let description: String
    let service: Service
    
    init(model: Service) {
        // set price
        let priceForAddons = model.addons.map({$0.price}).reduce(0, +)
        let priceTotal = model.price + priceForAddons
        self.price = PriceUtils.priceToCurrency(with: priceTotal)
        
        // set description
        var message = model.name
        
        if model.addons.count > 0 {
            message +=  ": "
            message += model.addons.map({$0.name}).joined(separator: ". ")
        }
        
        self.description = message
        self.service = model
    }
}
