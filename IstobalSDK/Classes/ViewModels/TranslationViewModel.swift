//
//  TranslationViewModel.swift
//  istobal
//
//  Created by Shanti Rodríguez on 6/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct TranslationViewModel {
    let name: String
    let description: String?
    let longDescription: String?
}

extension TranslationViewModel {
    init(translations: [Translation], name: String?) {
        var strName = name ?? " "
        
        guard let translation = TranslationViewModel.getMyTranslation(translations: translations) else {
            self.init(name: strName, description: nil, longDescription: nil)
            return
        }
        
        var description: String?
        var longDescription: String?
        
        // Get Name
        for field in translation.fields where field.field == "name" {
            strName = field.value
            break
        }
        
        // Get Description
        for field in translation.fields where field.field == "description" {
            description = field.value
            break
        }
        
        // Get Long Description
        for field in translation.fields where field.field == "longDescription" {
            longDescription = field.value
            break
        }
        
        self.init(name: strName, description: description, longDescription: longDescription)
    }
    
    /// get the user's translation according to the language of the device
    private static func getMyTranslation(translations: [Translation]) -> Translation? {
        let language = Locale.current.languageCode ?? "es"
        let filtered = translations.filter({$0.language == language}).first
        
        return (filtered != nil) ? filtered : translations.first
    }
}

