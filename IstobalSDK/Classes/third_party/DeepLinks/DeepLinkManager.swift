//
//  DeepLinkManager.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 4/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import UIKit

enum DeeplinkType {
    case notification(PushNotification) // accessible by push notification
    case smartwash // accessible by direct access
}

//swiftlint:disable:next variable_name
let Deeplinker = DeepLinkManager()
class DeepLinkManager {
    fileprivate init() {}
    
    private var deeplinkType: DeeplinkType?
    
    func handleRemoteNotification(_ notification: [AnyHashable: Any]) {
        deeplinkType = NotificationParser.shared.handleNotification(notification)
    }
    
    /// check existing deepling and perform action
    func checkDeepLink() {
        guard let deeplinkType = deeplinkType else {
            return
        }
        
        DeeplinkNavigator.shared.proceedToDeeplink(deeplinkType)
        
        /// reset deeplink after handling
        self.deeplinkType = nil
    }
}
