//
//  NotificationParser.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 4/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

final class NotificationParser {
    static let shared = NotificationParser()
    private init() {}
    
    func handleNotification(_ userInfo: [AnyHashable : Any]) -> DeeplinkType? {
        
        if let json = userInfo as? JSONObject, let notification = PushNotification(json: json) {
            return DeeplinkType.notification(notification)
        }
        
        return nil
    }
}
