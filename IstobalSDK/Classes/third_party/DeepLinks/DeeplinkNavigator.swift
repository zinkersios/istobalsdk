//
//  DeeplinkNavigator.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 4/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class DeeplinkNavigator {
    // MARK: - Properties
    static let shared = DeeplinkNavigator()
    weak var delegate: DeeplinkNavigatorDelegate?
    var isWashing = false
    var navigationController: UINavigationController?
    
    private init() {}
    
    // MARK: - Public
    func proceedToDeeplink(_ type: DeeplinkType) {
        switch type {
        case .notification(let notification):
            displayNotification(with: notification)
        default: ()
        }
    }
    
    // MARK: - Private
    private func displayNotification(with notification: PushNotification) {
//        log.debug(notification)
        debugPrint(notification)
        let loginState = UserDefaults.Account.integer(forKey: .loginState)
        if loginState != LoginState.logged.rawValue {
            return
        }
        
        switch notification.screen {
        case .stoped:
            showMachineStopped(with: notification)
        case .finished:
            showValoration(with: notification)
        case .installation, .ticket:
            showBanner(for: notification)
        default: ()
        }
    }
    
    private func showMachineStopped(with notification: PushNotification) {
        guard let currentVC = Utils.currentViewController() else { return }
        
        // is the car washing?
        if let navBar = currentVC as? UINavigationController,
            navBar.visibleViewController is DuringWashingViewController {
            self.delegate?.stopWashing(with: notification)
            return
        } else if isWashing == true {
            self.delegate?.stopWashing(with: notification)
            return
        }
        
        // La aplicación fue detenida mientras se estaba lavando el vehículo, por lo tanto hay que mostrar la información al usuario.
        let vc = MachineStoppedViewController.storyboardViewController()
        vc.typeOfStop = .background
        vc.notification = notification
        currentVC.present(vc, animated: true)
    }
    
    private func showValoration(with notification: PushNotification) {
        guard let currentVC = Utils.currentViewController() else { return }
        
        // is the car washing?
        if let navBar = currentVC as? UINavigationController,
            navBar.visibleViewController is DuringWashingViewController {
            self.delegate?.finishedWashing(with: notification)
            return
        } else if isWashing == true {
            self.delegate?.finishedWashing(with: notification)
            return
        }
        
        if currentVC is WashingScoreViewController { return }
        
        // La aplicación fue detenida mientras se estaba lavando el vehículo,
        // por lo tanto hay que mostrar la pantalla de valoración.
        let vc = WashingScoreViewController.storyboardViewController()
        vc.typeOfStop = .background
        vc.notification = notification
        currentVC.present(vc, animated: true)
    }
    
    // MARK: - Notification with Banner
    private func showBanner(for notification: PushNotification) {
        // Mostrar contenido automáticamente si la app esta activa pero en segundo plano
        if UIApplication.shared.applicationState != .active {
            showInstallationOrTicket(for: notification)
            return
        }
        // Mostrar contenido automáticamente si la app estaba inactiva y cerrada
//        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, appDelegate.finishLaunching == false {
//            showInstallationOrTicket(for: notification)
//            return
//        }
        
        // Show banner
        let rightView = UIImageView(image: #imageLiteral(resourceName: "right_chevron"))
        let banner = NotificationBanner(title: notification.title,
                                        subtitle: notification.body,
                                        rightView: rightView,
                                        style: .danger)
        banner.onTap = {
            self.showInstallationOrTicket(for: notification)
        }
        banner.show()
    }
    
    private func showInstallationOrTicket(for notification: PushNotification) {
        switch notification.screen {
        case .installation:
            showInstallation(for: notification)
        case .ticket:
            showTicket(for: notification)
        default:()
        }
    }
    
    private func showInstallation(for notification: PushNotification) {
//        if isWashing { return }
//        guard let currentVC = Utils.currentViewController() else { return }
//        guard let identifier = Int(notification.id) else { return }
//        
//        let vc = InstallationViewController.storyboardViewController()
//        vc.installation = Installation(id: identifier)
//        pushViewController(currentVC: currentVC, vc: vc)
    }
    
    private func showTicket(for notification: PushNotification) {
        if isWashing { return }
        guard let currentVC = Utils.currentViewController() else { return }
        guard let identifier = notification.idSecondary else { return }
        
        let vc = MyTicketViewController.storyboardViewController()
        vc.ticketID = identifier
        vc.isFromPushNotification = true
        pushViewController(currentVC: currentVC, vc: vc)
    }
    
    private func pushViewController(currentVC: UIViewController, vc: UIViewController) {
        if let navBar = currentVC as? UINavigationController {
            navBar.pushViewController(vc, animated: true)
        } else if let navBar = navigationController {
            navBar.pushViewController(vc, animated: true)
        }
    }
}
