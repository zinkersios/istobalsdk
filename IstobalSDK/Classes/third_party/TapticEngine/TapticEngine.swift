//
//  TapticEngine.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 31/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import AudioToolbox.AudioServices // Need to import to access 1st generation vibrator/Taptic Engine

/// Generates iOS Device vibrations by UIFeedbackGenerator.
open class TapticEngine {
    
    public static let impact: Impact = Impact()
    public static let notification: Notification = Notification()
    
    /// Wrapper of `UIImpactFeedbackGenerator`
    open class Impact {
        
        /// Impact feedback styles
        ///
        /// - light: A impact feedback between small, light user interface elements.
        /// - medium: A impact feedback between moderately sized user interface elements.
        /// - heavy: A impact feedback between large, heavy user interface elements.
        //swiftlint:disable:next nesting
        public enum ImpactStyle {
            case light, medium, heavy
        }
        
        private var style: ImpactStyle = .light
        private var generator: Any? = Impact.makeGenerator(.light)
        
        private static func makeGenerator(_ style: ImpactStyle) -> Any? {
            guard #available(iOS 10.0, *) else { return nil }
            
            let feedbackStyle: UIImpactFeedbackStyle
            switch style {
            case .light:
                feedbackStyle = .light
            case .medium:
                feedbackStyle = .medium
            case .heavy:
                feedbackStyle = .heavy
            }
            let generator: UIImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: feedbackStyle)
            generator.prepare()
            return generator
        }
        
        private func updateGeneratorIfNeeded(_ style: ImpactStyle) {
            guard self.style != style else { return }
            
            generator = Impact.makeGenerator(style)
            self.style = style
        }
        
        public func feedback(_ style: ImpactStyle = .medium) {
            guard #available(iOS 10.0, *) else { return }
            
            if !UIDevice.current.hasHapticFeedback && UIDevice.current.hasTapticEngine {
                playPeek()
                return
            }
            
            updateGeneratorIfNeeded(style)
            
            guard let generator = generator as? UIImpactFeedbackGenerator else { return }
            
            generator.impactOccurred()
            generator.prepare()
        }
        
        public func prepare(_ style: ImpactStyle) {
            guard #available(iOS 10.0, *) else { return }
            if !UIDevice.current.hasHapticFeedback { return }
            
            updateGeneratorIfNeeded(style)
            
            guard let generator = generator as? UIImpactFeedbackGenerator else { return }
            
            generator.prepare()
        }
        
        private func playPeek() {
            let peek = SystemSoundID(1519)
            AudioServicesPlaySystemSound(peek)
        }
    }
    
    /// Wrapper of `UINotificationFeedbackGenerator`
    open class Notification {
        
        /// Notification feedback types
        ///
        /// - success: A notification feedback, indicating that a task has completed successfully.
        /// - warning: A notification feedback, indicating that a task has produced a warning.
        /// - error: A notification feedback, indicating that a task has failed.
        //swiftlint:disable:next nesting
        public enum NotificationType {
            case success, warning, error
        }
        
        private var generator: Any? = {
            guard #available(iOS 10.0, *) else { return nil }
            
            let generator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
            generator.prepare()
            return generator
        }()
        
        public func feedback(_ type: NotificationType) {
            guard #available(iOS 10.0, *) else { return }
            guard let generator = generator as? UINotificationFeedbackGenerator else { return }
            
            let feedbackType: UINotificationFeedbackType
            switch type {
            case .success:
                feedbackType = .success
            case .warning:
                feedbackType = .warning
            case .error:
                feedbackType = .error
            }
            generator.notificationOccurred(feedbackType)
            generator.prepare()
        }
        
        public func prepare() {
            guard #available(iOS 10.0, *) else { return }
            guard let generator = generator as? UINotificationFeedbackGenerator else { return }
            
            generator.prepare()
        }
    }
}
