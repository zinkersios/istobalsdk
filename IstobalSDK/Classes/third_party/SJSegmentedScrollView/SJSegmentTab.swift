//
//  SJSegmentTab.swift
//  Pods
//
//  Created by Subins on 22/11/16.
//  Copyright © 2016 Subins Jose. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
//    associated documentation files (the "Software"), to deal in the Software without restriction,
//    including without limitation the rights to use, copy, modify, merge, publish, distribute,
//    sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//    substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
//  LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
//  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import UIKit
import Device

typealias DidSelectSegmentAtIndex = (_ segment: SJSegmentTab?, _ index: Int, _ animated: Bool) -> Void

public enum SegmentTabStyle {
    case gray
    case red
}

open class SJSegmentTab: UIView {

	let kSegmentViewTagOffset = 100
	let button = UIButton(type: .custom)
    let backgroundView = UIView()
    private var segmentedType: SegmentedType = .byDefault
    
	var didSelectSegmentAtIndex: DidSelectSegmentAtIndex?
	var isSelected = false {
		didSet {
			button.isSelected = isSelected
		}
	}
    
    convenience init(title: String, segmentedType: SegmentedType) {
		self.init(frame: CGRect.zero)
        self.segmentedType = segmentedType
		button.setTitle(title, for: .normal)
        addBackgroundView()
	}

	convenience init(view: UIView) {
		self.init(frame: CGRect.zero)

		insertSubview(view, at: 0)
		view.removeConstraints(view.constraints)
		addConstraintsToView(view)
	}

	required override public init(frame: CGRect) {
		super.init(frame: frame)

		translatesAutoresizingMaskIntoConstraints = false
		button.frame = bounds
		button.addTarget(self, action: #selector(SJSegmentTab.onSegmentButtonPress(_:)),
		                 for: .touchUpInside)        
		addSubview(button)
		addConstraintsToView(button)
	}

	func addConstraintsToView(_ view: UIView) {

		view.translatesAutoresizingMaskIntoConstraints = false
		let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|",
		                                                         options: [],
		                                                         metrics: nil,
		                                                         views: ["view": view])
		let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|",
		                                                           options: [],
		                                                           metrics: nil,
		                                                           views: ["view": view])
		addConstraints(verticalConstraints)
		addConstraints(horizontalConstraints)
        
	}

    func addBackgroundView() {
        var posX = (Device.size() == .screen4Inch) ? CGFloat(14) : CGFloat(25)
        var numberTabs = CGFloat(2)
        
        // configure background for MyTicketsViewController
        if segmentedType == .myTickets {
            numberTabs = 3
            posX = (Device.size() == .screen4Inch) ? (posX - 10) : (posX - 13)
        }
        
        let posY = CGFloat(10)
        let width = (Constants.Size.screenWidth / numberTabs) - (posX * 2)
        let height = heightForSegmentView() - (posY * 2)
        
        backgroundView.frame = CGRect(x: posX, y: posY, width: width, height: height)
        backgroundView.clipsToBounds = true
        backgroundView.layer.cornerRadius = 15
        insertSubview(backgroundView, at: 0)
    }
    
	required public init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	open func titleColor(_ color: UIColor) {

		button.setTitleColor(color, for: .normal)
	}

	open func titleFont(_ font: UIFont) {

		button.titleLabel?.font = font
	}
    
    open func updateStyle(for style: SegmentTabStyle) {
        switch style {
        case .gray:
            titleColor(.lightGray)
            backgroundView.backgroundColor = .clear
        case .red:
            titleColor(.white)
            backgroundView.backgroundColor = SDKColor.navigationBarTint
        }
    }

	@objc func onSegmentButtonPress(_ sender: AnyObject) {

		let index = tag - kSegmentViewTagOffset
		NotificationCenter.default.post(name: Notification.Name(rawValue: "DidChangeSegmentIndex"),
		                                object: index)

		if didSelectSegmentAtIndex != nil {
			didSelectSegmentAtIndex!(self, index, true)
		}
	}
}
