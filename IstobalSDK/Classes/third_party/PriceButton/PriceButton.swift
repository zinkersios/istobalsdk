//
//  PriceButton.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 21/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

public enum PriceButtonType {
    case duringWash
}

final class PriceButton: UIButton {

    // MARK: Public interface
    public var type: PriceButtonType = .duringWash {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    public var cornerRadius: CGFloat = 18 {
        didSet {
            self.setNeedsLayout()
        }
    }

    // MARK: Overrides
    override public func layoutSubviews() {
        super.layoutSubviews()
        setUp()
    }
    
    // MARK: Private
    private func setUp() {
        self.tintColor = Color.blue.value
        self.backgroundColor = Color.blue2.value
        self.layer.cornerRadius = cornerRadius
    }
    
    // MARK: Public
    /// set price
    func setPrice(with text: String) {
        let coinText = " €"
        
        /// small text (coin)
        var attrs = [NSAttributedStringKey.font: Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance]
        let attributedString = NSMutableAttributedString(string: coinText, attributes:attrs)
        
        /// normal text
        attrs = [NSAttributedStringKey.font: Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance]
        let normalString = NSMutableAttributedString(string: text, attributes:attrs)
        normalString.append(attributedString)
        
        self.setAttributedTitle(normalString, for: .normal)
    }
}
