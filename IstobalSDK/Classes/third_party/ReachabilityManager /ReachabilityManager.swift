//
//  ReachabilityManager.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Reachability

public protocol NetworkStatusListener: class {
    func networkStatusDidChange(status: Reachability.Connection)
}

open class ReachabilityManager: NSObject {
    open static let shared = ReachabilityManager()
    
    open var isNetworkAvailable: Bool {        
        return reachability.connection != .none
    }
    
    let reachability = Reachability()!
    var listeners = [NetworkStatusListener]()
    var reachabilityStatus: Reachability.Connection = .none
    
    @objc func reachabilityChanged(notification: Notification) {
        guard let reachability = notification.object as? Reachability else {
            debugPrint("reachabilityChanged 😱")
//            log.info("reachabilityChanged 😱")
            return
        }
        
        for listener in listeners {
            listener.networkStatusDidChange(status: reachability.connection)
        }
    }
    
    open func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
//            log.info("Could not start reachability notifier")
        }
    }
    
    open func stopMonitoring() {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: Notification.Name.reachabilityChanged,
                                                  object: reachability)
    }
    
    open func addListener(listener: NetworkStatusListener) {
        listeners.append(listener)
    }
    
    open func removeListener(listener: NetworkStatusListener) {
        listeners = listeners.filter { $0 !== listener }
    }
}
