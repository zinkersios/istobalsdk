//
//  AlertViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 25/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

class AlertViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTitleTopConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    weak var delegate: AlertViewControllerDelegate?
    var type = AlertViewControllerType.logOut
    var extraInformation = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        localized()
    }
    
    // MARK: - Private methods
    private func setUpView() {
        buttonHeightConstraint.constant = defaultButtonHeight()
        
        view.backgroundColor = Color.backgroundBlack.value
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.main
        
        lblTitle.textColor = Color.astral.value
        lblTitle.font = Font(.custom(SDKFont.bold), size: .standard(.title)).instance
        
        lblSubtitle.textColor = Color.atomic.value
        lblSubtitle.font = Font(.custom(SDKFont.regular), size: .standard(.h2)).instance
    }
    
    private func localized() {
        lblTitle.text = type.title
        lblSubtitle.text = type.subtitle
        btnAccept.setTitle( Utils.capitalisedText(with: type.acceptMessage), for: .normal)
        btnCancel.setTitle( Utils.capitalisedText(with: type.cancelMessage), for: .normal)
        
        switch type {
        case .logOut:
            setUpForLogOut()
        case .removeVehicle:
            setUpForRemoveVehicle()
        case .unsubscribeMe:
            setUpForUnsubscribeMe()
        case .withoutVehicle:
            setUpForWithoutVehicle()
        case .withoutVehicleInTicket:
            setUpForWithoutVehicleInTicket()
        case .giftReceived:
            setUpForGift()
        case .update:
            setUpForUpdate()
        case .billing:
            setUpForBilling()
        case .billingInfo:
            setUpForBillingInfo()
        }
    }
    
    private func setUpContainerHeight() {
        if Device.size() == .screen4Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.51)
        } else if Device.size() == .screen4_7Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.47)
        } else if Device.size() == .screen5_5Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.46)
        } else if Device.size() == .screen5_8Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.41)
        }
    }
    
    private func setUpForLogOut() {
        setUpContainerHeight()
    }
    
    private func setUpForRemoveVehicle() {
        setUpContainerHeight()
    }
    
    private func setUpForUnsubscribeMe() {
        if Device.size() == .screen4Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.56)
        } else if Device.size() == .screen5_8Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.43)
        }
    }
    
    private func setUpForWithoutVehicle() {
        setUpContainerHeight()
    }
    
    private func setUpForWithoutVehicleInTicket() {
        setUpContainerHeight()
    }
    
    private func setUpForGift() {
        if Device.size() == .screen4Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.47)
        } else if Device.size() == .screen4_7Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.42)
        } else if Device.size() == .screen5_5Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.39)
        } else {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.37)
        }
        
        lblSubtitle.text = String(format: .localized(.giftReceivedInfo), extraInformation)
        btnAccept.removeFromSuperview()
    }
    
    private func setUpForUpdate() {
        setUpContainerHeight()
    }
    
    private func setUpForBilling() {
        if Device.size() == .screen4Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.62)
        } else if Device.size() == .screen4_7Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.58)
        } else if Device.size() == .screen5_5Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.54)
        } else { // 5.8 Inch or >
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.50)
        }
    }
    
    private func setUpForBillingInfo() {
        lblTitleTopConstraint.constant = 15
        
        if Device.size() == .screen4Inch {
            lblSubtitle.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.92)
        } else if Device.size() == .screen4_7Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.94)
        } else if Device.size() == .screen5_5Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.87)
        } else {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.82)
        }
        
        btnAccept.removeFromSuperview()
        btnCancel.gy_styleName = StyleConstants.Button.primary
        btnCancel.setTitle( Utils.capitalisedText(with: type.acceptMessage), for: .normal)
    }
}

// MARK: - IBActions
extension AlertViewController {
    @IBAction func accept(_ sender: Any) {
        if type == .unsubscribeMe {
//            Utils.registerEvent(.doUnsubscribeMe)
        } else if type == .update {
//            Utils.registerEvent(.updateApp)
            Utils.openAppInStore()
        }
        self.delegate?.alertViewController(self, type: type, action: .accept)
    }
    
    @IBAction func cancel(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.alertViewController(self, type: type, action: .cancel)
        } else if type == .update {
            exit(0)
        } else {
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func dismissView(_ sender: Any) {
        if type != .update {
            self.dismiss(animated: true)
        }
    }
}

// MARK: AlertViewControllerDelegate
protocol AlertViewControllerDelegate: class {
    func alertViewController(_ controller: AlertViewController, type: AlertViewControllerType, action: ChosenAction)
}
