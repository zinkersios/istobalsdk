//
//  DraggableViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 12/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class DraggableViewController: UIViewController {
    
    // MARK: - Properties
    weak var delegate: DraggableViewControllerDelegate?
    var initialTouchPoint: CGPoint = CGPoint(x: 0, y: 0)
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        createPanGestureRecognizer()
    }
    
    private func createPanGestureRecognizer() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(DraggableViewController.hanleGesture(_:)))
        self.view.addGestureRecognizer(panGesture)
    }
}

// MARK: - IBActions
extension DraggableViewController {
    
    @IBAction func hanleGesture(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == .began {
            initialTouchPoint = touchPoint
        } else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.delegate?.draggableViewControllerDismissed(self)
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
}

// MARK: - DraggableViewController Delegate
protocol DraggableViewControllerDelegate: class {
    func draggableViewControllerDismissed(_ controller: DraggableViewController)
}
