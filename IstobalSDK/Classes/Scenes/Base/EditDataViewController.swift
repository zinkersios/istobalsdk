//
//  EditDataViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 4/5/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

class EditDataViewController: UIViewController, ErrorPresenting, StyleNavBar {
    // MARK: - IBOutlets
    @IBOutlet weak var grayView: UIView!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var textField: UITextField!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.byDefault, showBottomHairline: true, backButton: .red)
    }
    
    // MARK: - Private Methods
    private func configure() {
        grayView.backgroundColor = Color.gray7.value
        separator.backgroundColor = Color.separator.value
    }
    
    // MARK: - Methods
    func saveChanges() {}
}

// MARK: - IBActions
extension EditDataViewController {
    @IBAction func save(_ sender: Any) {
        view.endEditing(true)
        saveChanges()
    }
}

// MARK: - UITextField Delegate
extension EditDataViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        save((Any).self)
        return true
    }
}
