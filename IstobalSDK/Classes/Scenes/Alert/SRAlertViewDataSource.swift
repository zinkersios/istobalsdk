//
//  SRAlertViewDataSource.swift
//  istobal
//
//  Created by Shanti Rodríguez on 19/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class SRAlertViewDataSource: NSObject {
    
    private let type: AlertViewControllerType
    private let extraInformation: String
    public var onActionSelected: (AlertAction) -> Void = { _ in }
    
    public init(type: AlertViewControllerType, extraInformation: String?) {
        self.type = type
        self.extraInformation = extraInformation ?? ""
    }
}

// MARK: - UITableViewDataSource
extension SRAlertViewDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return type.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch type.cells[indexPath.row] {
        case .imageAndTitle:
            return imageWithTitleCell(tableView: tableView, indexPath: indexPath)
        case .title:
            return titleCell(tableView: tableView, indexPath: indexPath)
        case .subtitle:
            return subtitleCell(tableView: tableView, indexPath: indexPath)
        case .textField:
            return textFieldCell(tableView: tableView, indexPath: indexPath)
        case .selectAmount:
            return selectAmountCell(tableView: tableView, indexPath: indexPath)
        case .accept:
            return btnDefaultCell(tableView: tableView, indexPath: indexPath)
        case .cancel:
            return btnDefaultCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    private func imageWithTitleCell (tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AlertViewImageCell
        cell.setUp(type: type)
        return cell
    }
    
    private func titleCell (tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AlertViewTitleCell
        cell.setUp(type: type)
        return cell
    }
    
    private func subtitleCell (tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AlertViewSubtitleCell
        cell.setUp(type: type, extraInformation: extraInformation)
        return cell
    }
    
    private func textFieldCell (tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AlertViewTextFieldCell
        cell.setUp(type: type)
        
        cell.onUpdatedText = { [unowned self] text in
            self.onActionSelected(.selected(text))
        }
        return cell
    }
    
    private func btnDefaultCell (tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AlertViewDefaultCell
        cell.setUp(type: type, cell: type.cells[indexPath.row])
        
        cell.onButtonTap = { [unowned self] action in
            self.onActionSelected(action)
        }
        return cell
    }
    
    private func selectAmountCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AlertViewSelectAmountCell
        cell.setUp(type: type)
        
        cell.onUpdatedText = { [unowned self] text in
            self.onActionSelected(.selected(text))
        }
        return cell
    }
}
