//
//  SRAlertViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 18/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class SRAlertViewController: UIViewController, ErrorPresenting {
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: CenteredTableView!
    
    // MARK: - Properties
    private var textfieldValue = ""
    weak var delegate: SRAlertViewControllerDelegate?
    var type = AlertViewControllerType.logOut
    var extraInformation = ""
    var dataSource: UITableViewDataSource? {
        didSet {
            self.tableView.dataSource = dataSource
            self.tableView.reloadData()
        }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        view.backgroundColor = type.backgroundColor
        
//        for cell in Cells.forAlertView() {
//            tableView.registerNibForCellClass(cell)
//        }
        
        // DataSource
        let ds = SRAlertViewDataSource(type: type, extraInformation: extraInformation)
        ds.onActionSelected = { self.doAction($0) }
        dataSource = ds
    }
    
    private func doAction(_ action: AlertAction) {
        switch action {
        case .accept:
            accept()
        case .cancel:
            cancel()
        case .selected(let object):
            if let text = object as? String {
                textfieldValue = text
            }
        default:()
        }
    }
    
    private func setUpAddBalance() {
        guard let result = PriceUtils.stringToNumber(text: textfieldValue), result.doubleValue > 0 else {
            presentStatusBarNotification(.localized(.addsCorrectBalance))
            return
        }
        let price = Price.init(value: result.doubleValue)
        self.delegate?.alertViewController(self, type: type, action: .data(price))
    }
    
    private func accept() {
        if type == .unsubscribeMe {
//            Utils.registerEvent(.doUnsubscribeMe)
        } else if type == .update {
//            Utils.registerEvent(.updateApp)
//            Utils.openAppInStore()
        }
//        else if type == .selectAmount {
//            setUpAddBalance()
//            return
//        }
        self.delegate?.alertViewController(self, type: type, action: .accept)
    }
    
    private func cancel() {
        if self.delegate != nil {
            self.delegate?.alertViewController(self, type: type, action: .cancel)
        } else if type == .update {
            exit(0)
        } else {
            self.dismiss(animated: true)
        }
    }
}
