//
//  ListDataSource.swift
//  istobal
//
//  Created by Shanti Rodríguez on 13/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class ListDataSource<T>: NSObject, UITableViewDataSource, UITableViewDelegate {
    private let items: [T]
    private let state: APIState
    private let type: CellType
    private let cellFactory: (T, IndexPath) -> UITableViewCell
    var onItemSelected:((_ item: T) -> Void)?
    
    public init(state: APIState, type: CellType, items: [T], cellFactory: @escaping (T, IndexPath) -> UITableViewCell) {
        self.state = state
        self.type = type
        self.items = items
        self.cellFactory = cellFactory
    }
    
    func item(at indexPath: IndexPath) -> T {
        return self.items[indexPath.row]
    }
    
    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (state == .results) ? items.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch state {
        case .loading:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as LoadingCell
            cell.setUp(for: type)
            return cell
        case .results:
            return cellFactory(item(at: indexPath), indexPath)
        case .noResults, .noInternet, .error:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EmptyStateCell
            cell.setUpView(with: state, type: type)
            return cell
        }
    }
    
    // MARK: UITableViewDelegate    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if items.count > 0 {
            onItemSelected?(item(at: indexPath))
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
