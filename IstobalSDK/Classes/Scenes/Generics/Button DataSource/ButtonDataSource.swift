//
//  ButtonDataSource.swift
//  istobal
//
//  Created by Shanti Rodríguez on 25/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

struct ButtonDataSourceConfig {
    let padding: CGFloat
    let cellHeight: CGFloat
}

final class ButtonDataSource<T>: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private let items: [T]
    private let cellFactory: (T, IndexPath) -> UICollectionViewCell
    private let config: ButtonDataSourceConfig
    public var onItemSelected:((_ item: T) -> Void)?
    
    public init(items: [T], config: ButtonDataSourceConfig, cellFactory: @escaping (T, IndexPath) -> UICollectionViewCell) {
        self.items = items
        self.config = config
        self.cellFactory = cellFactory
    }
    
    func item(at indexPath: IndexPath) -> T {
        return self.items[indexPath.row]
    }
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return cellFactory(item(at: indexPath), indexPath)
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.onItemSelected?(item(at: indexPath))
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellsAcross: CGFloat = 2
        var widthRemainingForCellContent = collectionView.bounds.width
        
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.minimumLineSpacing = config.padding
            flowLayout.minimumInteritemSpacing = config.padding
            
            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
        }
        
        let cellWidth = widthRemainingForCellContent / cellsAcross
        return CGSize(width: cellWidth, height: config.cellHeight)
    }
}
