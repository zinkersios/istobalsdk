//
//  EditVehicleDataViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 7/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire

class EditVehicleDataViewController: UIViewController, StyleNavBar {

    // MARK: - IBOutlets
    @IBOutlet var grayView: UIView!
    @IBOutlet var separator: UIView!
    @IBOutlet var textField: UITextField!
    @IBOutlet weak var btnSave: UIBarButtonItem!
    
    // MARK: - Properties
    var type: EditVehicleCellType = .name
    var vehicle: Vehicle?
    weak var delegate: EditVehicleDataViewControllerDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.byDefault, showBottomHairline: true, backButton: .red)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    
    private func setUpView() {
        switch self.type {
        case .name:
            title = .localized(.vehicleNameCell)
            textField.text = vehicle?.name
        case .model:
            title = .localized(.vehicleModel)
            textField.text = vehicle?.model
        default:()
        }
        grayView.backgroundColor = Color.gray7.value
        separator.backgroundColor = Color.separator.value
        
        textField.tintColor = Color.red.value
        textField.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
    }
}

// MARK: - IBActions
extension EditVehicleDataViewController {
    @IBAction func save(_ sender: Any) {
        view.endEditing(true)
        guard let string = textField.text, var vehicle = vehicle else {
            return
        }
        
        switch self.type {
        case .name:
            vehicle.name = string
        case .model:
            vehicle.model = string
        default:()
        }
        
        KVNProgress.show(withStatus: .localized(.updatingData))
        self.delegate?.editVehicleDataViewController(self, updateVehicle: vehicle)
    }
}

// MARK: - UITextField Delegate
extension EditVehicleDataViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        save((Any).self)
        return true
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        guard let string = sender.text else {
            return
        }
        btnSave.isEnabled = string.count > 0
    }
}

// MARK: - Delegate
protocol EditVehicleDataViewControllerDelegate: class {
    func editVehicleDataViewController(_ controller: EditVehicleDataViewController, updateVehicle: Vehicle)
}
