//
//  EditVehicleViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 15/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire

final class EditVehicleViewController: UIViewController, StyleNavBar {
    
    // MARK: - IBOutlets
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    weak var delegate: EditVehicleViewControllerDelegate?
    fileprivate var dataSource: EditVehicleDataSource?
    var vehicle: Vehicle?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.byDefault, showBottomHairline: true, backButton: .red)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.editVehicle)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        self.automaticallyAdjustsScrollViewInsets = false
        view.backgroundColor = Color.gray7.value
        tableView.backgroundColor = Color.gray7.value
        tableView.separatorColor = Color.separator.value
        
        for cell in Cells.forEditVehicle() {
            tableView.registerNibForCellClass(cell)
        }
        setUpVehicle()
    }
    
    fileprivate func setUpVehicle() {
        guard let vehicle = vehicle else {
            return
        }
        
        self.title = vehicle.name
        dataSource = EditVehicleDataSource(with: vehicle)
        tableView.dataSource = dataSource
        tableView.reloadData()
    }
    
    fileprivate func showEdit(with type: EditVehicleCellType) {
        let vc = EditVehicleDataViewController.storyboardViewController()
        vc.vehicle = vehicle
        vc.delegate = self
        vc.type = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func showAlert() {
        let vc = SRAlertViewController.storyboardViewController()
        vc.delegate = self
        vc.type = .removeVehicle
        self.present(vc, animated: true)
    }
    
    fileprivate func editVehicleData(with data: Any?) {
        guard let type = data as? EditVehicleCellType else {
            return
        }
        
        switch type {
        case .name, .model:
            showEdit(with: type)
        case .brand:
            showBrands()
        }
    }
}

// MARK: - UITableView Delegate
extension EditVehicleViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return Constants.SectionFooterHeight.byDefault
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 24 : 34
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = .clear
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifiers.header) as? HeaderCell else {
            fatalError("Check for missing HeaderCell")
        }
        cell.lblTitle.text = section == 1 ? .localized(.drivers) : ""
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tableSection = EditVehicleTableSection(rawValue: indexPath.section) else {
            fatalError("no tableSection 😱")
        }
        
        switch tableSection {
        case .vehicleData:
            editVehicleData(with: dataSource?.data[tableSection]?[indexPath.row])
        case .remove:
            showAlert()
        default: ()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - API
extension EditVehicleViewController {
    fileprivate func deleteVehicle() {
        guard let vehicle = vehicle else { return }
        let vehicleID = "\(vehicle.id)"
        
        KVNProgress.show(withStatus: .localized(.eliminatingVehicle))
        
        let completionHandler: (Result<[Vehicle]>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            guard result.error == nil, let vehicles = result.value else {
                KVNProgress.showError(withStatus: .localized(.error404))
                return
            }
            
            strongSelf.delegate?.editVehicleViewController(strongSelf, vehicles: vehicles)
        }
        APIManager.sharedInstance.fetchDeleteVehicle(with: vehicleID, completionHandler: completionHandler)
    }
    
    fileprivate func updateVehicle(with vehicle: Vehicle) {
        let completionHandler: (Result<Bool>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            if result.isSuccess, result.value == true {
                KVNProgress.showSuccess(withStatus: .localized(.updatedVehicleData))
                strongSelf.vehicle = vehicle
                strongSelf.delegate?.editVehicleViewController(strongSelf, vehicle: vehicle)
                strongSelf.setUpVehicle()
            } else {
                KVNProgress.showError(withStatus: .localized(.errorGeneral))
            }
        }
        
        APIManager.sharedInstance.fetchEditVehicle(with: vehicle, completionHandler: completionHandler)
    }
    
    fileprivate func showBrands() {
        Brands.show(in: self) { (brand) in
            if let selectedBrand = brand, var vehicle = self.vehicle {
                KVNProgress.show(withStatus: .localized(.updatingData))
                vehicle.brand = selectedBrand
                self.updateVehicle(with: vehicle)
            }
        }
    }
}

// MARK: - Delegate
protocol EditVehicleViewControllerDelegate: class {
    func editVehicleViewController(_ controller: EditVehicleViewController, vehicle: Vehicle)
    func editVehicleViewController(_ controller: EditVehicleViewController, vehicles: [Vehicle])
}

// MARK: - EditVehicleDataViewControllerDelegate
extension EditVehicleViewController: EditVehicleDataViewControllerDelegate {
    func editVehicleDataViewController(_ controller: EditVehicleDataViewController, updateVehicle: Vehicle) {
        self.updateVehicle(with: updateVehicle)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.navigationController?.popViewController(animated: false)
        }
    }
}

// MARK: - SRAlertViewControllerDelegate
extension EditVehicleViewController: SRAlertViewControllerDelegate {
    func alertViewController(_ controller: SRAlertViewController, type: AlertViewControllerType, action: ChosenAction) {
        self.dismiss(animated: true) {
            self.setUpAction(for: action)
        }
    }
    
    private func setUpAction(for action: ChosenAction) {
        switch action {
        case .accept:
            deleteVehicle()
        default:()
        }
    }
}
