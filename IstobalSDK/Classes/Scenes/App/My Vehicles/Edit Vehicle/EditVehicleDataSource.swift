//
//  EditVehicleDataSource.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 15/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class EditVehicleDataSource: NSObject, UITableViewDataSource {
    
    var data = [EditVehicleTableSection: [Any]]()
    private var vehicle: Vehicle?
    
    init(with vehicle: Vehicle) {
        self.vehicle = vehicle
        
        /// vehicle data
        data[.vehicleData] = [EditVehicleCellType.name, EditVehicleCellType.brand, EditVehicleCellType.model]
        /// drivers
        data[.drivers] = vehicle.owners
        /// remove vehicle
        data[.remove] = ["remove"]
        
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return EditVehicleTableSection.itemsCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let tableSection = EditVehicleTableSection(rawValue: section), let total = data[tableSection] {
            return total.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tableSection = EditVehicleTableSection(rawValue: indexPath.section) else {
            fatalError("no tableSection 😱")
        }
        
        switch tableSection {
        case .vehicleData:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as VehicleDataCell
            cell.setUp(with: data[tableSection]?[indexPath.row], vehicle: vehicle)
            return cell
        case .drivers:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as DriversCell
            cell.setUp(with: data[tableSection]?[indexPath.row])
            return cell
        case .remove:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as RemoveVehicleCell            
            return cell
        }
    }
}
