//
//  MyVehiclesHeaderViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 13/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class MyVehiclesHeaderViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    
    // MARK: - Properties
    weak var delegate: MyVehiclesHeaderViewControllerDelegate?
    var vehicleDeleted = false
    var vehicles: [Vehicle]? {
        didSet {
            updateView()
        }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "MyVehicleCollectionViewCell", bundle: SDKNavigation.getBundle()), forCellWithReuseIdentifier: "MyVehicleCollectionViewCell")
        
        btnBack.tintColor = SDKColor.secondaryLabel
        btnNext.tintColor = SDKColor.secondaryLabel
        btnBack.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        btnNext.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        
        self.pageControl.currentPageIndicatorTintColor = SDKColor.navigationBarTint
        self.pageControl.pageIndicatorTintColor = SDKColor.secondaryLabel
    }
    
    /// update current page
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(collectionView.contentOffset.x / collectionView.bounds.size.width)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setNameOfPreviousVehicle()
        setNameOfNextVehicle()
        setTickets()
    }
    
    fileprivate func setNameOfPreviousVehicle() {
        let index = pageControl.currentPage - 1
        
        if let vehicle = vehicles?.elementAt(index: index) {
            btnBack.setTitle(vehicle.name, for: .normal)
            btnBack.isHidden = false
        } else {
            btnBack.isHidden = true
        }
    }
    
    fileprivate func setNameOfNextVehicle() {
        let index = pageControl.currentPage + 1
        
        if let vehicle = vehicles?.elementAt(index: index) {
            btnNext.setTitle(vehicle.name, for: .normal)
            btnNext.isHidden = false
        } else {
            btnNext.isHidden = true
        }
    }
    
    fileprivate func setTickets() {
        if let vehicle = vehicles?.elementAt(index: pageControl.currentPage) {            
            self.delegate?.myVehiclesHeaderViewController(self, tickets: vehicle.tickets)
        }
    }
    
    private func updateView() {
        if vehicleDeleted == true && pageControl.currentPage > 0 {
            pageControl.currentPage = 0
            collectionView.setContentOffset(CGPoint(x: CGFloat(pageControl.currentPage) * collectionView.bounds.size.width, y: 0), animated: false)
        }
        
        setNameOfPreviousVehicle()
        setNameOfNextVehicle()
        setTickets()
        
        if let vehicles = vehicles {
            pageControl.numberOfPages = vehicles.count
        }
        collectionView.reloadData()
    }
}

// MARK: - UICollectionViewDataSource
extension MyVehiclesHeaderViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let vehicles = vehicles {
            return vehicles.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as MyVehicleCollectionViewCell
        cell.setUp(with: vehicles?[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.size.width, height: self.collectionView.bounds.size.height)
    }
}

// MARK: - IBActions
extension MyVehiclesHeaderViewController {
    @IBAction func showPreviousVehicle(_ sender: Any) {
        pageControl.currentPage = (pageControl.currentPage - 1)
        setUpVehicleButtons()
    }
    
    @IBAction func showNextVehicle(_ sender: Any) {
        pageControl.currentPage = (pageControl.currentPage + 1)
        setUpVehicleButtons()
    }
    
    private func setUpVehicleButtons() {
        collectionView.setContentOffset(CGPoint(x: CGFloat(pageControl.currentPage) * collectionView.bounds.size.width, y: 0), animated: true)
        setNameOfNextVehicle()
        setNameOfPreviousVehicle()
        setTickets()
    }
}

// MARK: - Delegate
protocol MyVehiclesHeaderViewControllerDelegate: class {
    func myVehiclesHeaderViewController(_ controller: MyVehiclesHeaderViewController, tickets: [Ticket])
}
