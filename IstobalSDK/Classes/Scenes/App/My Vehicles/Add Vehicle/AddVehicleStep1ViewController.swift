//
//  AddVehicleStep1ViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 22/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class AddVehicleStep1ViewController: UIViewController, StyleNavBar {

    // MARK: - IBOutlets
    @IBOutlet var btnCancel: UIBarButtonItem!
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblRegistration: UILabel!
    @IBOutlet weak var lblExample: UILabel!
    @IBOutlet var txtRegistration: UITextField!
    @IBOutlet var registrationStatus: IconButton!
    // MARK: - Properties
    var vehicleName: String?
    var isFromTicket: Bool = false
    weak var delegate: AddVehicleStep1ViewControllerDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        changeNavBarStyle(.byDefault, showBottomHairline: false, backButton: .gray)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtRegistration.becomeFirstResponder()
//        Utils.registerScreen(.addVehicle1)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        btnCancel.title = Utils.capitalisedText(with: .localized(.cancel))
        btnCancel.setStyle(.gray)
        
        lblQuestion.text = .localized(.addVehicleQuestion1)
        lblRegistration.text = .localized(.registrationNumber)
        lblExample.text = .localized(.exampleLicensePlate)
    }
    
    fileprivate func showStep2(with registrationMumber: String) {
        let vc = AddVehicleStep2ViewController.storyboardViewController()
        vc.registrationNumber = registrationMumber
        vc.vehicleName = vehicleName
        vc.isFromTicket = isFromTicket
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - IBActions
extension AddVehicleStep1ViewController {
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func showNextPage(_ sender: Any) {
        guard let value = txtRegistration.text else {
            return
        }
        
        if value.isEmpty || value.count < 4 {
            lblExample.text = .localized(.exampleLicensePlateError)
            registrationStatus.updateState(with: .error, feedback: true)
            return
        }                
        
        lblExample.text = .localized(.exampleLicensePlate)
        registrationStatus.updateState(with: .success)
        showStep2(with: value)
    }
}

// MARK: - UITextFieldDelegate
extension AddVehicleStep1ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        showNextPage((Any).self)
        return true
    }
}

// MARK: - Delegate
protocol AddVehicleStep1ViewControllerDelegate: class {
    func addVehicleStep1ViewController(_ controller: AddVehicleStep1ViewController, vehicles: [Vehicle])
}
