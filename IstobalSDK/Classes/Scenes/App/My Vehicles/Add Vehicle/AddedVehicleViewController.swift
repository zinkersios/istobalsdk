//
//  AddedVehicleViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 23/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class AddedVehicleViewController: UIViewController, StyleNavBar {

    // MARK: - IBOutlets
    @IBOutlet var imgVehicle: UIImageView!
    @IBOutlet var lblCongratulations: UILabel!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var lblRegistrationNumber: UILabel!
    @IBOutlet var btnFinalize: UIButton!
    @IBOutlet var heightButtonConstraint: NSLayoutConstraint!
    // MARK: - Properties
    var registrationNumber = ""
    var vehicleName = ""
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.addedVehicle)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        heightButtonConstraint.constant = defaultButtonHeight()
        
        lblVehicleName.text = vehicleName
        lblRegistrationNumber.text = registrationNumber
        lblCongratulations.text = .localized(.congratsAddedVehicle)
        btnFinalize.setTitle( Utils.capitalisedText(with: .localized(.continuar)), for: .normal)
        
        lblRegistrationNumber.textColor = .lightGray
        lblRegistrationNumber.font = Font(.installed(.helveticaNeueMedium), size: .standard(.h2)).instance
        
        // Shadow
        imgVehicle.addDropShadow()
    }
}

// MARK: - IBActions
extension AddedVehicleViewController {
    @IBAction func finalize(_ sender: Any) {
        dismiss(animated: true)
    }
}
