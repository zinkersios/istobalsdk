//
//  IntroAddVehicleViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 22/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

final class IntroAddVehicleViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnAddVehicle: UIButton!
    @IBOutlet var btnContinueWithoutVehicle: UIButton!
    @IBOutlet var heightButtonConstraint: NSLayoutConstraint!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.introAddVehicle)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        heightButtonConstraint.constant = defaultButtonHeight()
        
        if Device.size() == .screen4Inch {
            lblTitle.font = Font(.custom(SDKFont.light), size: .custom(25)).instance                        
        }
        
        btnAddVehicle.backgroundColor = Color.blue.value
        lblTitle.text = .localized(.introAddVehicle)
        lblSubtitle.text = .localized(.subIntroAddVehicle)
        lblTitle.setLineHeight(0.88)
        
        btnAddVehicle.setTitle( Utils.capitalisedText(with: .localized(.addVehicle)), for: .normal)
        btnContinueWithoutVehicle.setTitle( Utils.capitalisedText(with: .localized(.continueWithoutVehicle)), for: .normal)
    }
}

// MARK: - IBActions
extension IntroAddVehicleViewController {
    @IBAction func showNextView(_ sender: Any) {
        let vc = AddVehicleStep1ViewController.storyboardViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func dimissView(_ sender: Any) {
        dismiss(animated: true)
    }
}
