//
//  AddVehicleStep2ViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 23/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire

final class AddVehicleStep2ViewController: UIViewController, StyleNavBar {
    
    // MARK: - IBOutlets
    @IBOutlet var btnCancel: UIBarButtonItem!
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var txtVehicleName: UITextField!
    @IBOutlet var vehicleNameStatus: IconButton!
    @IBOutlet var lblExampleVehicleName: UILabel!
    // MARK: - Properties
    var registrationNumber: String?
    var vehicleName: String?
    var isFromTicket: Bool = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        changeNavBarStyle(.byDefault, showBottomHairline: false, backButton: .gray)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtVehicleName.becomeFirstResponder()
//        Utils.registerScreen(.addVehicle2)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        btnCancel.title = Utils.capitalisedText(with: .localized(.cancel))
        btnCancel.setStyle(.gray)
        
        lblQuestion.text = .localized(.addVehicleQuestion2)
        lblVehicleName.text = .localized(.vehicleName)
        lblExampleVehicleName.text = .localized(.exampleVehicleName)
        txtVehicleName.text = vehicleName
        
        self.navigationController?.delegate = self
    }
    
    fileprivate func showFinalStep(with name: String, registrationNum: String) {
        let vc = AddedVehicleViewController.storyboardViewController()
        vc.vehicleName = name
        vc.registrationNumber = registrationNum
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

// MARK: - API
extension AddVehicleStep2ViewController {
    fileprivate func saveVehicle(with name: String, registrationNum: String) {
        let params = ["name": name, "licenseNumber": registrationNum]
        KVNProgress.show(withStatus: .localized(.savingVehicle))
        
        let completionHandler: (Result<[Vehicle]>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            guard result.error == nil else {
                if let error = result.error as? APIManagerError {
                    KVNProgress.showError(withStatus: error.localizedTitle)
                }
                strongSelf.dismiss(animated: true)
                return
            }
            
            if let vehicles = result.value, let vc = strongSelf.navigationController?.viewControllers.first as? AddVehicleStep1ViewController {
                vc.delegate?.addVehicleStep1ViewController(vc, vehicles: vehicles)
            }
            
            if strongSelf.isFromTicket {
                strongSelf.dismiss(animated: false)
            } else {
                strongSelf.showFinalStep(with: name, registrationNum: registrationNum)
                KVNProgress.dismiss()
            }
        }
        
        APIManager.sharedInstance.fetchNewVehicle(with: params, completionHandler: completionHandler)
    }
}

// MARK: - IBActions
extension AddVehicleStep2ViewController {
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func showNextPage(_ sender: Any) {
        guard let name = txtVehicleName.text, let registrationNum = registrationNumber else {
            return
        }
        
        if name.isEmpty {
            vehicleNameStatus.updateState(with: .error, feedback: true)
            return
        }
        vehicleNameStatus.updateState(with: .success)
        saveVehicle(with: name, registrationNum: registrationNum)
    }
}

// MARK: - UITextFieldDelegate
extension AddVehicleStep2ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        showNextPage((Any).self)
        return true
    }
}

// MARK: - UINavigationControllerDelegate
extension AddVehicleStep2ViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        (viewController as? AddVehicleStep1ViewController)?.vehicleName = txtVehicleName.text
    }
}
