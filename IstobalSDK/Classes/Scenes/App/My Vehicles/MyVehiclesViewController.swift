//
//  MyVehiclesViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 29/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device
import Alamofire
import KVNProgress

final class MyVehiclesViewController: UIViewController, StyleNavBar {
    
    // MARK: - IBOutlets
//    @IBOutlet var backgroundHeightConstraint: NSLayoutConstraint!
    @IBOutlet var headerContainer: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnEditVehicle: UIBarButtonItem!
    @IBOutlet var emptyState: EmptyStateView!
    
    // MARK: - Properties
    fileprivate var vehicleDeleted = false
    fileprivate var tickets = [Ticket]() {
        didSet { self.tableView.reloadData() }
    }
    fileprivate var vehicles = [Vehicle]() {
        didSet { setUpVehicles() }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        DeeplinkNavigator.shared.navigationController = self.navigationController
        navigationItem.titleView = titleView(with: .localized(.myVehicles), style: .myProfile)
        emptyState.setUp(for: .myVehicles)
        setUpTableView()
        setUpView()
        getMyVehicles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.myProfile, showBottomHairline: false, backButton: .white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        changeNavBarStyle(.myProfile, showBottomHairline: false, backButton: .white)
//        Utils.registerScreen(.myVehicles)
    }
    
    private func setUpView() {
        var topPadding = CGFloat(60) // 60: lo uso para que el fondo vaya hasta la mitad del botón COMPRAR LAVADO        
        var height = (Device.size() == .screen4_7Inch) ? 267 : 288
        
        if Device.size() == .screen4Inch {
            height = 233
        } else if Device.size() == .screen5_8Inch {
            topPadding += 24
        }
        
//        backgroundHeightConstraint.constant = UIScreen.main.bounds.height//CGFloat(height) + topPadding + tableView.frame.origin.y
        headerContainer.frame.size.height = CGFloat(height)
    }
    
    private func setUpTableView() {
        tableView.tableFooterView = UIView()
        tableView.separatorColor = Color.separator.value
        tableView.registerCell(ofType: HeaderMyVehicleCell.self)
        tableView.registerCell(ofType: TicketCell.self)
        tableView.registerCell(ofType: EmptyStateCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    fileprivate func setUpNoResults() {
        emptyState.setUpNoResults(with: .myVehicles)
        emptyState.isHidden = false
        btnEditVehicle.tintColor = .clear
        btnEditVehicle.isEnabled = false
    }
}

// MARK: - API
extension MyVehiclesViewController {
    fileprivate func getMyVehicles() {
        let completionHandler: (Result<[Vehicle]>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            guard result.error == nil else {
                strongSelf.emptyState.handleError(result.error, type: .myVehicles)
                return
            }
            
            guard let fetchedVehicles = result.value, fetchedVehicles.count > 0 else {
                strongSelf.setUpNoResults()
                return
            }
            
            strongSelf.vehicles = fetchedVehicles
        }
        
        APIManager.sharedInstance.fetchVehicles(completionHandler)
    }
    
    fileprivate func setUpVehicles() {
        if vehicles.count == 0 {
            setUpNoResults()
            return
        }
        
        emptyState.isHidden = true
        tableView.isHidden = false
        btnEditVehicle.tintColor = SDKColor.navigationBarTint
        btnEditVehicle.isEnabled = true
        
        if let header = self.childViewControllers[0] as? MyVehiclesHeaderViewController {
            header.delegate = self
            header.vehicleDeleted = vehicleDeleted
            header.vehicles = vehicles
        }
    }
}

// MARK: - UITableViewDelegate
extension MyVehiclesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let button = UIButton(type: .system)
//        button.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: defaultButtonHeight())
//        button.gy_styleName = StyleConstants.Button.seeAllTickets
//        button.addTarget(self, action:#selector(showTickets), for: .touchUpInside)
//        return (tickets.count == 0) ? nil : button
       return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
//        if tickets.count == 0 {
//            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EmptyStateCell
//            cell.setUpView(with: .noResults, type: .ticketsForMyVehicles)
//            return cell
//        }
//
//        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as TicketCell
//        cell.setUp(with: tickets[indexPath.row])
//        return cell
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tickets.count > 0 {
//            let vc = TicketDetailViewController.storyboardViewController()
//            vc.modalPresentationStyle = .overFullScreen
//            vc.delegate = self
//            vc.ticket = tickets[indexPath.row]
//            self.present(vc, animated: true)
//        }
//        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - UITableViewDelegate
extension MyVehiclesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifiers.headerMyVehicle) as? HeaderMyVehicleCell else {
            fatalError("Check for missing HeaderMyVehicleCell")
        }
//        cell.btnBuy.addTarget(self, action:#selector(showHome), for: .touchUpInside)
        return cell.contentView
    }
}

// MARK: IBActions
extension MyVehiclesViewController {
    @IBAction func editVehicle(_ sender: Any) {
        let vc = EditVehicleViewController.storyboardViewController()
        
        if let header = self.childViewControllers.first as? MyVehiclesHeaderViewController, let vehicle = vehicles.elementAt(index: header.pageControl.currentPage) {
            vc.vehicle = vehicle
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func addVehicle(_ sender: Any) {
        let vc = AddVehicleStep1ViewController.storyboardViewController()
        vc.delegate = self
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated:true, completion: nil)
    }
    
    @IBAction func showHome(_ sender: Any) {
//        if let header = self.childViewControllers.first as? MyVehiclesHeaderViewController, let vehicle = vehicles.elementAt(index: header.pageControl.currentPage) {
//            UserDefaults.Account.set(vehicle.id, forKey: .selectedVehicleToMakePurchase)
//        }
//
//        self.sideMenuController?.performSegue(withIdentifier: TypeController.home.rawValue, sender: nil)
//        NCenterUtils.updateSideMenu(with: .home)
    }
    
    @IBAction func showTickets(_ sender: Any) {
//        self.sideMenuController?.performSegue(withIdentifier: TypeController.myTickets.rawValue, sender: nil)
//        NCenterUtils.updateSideMenu(with: .myTickets)
    }
}

// MARK: - EditVehicleViewControllerDelegate
extension MyVehiclesViewController: EditVehicleViewControllerDelegate {
    func editVehicleViewController(_ controller: EditVehicleViewController, vehicle: Vehicle) {
        if let header = self.childViewControllers[0] as? MyVehiclesHeaderViewController {
            vehicles.remove(at: header.pageControl.currentPage)
            vehicles.insert(vehicle, at: header.pageControl.currentPage)
            header.vehicles = vehicles
        }
    }
    
    func editVehicleViewController(_ controller: EditVehicleViewController, vehicles: [Vehicle]) {
        self.navigationController?.popViewController(animated: false)
        vehicleDeleted = true
        self.vehicles = vehicles
        KVNProgress.showSuccess(withStatus: .localized(.vehicleDeleted))
    }
}

// MARK: - EditVehicleViewControllerDelegate
extension MyVehiclesViewController: AddVehicleStep1ViewControllerDelegate {
    func addVehicleStep1ViewController(_ controller: AddVehicleStep1ViewController, vehicles: [Vehicle]) {
        vehicleDeleted = false
        self.vehicles = vehicles
    }
}

// MARK: - MyVehiclesHeaderViewControllerDelegate
extension MyVehiclesViewController: MyVehiclesHeaderViewControllerDelegate {
    func myVehiclesHeaderViewController(_ controller: MyVehiclesHeaderViewController, tickets: [Ticket]) {
        self.tickets = tickets
    }
}

// MARK: DraggableViewControllerDelegate
extension MyVehiclesViewController: DraggableViewControllerDelegate {    
    func draggableViewControllerDismissed(_ controller: DraggableViewController) {
        self.dismiss(animated: true)
    }
}
