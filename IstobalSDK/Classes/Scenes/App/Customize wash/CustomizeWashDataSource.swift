//
//  CustomizeWashDataSource.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 9/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class CustomizeWashDataSource: NSObject, UITableViewDataSource {
    
    // MARK: - Properties
    var data = [CustomizeWashTableSection: [Any]]()        
    private var selectedService: Service?
    private var selectedAddons = [Addon]()
    weak var delegate: CustomizeWashDataSourceDelegate?
    
    // MARK: - View Life Cycle
    init(services: [Service], addons: [Addon], selectedService: Service?, selectedAddons: [Addon]) {
        data[.services] = services
        data[.addons] = addons
        self.selectedService = selectedService
        self.selectedAddons = selectedAddons
        super.init()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return CustomizeWashTableSection.itemsCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let tableSection = CustomizeWashTableSection(rawValue: section), let data = data[tableSection] else {
            return 0
        }
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let tableSection = CustomizeWashTableSection(rawValue: indexPath.section) else {
            fatalError("no tableSection in CustomizeWashDataSource")
        }
        
        let objects = data[tableSection]?[indexPath.row]
        
        switch tableSection {
        case .services:
            return setUpServiceCell(tableView, indexPath: indexPath, object: objects)
        case .addons:
            return setUpAddonCell(tableView, indexPath: indexPath, object: objects)
        }
    }
    
    private func setUpServiceCell(_ tableView: UITableView, indexPath: IndexPath, object: Any?) -> CustomizeWashCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as CustomizeWashCell
        
        guard let service = object as? Service else {
            return cell
        }
        
        cell.setUp(with: service, selectedService: selectedService)
        
        cell.onCheckboxTap = { [unowned self] in
            if service == self.selectedService {
                self.selectedService = nil
            } else {
                self.selectedService = service
            }
            
            self.delegate?.customizeWashDataSource(self, selectedService: self.selectedService, selectedAddons: self.selectedAddons)
            tableView.reloadData()
        }
        
        cell.onInfoButtonTap = { [unowned self] service in
            self.delegate?.customizeWashDataSource(self, showTitle: service.name, message: service.longDescription)
        }
        
        return cell
    }
    
    private func setUpAddonCell(_ tableView: UITableView, indexPath: IndexPath, object: Any?) -> AddonCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as AddonCell
        
        guard let addon = object as? Addon else {
            return cell
        }
        
        cell.setUp(with: addon, selectedAddons: selectedAddons)
        
        cell.onCheckboxTap = { [unowned self] isOn in            
            if isOn {
                self.selectedAddons.append(addon)
            } else {
                self.selectedAddons.removeObject(obj: addon)
            }
            self.delegate?.customizeWashDataSourceUpdatePrice(self, selectedService: self.selectedService, selectedAddons: self.selectedAddons)
        }
        
        cell.onInfoButtonTap = { [unowned self] addon in
            self.delegate?.customizeWashDataSource(self, showTitle: addon.name, message: addon.longDescription)
        }
        
        return cell
    }
}

// MARK: - Delegate
protocol CustomizeWashDataSourceDelegate: class {
    func customizeWashDataSource(_ dataSource: CustomizeWashDataSource, selectedService: Service?, selectedAddons: [Addon])
    func customizeWashDataSourceUpdatePrice(_ dataSource: CustomizeWashDataSource, selectedService: Service?, selectedAddons: [Addon])
    func customizeWashDataSource(_ dataSource: CustomizeWashDataSource, showTitle: String, message: String?)
}
