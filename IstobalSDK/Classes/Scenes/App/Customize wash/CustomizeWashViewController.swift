//
//  CustomizeWashViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 9/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class CustomizeWashViewController: UIViewController, StyleNavBar, ErrorPresenting {

    // MARK: - IBOutlets
    @IBOutlet var footerView: FinalizePurchaseView!
    @IBOutlet var heightFooterViewConstraint: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    fileprivate var dataSource: CustomizeWashDataSource? {
        didSet { tableView.reloadData() }
    }
    
    var services = [Service]()
    var installation: Installation?
    var offer: Offer?
    var delegate: ServicesViewDelegate?
    fileprivate var totalPurchase: Double = 0
    fileprivate var selectedService: Service?
    fileprivate var selectedAddons = [Addon]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = .localized(.customizeWash)
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.byDefault, showBottomHairline: true, backButton: .red)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        view.backgroundColor = Color.gray7.value
        tableView.backgroundColor = Color.gray7.value
        heightFooterViewConstraint.constant = heightForFooter()
        
        footerView.lblMakePayment.text = Utils.capitalisedText(with: .localized(.purchaseSummary))        
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 50))
        tableView.separatorColor = Color.separator.value
        tableView.estimatedRowHeight = 70.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.registerCell(ofType: HeaderCell.self)
        tableView.registerCell(ofType: CustomizeWashCell.self)
        tableView.registerCell(ofType: AddonCell.self)
        setUpDataSource(with: [])
        
        footerView.onButtonTap = { [unowned self] in            
            self.showPurchaseSummary()
        }
    }
    
    fileprivate func setUpDataSource(with addons: [Addon]) {
        dataSource = CustomizeWashDataSource(services: services, addons: addons, selectedService: selectedService, selectedAddons: selectedAddons)
        dataSource?.delegate = self
        tableView.dataSource = dataSource
    }
    
    private func showPurchaseSummary() {        
        if totalPurchase <= 0 {
            presentStatusBarNotification(.localized(.chooseAprogram), style: .warning)
            return
        }
        self.delegate!.onServiceReadyToBuy(installationId: "\(self.installation?.id ?? 0)", selectedService: self.selectedService!, selectedAddons: self.selectedAddons)
        self.goBackToApp()

    }
    
    private func goBackToApp(){
        if let viewControllers = self.navigationController?.viewControllers {
            if viewControllers.count > 2 {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
            } else {
                // fail
            }
        }
    }
    
    // check if there is a selected or redeemed offer
    private func checkOffer() -> Offer {
        if let oferta = offer {
            return oferta // user selected an offer in OffersViewController
        } else {
            if let offerData = UserDefaults.User.data(forKey: .lastOfferRedeemed),
                let offer = try? JSONDecoder().decode(Offer.self, from: offerData) {
                
                if let offers = installation?.offers, !offers.contains(offer) {
                    installation?.offers.append(offer)
                }
                return offer // User redeemed an offer from TicketReadingViewController
            }
            // apply the best offer
            else if let oferta = APIHelper.getTheBestOffer(with: installation?.offers, selected: selectedService) {
                return oferta
            }
        }
        return Offer.noUseCoupon()
    }
    
    fileprivate func updatePrice() {
        footerView.lblPrice.text = PriceUtils.priceToCurrency(with: totalPurchase)
    }
}

// MARK: - UITableView Delegate
extension CustomizeWashViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.SectionHeaderHeight.byDefault
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifiers.header) as? HeaderCell else {
            fatalError("Check for missing HeaderCell")
        }
        
        let number = "\(section + 1). "
        
        if let tableSection = CustomizeWashTableSection(rawValue: section) {
            switch tableSection {
            case .services:
                cell.lblTitle.text = number + .localized(.customizeWashOption1)
            case .addons:
                let title = (selectedService != nil) ? number + .localized(.customizeWashOption2) : " "
                cell.lblTitle.text = title
            }
        }
        return cell.contentView
    }
}

// MARK: - CustomizeWashDataSourceDelegate
extension CustomizeWashViewController: CustomizeWashDataSourceDelegate {
    func customizeWashDataSource(_ dataSource: CustomizeWashDataSource, selectedService: Service?, selectedAddons: [Addon]) {
        
        if self.selectedService != selectedService {
            self.selectedAddons.removeAll()
        } else {
            self.selectedAddons = selectedAddons
        }
        
        self.selectedService = selectedService        
        totalPurchase = 0
        
        if let service = selectedService {
            totalPurchase = service.price
            setUpDataSource(with: service.addons)
        } else {
            setUpDataSource(with: self.selectedAddons)
        }
        
        updatePrice()
    }
    
    func customizeWashDataSourceUpdatePrice(_ dataSource: CustomizeWashDataSource, selectedService: Service?, selectedAddons: [Addon]) {
        guard let service = selectedService else {
            return
        }
        
        self.selectedAddons = selectedAddons
        
        let totalPrice = selectedAddons.map({$0.price}).reduce(0, +)
        totalPurchase = service.price + totalPrice
        
        updatePrice()
    }
    
    func customizeWashDataSource(_ dataSource: CustomizeWashDataSource, showTitle: String, message: String?) {
        presentAlertView(with: showTitle, message: message)
    }
}
