//
//  ValorationsDataSource.swift
//  istobal
//
//  Created by Shanti Rodríguez on 26/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class ValorationsDataSource: NSObject, UITableViewDataSource {
    var valorations: [Valoration]
    var state: APIState = .loading
    var type: CellType = .installations
    
    init(valorations: [Valoration]) {
        self.valorations = valorations
        super.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch state {
        case .results:
            return valorations.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch state {
        case .loading:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as LoadingCell
            cell.setUp(for: type)
            return cell
        case .results:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ValorationCell
            cell.setUp(with: valorations[indexPath.row])
            return cell
        case .noResults, .noInternet, .error:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EmptyStateCell
            cell.setUpView(with: state, type: type)
            return cell
        }
    }
}
