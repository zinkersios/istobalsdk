//
//  ValorationsViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 19/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Alamofire

final class ValorationsViewController: UIViewController, StyleNavBar, ErrorPresenting {

    // MARK: - IBOutlets
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    var installation: Installation?
    var valorationsDataSource: ValorationsDataSource? {
        didSet { tableView.reloadData() }
    }
    fileprivate var apiHasBeenCalled = false
    fileprivate var filter = ValorationFilter(installationID: 0, page: 1)
    fileprivate var activityIndicator: LoadMoreActivityIndicator?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: false)
        setUpTableView()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.byDefault, showBottomHairline: true, backButton: .red)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !apiHasBeenCalled {
            getValorations()
        }
    }
    
    private func setUpTableView() {
        view.backgroundColor = Color.gray7.value
        tableView.backgroundColor = Color.gray7.value
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        activityIndicator = LoadMoreActivityIndicator(tableView: tableView, spacingFromLastCell: 10, spacingFromLastCellWhenLoadMoreActionStart: 60)
        
        tableView.registerCell(ofType: LoadingCell.self)
        tableView.registerCell(ofType: ValorationCell.self)
        tableView.registerCell(ofType: ValorationHeaderCell.self)
        tableView.registerCell(ofType: EmptyStateCell.self)
        
        tableView.tableFooterView = UIView()        
        updateTableView(with: .loading)
    }
    
    fileprivate func updateTableView(with state: APIState) {
        valorationsDataSource = ValorationsDataSource(valorations: [])
        valorationsDataSource?.state = state
        valorationsDataSource?.type = .valorations
        tableView.dataSource = valorationsDataSource
        tableView.separatorColor = .clear
    }
    
    private func setUpView() {
        guard let installation = installation else {
            return
        }
        title = .localized(.valuations) + " " + installation.companyName
    }
}

// MARK: - API
extension ValorationsViewController {
    private func handleError(_ error: Error?) {
        if !ReachabilityManager.shared.isNetworkAvailable {
            updateTableView(with: .noInternet)
        } else {
            updateTableView(with: .error)
        }
    }
    
    fileprivate func getValorations(_ scrollView: UIScrollView? = nil) {
        guard let installation = installation else { return }
        apiHasBeenCalled = true
        filter.installationID = installation.id
        
        let completionHandler: (Result<ValorationsDataSource>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            guard result.error == nil else {
                strongSelf.handleError(result.error)
                return
            }
            guard let fetchedValorations = result.value else {
                strongSelf.updateTableView(with: .noResults)
                return
            }
            
            if let valorations = strongSelf.valorationsDataSource?.valorations, valorations.count > 0 {
                if fetchedValorations.valorations.count == 0 {
                    strongSelf.noMoreValorations(with: scrollView!)
                } else {
                    strongSelf.filter.page += 1
                    strongSelf.addNewValorations(with: fetchedValorations.valorations, scrollView: scrollView!)
                }
            } else {
                strongSelf.filter.page += 1
                fetchedValorations.state = .results
                strongSelf.valorationsDataSource = fetchedValorations
                strongSelf.tableView.dataSource = strongSelf.valorationsDataSource
                strongSelf.valorationsDataSource?.type = .valorations
                strongSelf.tableView.separatorColor = Color.separator.value
            }
        }
        APIManager.sharedInstance.fetchValorations(with: filter.convertToDictionary(), completionHandler: completionHandler)
    }
    
    private func addNewValorations(with valorations: [Valoration], scrollView: UIScrollView) {
        for valoration in valorations {
            valorationsDataSource?.valorations.append(valoration)
        }
        
        tableView.reloadData()
        self.activityIndicator?.loadMoreActionFinshed(scrollView: scrollView)
    }
    
    private func noMoreValorations(with scrollView: UIScrollView) {
        presentStatusBarNotification(.localized(.noMoreRatings), style: .info)
        self.activityIndicator?.loadMoreActionFinshed(scrollView: scrollView)
    }
}

// MARK: - UITableViewDelegate
extension ValorationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch valorationsDataSource?.state {
        case .results?:
            return 51
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ValorationHeaderCell") as? ValorationHeaderCell else {
            fatalError("Check for missing ValorationHeaderCell")
        }
        cell.setUp(with: valorationsDataSource?.valorations, installation: installation)
        return cell.contentView
    }
}

// MARK: - ScrollView Delegate
extension ValorationsViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        activityIndicator?.scrollViewDidScroll(scrollView: scrollView) {
            getValorations(scrollView)
        }
    }
}
