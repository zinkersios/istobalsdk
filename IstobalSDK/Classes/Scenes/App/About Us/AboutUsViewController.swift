//
//  AboutUsViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 5/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import SwiftWebViewProgress

final class AboutUsViewController: UIViewController, StyleNavBar {

    // MARK: - IBOutlets
    @IBOutlet var webView: UIWebView!
    @IBOutlet var goBack: UIBarButtonItem!
    @IBOutlet var goForward: UIBarButtonItem!
    // MARK: - Properties
    fileprivate var progressView: WebViewProgressView!
    fileprivate var progressProxy: WebViewProgress!
    var type: WebViewType = .aboutUs
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        DeeplinkNavigator.shared.navigationController = self.navigationController
        setUpView()
        setUpURL()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpTitle()
    }
    
    func superDeinit() {
        if let wView = webView {
            wView.stopLoading()
            wView.delegate = nil
        }
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        self.automaticallyAdjustsScrollViewInsets = false
        progressProxy = WebViewProgress()
        webView.delegate = progressProxy
        progressProxy.webViewProxyDelegate = self
        progressProxy.progressDelegate = self
        
        /// progressView
        guard let navController = navigationController else {
            return
        }
        
        let progressBarHeight: CGFloat = 2.0
        let navigationBarBounds = navController.navigationBar.bounds
        let barFrame = CGRect(x: 0, y: navigationBarBounds.size.height - progressBarHeight, width: navigationBarBounds.width, height: progressBarHeight)
        progressView = WebViewProgressView(frame: barFrame)
        progressView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
    }
    
    private func setUpTitle() {
        switch type {
        case .aboutUs:
            navigationItem.titleView = titleView(with: .localized(.aboutUs), style: .byDefault)
            changeNavBarStyle(.byDefault)
        case .bookAppointment:
            navigationItem.titleView = titleView(with: .localized(.bookAppointment), style: .byDefault)
            changeNavBarStyle(.byDefault, showBottomHairline: true, backButton: .red)
        }
    }
    
    private func setUpURL() {
        switch type {
        case .aboutUs:
            let url = URL(string: Constants.URL.aboutUs)
            loadWebView(with: url!)
        case .bookAppointment:
            let url = URL(string: Constants.URL.bookAppointment)
            loadWebView(with: url!)
        }
    }
    
    private func loadWebView(with url: URL) {
        var request = URLRequest(url: url)
        
        if type == .bookAppointment, let userViewModel = UserViewModel.getUser() {
            request.setValue(userViewModel.user.email, forHTTPHeaderField: Constants.JSONKey.email)
            request.setValue(encodedString(with: userViewModel.fullName), forHTTPHeaderField: "name")
        }
        
        self.webView.loadRequest(request)
        self.navigationController?.navigationBar.addSubview(progressView)
    }
    
    private func encodedString(with string: String) -> String {
        return string.folding(options: .diacriticInsensitive, locale: .current)
    }
}

// MARK: - WebViewProgressDelegate
extension AboutUsViewController: UIWebViewDelegate, WebViewProgressDelegate {
    func webViewProgress(_ webViewProgress: WebViewProgress, updateProgress progress: Float) {
        progressView.setProgress(progress, animated: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        goBack.isEnabled = webView.canGoBack ? true : false
        goForward.isEnabled = webView.canGoForward ? true : false
        
        if type == .bookAppointment, webView.request?.url?.host == Constants.Host.istobal {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
