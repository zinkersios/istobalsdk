//
//  BuyWashController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 7/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

final class BuyWashViewController: UIViewController, StyleNavBar {
    
    // MARK: - IBOutlets
    @IBOutlet var tableView: UITableView!
    
    // MARK: - Properties
    fileprivate var items = Wash.getWashWithPrograms()
    var installation: Installation?
    var services = [Service]()
    var statusBarStyle: UIStatusBarStyle = .lightContent
    weak var delegate: BuyWashViewControllerDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()                
        title = .localized(.chooseServices)
        view.backgroundColor = Color.black9.value
        
        tableView.tableFooterView = UIView()
        tableView.registerCell(ofType: BuyWashCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.buyWash, showBottomHairline: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.buyWash)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = statusBarStyle
    }
}

// MARK: - UITableView DataSource
extension BuyWashViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as BuyWashCell
        cell.setUp(with: items[indexPath.row])
        return cell
    }
}

// MARK: - UITableView Delegate
extension BuyWashViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.buyWashViewControllerShowPurchaseSummary(self, wash: items[indexPath.row], services: services)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Device.size() {
        case .screen3_5Inch, .screen4Inch:
            return 100
        default:
            return 108
        }
    }
}

// MARK: - IBOutlets
extension BuyWashViewController {
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

// MARK: - BuyWashViewController Delegate
protocol BuyWashViewControllerDelegate: class {
    func buyWashViewControllerShowPurchaseSummary(_ controller: BuyWashViewController, wash: Wash, services: [Service])
}
