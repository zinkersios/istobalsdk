//
//  DuringWashingViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 21/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device
import KVNProgress
import Alamofire

final class DuringWashingViewController: UIViewController, StyleNavBar {
    
    // MARK: - IBOutlets
    @IBOutlet var ring: UICircularProgressRingView!
    @IBOutlet var avatarView: AvatarView!
    @IBOutlet var lblTimer: UILabel!
    @IBOutlet var lblWash: VerticalAlignLabel!
    @IBOutlet var lblApplying: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet var lblWashingType: UILabel!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var btnClose: UIBarButtonItem!
    // Constraints
    @IBOutlet var topVehicleConstraint: NSLayoutConstraint!
    @IBOutlet var leadingVehicleConstraint: NSLayoutConstraint!
    @IBOutlet var bottomVehicleConstraint: NSLayoutConstraint!
    @IBOutlet var trailingVehicleConstraint: NSLayoutConstraint!
    // MARK: - Properties
    fileprivate let stopWashingView = StopWashingView(frame: CGRect(x: 0, y: 0, width: Constants.Size.screenWidth, height: Constants.Size.screenHeight))
    fileprivate var footerPosY = Constants.Size.screenHeight
    fileprivate var maxDragY = CGFloat(0)
    fileprivate var minDragY = CGFloat(0)
    fileprivate var updatedTicket: Ticket?
    var ticket: Ticket?
    var typeOfService: TypeOfService?
    fileprivate var typeOfStopSelected: TypeOfStop = .normal
    // Timer
    fileprivate var timer = Timer()
    fileprivate var timerForGetTicket = Timer()
    fileprivate var originalSeconds = 0
    fileprivate var seconds = 0
    fileprivate var startTime: Date?
    fileprivate var stopTime: Date?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.setHidesBackButton(true, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appComesToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        updateUI()
        setUpProgress()
        setUpView()
        setUpStopView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        changeNavBarStyle(.myProfile, showBottomHairline: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopWashingView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.washing)
    }
    
    // MARK: - Private Methods
    private func updateUI() {
        guard let ticket = ticket, let service = typeOfService?.service else {
            return
        }
        let vehicleName = ticket.vehicleName ?? ""
        
        self.title = .localized(.washing) + " " + vehicleName
        setUpWashLabel(with: service.name)
        DeeplinkNavigator.shared.delegate = self
        DeeplinkNavigator.shared.isWashing = true
        
        if service.duration > 0 {
            originalSeconds = service.duration
            seconds = service.duration
            runTimer(with: originalSeconds)
        }
    }
    
    private func setUpProgress() {
        let imagePadding = (Device.size() == .screen4Inch) ? CGFloat(10) : CGFloat(11)
        
        if Device.size() == .screen4Inch || Device.size() == .screen4_7Inch {
            leadingVehicleConstraint.constant = imagePadding
            trailingVehicleConstraint.constant = imagePadding
            topVehicleConstraint.constant = imagePadding
            bottomVehicleConstraint.constant = imagePadding
        }
        // set up ring
        ring.gradientColors = [Color.blue.value, Color.green.value]
        ring.ringStyle = .gradient
        ring.value = 100.0
        ring.addDropShadow()
        avatarView.enableShadow = true
        avatarView.setImage(UIImage(named: "av_smart", in: SDKNavigation.getBundle(), compatibleWith: nil))
    }
    
    private func setUpWashLabel(with program: String) {
        /// small text
        let washed = Utils.capitalisedText(with: .localized(.washed)) + "\n"
        var attrs = [NSAttributedStringKey.font: Font(.custom(SDKFont.light), size: .standard(.h5_5)).instance]
        let smallString = NSMutableAttributedString(string: washed, attributes:attrs)
        /// normal text
        attrs = [NSAttributedStringKey.font: Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance]
        let normalString = NSMutableAttributedString(string: program, attributes:attrs)
        smallString.append(normalString)
        
        self.lblWash.attributedText = smallString
        lblWash.verticalAlignment = .bottom
    }
    
    private func setUpView() {
        view.backgroundColor = Color.whiteSmoke.value
        lblApplying.text = .localized(.applying)
        lblInfo.text = .localized(.waitForTheWashToStart)
        lblApplying.font = Font(.custom(SDKFont.italic), size: .standard(.h3_5)).instance
        lblInfo.font = Font(.custom(SDKFont.regular), size: .standard(.h2)).instance
        lblWashingType.font = Font(.custom(SDKFont.bold), size: .custom(39)).instance
    }
    
    private func setUpStopView() {
        footerView.layoutIfNeeded()
        footerPosY = Constants.Size.screenHeight - footerView.frame.size.height
        
        if let window = UIApplication.shared.delegate?.window{
            stopWashingView.frame.origin.y = footerPosY
            window!.addSubview(stopWashingView)
        }
        
//        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let window = appDelegate.window {
//            stopWashingView.frame.origin.y = footerPosY
//            window.addSubview(stopWashingView)
//        }
        
        let panGesture = PanDirectionGestureRecognizer(direction: .vertical, target: self, action: #selector(DuringWashingViewController.hanleGesture(_:)))
        stopWashingView.upHeightConstraint.constant = footerView.bounds.size.height
        stopWashingView.addGestureRecognizer(panGesture)
        
        maxDragY = footerView.frame.size.height + 180
        minDragY = footerPosY - 180
        
        stopWashingView.onButtonTap = { [unowned self] isMachineStopped in
            self.updateStopWashingPosY(with: self.footerPosY)
            if isMachineStopped {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(350)) {
                    self.stopWashingView.isHidden = true
                    self.stopMachine()
                }
            }
        }
        
        stopWashingView.onFullScreenTap = { [unowned self] in
            self.updateStopWashingPosY(with: 0)
        }
    }
    
    fileprivate func updateStopWashingPosY(with posY: CGFloat) {
        UIView.animate(withDuration: 0.3, animations: {
            self.stopWashingView.frame.origin.y = posY
            self.stopWashingView.isUp = (posY == 0) ? true : false
        })
    }
    
    private func stopMachine() {
        guard let codeId = typeOfService?.codeId else { return }
        KVNProgress.show(withStatus: .localized(.stoppingWashing))
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            guard result.error == nil, let ticket = result.value else {
                strongSelf.stopWashingView.isHidden = false
                KVNProgress.showError(withStatus: .localized(.errorGeneral))
                return
            }
            strongSelf.updatedTicket = ticket
            strongSelf.showMachineStopped(with: .normal)
        }
        APIManager.sharedInstance.fetchStopWash(with: codeId, completionHandler: completionHandler)
    }
    
    fileprivate func showMachineStopped(with type: TypeOfStop) {
        DeeplinkNavigator.shared.delegate = nil
        typeOfStopSelected = type
        stopWashingView.removeFromSuperview()
        
        let vc = MachineStoppedViewController.storyboardViewController()
        vc.delegate = self
        vc.typeOfStop = typeOfStopSelected
        vc.ticket = ticket
        self.present(vc, animated: true)
        KVNProgress.dismiss()
    }
    
    fileprivate func showWashingScore(with type: TypeOfStop) {
        DeeplinkNavigator.shared.delegate = nil
        typeOfStopSelected = type
        
        let vc = WashingScoreViewController.storyboardViewController()
        vc.delegate = self
        vc.ticket = ticket
        vc.typeOfStop = type
        vc.typeOfService = typeOfService
        self.present(vc, animated: true)
    }
}

// MARK: - Timer
extension DuringWashingViewController {
    fileprivate func runTimer(with seconds: Int) {
        startTime = Date()
        stopTime = Date.init(timeIntervalSinceNow: TimeInterval(seconds))
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(DuringWashingViewController.updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if seconds < 1 {
            countdownToActivateCloseButton()
        } else {
            seconds -= 1
            lblTimer.text = Utils.timeString(time: TimeInterval(seconds))
        }
    }
    
    @objc fileprivate func appMovedToBackground() {
        timer.invalidate()
    }
    
    @objc fileprivate func appComesToForeground() {
        guard let startDate = startTime, let stopDate = stopTime else { return }
        let now = Date()
        
        if now >= stopDate {
            lblTimer.text = "00:00"
            countdownToActivateCloseButton()
        } else {
            let elapsed = now.timeIntervalSince(startDate)
            let duration = Int(elapsed)
            
            if duration > 0 {
                seconds = originalSeconds - duration
                originalSeconds = seconds
                runTimer(with: seconds)
            }
        }
    }
    
    fileprivate func invalidateTimer() {
        startTime = nil
        stopTime = nil
        timer.invalidate()
        timerForGetTicket.invalidate()
    }
}

// MARK: - Get Ticket status
extension DuringWashingViewController {
    fileprivate func countdownToActivateCloseButton() {
        invalidateTimer()
        lblApplying.text = "Finalizando"
        lblWashingType.text = "Lavado..."
        restartTimer()
    }
    
    private func restartTimer() {
        timerForGetTicket = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(DuringWashingViewController.updateTimerForGetTicket), userInfo: nil, repeats: false)
    }
    
    @objc func updateTimerForGetTicket() {
        invalidateTimer()
        guard let ticket = ticket, let currentTypeOfService = typeOfService else { return }
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            guard let item = result.value else {
                strongSelf.restartTimer()
                return
            }
            
            if currentTypeOfService.checkIfConsumed(with: item) {
                strongSelf.updatedTicket = ticket
                strongSelf.showWashingScore(with: .foreground)
            } else {
                strongSelf.restartTimer()
            }
        }
        APIManager.sharedInstance.fetchTicket(with: String(ticket.id), completionHandler: completionHandler)
    }
}

// MARK: - IBActions
extension DuringWashingViewController {
    @IBAction func close(_ sender: Any) {
        DeeplinkNavigator.shared.isWashing = false
        self.dismiss(animated: true) {
            if let vc = self.navigationController?.viewControllers.first as? LegalNoticeViewController {
                vc.isFromDuringWashing = true
                vc.updatedTicket = self.updatedTicket
            }
            NotificationCenter.default.post(name: Constants.NCenter.reDownloadMyTickets, object: nil)
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    @IBAction func hanleGesture(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view)
        
        if sender.state == .changed {
            if stopWashingView.isUp {
                if touchPoint.y < maxDragY {
                    stopWashingView.frame.origin.y = touchPoint.y
                } else {
                    updateStopWashingPosY(with: footerPosY)
                    sender.isEnabled = false
                }
            } else {
                if touchPoint.y > minDragY {
                    stopWashingView.frame.origin.y = touchPoint.y
                } else {
                    updateStopWashingPosY(with: 0)
                    sender.isEnabled = false
                }
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            sender.isEnabled = true
            panGestureEnded(touchPoint)
        }
    }
    
    private func panGestureEnded(_ touchPoint: CGPoint) {
        if touchPoint.y > 0 && touchPoint.y < maxDragY { // check for correct position
            updateStopWashingPosY(with: 0)
        } else if touchPoint.y < footerPosY && !stopWashingView.isUp { // If the user leaves half (up)
            updateStopWashingPosY(with: footerPosY)
        } else if stopWashingView.frame.origin.y > footerPosY && !stopWashingView.isUp { // If the user leaves half (down)
            updateStopWashingPosY(with: footerPosY)
        }
    }
}

// MARK: - Delegate
extension DuringWashingViewController: WashingScoreViewControllerDelegate, MachineStoppedViewControllerDelegate, DeeplinkNavigatorDelegate {
    func washingScoreViewControllerWasClosed(_ controller: WashingScoreViewController, updatedTicket: Ticket?) {
        self.updatedTicket = updatedTicket
        close(Any.self)
    }
    
    func machineStoppedViewControllerWasClosed(_ controller: MachineStoppedViewController, updatedTicket: Ticket?) {
        self.updatedTicket = updatedTicket
        close(Any.self)
    }
    
    func machineStoppedViewControllerWasClosed(_ controller: MachineStoppedViewController, show updatedTicket: Ticket?) {
        let vc = MyTicketViewController.storyboardViewController()
        vc.ticket = updatedTicket
        vc.isFromPushNotification = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func stopWashing(with notification: PushNotification) {
        guard let ticket = ticket, "\(ticket.id)" == notification.id else { return }
        invalidateTimer()
        showMachineStopped(with: .foreground)
    }
    
    func finishedWashing(with notification: PushNotification) {
        guard let ticket = ticket, "\(ticket.id)" == notification.id else { return }
        invalidateTimer()
        showWashingScore(with: .foreground)
    }
}
