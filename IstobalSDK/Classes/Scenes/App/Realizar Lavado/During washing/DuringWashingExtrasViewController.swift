//
//  DuringWashingExtrasViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 21/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class DuringWashingExtrasViewController: UITableViewController {

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.whiteSmoke.value
        tableView.separatorColor = Color.separator.value
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.registerCell(ofType: DuringWashingCell.self)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as DuringWashingCell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
