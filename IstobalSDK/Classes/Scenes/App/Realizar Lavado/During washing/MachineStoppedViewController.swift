//
//  MachineStoppedViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 19/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Alamofire

final class MachineStoppedViewController: UIViewController, HiddenNavigationBar {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnClose: UIBarButtonItem!
    @IBOutlet var btnContinue: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnContinueHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    weak var delegate: MachineStoppedViewControllerDelegate?
    var typeOfStop: TypeOfStop = .normal
    var ticket: Ticket?
    var notification: PushNotification?
    fileprivate var updatedTicket: Ticket?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if typeOfStop == .foreground, let ticket = ticket {
            getUpdatedTicket(with: "\(ticket.id)")
        } else if typeOfStop == .background, let notification = notification {
            getUpdatedTicket(with: notification.id)
        }
    }
    
    private func setUpView() {
        transparentNavBar(navBar)
        btnClose.setStyle(.blue)
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        btnContinueHeightConstraint.constant = defaultButtonHeight()
        
        lblTitle.text = .localized(.machineStoppedTitle)
        lblSubtitle.text = .localized(.machineStoppedDetail)
        
        if typeOfStop == .background {
            setUpButtons(false)
            btnContinue.setTitle( Utils.capitalisedText(with: .localized(.goTicket)), for: .normal)
        } else {
            btnContinue.setTitle( Utils.capitalisedText(with: .localized(.continuar)), for: .normal)
            
            if typeOfStop == .foreground {
                setUpButtons(false)
            }
        }
    }
    
    private func setUpButtons(_ isEnabled: Bool) {
        btnContinue.isEnabled = isEnabled
        btnClose.isEnabled = isEnabled
        btnContinue.alpha = isEnabled ? 1.0 : 0.5
        
        if isEnabled {
            btnClose.setStyle(.blue)
        } else {
            btnClose.setStyle(.white)
        }
    }
    
    private func getUpdatedTicket(with id: String) {
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            guard let item = result.value else {
                strongSelf.setUpButtons(true)
                return
            }
            
            strongSelf.updatedTicket = item
            strongSelf.setUpButtons(true)
        }
        APIManager.sharedInstance.fetchTicket(with: id, completionHandler: completionHandler)
    }
}

// MARK: - IBActions
extension MachineStoppedViewController {
    @IBAction func close (_ sender: Any) {
        if typeOfStop == .background {
            self.dismiss(animated: true, completion: {
                self.delegate?.machineStoppedViewControllerWasClosed(self, show: self.updatedTicket)
            })
        } else {
            self.delegate?.machineStoppedViewControllerWasClosed(self, updatedTicket: updatedTicket)
        }
    }
}

// MARK: - Delegate
protocol MachineStoppedViewControllerDelegate: class {
    func machineStoppedViewControllerWasClosed(_ controller: MachineStoppedViewController, updatedTicket: Ticket?)
    func machineStoppedViewControllerWasClosed(_ controller: MachineStoppedViewController, show updatedTicket: Ticket?)
}

