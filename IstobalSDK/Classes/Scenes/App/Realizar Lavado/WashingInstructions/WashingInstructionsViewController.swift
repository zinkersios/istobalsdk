//
//  WashingInstructionsViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

final class WashingInstructionsViewController: UIViewController, HiddenNavigationBar {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var headerView: TopCornersView!
    @IBOutlet var footerView: BottomCornersView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnBack: UIBarButtonItem!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var footerHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var isOutVehicle = false
    var ticket: Ticket?
    var typeOfService: TypeOfService?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.washInstructions)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        if Device.size() == .screen5_8Inch {
            footerHeightConstraint = SRConstraint.changeMultiplier(constraint: footerHeightConstraint, multiplier: StyleConstants.Multiplier.footer)
        }
        
        transparentNavBar(navBar)
        headerView.backgroundColor = Color.solitude.value
        navBar.topItem?.title = .localized(.instructions)
        btnAccept.setTitle( Utils.capitalisedText(with: .localized(.accept)), for: .normal)
        btnBack.setStyle(.blue)
        
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        headerView.radius = StyleConstants.CornerRadius.ticket
        footerView.radius = StyleConstants.CornerRadius.ticket
        
        // add gradient
        let frame = CGRect(x: 0, y: 0, width: footerView.frame.width, height: footerView.frame.size.height)
        let gradient = Gradient.get(with: .white, size: frame)
        footerView.layer.insertSublayer(gradient, at: 0)
        
        if let tableView = self.childViewControllers[0] as? InstructionsTableView {
            tableView.isOutVehicle = isOutVehicle
            tableView.updateInstructions()
        }
    }
}

// MARK: - IBOutlets
extension WashingInstructionsViewController {
    @IBAction func dismissView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showNextView(_ sender: Any) {
        let vc = StartWashViewController.storyboardViewController()
        vc.isOutVehicle = isOutVehicle
        vc.ticket = ticket
        vc.typeOfService = typeOfService
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
