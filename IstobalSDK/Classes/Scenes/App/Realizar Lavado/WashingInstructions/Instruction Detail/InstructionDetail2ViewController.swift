//
//  InstructionDetail2ViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 12/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

class InstructionDetail2ViewController: DraggableViewController, HiddenNavigationBar {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnClose: UIBarButtonItem!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblBody: UILabel!
    @IBOutlet var imgAHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imgBHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imgATopConstraint: NSLayoutConstraint!
    @IBOutlet var lblBodyTopConstraint: NSLayoutConstraint!
    @IBOutlet var imgBTopConstraint: NSLayoutConstraint!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    private func setUpView() {
        transparentNavBar(navBar)
        btnClose.setStyle(.blue)
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        lblTitle.text = .localized(.instruction2)
        lblBody.text = .localized(.instructionDetail2Body)
        
        if Device.size() == .screen4Inch {
            lblTitle.setLineHeight()
            imgAHeightConstraint = SRConstraint.changeMultiplier(constraint: imgAHeightConstraint, multiplier: 0.12)
            imgBHeightConstraint = SRConstraint.changeMultiplier(constraint: imgBHeightConstraint, multiplier: 0.2)
            imgATopConstraint.constant = 15
            imgBTopConstraint.constant = 15
            lblBodyTopConstraint.constant = 15
        }
    }
}

// MARK: - IBActions
extension InstructionDetail2ViewController {
    @IBAction func close (_ sender: Any) {
        self.dismiss(animated: true)
    }
}
