//
//  InstructionDetail1ViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 12/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

class InstructionDetail1ViewController: DraggableViewController, HiddenNavigationBar {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnClose: UIBarButtonItem!
    @IBOutlet var stepA: RoundedButton!
    @IBOutlet var stepB: RoundedButton!
    @IBOutlet var stepC: RoundedButton!
    @IBOutlet var stepD: RoundedButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblStepA: UILabel!
    @IBOutlet var lblStepB: UILabel!
    @IBOutlet var lblStepC: UILabel!
    @IBOutlet var lblStepD: UILabel!
    @IBOutlet var imgTopConstraint: NSLayoutConstraint!
    @IBOutlet var steapATopConstraint: NSLayoutConstraint!
    @IBOutlet var stepBTopConstraint: NSLayoutConstraint!
    @IBOutlet var stepCTopConstraint: NSLayoutConstraint!
    @IBOutlet var stepDTopConstraint: NSLayoutConstraint!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    private func setUpView() {
        transparentNavBar(navBar)
        btnClose.setStyle(.blue)
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        
        lblTitle.text = .localized(.instructionDetail1Title)
        lblStepA.text = .localized(.stepABody)
        lblStepB.text = .localized(.stepBBody)
        lblStepC.text = .localized(.stepCBody)
        lblStepD.text = .localized(.stepDBody)
        
        setUpStepsButtons(with: stepA, title: .localized(.stepA))
        setUpStepsButtons(with: stepB, title: .localized(.stepB))
        setUpStepsButtons(with: stepC, title: .localized(.stepC))
        setUpStepsButtons(with: stepD, title: .localized(.stepD))
        
        if Device.size() == .screen4Inch {
            lblTitle.setLineHeight()
            imgTopConstraint.constant = 15
            steapATopConstraint.constant = 20
            stepBTopConstraint.constant = 18
            stepCTopConstraint.constant = 18
            stepDTopConstraint.constant = 18
        }
    }
    
    private func setUpStepsButtons(with button: RoundedButton, title: String) {
        button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        button.setTitle(title, for: .normal)
    }
}

// MARK: - IBActions
extension InstructionDetail1ViewController {
    @IBAction func close (_ sender: Any) {
        self.dismiss(animated: true)
    }
}
