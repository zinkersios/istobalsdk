//
//  InstructionDetail3OutsideViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 12/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class InstructionDetail3OutsideViewController: DraggableViewController, HiddenNavigationBar {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnClose: UIBarButtonItem!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblBody: UILabel!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    private func setUpView() {
        transparentNavBar(navBar)
        btnClose.setStyle(.blue)
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        lblTitle.text = .localized(.instructionDetail3OutsideTitle)
        lblBody.text = .localized(.instructionDetail3OutsideBody)
    }
}

// MARK: - IBActions
extension InstructionDetail3OutsideViewController {
    @IBAction func close (_ sender: Any) {
        self.dismiss(animated: true)
    }
}
