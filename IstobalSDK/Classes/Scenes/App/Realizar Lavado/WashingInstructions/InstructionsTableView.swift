//
//  InstructionsTableView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class InstructionsTableView: UITableViewController {

    // MARK: - Properties
    private var instructions = Instruction.getInstructionsInside()
    var isOutVehicle = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = Color.separator.value
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.registerCell(ofType: InstructionCell.self)
    }
    
    func updateInstructions() {
        if isOutVehicle {
            instructions = Instruction.getInstructionsOutside()
            tableView.reloadData()
        }
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instructions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as InstructionCell
        cell.setUp(with: instructions[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let instruction = instructions[indexPath.row]
        var vc: UIViewController?
        
        switch instruction.step {
        case "1":
            vc = InstructionDetail1ViewController.storyboardViewController()
        case "2":
            vc = InstructionDetail2ViewController.storyboardViewController()
        case "3":
            vc = isOutVehicle ? InstructionDetail3OutsideViewController.storyboardViewController() : InstructionDetail3ViewController.storyboardViewController()
        default:()
        }
        
        guard let controller = vc else {
            return
        }
        
        controller.modalPresentationStyle = .overFullScreen
        self.present(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
