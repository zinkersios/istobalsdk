//
//  ScanRegistrationNumberViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

final class ScanRegistrationNumberViewController: UIViewController, StyleNavBar {

    // MARK: - IBOutlets
    @IBOutlet var lblInfo: UILabel!
    @IBOutlet var lblRegistrationNumber: UILabel!
    @IBOutlet var lblNumberOfTickets: UILabel!
    @IBOutlet var btnBuyWash: UIButton!
    @IBOutlet var numTicketsTopConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = .localized(.registrationNumberScan)
        setUpView()                
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.myProfile, showBottomHairline: false)
    }
    
    private func setUpView() {
        lblInfo.text = .localized(.registrationNumberScanInfo)
        //lblInfo.setLineHeight(0.88)
        
        var fontSize = 53.0
        
        if Device.size() == .screen4Inch {
            numTicketsTopConstraint.constant = 20
            fontSize = 38.0
        } else if Device.size() == .screen4_7Inch {
            fontSize = 48.0
        }
        
        lblRegistrationNumber.font = Font(.installed(.helveticaNeueMedium), size: .custom(fontSize)).instance
        lblNumberOfTickets.text = .localized(.youDontHaveTickets)
        
        btnBuyWash.setTitle( Utils.capitalisedText(with: .localized(.buyNewWash)), for: .normal)
        btnBuyWash.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        
        setUpViewWithTicket()
    }

    /// configuración en caso de tener ticket
    private func setUpViewWithTicket() {
        lblNumberOfTickets.text = .localized(.youHaveTickets)
        btnBuyWash.backgroundColor = .clear
        btnBuyWash.layer.borderWidth = 1
        btnBuyWash.layer.borderColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1).cgColor
        btnBuyWash.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        
        if let tickets = self.childViewControllers[0] as? ScanTicketsViewController {
            tickets.delegate = self
        }
    }
}

// MARK: - IBActions
extension ScanRegistrationNumberViewController {
    @IBAction func buyWash(_ sender: Any) {
        
    }
}

// MARK: - ScanTicketsViewControllerDelegate
extension ScanRegistrationNumberViewController: ScanTicketsViewControllerDelegate {
    func scanTicketsViewController(_ controller: ScanTicketsViewController) {
        let vc = PreparingWashViewController.storyboardViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
