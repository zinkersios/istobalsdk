//
//  ScanTicketsViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class ScanTicketsViewController: UITableViewController {
    
    // MARK: - Properties
    weak var delegate: ScanTicketsViewControllerDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 190
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.registerCell(ofType: ScannerTicketCell.self)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ScannerTicketCell
        
        cell.onButtonTap = { [unowned self] in
            self.delegate?.scanTicketsViewController(self)
        }
        
        return cell
    }
}

// MARK: - ScanTicketsViewControllerDelegate
protocol ScanTicketsViewControllerDelegate: class {
    func scanTicketsViewController(_ controller: ScanTicketsViewController)
}
