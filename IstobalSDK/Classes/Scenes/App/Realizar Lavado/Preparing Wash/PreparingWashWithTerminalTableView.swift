//
//  PreparingWashWithTerminalTableView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class PreparingWashWithTerminalTableView: UITableViewController {

    // MARK: - Properties
    private let instructions = TerminalInstruction.getInstructions()
    weak var delegate: PreparingWashWithTerminalTableViewDelegate?
    
    var typeOfService: TypeOfService? {
        didSet { tableView.reloadData() }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = Color.separator.value
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.registerCell(ofType: TerminalCell.self)
        tableView.registerCell(ofType: TerminalWithViewCell.self)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instructions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let instrucction = instructions[indexPath.row]
        
        if instrucction.showQR {
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as TerminalWithViewCell
            cell.setUp(with: instrucction, typeOfService: typeOfService)
            
            cell.onButtonTap = { [unowned self] in
                self.delegate?.showQR(self)
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as TerminalCell
            cell.setUp(with: instrucction)
            return cell
        }
    }
}

protocol PreparingWashWithTerminalTableViewDelegate: class {
    func showQR(_ controller: PreparingWashWithTerminalTableView)
}
