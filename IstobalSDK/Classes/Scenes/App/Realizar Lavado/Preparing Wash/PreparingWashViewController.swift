//
//  PreparingWashViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

final class PreparingWashViewController: UIViewController, StyleNavBar {

    // MARK: - IBOutlets
    @IBOutlet var btnCancel: UIBarButtonItem!
    @IBOutlet var avatarView: AvatarView!
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblOutside: UILabel!
    @IBOutlet var lblInside: UILabel!
    @IBOutlet var viewOutside: UIView!
    @IBOutlet var viewInside: UIView!
    @IBOutlet var btnOutside: UIButton!
    @IBOutlet var btnInside: UIButton!
    @IBOutlet var stackViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var isOutVehicle = false
    var isFromTicket = false
    var ticket: Ticket?    
    weak var delegate: PreparingWashViewControllerDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = .localized(.preparingWash)
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.myProfile, showBottomHairline: false)
        self.navigationController?.navigationBar.isHidden = false                
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.prepareWash)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        if Device.size() == .screen5_8Inch {
            stackViewHeightConstraint = SRConstraint.changeMultiplier(constraint: stackViewHeightConstraint, multiplier: 0.22)
        }
        
        btnCancel.setStyle(.white)
        btnCancel.title = Utils.capitalisedText(with: .localized(.cancel))
        
        avatarView.enableShadow = true
        avatarView.setImage(UIImage(named: "av_smart", in: SDKNavigation.getBundle(), compatibleWith: nil))
        
        lblQuestion.font = Font(.custom(SDKFont.regular), size: .standard(.title)).instance
        lblQuestion.text = .localized(.preparingWashQuestion)
        
        /// set corner radius
        viewOutside.layer.cornerRadius = StyleConstants.CornerRadius.main
        viewInside.layer.cornerRadius = StyleConstants.CornerRadius.main
        btnOutside.layer.cornerRadius = StyleConstants.CornerRadius.main
        btnInside.layer.cornerRadius = StyleConstants.CornerRadius.main
        
        btnOutside.setBackgroundColor(color: Color.black3.value, forState: .highlighted)
        btnInside.setBackgroundColor(color: Color.black3.value, forState: .highlighted)
        
        lblOutside.text = .localized(.outsideVehicle)
        lblInside.text = .localized(.insideVehicle)
    }
    
    fileprivate func showLegaNotice() {
        let vc = LegalNoticeViewController.storyboardViewController()
        vc.isOutVehicle = isOutVehicle
        vc.ticket = ticket
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - IBActions
extension PreparingWashViewController {
    @IBAction func dismissView(_ sender: Any) {
        if isFromTicket {
            dismiss(animated: true, completion: nil)
            return
        }
        
        if let nav = navigationController {
            nav.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnOutside(_ sender: Any) {
        isOutVehicle = true
        showLegaNotice()
    }
    
    @IBAction func btnInside(_ sender: Any) {
        isOutVehicle = false
        showLegaNotice()
    }
}

// MARK: - Delegate
protocol PreparingWashViewControllerDelegate: class {
    func preparingWashViewControllerWasClosed(_ controller: PreparingWashViewController)
}
