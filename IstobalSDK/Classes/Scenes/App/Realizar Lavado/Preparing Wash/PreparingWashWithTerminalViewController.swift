//
//  PreparingWashWithTerminalViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device
import KVNProgress
import Alamofire

final class PreparingWashWithTerminalViewController: UIViewController, HiddenNavigationBar {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var footerView: BottomCornersView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnClose: UIBarButtonItem!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var footerHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    weak var delegate: PreparingWashWithTerminalViewControllerDelegate?
    var isOutVehicle = false
    var typeOfService: TypeOfService?
    var ticket: Ticket?
    var error: Error?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func setUpView() {
        if Device.size() == .screen5_8Inch {
            footerHeightConstraint = SRConstraint.changeMultiplier(constraint: footerHeightConstraint, multiplier: StyleConstants.Multiplier.footer)
        }
        
        transparentNavBar(navBar)
        btnAccept.setTitle( Utils.capitalisedText(with: .localized(.understood)), for: .normal)
        btnAccept.backgroundColor = Color.blue.value
        btnClose.setStyle(.blue)
        
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        footerView.radius = StyleConstants.CornerRadius.ticket
        
        lblTitle.textColor = Color.astral.value
        lblSubtitle.textColor = Color.red.value
        lblTitle.font = Font(.custom(SDKFont.regular), size: .standard(.header)).instance
        lblSubtitle.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        
        lblTitle.text = .localized(.terminalWashTitle)
        lblTitle.setLineHeight()
        
        if let error = error as? APIManagerError {
            lblSubtitle.text = error.localizedTitle + " " + .localized(.terminalWashSubTitle)
        } else {
            lblSubtitle.text = .localized(.terminalWashSubTitle)
        }
        
        // add gradient
        let frame = CGRect(x: 0, y: 0, width: footerView.frame.width, height: footerView.frame.size.height)
        let gradient = Gradient.get(with: .white, size: frame)
        footerView.layer.insertSublayer(gradient, at: 0)
        
        updateTableView()
    }
    
    fileprivate func updateTableView() {
        if let tableView = self.childViewControllers.first as? PreparingWashWithTerminalTableView {
            tableView.typeOfService = typeOfService
            tableView.delegate = self
        }
    }
}

// MARK: - IBOutlets
extension PreparingWashWithTerminalViewController {
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func showNextView(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    fileprivate func showQR() {
        let vc = QRDetailViewController.storyboardViewController()
        vc.typeOfService = typeOfService
        self.present(vc, animated: true)
    }
}

// MARK: - PreparingWashWithTerminalTableViewDelegate
extension PreparingWashWithTerminalViewController: PreparingWashWithTerminalTableViewDelegate {
    func showQR(_ controller: PreparingWashWithTerminalTableView) {
        if let typeOfService = typeOfService, typeOfService.code != nil {
            showQR()
        } else {
            KVNProgress.show(withStatus: .localized(.generatingQR))
            startTimer(with: typeOfService) // obtener el "code" del typeOfService seleccionado porque aún no se ha generado
        }
    }
    
    private func startTimer(with typeOfService: TypeOfService?) {
        Timer.scheduledTimer(timeInterval: 5.0, target: self,
                             selector: #selector(getUpdatedTicket),
                             userInfo: typeOfService,
                             repeats: false)
    }
    
    @objc private func getUpdatedTicket() {
        guard let ticket = ticket, let typeOfService = typeOfService else {
            return
        }
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self, let item = result.value else {
                KVNProgress.showError(withStatus: .localized(.errorGeneral))
                return
            }
            
            // get typeOfService selected and check if already have the code
            let filtered = item.ticketLines.filter({$0.codeId == typeOfService.codeId}).first
            if let updatedTypeOfService = filtered?.serviceType, updatedTypeOfService.code != nil {
                strongSelf.delegate?.preparingWashWithTerminalViewController(strongSelf, updatedTicket: item)
                strongSelf.typeOfService = updatedTypeOfService
                strongSelf.updateTableView()
                strongSelf.showQR()
                KVNProgress.dismiss()
            } else {
                strongSelf.startTimer(with: typeOfService) // aún no tenemos codigo, por lo que hay que volver a intentarlo
            }
        }
        APIManager.sharedInstance.fetchTicket(with: "\(ticket.id)", completionHandler: completionHandler)
    }
}

// MARK: - Delegate
protocol PreparingWashWithTerminalViewControllerDelegate: class {
    func preparingWashWithTerminalViewController(_ controller: PreparingWashWithTerminalViewController, updatedTicket: Ticket)
}
