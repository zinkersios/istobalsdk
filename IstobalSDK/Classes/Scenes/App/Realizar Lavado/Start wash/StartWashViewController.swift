//
//  StartWashViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device
import KVNProgress
import Alamofire

final class StartWashViewController: UIViewController, HiddenNavigationBar {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var viewStart: UIView!
    @IBOutlet var viewDraggable: RoundedView!
    @IBOutlet var arrowsStack: UIStackView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnBack: UIBarButtonItem!
    @IBOutlet var btnRepeatInstructions: UIButton!
    @IBOutlet var roundedView: RoundedView!
    @IBOutlet var imgVehicle: RoundedImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var lblStart: UILabel!
    @IBOutlet var lblStartInfo: UILabel!
    @IBOutlet var arrowsStackHeightConstraint: NSLayoutConstraint!
    @IBOutlet var arrowsStackLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var viewStartTopConstraint: NSLayoutConstraint!
    @IBOutlet var repeatBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet var startViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties 
    fileprivate var finalTouchPoint: CGPoint?
    fileprivate var viewDraggableFrame: CGRect = CGRect()
    fileprivate var viewDraggableFrameReset: CGRect = CGRect()
    fileprivate var maxPosX = CGFloat(0)
    var isOutVehicle = false
    var ticket: Ticket?
    var typeOfService: TypeOfService?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " "
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.startWash)
    }
    
    // MARK: - Private methods
    private func setUpView() {
        transparentNavBar(navBar)
        btnBack.setStyle(.blue)
        btnRepeatInstructions.setTitle( Utils.capitalisedText(with: .localized(.backToTheInstructions)), for: .normal)
        btnRepeatInstructions.backgroundColor = Color.separator.value
        btnRepeatInstructions.setTitleColor(UIColor.lightGray, for: .normal)
        
        repeatBtnHeightConstraint.constant = defaultButtonHeight()
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        roundedView.isShadowEnable = true
        
        if Device.size() == .screen5_8Inch {
            startViewHeightConstraint = SRConstraint.changeMultiplier(constraint: startViewHeightConstraint, multiplier: 0.136)
        }
        
        if isOutVehicle {
            startWashingFromOutSide()
        } else {
            startWashingFromInSide()
        }
    }
    
    private func startWashingFromOutSide() {
        lblTitle.text = .localized(.startWashingFromOutside)
        lblSubtitle.text = .localized(.startWashingFromOutsideInfo)
    }
}

// MARK: - API
extension StartWashViewController {
    private func showInfoTerminal(with error: Error?) {
        let vc = PreparingWashWithTerminalViewController.storyboardViewController()
        vc.error = error
        
        self.present(vc, animated: true) {
            self.updateViewDraggableFrame(with: self.viewDraggableFrameReset)
            self.setUpDraggableGesture()
        }
    }
    
    private func showDuringWashing() {
        let vc = DuringWashingViewController.storyboardViewController()
        vc.ticket = ticket
        vc.typeOfService = typeOfService
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func fetchStartWash() {        
        guard let codeId = typeOfService?.codeId else { return }
        
        KVNProgress.show(withStatus: .localized(.loadingWait))
        
        let completionHandler: (Result<Bool>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            if result.isSuccess, let status = result.value, status == true {
                strongSelf.showDuringWashing()
            } else {
                strongSelf.showInfoTerminal(with: result.error)
            }
            
            KVNProgress.dismiss()
        }
        APIManager.sharedInstance.fetchStartWash(with: codeId, completionHandler: completionHandler)
        
//        Utils.registerEvent(.startWash)
    }
}

// MARK: - Start washing from inside
extension StartWashViewController {
    fileprivate func startWashingFromInSide() {
        lblTitle.text = .localized(.startWashingFromInside)
        lblStart.text = Utils.capitalisedText(with: .localized(.start))
        lblStartInfo.text = .localized(.startWashingFromInsideInfo)
        viewStart.isHidden = false
        lblStartInfo.isHidden = false
        setGradient()
        
        // configure size
        if Device.size() == .screen4_7Inch {
            arrowsStack.spacing = 7
        } else if Device.size() == .screen4Inch {
            viewStartTopConstraint.constant = 30
            arrowsStackHeightConstraint.constant = 12
            arrowsStackLeadingConstraint.constant = 8
            arrowsStack.spacing = 1
        }
        
        // get frames
        viewDraggable.layoutIfNeeded()
        viewStart.layoutIfNeeded()
        viewDraggableFrame = viewDraggable.frame
        viewDraggableFrameReset = viewDraggable.frame
        maxPosX = (viewStart.bounds.size.width - viewDraggable.bounds.size.width) - viewDraggable.frame.origin.x //CGFloat(240)
        
        setUpDraggableGesture()
    }
    
    fileprivate func setUpDraggableGesture() {
        let panGesture = PanDirectionGestureRecognizer(direction: .horizontal, target: self, action: #selector(StartWashViewController.hanleGesture(_:)))
        viewDraggable.addGestureRecognizer(panGesture)
        finalTouchPoint = nil
    }
    
    private func setGradient() {
        let frame = CGRect(x: 0, y: 0, width: viewStart.bounds.size.width, height: viewStart.frame.size.height)
        let gradient = Gradient.get(with: .wash, size: frame)
        viewStart.layer.insertSublayer(gradient, at: 0)
        viewStart.layer.cornerRadius = cornerRadiusForStartWash()
        viewStart.addInnerShadow(onSide: .all, shadowColor: Constants.Color.shadow, shadowSize: 20, cornerRadius: cornerRadiusForStartWash(), shadowOpacity: 1.0)
    }
    
    fileprivate func updateViewDraggableFrame(with frame: CGRect) {
        UIView.animate(withDuration: 0.3, animations: {
            self.viewDraggable.frame = frame
            self.viewDraggableFrame = frame
        })
    }
}

// MARK: - IBActions
extension StartWashViewController {
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func repeatInstructions(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func hanleGesture(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view)
        let extra = CGFloat(24)
        
        if sender.state == .changed {
            if touchPoint.x >= 50 {
                viewDraggable.frame.origin.x = touchPoint.x - (viewDraggable.frame.size.width/2) - extra
                
                if viewDraggable.frame.origin.x >= maxPosX {
                    finalTouchPoint = touchPoint
                    sender.isEnabled = false
                }
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if let pointX = finalTouchPoint?.x {
                if pointX > maxPosX {
                    self.viewDraggableFrame.origin.x = maxPosX
                    updateViewDraggableFrame(with: viewDraggableFrame)
                }
                
                TapticEngine.impact.feedback()
                fetchStartWash()
            } else {
                updateViewDraggableFrame(with: viewDraggableFrame)
            }
        }
    }
}
