//
//  LegalNoticeViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

final class LegalNoticeViewController: UIViewController, HiddenNavigationBar {

    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var headerView: TopCornersView!
    @IBOutlet var footerView: BottomCornersView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnBack: UIBarButtonItem!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var footerHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var updatedTicket: Ticket?
    var isOutVehicle = false
    var ticket: Ticket?
    var isFromDuringWashing = false
    var typeOfService: TypeOfService?
    weak var delegate: LegalNoticeViewControllerDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        if isFromDuringWashing {
            self.delegate?.legalNoticeViewController(self, updatedTicket: updatedTicket)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
//        Utils.registerScreen(.acceptLegalTerms)
    }
    
    private func setUpView() {
        if Device.size() == .screen5_8Inch {
            footerHeightConstraint = SRConstraint.changeMultiplier(constraint: footerHeightConstraint, multiplier: StyleConstants.Multiplier.footer)
        }
        
        transparentNavBar(navBar)
        headerView.backgroundColor = Color.solitude.value
        navBar.topItem?.title = .localized(.legalNotice)
        btnAccept.setTitle( Utils.capitalisedText(with: .localized(.accept)), for: .normal)
        btnBack.tintColor = Color.astral.value
        
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        headerView.radius = StyleConstants.CornerRadius.ticket
        footerView.radius = StyleConstants.CornerRadius.ticket
        
        lblTitle.text = .localized(.legalNoticeTitle)
        textView.text = .localized(.legalNoticeBody)
        lblTitle.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        lblTitle.textColor = Color.astral.value
        textView.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        textView.textColor = Color.gray3.value
        
        // add gradient
        let frame = CGRect(x: 0, y: 0, width: footerView.frame.width, height: footerView.frame.size.height)
        let gradient = Gradient.get(with: .white, size: frame)
        footerView.layer.insertSublayer(gradient, at: 0)
    }
}

// MARK: - IBOutlets
extension LegalNoticeViewController {
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func showNextView(_ sender: Any) {
        let vc = WashingInstructionsViewController.storyboardViewController()
        vc.isOutVehicle = isOutVehicle
        vc.ticket = ticket
        vc.typeOfService = typeOfService
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

protocol LegalNoticeViewControllerDelegate: class {
    func legalNoticeViewController(_ controller: LegalNoticeViewController, updatedTicket: Ticket?)
}
