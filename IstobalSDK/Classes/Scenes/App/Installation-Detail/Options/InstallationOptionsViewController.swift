//
//  InstallationOptionsViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 5/3/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Lightbox

final class InstallationOptionsViewController: OptionsMenuViewController {
    // MARK: - Properties
    weak var delegate: InstallationOptionsViewControllerDelegate?
    var installation: Installation?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDataSource()
    }
    
    // MARK: - Private methods
    private func setUpDataSource() {
        let item1 = OptionMenu(name: .localized(.seeGallery), action: .installationGallery)
        let item2 = OptionMenu(name: .localized(.reportSuggestion), action: .installationReportProblem)
        let item3 = OptionMenu(name: .localized(.contactDetails), action: .installationContactDetails)
        let array = [item1, item2, item3]
        updateDataSource(with: array)
    }
    
    override func doAction(_ item: OptionMenu) {
        switch item.action {
        case .installationGallery:
            seeGallery()
        case .installationReportProblem:
            reportProblem()
        case .installationContactDetails:
            showInstallationData()
        default:()
        }
    }
}

// MARK: - Actions
extension InstallationOptionsViewController {
    private func seeGallery() {
//        Utils.registerEvent(.imageGallery)
        
        guard let installation = installation, installation.images.count > 0 else {
            presentStatusBarNotification(.localized(.installationWithoutPhotos))
            return
        }
        
        LightboxConfig.CloseButton.text = .localized(.close)
        let images = installation.images.enumerated().map { LightboxImage(imageURL: $0.1) }
        let controller = LightboxController(images: images)
        controller.dismissalDelegate = self
        present(controller, animated: true)
    }
    
    private func reportProblem() {
        self.delegate?.installationOptionsViewController(self, selectedOption: .reportProblem)
    }
    
    private func showInstallationData() {
        let vc = InstallationDataViewController.storyboardViewController()
        vc.installation = installation
        self.present(vc, animated: true)
    }
}

// MARK: - LightboxControllerDismissalDelegate
extension InstallationOptionsViewController: LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        dismiss(animated: true)
    }
}

// MARK: - Delegate
protocol InstallationOptionsViewControllerDelegate: class {
    func installationOptionsViewController(_ controller: InstallationOptionsViewController, selectedOption: InstallationOption)
}
