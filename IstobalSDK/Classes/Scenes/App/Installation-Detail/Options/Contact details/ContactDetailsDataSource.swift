//
//  ContactDetailsDataSource.swift
//  istobal
//
//  Created by Shanti Rodríguez on 7/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class ContactDetailsDataSource: NSObject {
    // MARK: - Private Properties
    private let contactData: [InstallationContactData]
    // MARK: - Properties
    var onDataSelected: ((InstallationContactData) -> Void)?
    
    init(data: [InstallationContactData]) {
        self.contactData = data
    }
    
    private func data(at indexPath: IndexPath) -> InstallationContactData {
        return self.contactData[indexPath.row]
    }
}

// MARK: - UITableViewDataSource

extension ContactDetailsDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data(at: indexPath)
        
        if item.type == .name {
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ContactDetailNameCell
            cell.setUp(with: item)
            return cell
        }
        
//        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ContactDetailCell
//        cell.setUp(with: item)
        return UITableViewCell()
    }
}

// MARK: - UITableViewDelegate

extension ContactDetailsDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onDataSelected?(data(at: indexPath))
    }
}
