//
//  InstallationDataViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 7/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class InstallationDataViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var installation: Installation?
    private var dataSource: (UITableViewDataSource & UITableViewDelegate)? {
        didSet {
            self.tableView.dataSource = dataSource
            self.tableView.delegate = dataSource
            self.tableView.reloadData()
        }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        setUpView()
    }
    
    // MARK: - Private methods
    private func setUpTableView() {
//        for cell in Cells.forContactDetails() {
//            tableView.registerNibForCellClass(cell)
//        }
        
        tableView.estimatedRowHeight = 64.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.view.backgroundColor = Color.daintree.value
    }
    
    private func setUpView() {
        guard let installation = installation else { return }
        
        let data = InstallationContactData.getData(installation: installation)
        let ds = ContactDetailsDataSource(data: data)
        ds.onDataSelected = { [unowned self] in
            self.show(data: $0)
        }
        
        self.dataSource = ds
    }
    
    private func show(data: InstallationContactData) {
        switch data.type {
        case .address:
            Maps.openMaps(with: installation)
        case .phone:
            Utils.callNumber(with: installation?.phone)
        case .email:
            Utils.openMail(with: installation?.email)
        default:()
        }
    }
}

// MARK: - Actions
extension InstallationDataViewController {
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
