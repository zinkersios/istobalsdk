//
//  ServicesViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 12/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class ServicesViewController: UIViewController, StyleNavBar, ErrorPresenting {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Properties
    private let router = DefaultAppRouter()
    private var services = [TypeOfService]()
    private var installationID = 0
    var installation: Installation?
    var offer: Offer?
    var serviceViewDelegate: ServicesViewDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.byDefault, showBottomHairline: true, backButton: .red)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        self.title = .localized(.services)
        tableView.registerCell(ofType: InstallationServiceCell.self)
        installationID = installation?.id ?? 0
        debugPrint("instalation id: \(self.installationID)")
        
        if let typeOfServices = installation?.typeOfServices {
            services = typeOfServices
            tableView.reloadData()
        }else{
            self.getServices()
        }
    }
    
    private func getServices(){
        APIManager.sharedInstance.fetchInstallation(with: "\(self.installation?.id ?? 0)") { (result) in
            if result.error != nil{
                debugPrint("there's a problem with the installation")
            }else{
                if let services = result.value{
                    self.installation = Installation(id: self.installation?.id ?? 0, typeOfServices: services)
                    self.setUpView()
                }
            }
        }

    }
    
    private func setUpAction(with action: InstallationAction) {
        switch action {
        case .buyWash(let services):
            showBuyWash(with: services)
        case .alert(let title, let message):
            presentAlertView(with: title, message: message)
        default:()
        }
    }
    
    private func showBuyWash(with services: [Service]) {
        let vc = BuyWashViewController.storyboardViewController()
        vc.delegate = self
        vc.installation = installation
        vc.services = services
        vc.statusBarStyle = .default
        
        let navController = UINavigationController(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        self.present(navController, animated:true)
    }
}

// MARK: - UITableView DataSource
extension ServicesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as InstallationServiceCell
        cell.setUp(with: services[indexPath.row], installationID: installationID)
        
        cell.onButtonTap = { [unowned self] action in
            self.setUpAction(with: action)
        }
        return cell
    }
}

extension ServicesViewController: BuyWashViewControllerDelegate {
    func buyWashViewControllerShowPurchaseSummary(_ controller: BuyWashViewController, wash: Wash, services: [Service]) {
        self.dismiss(animated: true)
        
        switch wash.type {
        case .withPrograms:
            let params: CustomizeWashRoute = (installation: installation, services: services, offer: offer, delegate: self.serviceViewDelegate!)
            router.perform(.customizeWash, from: self, parameters: params)
        default:()
        }
    }
}
