//
//  RateAppViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 13/2/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Device
import StoreKit

class RateAppViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnRememberLater: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.rateApp)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        buttonHeightConstraint.constant = defaultButtonHeight()
        
        if Device.size() == .screen4_7Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.61)
        } else if Device.size() == .screen4Inch {
            containerHeightConstraint = SRConstraint.changeMultiplier(constraint: containerHeightConstraint, multiplier: 0.65)
        } else if Device.size() == .screen5_5Inch {
            lblSubtitle.numberOfLines = 2
        }
        
        view.backgroundColor = Color.backgroundBlack.value
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.main
        
        // Text
        lblTitle.text = .localized(.rateTitle)
        lblSubtitle.text = .localized(.rateMessage)
        btnAccept.setTitle( Utils.capitalisedText(with: .localized(.rateNow)), for: .normal)
        btnRememberLater.setTitle( Utils.capitalisedText(with: .localized(.rateLater)), for: .normal)
        btnNo.setTitle( Utils.capitalisedText(with: .localized(.rateNot)), for: .normal)
        
        /// Color
        lblTitle.textColor = Color.astral.value
        lblTitle.font = Font(.custom(SDKFont.bold), size: .standard(.title)).instance
        lblSubtitle.textColor = Color.atomic.value
        lblSubtitle.font = Font(.custom(SDKFont.regular), size: .standard(.h2)).instance
    }
}

// MARK: - IBActions
extension RateAppViewController {
    @IBAction func accept(_ sender: Any) {
        self.btnAccept.isEnabled = false
        self.btnRememberLater.isEnabled = false
        UserDefaults.App.set(true, forKey: .notAskForRating)
        
        // Open App Store
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self
        
        let parameters = [ SKStoreProductParameterITunesItemIdentifier: "1209416535"]
        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, _) -> Void in
            if loaded {
                self?.present(storeViewController, animated: true, completion: nil)
            }
        }
        
//        Utils.registerEvent(.rateApp)
    }
    
    @IBAction func rememberLater(_ sender: Any) {
        UserDefaults.Account.set(0, forKey: .totalLogins)
        self.dismiss(animated: true)
    }
    
    @IBAction func not(_ sender: Any) {
        UserDefaults.App.set(true, forKey: .notAskForRating)
        self.dismiss(animated: true)
    }
}

extension RateAppViewController: SKStoreProductViewControllerDelegate {
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true) {
            self.dismiss(animated: false)
        }
    }
}
