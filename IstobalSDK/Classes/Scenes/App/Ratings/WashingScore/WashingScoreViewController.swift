//
//  WashingScoreViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 29/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import KVNProgress
import IQKeyboardManagerSwift
import Device
import Alamofire

final class WashingScoreViewController: UIViewController, HiddenNavigationBar, ErrorPresenting {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnClose: UIBarButtonItem!
    @IBOutlet var btnSend: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var lblTicket: UILabel!
    @IBOutlet weak var lblInstallationName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet var logoView: AvatarView!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet var textView: PlaceholderTextView!
    
    // MARK: - Properties
    var ticket: Ticket?
    var typeOfService: TypeOfService?
    var typeOfStop: TypeOfStop = .normal
    var notification: PushNotification?
    weak var delegate: WashingScoreViewControllerDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpTicket()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.scoreWash)
        
        if typeOfStop == .background, let notification = notification {
            KVNProgress.show(withStatus: .localized(.loadingWait))
            getUpdatedTicket(with: notification.id)
        }
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        transparentNavBar(navBar)
        btnClose.setStyle(.blue)
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        
        lblTitle.text = .localized(.rateLastWash)
        btnSend.setTitle( Utils.capitalisedText(with: .localized(.send)), for: .normal)
        
        logoView.enableShadow = true
        logoView.setImage(Constants.Image.placeholderInstallation)
        
        ratingView.delegate = self
        IQKeyboardManager.shared.enable = true
    }
    
    private func setUpTicket() {
        guard let ticket = ticket, let service = typeOfService?.service else {
            return
        }
        
        let vm = TicketViewModel(model: ticket)
        
        lblTicket.text = service.name
        lblInstallationName.text = ticket.installation?.companyName
        lblDate.text = vm.purchasedOn
        
        // set image
        if let url = ticket.installation?.image {
            logoView.imageView.kf.setImage(with: url, placeholder: Constants.Image.placeholderInstallation)
        }
    }
    
    fileprivate func sendValoration(with codeId: String, comment: String, installationID: String) {
        let rating = Int(ratingView.rating)
        let valoration = Valoration(comment: comment, image: "", name: "", rating: rating, surname: "", codeId: codeId, installationID: installationID)
        
        KVNProgress.show(withStatus: .localized(.postingReview))
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            guard result.error == nil, let item = result.value else {
                strongSelf.dismissKVN(with: nil)
                return
            }
            
            KVNProgress.showSuccess(withStatus: .localized(.postingReviewOK))
            strongSelf.delegate?.washingScoreViewControllerWasClosed(strongSelf, updatedTicket: item)
        }
        APIManager.sharedInstance.fetchRate(with: valoration, completionHandler: completionHandler)
        
//        Utils.registerEvent(.rateWash)
    }
    
    /// Si el usuario no valora y cierra la pantalla, antes de cerrar llamamos al WS de /tiket/id para obtener el ticket actualizado.
    fileprivate func getUpdatedTicket(with id: String) {
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            guard let item = result.value else {
                strongSelf.dismissKVN(with: nil)
                return
            }
            
            if strongSelf.typeOfStop == .background {
                strongSelf.ticket = item
                strongSelf.setUpTicketAndService()
            } else {
                strongSelf.dismissKVN(with: item)
            }
        }
        APIManager.sharedInstance.fetchTicket(with: id, completionHandler: completionHandler)
    }
    
    private func setUpTicketAndService() {
        guard let ticket = ticket,
            let notification = notification,
            let idSecondary = notification.idSecondary
            else {
                dismissKVN(with: nil)
                return
        }
        
        let filtered = ticket.ticketLines.filter({$0.codeId == idSecondary}).first
        self.typeOfService = filtered?.serviceType
        setUpTicket()
        KVNProgress.dismiss()
    }
    
    private func dismissKVN(with updatedTicket: Ticket?) {
        if typeOfStop == .background {
            self.dismiss(animated: true)
        } else {
            self.delegate?.washingScoreViewControllerWasClosed(self, updatedTicket: updatedTicket)
        }
        
        KVNProgress.dismiss()
    }
}

// MARK: - IBActions
extension WashingScoreViewController {
    @IBAction func dismissView(_ sender: Any) {
        if typeOfStop == .foreground, let ticket = ticket {
            KVNProgress.show(withStatus: .localized(.loadingWait))
            getUpdatedTicket(with: "\(ticket.id)")
        } else if typeOfStop == .background {
            self.dismiss(animated: true)
        } else {
            self.delegate?.washingScoreViewControllerWasClosed(self, updatedTicket: nil)
        }
    }
    
    @IBAction func send(_ sender: Any) {
        guard let codeId = typeOfService?.codeId,
            let installationID = ticket?.installation?.id
            else {
                presentStatusBarNotification(.localized(.addComment))
                return
        }
        
        if let comment = textView.text, comment.count > 0 {
            sendValoration(with: codeId, comment: comment, installationID: "\(installationID)")
        } else {
            sendValoration(with: codeId, comment: "", installationID: "\(installationID)")
        }
    }
}

// MARK: FloatRatingViewDelegate
extension WashingScoreViewController: FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Float) {
        debugPrint(rating)
//        log.info(rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        switch rating {
        case 1.0:
            lblRating.text = .localized(.score1)
        case 2.0:
            lblRating.text = .localized(.score2)
        case 3.0:
            lblRating.text = .localized(.score3)
        case 4.0:
            lblRating.text = .localized(.score4)
        case 5.0:
            lblRating.text = .localized(.score5)
        default: lblRating.text = ""
        }
    }
}

// MARK: - Delegate
protocol WashingScoreViewControllerDelegate: class {
    func washingScoreViewControllerWasClosed(_ controller: WashingScoreViewController, updatedTicket: Ticket?)
}

