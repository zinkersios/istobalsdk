//
//  TicketServiceInfoViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 24/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

final class TicketServiceInfoViewController: DraggableViewController, HiddenNavigationBar {

    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var btnClose: UIBarButtonItem!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var imgTopConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    fileprivate var instructions = Instruction.getTicketInstructions()
    var ticket: Ticket?
    var typeOfService: TypeOfService?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.serviceInstructions)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        transparentNavBar(navBar)
        btnClose.setStyle(.gray)
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        
        if Device.size() ==  .screen4Inch {
            imgTopConstraint.constant = 34
            headerView.frame.size.height = 190
        } else if Device.size() ==  .screen4_7Inch {
            headerView.frame.size.height = 200
        }
        
        if let ticket = ticket {
            let viewModel = TicketViewModel(model: ticket)
            var name = viewModel.title + "\n"
            var subtitle = " "
            
            if let type = typeOfService {
                name =  type.name + "\n"
                subtitle = type.code ?? " "
            }
            
            // set time
            if let service = typeOfService?.service {
                lblTime.text = .localized(.washingTime) + " " + Utils.secondsToHumanReadable(time: TimeInterval(service.duration))
                lblPrice.text = PriceUtils.priceToCurrency(with: service.totalPriceWithAddons)
            }
            
            setUpTitle(with: name, subtitle: subtitle)
            
            if ticket.vehicleID == nil {
                instructions = Instruction.getNoVehicleInstructions()
                tableView.reloadData()
            }
        }
        
        lblTime.textColor = Color.astral.value
        lblPrice.textColor = Color.astral.value
        lblTime.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        lblPrice.font = Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance
    }
    
    private func setUpTableView() {
        tableView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        tableView.separatorColor = Color.separator.value
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 60
        
        tableView.registerCell(ofType: HeaderCell.self)
        tableView.registerCell(ofType: TicketServiceInfoCell.self)
    }
    
    private func setUpTitle(with title: String, subtitle: String) {
        /// small text (ticket)
        var attrs = [NSAttributedStringKey.font: Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance,
                     NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        let attributedString = NSMutableAttributedString(string: subtitle, attributes:attrs)
        
        /// normal text (title)
        attrs = [NSAttributedStringKey.font: Font(.custom(SDKFont.bold), size: .standard(.header)).instance,
                 NSAttributedStringKey.foregroundColor: Color.blue.value]
        let normalString = NSMutableAttributedString(string: title, attributes:attrs)
        normalString.append(attributedString)
        
        lblTitle.attributedText = normalString
    }
}

// MARK: - Table view data source
extension TicketServiceInfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instructions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as TicketServiceInfoCell
        cell.setUp(with: instructions[indexPath.row])
        return cell
    }
}

// MARK: - Table view data source
extension TicketServiceInfoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifiers.header) as? HeaderCell else {
            fatalError("Check for missing HeaderCell")
        }
        
        cell.setUpForServiceInfo(with: ticket)
        return cell.contentView
    }
}

// MARK: - IBActions
extension TicketServiceInfoViewController {
    @IBAction func dismissView (_ sender: Any) {
        self.dismiss(animated: true)
    }
}
