//
//  MyTicketsViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 29/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Alamofire

final class MyTicketsViewController: SJSegmentedViewController, StyleNavBar {
    
    // MARK: - Properties
    var selectedSegment: SJSegmentTab?
    fileprivate var allMyTicketsController = AllMyTicketsViewController.storyboardViewController()
    fileprivate var activeTicketsController = ActiveTicketsViewController.storyboardViewController()
    fileprivate var ticketsConsumedController = TicketsConsumedViewController.storyboardViewController()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        setUpSegmentedViewController()
        super.viewDidLoad()
        DeeplinkNavigator.shared.navigationController = self.navigationController
        navigationController?.navigationBar.hideBottomHairline()        
        navigationItem.titleView = titleView(with: .localized(.myTickets), style: .byDefault)
        getMyTickets()
        
        // re-download my tickets
        NotificationCenter.default.addObserver(forName: Constants.NCenter.reDownloadMyTickets, object: nil, queue: OperationQueue.main) { [weak self] _ in
            self?.reDownloadMyTickets()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.byDefault)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let ticket = Ticket.getLastTicketPurchased() {
            UserDefaults.User.set( Data(), forKey: .lastTicketPurchased)
            UserDefaults.User.set(Data(), forKey: .lastOfferRedeemed)
            NCenterUtils.updateSideMenu(with: .myTickets)
            showTicketDetail(ticket)
        }
    }
    
    /// configure SJSegmentedViewController
    private func setUpSegmentedViewController() {
        allMyTicketsController.title = .localized(.tabAll)
        activeTicketsController.title = .localized(.tabActive)
        ticketsConsumedController.title = .localized(.tabConsumed)
        
        segmentedScrollViewColor = SDKColor.secondaryLabel
        segmentedType = .myTickets
        headerViewController = nil
        segmentControllers = [activeTicketsController, ticketsConsumedController, allMyTicketsController]
        delegate = self
        
        // delegate for ticket selected
        allMyTicketsController.delegate = self
        activeTicketsController.delegate = self
        ticketsConsumedController.delegate = self
    }
    
    fileprivate func showTicketDetail(_ ticket: Ticket?) {
        let vc = MyTicketViewController.storyboardViewController()
        vc.ticket = ticket
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - API
extension MyTicketsViewController {
    fileprivate func getMyTickets() {
        let completionHandler: (Result<TicketDataSource>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }            
            
            strongSelf.allMyTicketsController.setUpTickets(with: result)
            strongSelf.activeTicketsController.setUpTickets(with: result)                        
            strongSelf.ticketsConsumedController.setUpTickets(with: result)
        }
        APIManager.sharedInstance.fetchMyTickets(completionHandler)
    }
    
    fileprivate func reDownloadMyTickets() {
        allMyTicketsController.updateTableView(with: .loading)
        activeTicketsController.updateTableView(with: .loading)
        ticketsConsumedController.updateTableView(with: .loading)
        getMyTickets()
    }
}

// MARK: - Delegates
// MARK: SJSegmentedViewController Delegate
extension MyTicketsViewController: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        if selectedSegment != nil {
            selectedSegment?.updateStyle(for: .gray)
        }
        
        if segments.count > 0 {
            selectedSegment = segments[index]
            selectedSegment?.updateStyle(for: .red)
        }
    }
}

// MARK: DraggableViewControllerDelegate
extension MyTicketsViewController: DraggableViewControllerDelegate {    
    func draggableViewControllerDismissed(_ controller: DraggableViewController) {
        UIApplication.shared.statusBarStyle = .default
        self.dismiss(animated: true)
    }
}

// MARK: AllMyTicketsViewController Delegate
extension MyTicketsViewController: AllMyTicketsViewControllerDelegate {
    func allMyTicketsViewController(_ controller: AllMyTicketsViewController, selectedTicket: Ticket?) {
        self.showTicketDetail(selectedTicket)
    }
}

// MARK: ActiveTicketsViewController Delegate
extension MyTicketsViewController: ActiveTicketsViewControllerDelegate {
    func activeTicketsViewController(_ controller: ActiveTicketsViewController, selectedTicket: Ticket?) {
        self.showTicketDetail(selectedTicket)
    }
}

// MARK: TicketsConsumedViewController Delegate
extension MyTicketsViewController: TicketsConsumedViewControllerDelegate {
    func ticketsConsumedViewController(_ controller: TicketsConsumedViewController, selectedTicket: Ticket?) {
        self.showTicketDetail(selectedTicket)
    }
}
