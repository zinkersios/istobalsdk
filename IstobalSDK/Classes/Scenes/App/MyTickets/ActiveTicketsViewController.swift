//
//  ActiveTicketsViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 9/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Alamofire

final class ActiveTicketsViewController: UITableViewController {

    // MARK: - Properties
    weak var delegate: ActiveTicketsViewControllerDelegate?
    var ticketsDataSource: TicketsDataSource? {
        didSet { tableView.reloadData() }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.gray7.value
        setUpView()
        
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    func viewForObserve() -> UIView {        
        return self.tableView
    }

    private func setUpView() {
        for cell in Cells.forMyTickets() {
            tableView.registerNibForCellClass(cell)
        }
        updateTableView(with: .loading)
    }
    
    func updateTableView(with state: APIState) {
        ticketsDataSource = TicketsDataSource(tickets: [])
        ticketsDataSource?.state = state
        ticketsDataSource?.type = .activeTickets
        ticketsDataSource?.delegate = self
        tableView.dataSource = ticketsDataSource
        tableView.separatorColor = .clear
    }
}

// MARK: - API
extension ActiveTicketsViewController {
    private func handleLoadTicketsError(_ error: Error?) {
        if !ReachabilityManager.shared.isNetworkAvailable {
            updateTableView(with: .noInternet)
        } else {
            updateTableView(with: .error)
        }
    }
    
    func setUpTickets(with result: Result<TicketDataSource>) {
        guard result.error == nil else {
            handleLoadTicketsError(result.error)
            return
        }
        guard let data = result.value else {
            updateTableView(with: .noResults)
            return
        }
        
        let fetchedTickets = data.active
        
        fetchedTickets.delegate = self
        
        if fetchedTickets.tickets.count > 0 {
            fetchedTickets.state = .results
            fetchedTickets.type = .activeTickets
            ticketsDataSource = fetchedTickets
            tableView.dataSource = ticketsDataSource
            tableView.separatorColor = Color.gray6.value
        } else {
            updateTableView(with: .noResults)
        }
    }
}

// MARK: - UITableViewDelegate
extension ActiveTicketsViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tickets = ticketsDataSource?.tickets, tickets.count > 0 {
            self.delegate?.activeTicketsViewController(self, selectedTicket: self.ticketsDataSource?.tickets[indexPath.row])
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - TicketsDataSource Delegate
extension ActiveTicketsViewController: TicketsDataSourceDelegate {
    func ticketsDataSourceTryAgain(_ dataSource: TicketsDataSource) {
        updateTableView(with: .loading)
    }
}

// MARK: - ActiveTicketsViewController Delegate
protocol ActiveTicketsViewControllerDelegate: class {
    func activeTicketsViewController(_ controller: ActiveTicketsViewController, selectedTicket: Ticket?)
}
