//
//  QRDetailViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 30/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class QRDetailViewController: UIViewController, HiddenNavigationBar {

    // MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var btnClose: UIBarButtonItem!
    @IBOutlet weak var lblCode: UILabel!
    // MARK: - Properties
    var image: UIImage?
    var typeOfService: TypeOfService?
    var qrCode: String?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        if let qrImage = image {
            updateView(with: qrImage, qrCode: qrCode)
        } else {
            generateQR()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerEvent(.showQR)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        UIApplication.shared.statusBarStyle = .default
        transparentNavBar(navBar)
        btnClose.setStyle(.blue)
        navBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Color.astral.value,
                                      NSAttributedStringKey.font: Font(.custom(SDKFont.bold), size: .standard(.h3)).instance]
        navBar.topItem?.title = Utils.capitalisedText(with: .localized(.code))
        
        lblCode.font = Font(.custom(SDKFont.bold), size: .custom(40)).instance
    }
    
    private func updateView(with image: UIImage, qrCode: String?) {
        imageView.image = image
        
        if let code = qrCode {
            lblCode.text = code
        }
    }
    
    private func generateQR() {
        guard let typeOfService = typeOfService, let code = typeOfService.code else {
            return
        }
        
        if let image = Utils.generateQR(with: code) {
            updateView(with: image, qrCode: code)
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
