//
//  TicketsDataSource.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 10/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class TicketsDataSource: NSObject, UITableViewDataSource {

    // MARK: - Properties
    var tickets: [Ticket]
    var state: APIState = .loading
    var type: CellType = .allTickets
    weak var delegate: TicketsDataSourceDelegate?
    
    // MARK: - View Life Cycle
    init(tickets: [Ticket]) {
        self.tickets = tickets
        super.init()
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch state {
        case .loading:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as LoadingCell
            cell.setUp(for: type)
            return cell
        case .results:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as TicketCell
            cell.setUp(with: tickets[indexPath.row])
            return cell
        case .noResults, .noInternet, .error:
            return configureEmptyStateCell(tableView, indexPath:indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch state {
        case .results:
            return tickets.count
        default:
            return 1
        }
    }
    
    // MARK: - Configure EmptySate Cell
    private func configureEmptyStateCell(_ tableView: UITableView, indexPath: IndexPath) -> EmptyStateCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as EmptyStateCell
        cell.setUpView(with: state, type: type)
        
        if state == .noInternet {
            cell.onButtonTap = { [unowned self] in
                self.delegate?.ticketsDataSourceTryAgain(self)
            }
        }
        
        return cell
    }
}

// MARK: - Delegate
protocol TicketsDataSourceDelegate: class {
    func ticketsDataSourceTryAgain(_ dataSource: TicketsDataSource)
}
