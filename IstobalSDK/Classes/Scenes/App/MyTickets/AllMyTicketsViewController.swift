//
//  AllMyTicketsViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 9/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Alamofire

final class AllMyTicketsViewController: UITableViewController, SignOff {

    // MARK: - Properties
    weak var delegate: AllMyTicketsViewControllerDelegate?
    var ticketsDataSource: TicketsDataSource? {
        didSet { tableView.reloadData() }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.gray7.value
        setUpView()
        
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    func viewForObserve() -> UIView {
        return self.tableView
    }
    
    private func setUpView() {
        for cell in Cells.forMyTickets() {
            tableView.registerNibForCellClass(cell)
        }
        updateTableView(with: .loading)
    }
    
    func updateTableView(with state: APIState) {
        ticketsDataSource = TicketsDataSource(tickets: [])
        ticketsDataSource?.state = state
        ticketsDataSource?.delegate = self
        tableView.dataSource = ticketsDataSource
        tableView.separatorColor = .clear
    }        
}

// MARK: - API
extension AllMyTicketsViewController {
    private func handleLoadTicketsError(_ error: Error?) {
        if let error = error as? APIManagerError, error.code == Constants.HTTPCode.unauthorised {
            forceLogOut()
        } else if !ReachabilityManager.shared.isNetworkAvailable {
            updateTableView(with: .noInternet)
        } else {
            updateTableView(with: .error)
        }
    }
    
    func setUpTickets(with result: Result<TicketDataSource>) {
        guard result.error == nil else {
            handleLoadTicketsError(result.error)
            return
        }
        
        guard let data = result.value else {
            updateTableView(with: .noResults)
            return
        }
        
        let fetchedTickets = data.all
        fetchedTickets.delegate = self
        
        if fetchedTickets.tickets.count > 0 {
            fetchedTickets.state = .results
            ticketsDataSource = fetchedTickets
            tableView.dataSource = ticketsDataSource
            tableView.separatorColor = Color.gray6.value
        } else {
            updateTableView(with: .noResults)
        }
    }
}

// MARK: - UITableViewDelegate
extension AllMyTicketsViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tickets = ticketsDataSource?.tickets, tickets.count > 0 {
            self.delegate?.allMyTicketsViewController(self, selectedTicket: self.ticketsDataSource?.tickets[indexPath.row])
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - TicketsDataSource Delegate
extension AllMyTicketsViewController: TicketsDataSourceDelegate {
    func ticketsDataSourceTryAgain(_ dataSource: TicketsDataSource) {
        updateTableView(with: .loading)
    }
}

// MARK: - AllMyTicketsViewController Delegate
protocol AllMyTicketsViewControllerDelegate: class {
    func allMyTicketsViewController(_ controller: AllMyTicketsViewController, selectedTicket: Ticket?)
}
