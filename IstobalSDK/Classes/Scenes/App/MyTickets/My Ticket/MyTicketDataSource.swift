//
//  MyTicketDataSource.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class MyTicketDataSource: NSObject {
    // MARK: - Private Properties
    private let headerID = String(describing: MyTicketHeaderCell.self)
    private let footerID = String(describing: MyTicketFooterCell.self)
    private let ticket: Ticket
    private let viewModel: TicketViewModel
    public var onAction: ((TicketAction) -> Void)?
    
    // MARK: - Public
    public init(ticket: Ticket) {
        self.ticket = ticket
        self.viewModel = TicketViewModel(model: ticket)
    }
    
    private func service(at indexPath: IndexPath) -> (typeOfService: TypeOfService, ticketLine: TicketLine) {
        let typeOfService = ticket.ticketLines[indexPath.row].serviceType
        let ticketLine = ticket.ticketLines[indexPath.row]
        return (typeOfService: typeOfService, ticketLine: ticketLine)
    }
}

// MARK: - UITableViewDataSource
extension MyTicketDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ticket.ticketLines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = service(at: indexPath)
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MyTicketServiceCell
        cell.setUp(for: data.typeOfService, ticketLine: data.ticketLine, ticket: ticket)
        cell.onAction = { [unowned self] in
            self.onAction?($0)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MyTicketDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: headerID) as? MyTicketHeaderCell
        cell?.setUp(with: viewModel)
        cell?.btnVehicleName.addTarget(self, action: #selector(selectVehicle), for: .touchUpInside)
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: footerID) as? MyTicketFooterCell
        return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let typeOfService = service(at: indexPath).typeOfService
        
        switch typeOfService.status {
        case .canceled, .consumed, .expired:
            onAction?(.showMessage(title: typeOfService.name, message: typeOfService.status.message))
        default:
            debugPrint("ticket status \(typeOfService.status.rawValue)")
//            log.debug("ticket status \(typeOfService.status.rawValue)")
        }
    }
}

// MARK: - Private Methods
extension MyTicketDataSource {
    @IBAction func selectVehicle(_ sender: Any) {
        onAction?(.selectVehicle)
    }
}
