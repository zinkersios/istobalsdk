//
//  TicketQR.swift
//  istobal
//
//  Created by Shanti Rodríguez on 10/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire

final class TicketQR {
    // MARK: - Properties
    private var viewController: UIViewController
    private var ticket: Ticket?
    var onUpdatedTicket:((_ ticket: Ticket) -> Void)?
    
    // MARK: - Public
    required init(ticket: Ticket?, viewController: UIViewController) {
        self.ticket = ticket
        self.viewController = viewController
    }
    
    /// show or get qr
    func showOrGet(for typeOfService: TypeOfService?) {
        if let typeOfService = typeOfService, let code = typeOfService.code, code != "000000" {
            showQR(with: code)
        } else {
            KVNProgress.show(withStatus: .localized(.generatingQR))
            startTimer(with: typeOfService) // obtener el "code" del typeOfService seleccionado en caso de aún no se haya generado
        }
    }
    
    // MARK: - Private Methods
    private func startTimer(with typeOfService: TypeOfService?) {
        Timer.scheduledTimer(timeInterval: 5.0, target: self,
                             selector: #selector(getUpdatedTicket(_:)),
                             userInfo: typeOfService,
                             repeats: false)
    }
    
    @objc func getUpdatedTicket(_ timer: Timer) {
        guard let ticket = ticket, let typeOfService = timer.userInfo as? TypeOfService else {
            return
        }
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self, let item = result.value else {
                KVNProgress.showError(withStatus: .localized(.errorGeneral))
                return
            }
            
            // get typeOfService selected and check if already have the code
            let filtered = item.ticketLines.filter({$0.codeId == typeOfService.codeId}).first
            if let updatedTypeOfService = filtered?.serviceType, updatedTypeOfService.code != nil {
                strongSelf.onUpdatedTicket?(item)
                strongSelf.showOrGet(for: updatedTypeOfService)
                KVNProgress.dismiss()
            } else {
                strongSelf.startTimer(with: typeOfService) // aún no tenemos codigo, por lo que hay que volver a intentarlo
            }
        }
        APIManager.sharedInstance.fetchTicket(with: "\(ticket.id)", completionHandler: completionHandler)
    }
    
    private func showQR(with id: String) {
        if let image = Utils.generateQR(with: id) {
            let vc = QRDetailViewController.storyboardViewController()
            vc.qrCode = id
            vc.image = image
            viewController.present(vc, animated: true)
        }
    }
}
