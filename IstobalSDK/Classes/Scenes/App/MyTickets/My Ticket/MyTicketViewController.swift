//
//  MyTicketViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Alamofire
import KVNProgress

final class MyTicketViewController: UIViewController, StyleNavBar, KVNProgressPresenting, ErrorPresenting {
    // MARK: - IBOutlets
    @IBOutlet weak var emptyState: EmptyStateView!
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var btnMap: UIBarButtonItem!
//    @IBOutlet weak var btnMore: UIBarButtonItem!
    // MARK: - Properties
    var isFromPushNotification = false
    var ticket: Ticket?
    var ticketID: String?
    private var startWash: StartWashFromTicket?
    private var ticketQR: TicketQR?
    private var ticketVehicle: TicketVehicle?
    private let ticketRouter = TicketRouter()
    private var dataSource: (UITableViewDataSource & UITableViewDelegate)? {
        didSet {
            self.tableView.dataSource = dataSource
            self.tableView.delegate = dataSource
            self.tableView.reloadData()
        }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        if ticket == nil {
            getTicket()
        } else {
            setUpTicket()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeNavBarStyle(.myProfile, showBottomHairline: false, backButton: .white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.ticket)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        
        
        tableView.contentInset = UIEdgeInsets(top: -24, left: 0, bottom: 0, right: 0)
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.sectionFooterHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 60
        tableView.estimatedSectionFooterHeight = 60
        
        for cell in Cells.forMyTicket() {
            tableView.registerNibForCellClass(cell)
        }
    }
    
    private func setUpTicket() {
        guard let ticket = ticket else { return }
        emptyState.isHidden = true
        tableView.isHidden = false
//        updateButton(button: btnMap, state: ticket.status)
//        updateButton(button: btnMore, state: ticket.status)
        
        let ds = MyTicketDataSource(ticket: ticket)
        ds.onAction = { [unowned self] in
            self.doAction($0)
        }
        dataSource = ds
    }
    
    private func updateTicket(with ticket: Ticket) {
        self.ticket = ticket
        setUpTicket()
        NotificationCenter.default.post(name: Constants.NCenter.reDownloadMyTickets, object: nil)
    }
    
    private func updateButton(button: UIBarButtonItem, state: TicketStatus) {
        button.isEnabled = (state != .redeemed)
        button.tintColor = (state == .redeemed) ? .clear : .white
    }
}

// MARK: - Navigation
extension MyTicketViewController {
    private func setUpTicketVehicle() {
        ticketVehicle = TicketVehicle(ticket: ticket, viewController: self)
        ticketVehicle?.onUpdatedTicket = { [unowned self] in
            self.updateTicket(with: $0)
        }
        ticketVehicle?.showMyVehicles()
    }
    
    private func setUpQR(typeOfService: TypeOfService?) {
        ticketQR = TicketQR(ticket: ticket, viewController: self)
        ticketQR?.onUpdatedTicket = { [unowned self] in
            self.updateTicket(with: $0)
        }
        ticketQR?.showOrGet(for: typeOfService)
    }
    
    private func setUpStartWash(with typeOfService: TypeOfService?) {
        startWash = StartWashFromTicket(ticket: ticket, typeOfService: typeOfService, viewController: self)
        startWash?.onAction = { [unowned self] in
            self.doAction($0)
        }
        startWash?.checkIfUserSelectedVehicle()
    }
    
    private func showInfoTerminal(with typeOfService: TypeOfService?, error: Error?) {
        let vc = PreparingWashWithTerminalViewController.storyboardViewController()
        vc.typeOfService = typeOfService
        vc.ticket = ticket
        vc.error = error
        vc.delegate = self
        self.present(vc, animated: true)
    }
    
    fileprivate func showLegalNotice(with typeOfService: TypeOfService?) {
        let vc = LegalNoticeViewController.storyboardViewController()
        vc.isOutVehicle = false
        vc.ticket = ticket
        vc.typeOfService = typeOfService
        vc.delegate = self
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated:true, completion: nil)
    }
}

// MARK: - API
extension MyTicketViewController {
    private func getTicket() {
        guard let id = ticketID else { return }
        emptyState.setUp(for: .ticket)
        tableView.isHidden = true
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self, let item = result.value else {
                KVNProgress.showError(withStatus: .localized(.errorGettingTicket))
                self?.navigationController?.popViewController(animated: true)
                return
            }
            strongSelf.ticket = item
            strongSelf.setUpTicket()
            KVNProgress.dismiss()
        }
        APIManager.sharedInstance.fetchTicket(with: id, completionHandler: completionHandler)
    }
}

// MARK: - Actions
extension MyTicketViewController {
    @IBAction func showRoute(_ sender: Any) {
        Maps.openMaps(with: ticket?.installation)
    }
    
    @IBAction func showMenu(_ sender: Any) {
        let vc = TicketDetailMoreViewController.storyboardViewController()
        vc.ticket = ticket
        vc.delegate = self
        self.present(vc, animated: true)
    }
    
    private func doAction(_ action: TicketAction) {
        switch action {
        case .selectVehicle:
            setUpTicketVehicle()
        case .showInfo(let typeOfService):
            let data = TicketServiceRoute(ticket: ticket, typeOfService: typeOfService)
            ticketRouter.perform(.serviceInfo, from: self, parameters: data)
        case .showQR(let typeOfService):
            setUpQR(typeOfService: typeOfService)
        case .startWash(let typeOfService):
            setUpStartWash(with: typeOfService)
        case .showMessage(let title, let message):
            presentAlertView(with: title, message: message)
        case .showLegalNotice(let typeOfService):
            showLegalNotice(with: typeOfService)
        case .showInfoTerminal(let typeOfService, let error):
            showInfoTerminal(with: typeOfService, error: error)
        }
    }
}

// MARK: - Delegates
extension MyTicketViewController: TicketDetailMoreDelegate, PreparingWashWithTerminalViewControllerDelegate, LegalNoticeViewControllerDelegate {
    // MARK: TicketDetailMoreDelegate
    func ticketDetailMore(_ controller: TicketDetailMoreViewController, updatedTicket: Ticket) {
        updateTicket(with: updatedTicket)
    }
    // MARK: PreparingWashWithTerminalViewControllerDelegate
    func preparingWashWithTerminalViewController(_ controller: PreparingWashWithTerminalViewController, updatedTicket: Ticket) {
        updateTicket(with: updatedTicket)
    }
    // MARK: LegalNoticeViewControllerDelegate
    func legalNoticeViewController(_ controller: LegalNoticeViewController, updatedTicket: Ticket?) {
        if let ticket = updatedTicket {
            updateTicket(with: ticket)
        }
        self.dismiss(animated: true)
    }
}
