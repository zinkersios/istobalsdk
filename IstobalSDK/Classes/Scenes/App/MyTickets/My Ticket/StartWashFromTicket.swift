//
//  StartWashFromTicket.swift
//  istobal
//
//  Created by Shanti Rodríguez on 10/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import KVNProgress
import Alamofire

final class StartWashFromTicket {
    // MARK: - Properties
    private var typeOfService: TypeOfService?
    private var viewController: UIViewController
    private var ticket: Ticket?
    var onAction:((_ action: TicketAction) -> Void)?
    
    // MARK: - Public
    required init(ticket: Ticket?, typeOfService: TypeOfService?, viewController: UIViewController) {
        self.ticket = ticket
        self.viewController = viewController
        self.typeOfService = typeOfService
    }
    
    func checkIfUserSelectedVehicle() {
        if ticket?.vehicleID != nil {
            checkBeacon(with: "")
        } else {
            showAlertView()
        }
    }
    
    // MARK: - Private Methods
    private func showAlertView() {
        let vc = SRAlertViewController.storyboardViewController()
        vc.delegate = self
        vc.type = .withoutVehicleInTicket
        viewController.present(vc, animated: true)
    }
    
    private func checkBeacon(with ID: String) {
//        guard let ticket = ticket else { return }
//        guard let installationID = ticket.installation?.id else { return }
//        guard let typeOfService = typeOfService, let codeID = typeOfService.codeId else { return }
//
//        var vehicleID = ""
//        if let mID = ticket.vehicleID {
//            vehicleID = "\(mID)"
//        }
//
//        KVNProgress.show(withStatus: .localized(.loadingWait))
//
//        let completionHandler: (Result<Bool>) -> Void = { [weak self] (result) in
//            guard let strongSelf = self else { return }
//
//            if result.isSuccess, let positioned = result.value, positioned == true {
//                strongSelf.onAction?(.showLegalNotice(typeOfService: typeOfService))
//            } else {
//                strongSelf.onAction?(.showInfoTerminal(typeOfService: typeOfService, error: result.error))
//            }
//            KVNProgress.dismiss()
//        }
//        APIManager.sharedInstance.fetchUserPosition(with: ID, vehicleID: vehicleID, installationID: "\(installationID)",
//                                                    codeID: codeID, completionHandler: completionHandler)
        self.onAction?(.showLegalNotice(typeOfService: typeOfService))
    }
}

// MARK: - Delegates
extension StartWashFromTicket: SRAlertViewControllerDelegate {
    func alertViewController(_ controller: SRAlertViewController, type: AlertViewControllerType, action: ChosenAction) {
        viewController.dismiss(animated: true) {
            self.alertViewAction(for: action)
        }
    }
    
    private func alertViewAction(for action: ChosenAction) {
        switch action {
        case .accept:
            onAction?(.selectVehicle)
        case .cancel:
            onAction?(.showQR(typeOfService: typeOfService))
        default:()
        }
    }
}
