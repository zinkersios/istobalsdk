//
//  TicketVehicle.swift
//  istobal
//
//  Created by Shanti Rodríguez on 10/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import KVNProgress

final class TicketVehicle {
    // MARK: - Properties
    private var viewController: UIViewController
    private var ticket: Ticket?
    var onUpdatedTicket:((_ ticket: Ticket) -> Void)?
    
    // MARK: - Public
    required init(ticket: Ticket?, viewController: UIViewController) {
        self.ticket = ticket
        self.viewController = viewController
    }
    
    func showMyVehicles() {
        SRAlert.showVehicles(in: viewController) { [unowned self] (action) in
            switch action {
            case .add:
                self.addNewVehicle()
            case .selected(let object):
                if let vehicle = object as? BasicVehicle, vehicle.id != self.ticket?.vehicleID {
                    self.updateTicketVehicle(with: vehicle)
                }
            default:()
            }
        }
    }
    
    // MARK: - Private Methods
    private func addNewVehicle() {
        let vc = AddVehicleStep1ViewController.storyboardViewController()
        vc.delegate = self
        vc.isFromTicket = true
        let navController = UINavigationController(rootViewController: vc)
        viewController.present(navController, animated:true, completion: nil)
    }
    
    private func updateTicketVehicle(with vehicle: BasicVehicle) {
        guard var tmpTicket = ticket else { return }
        tmpTicket.vehicleID = vehicle.id
        KVNProgress.show(withStatus: .localized(.updatingTicket))
        
        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            guard result.error == nil, let ticket = result.value else {
                KVNProgress.showError(withStatus: .localized(.errorGeneral))
                return
            }
            KVNProgress.showSuccess(withStatus: .localized(.updatedTicket))
            strongSelf.onUpdatedTicket?(ticket)
        }
        APIManager.sharedInstance.updateTicketVehicle(with: tmpTicket, completionHandler: completionHandler)
    }
}

// MARK: - Delegates
extension TicketVehicle: AddVehicleStep1ViewControllerDelegate {
    
    // MARK: AddVehicleStep1ViewControllerDelegate
    func addVehicleStep1ViewController(_ controller: AddVehicleStep1ViewController, vehicles: [Vehicle]) {
        if let vehicle = vehicles.last {
            let basicVehicle = BasicVehicle(id: vehicle.id, name: vehicle.name, isAssignedVehicle: false)
            self.updateTicketVehicle(with: basicVehicle)
        }
    }
}
