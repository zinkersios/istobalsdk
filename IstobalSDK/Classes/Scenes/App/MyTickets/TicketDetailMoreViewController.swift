//
//  TicketDetailMoreViewController.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 11/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
//import Branch
import KVNProgress
import Alamofire

final class TicketDetailMoreViewController: OptionsMenuViewController {
    // MARK: - Properties
    var ticket: Ticket?
    weak var delegate: TicketDetailMoreDelegate?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDataSource()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        Utils.registerScreen(.ticketOptions)
    }
    
    // MARK: - Private methods
    private func setUpDataSource() {
        let item1 = OptionMenu(name: .localized(.giveThisTicket), action: .giftTicket)
        let array = [item1]
        updateDataSource(with: array)
    }
    
    override func doAction(_ item: OptionMenu) {
        switch item.action {
//        case .giftTicket:
//            giftTicket()
        default:()
        }
    }
    
//    private func setUpBranch(with ticketID: Int, userID: String) {
//        let lp: BranchLinkProperties = BranchLinkProperties()
//        lp.addControlParam("ticket_id", withValue: "\(ticketID)")
//        lp.addControlParam("user_id", withValue: userID)
//
//        let buo = BranchUniversalObject(canonicalIdentifier: "\(ticketID)")
//        buo.showShareSheet(with: lp, andShareText: .localized(.giftMessage), from: self) { (_, _) in
//            self.dismiss(animated: true)
//        }
//
//        KVNProgress.dismiss()
//    }
}

// MARK: - Actions
extension TicketDetailMoreViewController {
//    private func giftTicket() {
//        guard let ticket = ticket, let userID = APIManager.sharedInstance.userID else {
//            return
//        }
//
//        if ticket.status == .consumed {
//            presentStatusBarNotification(.localized(.shareConsumed))
//            return
//        }
//
//        KVNProgress.show(withStatus: .localized(.generatingGift))
//
//        let completionHandler: (Result<Ticket>) -> Void = { [weak self] (result) in
//            guard let strongSelf = self else { return }
//
//            guard result.error == nil, let ticket = result.value else {
//                KVNProgress.showError(withStatus: .localized(.errorGeneral))
//                return
//            }
//
//            strongSelf.ticket = ticket
//            strongSelf.delegate?.ticketDetailMore(strongSelf, updatedTicket: ticket)
////            strongSelf.setUpBranch(with: ticket.id, userID: userID)
//        }
//        APIManager.sharedInstance.sendGift(with: ticket.id, completionHandler: completionHandler)
//    }
}

// MARK: - Delegate
protocol TicketDetailMoreDelegate: class {
    func ticketDetailMore(_ controller: TicketDetailMoreViewController, updatedTicket: Ticket)
}
