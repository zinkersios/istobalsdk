//
//  OptionsMenuViewController.swift
//  istobal
//
//  Created by Shanti Rodríguez on 14/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

class OptionsMenuViewController: UIViewController, ErrorPresenting {
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: CenteredTableView!
    
    // MARK: - Properties
    var statusBarStyle = UIStatusBarStyle.lightContent
    private var dataSource: (UITableViewDataSource & UITableViewDelegate)? {
        didSet {
            self.tableView.dataSource = dataSource
            self.tableView.delegate = dataSource
            self.tableView.reloadData()
        }
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
    }
    
    // MARK: - Private methods
    private func setUpTableView() {
        for cell in Cells.forContactDetails() {
            tableView.registerNibForCellClass(cell)
        }
        tableView.estimatedRowHeight = 64.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.view.backgroundColor = Color.daintree.value
    }
    
    private func cell(for item: OptionMenu, indexPath: IndexPath) -> UITableViewCell {
        if item.cell == .big {
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as OptionMenuBigCell
            cell.setUp(with: item)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as OptionMenuCell
        cell.setUp(with: item)
        return cell
    }
    
    func doAction(_ item: OptionMenu) {
        debugPrint("selected: \(item)")
//        log.debug("selected: \(item)")
    }
}

// MARK: - IBActions
extension OptionsMenuViewController {
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

// MARK: - Public Methods
extension OptionsMenuViewController {
    func updateDataSource(with items: [OptionMenu]) {
        let st: APIState = (items.count == 0) ? .noResults : .results
        
        let ds = ListDataSource<OptionMenu>(
            state: st,
            type: .transactions,
            items: items,
            cellFactory: { [unowned self] in self.cell(for: $0, indexPath: $1) }
        )
        ds.onItemSelected = { [unowned self] in
            self.doAction($0)
        }
        dataSource = ds
    }
}
