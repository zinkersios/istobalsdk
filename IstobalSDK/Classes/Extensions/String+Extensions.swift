//
//  String+Extensions.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

extension String {
    /// To check text field or String is blank or not
    var isBlank: Bool {
        let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
        return trimmed.isEmpty
    }
    
    /// Validate Email
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            let textRange: NSRange = NSRange(location: 0, length: self.count)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: textRange) != nil
        } catch {
            return false
        }
    }
    
    // Credit Card
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
        let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
        return String(cleanedUpCopy.enumerated().map {
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
            }.joined().dropFirst())
    }
}
