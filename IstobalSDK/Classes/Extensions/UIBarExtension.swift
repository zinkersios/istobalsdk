//
//  UIBarExtension.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 3/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    func hideBottomHairline() {        
        if let oldView = self.viewWithTag(Constants.Tag.hairLine) {
            oldView.removeFromSuperview()
        }
    }
    
    func showBottomHairline() {
        let bottomBorderRect = CGRect(x: 0, y: 44, width: Constants.Size.screenWidth, height: 0.5)
        let navigationSeparator = UIView(frame: bottomBorderRect)
        navigationSeparator.backgroundColor = .gray
        navigationSeparator.tag = Constants.Tag.hairLine
        hideBottomHairline()
        self.addSubview(navigationSeparator)
    }
}

extension UIView {
    fileprivate var hairlineImageView: UIImageView? {
        return hairlineImageView(in: self)
    }
    
    fileprivate func hairlineImageView(in view: UIView) -> UIImageView? {
        if let imageView = view as? UIImageView, imageView.bounds.height <= 1.0 {
            return imageView
        }
        
        for subview in view.subviews {
            if let imageView = self.hairlineImageView(in: subview) { return imageView }
        }
        
        return nil
    }
}
