//
//  Array+Extension.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

extension Array {
    func elementAt(index: Int) -> Element? {
        return index < self.count && index >= 0 ? self[index] : nil
    }
    
    mutating func removeObject<T>(obj: T) where T: Equatable {
        self = self.filter({$0 as? T != obj})
    }
}
