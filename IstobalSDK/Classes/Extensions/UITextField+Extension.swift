//
//  UITextField+Extension.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

extension UITextField {
    // MARK: - Styles
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = Color.gray2.value.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    // MARK: - Form
    func isEmpty() -> Bool {
        if let text = self.text, text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).isEmpty {
            return true
        }
        return false
    }
}
