//
//  UIBarButtonItem+Extension.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

enum ItemStyle {
    case red
    case redLight
    case gray
    case white
    case blue
}

extension UIBarButtonItem {
    func setStyle(_ type: ItemStyle) {
        var color = Color.red.value
        var font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        
        switch type {
        case .redLight:
            font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        case .gray:
            color = Color.gray2.value
            font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        case .white:
            color = UIColor.white
            font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        case .blue:
            color = Color.astral.value
        default: ()
        }
        
        self.tintColor = color
        self.setTitleTextAttributes([NSAttributedStringKey.font: font], for: .normal)
    }
}
