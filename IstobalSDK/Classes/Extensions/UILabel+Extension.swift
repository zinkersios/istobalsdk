//
//  UILabel+Extension.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 2/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setLineHeight(_ lineHeight: CGFloat = 0.8) {
        guard let text = self.text else {
            return
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment
        
        let attrString = NSMutableAttributedString(string: text)
        attrString.addAttribute(NSAttributedStringKey.font, value: self.font, range: NSMakeRange(0, attrString.length))
        attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
    
    /// set Info
    func setBigText(with text: String, smallText: String, type: BigSmallText = .byDefault) {
        var smallFont = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        let bigFont = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        var smallColor = Color.blue.value
        
        switch type {
        case .buyService:
            smallFont = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
            smallColor = .lightGray
        case .ticketService:
            smallFont = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
            smallColor = Color.blue.value
        default:()
        }
        
        /// small text
        var attrs = [NSAttributedStringKey.font: smallFont,
                     NSAttributedStringKey.foregroundColor: smallColor]
        let attributedString = NSMutableAttributedString(string: smallText, attributes:attrs)
        
        /// normal text
        attrs = [NSAttributedStringKey.font: bigFont]
        let normalString = NSMutableAttributedString(string: text, attributes:attrs)
        normalString.append(attributedString)
        
        self.attributedText = normalString
    }
}
