//
//  Double+Extensions.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

extension Double {
    func floor(nearest: Double) -> Double {
        let intDiv = Double(Int(self / nearest))
        return intDiv * nearest
    }
}
