//
//  URL+Extension.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 26/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

extension URL {
    init?(json: [String: Any], key: String) {
        guard let string = json[key] as? String else { return nil }
        self.init(string: string)
    }
}
