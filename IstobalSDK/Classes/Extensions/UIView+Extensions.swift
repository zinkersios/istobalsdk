//
//  UIView+Extensions.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 21/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    // MARK: - Shadow
    func addDropShadow(_ type: DropShadowType = .round) {
        switch type {
        case .byDefault:
            self.addDropShadow(with: StyleConstants.Offset.primary,
                               radius: StyleConstants.CornerRadius.main,
                               opacity: StyleConstants.Opacity.main)
        case .round:
            self.addDropShadow(with: StyleConstants.Offset.secondary,
                               radius: 10,
                               opacity: StyleConstants.Opacity.full,
                               shadowColor: Constants.Color.shadow)
        case .cell:
            self.addDropShadow(with: StyleConstants.Offset.secondary,
                               radius: 4,
                               opacity: StyleConstants.Opacity.full,
                               shadowColor: Constants.Color.shadow)
        }
    }
    
    func addDropShadow(with offset: CGSize, radius: CGFloat, opacity: Float, shadowColor: UIColor = .black) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
    }
    
    // MARK: - Round
    
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func round(corners: UIRectCorner, radius: CGFloat = StyleConstants.CornerRadius.main) {
        let maskPath = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
    
    /// Fix NavBar Constraints
    func applyNavBarConstraints(size: CGFloat) {
        let widthConstraint = self.widthAnchor.constraint(equalToConstant: size)
        let heightConstraint = self.heightAnchor.constraint(equalToConstant: size)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
    }
}
