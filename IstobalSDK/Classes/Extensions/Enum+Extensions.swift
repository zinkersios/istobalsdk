//
//  Enum+Extensions.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

extension RawRepresentable where RawValue == Int {
    
    static var itemsCount: Int {
        var index = 0
        while Self(rawValue: index) != nil { index += 1 }
        return index
    }
}
