//
//  UserDefaultable.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

// MARK: - Key Namespaceable
protocol KeyNamespaceable { }

extension KeyNamespaceable {
    static func namespace(_ key: String) -> String {
        return "\(Self.self).\(key)"
    }
    
    static func namespace<T: RawRepresentable>(_ key: T) -> String where T.RawValue == String {
        return namespace(key.rawValue)
    }
}

// MARK: - Bool Defaults
protocol BoolUserDefaultable: KeyNamespaceable {
    associatedtype BoolDefaultKey: RawRepresentable
}

extension BoolUserDefaultable where BoolDefaultKey.RawValue == String {
    // Set
    static func set(_ bool: Bool, forKey key: BoolDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(bool, forKey: key)
    }
    // Get
    static func bool(forKey key: BoolDefaultKey) -> Bool {
        let key = namespace(key)
        return UserDefaults.standard.bool(forKey: key)
    }
}

// MARK: - Integer Defaults
protocol IntegerUserDefaultable: KeyNamespaceable {
    associatedtype IntegerDefaultKey: RawRepresentable
}

extension IntegerUserDefaultable where IntegerDefaultKey.RawValue == String {
    // Set
    static func set(_ integer: Int, forKey key: IntegerDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(integer, forKey: key)
    }
    // Get
    static func integer(forKey key: IntegerDefaultKey) -> Int {
        let key = namespace(key)
        return UserDefaults.standard.integer(forKey: key)
    }
}

// MARK: - Data Defaults
protocol DataUserDefaultable: KeyNamespaceable {
    associatedtype DataDefaultKey: RawRepresentable
}

extension DataUserDefaultable where DataDefaultKey.RawValue == String {
    // Set
    static func set(_ object: Data, forKey key: DataDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(object, forKey: key)
    }
    // Get
    static func data(forKey key: DataDefaultKey) -> Data? {
        let key = namespace(key)
        return UserDefaults.standard.data(forKey: key)
    }
}

// MARK: - String Defaults
protocol StringUserDefaultable: KeyNamespaceable {
    associatedtype StringDefaultKey: RawRepresentable
}

extension StringUserDefaultable where StringDefaultKey.RawValue == String {
    // Set
    static func set(_ string: String, forKey key: StringDefaultKey) {
        let key = namespace(key)
        UserDefaults.standard.set(string, forKey: key)
    }
    // Get
    static func string(forKey key: StringDefaultKey) -> String? {
        let key = namespace(key)
        return UserDefaults.standard.string(forKey: key)
    }
}

// MARK: - Use
// Preparation
extension UserDefaults {
    
    struct App: BoolUserDefaultable {
        init() { }
        //swiftlint:disable:next nesting
        enum BoolDefaultKey: String {
            case firstRun
            case tutorialShowed
            case mapPermissionsShowed
            case addVehicleShowed
            case notAskForRating
            case userIsInHome
        }
    }
        
    struct User: DataUserDefaultable {
        init() { }
        //swiftlint:disable:next nesting
        enum DataDefaultKey: String {
            case user
            case creditCards
            case lastTicketPurchased
            case lastOfferRedeemed
            case myVehicles
            case favorites
        }
    }
    
    struct Account: IntegerUserDefaultable {
        init() {}
        //swiftlint:disable:next nesting
        enum IntegerDefaultKey: String {
            case loginState
            case selectedVehicleToMakePurchase
            case totalLogins
        }
    }
    
    struct Settings: StringUserDefaultable {
        init() { }
        //swiftlint:disable:next nesting
        enum StringDefaultKey: String {
            case firebaseToken
            case firebaseTokenToCloseSession
        }
    }
}
