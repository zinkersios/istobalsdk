//
//  Bundle+Extension.swift
//  istobal
//
//  Created by Shanti Rodríguez on 12/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    var releaseVersionNumberPretty: String {
        return "\(releaseVersionNumber ?? "1.0.0")"
    }
    var buildVersionNumberPretty: String {
        return "\(buildVersionNumber ?? "1")"
    }
}
