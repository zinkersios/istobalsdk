//
//  UIDevice+Extension.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 29/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import Device

extension UIDevice {
    var hasTapticEngine: Bool {
        return Device.version() == .iPhone6S || Device.version() == .iPhone6SPlus || Device.version() == .iPhone7 || Device.version() == .iPhone7Plus
    }
    
    var hasHapticFeedback: Bool {
        return Device.version() == .iPhone7 || Device.version() == .iPhone7Plus
    }
}
