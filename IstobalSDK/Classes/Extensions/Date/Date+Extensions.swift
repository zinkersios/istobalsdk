//
//  Date+Extensions.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 16/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    // The date format specified in the RFC 8601 standard
    // is common place on internet communication
    // expects a string as: 2017-01-17T20:15:00+0100
    static func date(rfc8601Date: String?) -> Date? {
        if let string = rfc8601Date {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            return formatter.date(from: string)
        }
        return nil
    }
    
    // MARK: Date Formatter
    struct Formatter {
        static let custom = DateFormatter(dateFormat: "dd/MM/yyyy")
        static let ticket = DateFormatter(dateFormat: "dd MMM yy")
        static let notification = DateFormatter(dateFormat: "dd/MM/yy \n HH:mm")
    }
    
    var customFormatted: String {
        return Formatter.custom.string(from: self)
    }
    
    var ticketFormatted: String {
        return Formatter.ticket.string(from: self)
    }
    
    var notificationFormatted: String {
        return Formatter.notification.string(from: self)
    }
}

extension DateFormatter {
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormat = dateFormat
    }
}
