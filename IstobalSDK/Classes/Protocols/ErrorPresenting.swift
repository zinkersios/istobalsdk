//
//  ErrorPresenting.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import NotificationBannerSwift

protocol ErrorPresenting {
    func presentStatusBarNotification(_ text: String, style: BannerStyle)
    func presentAlertView(with title: String?, message: String?)
}

extension ErrorPresenting where Self: UIViewController {
    
    func presentStatusBarNotification(_  text: String, style: BannerStyle = .warning) {
        let banner = StatusBarNotificationBanner(title: text, style: style)
        banner.show()
    }
    
    func presentAlertView(with title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(dismissAction)
        present(alertController, animated: true)
    }
}
