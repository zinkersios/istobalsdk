//
//  Storyboardable.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

protocol Storyboardable: class {
    static var defaultStoryboardName: String { get }
}

extension Storyboardable where Self: UIViewController {
    static var defaultStoryboardName: String {
        return String(describing: self)
    }
    
    static func storyboardViewController() -> Self {
        let storyboard = UIStoryboard(name: defaultStoryboardName, bundle: SDKNavigation.getBundle())
        
        guard let vc = storyboard.instantiateInitialViewController() as? Self else {
            fatalError("Could not instantiate initial storyboard with name: \(defaultStoryboardName)")
        }
        
        return vc
    }
    
    static func storyboardViewController(with storyboardName: String) -> Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: SDKNavigation.getBundle())
        guard let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as? Self else {
            fatalError("Could not instantiate initial storyboard with name: \(storyboardName)")
        }
        
        return vc
    }
}

extension UIViewController: Storyboardable { }
