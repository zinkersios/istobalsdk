//
//  SRAlertViewControllerDelegate.swift
//  istobal
//
//  Created by Shanti Rodríguez on 18/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

protocol SRAlertViewControllerDelegate: class {
    func alertViewController(_ controller: SRAlertViewController, type: AlertViewControllerType, action: ChosenAction)
}
