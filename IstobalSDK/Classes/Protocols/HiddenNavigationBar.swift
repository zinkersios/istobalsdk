//
//  HiddenNavigationBar.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

protocol HiddenNavigationBar {
    func transparentNavBar(_ navBar: UINavigationBar)
}

extension HiddenNavigationBar where Self: UIViewController {
        
    func transparentNavBar(_ navBar: UINavigationBar) {
        navBar.isTranslucent = true
        //To see background Image instead of nainvigation bar
        navBar.setBackgroundImage(UIImage(), for: .default)
        
        //To hide shadow of the navigation bar
        navBar.shadowImage = UIImage()
    }
}
