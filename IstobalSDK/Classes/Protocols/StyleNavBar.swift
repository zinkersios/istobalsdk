//
//  StyleNavBar.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 1/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import ETNavBarTransparent

protocol StyleNavBar {
    func changeNavBarStyle(_ style: NavBarStyle, showBottomHairline: Bool, backButton: BackButton)
}

// Gradient color direction
enum Direction {
    case vertical, horizontal
}

// Back button type
enum BackButton {
    case gray, white, red
}

extension StyleNavBar where Self: UIViewController {
    
    func changeNavBarStyle(_ style: NavBarStyle, showBottomHairline: Bool = true, backButton: BackButton = .gray) {
        var tintColor: UIColor = SDKColor.navigationBarTint
        var textColor: UIColor = SDKColor.navigationBarTint
        var back = backButton
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.backgroundColor = SDKColor.navigatioBarBackground
        self.view.backgroundColor = SDKColor.appBackground
        
        switch style {
        case .myProfile:
            back = .white
            UIApplication.shared.statusBarStyle = .lightContent
            self.navBarBgAlpha = 0
        case .installationDetail:
            self.navBarBgAlpha = 0
            UIApplication.shared.statusBarStyle = .lightContent
        case .buyWash:
            self.navBarBgAlpha = 1
            UIApplication.shared.statusBarStyle = .lightContent
        default:
            textColor = SDKColor.navigationBarTint
            tintColor = SDKColor.navigationBarTint
            self.navBarBgAlpha = 1
            UIApplication.shared.statusBarStyle = .default
        }
        
        self.navBarTintColor = tintColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: textColor,
                                                                   NSAttributedStringKey.font: Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance]
        
        // show or hide line
        if showBottomHairline {
            navigationController?.navigationBar.showBottomHairline()
        } else {
            navigationController?.navigationBar.hideBottomHairline()
        }
        
        setUpBackButton(for: back)
    }
    
    private func setUpBackButton(for type: BackButton) {
        let backImage: UIImage? = UIImage(named: "ic_nav_bar_back_white", in: SDKNavigation.getBundle(), compatibleWith: nil)
    
//        switch type {
//        case .gray:
//            backImage = UIImage(named: "ic_nav_bar_back", in: SDKNavigation.getBundle(), compatibleWith: nil)
//        case .white:
//            backImage = UIImage(named: "ic_nav_bar_back_white", in: SDKNavigation.getBundle(), compatibleWith: nil)
//        case .red:
//            backImage = UIImage(named: "ic_nav_bar_back_red", in: SDKNavigation.getBundle(), compatibleWith: nil)
//        }
        
        if let image = backImage {
            navigationController?.navigationBar.backIndicatorImage = image
            navigationController?.navigationBar.backIndicatorTransitionMaskImage = image
        }
    }
    
    // TitleView for Nav Bar
    func titleView(with string: String, style: NavBarStyle) -> UILabel {
        let label = UILabel()
        label.text = string
        label.font = Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance
        
        switch style {
        case .myProfile:
            label.textColor = SDKColor.navigationBarTint
        default:
            label.textColor = SDKColor.navigationBarTint
        }
        
        return label
    }
}
