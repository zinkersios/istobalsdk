//
//  DeeplinkNavigatorDelegate.swift
//  istobal
//
//  Created by Shanti Rodríguez on 8/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

protocol DeeplinkNavigatorDelegate: class {
    func stopWashing(with notification: PushNotification)
    func finishedWashing(with notification: PushNotification)
}
