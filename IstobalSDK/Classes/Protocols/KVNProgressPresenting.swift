//
//  KVNProgressPresenting.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 21/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Alamofire
import KVNProgress

protocol KVNProgressPresenting {
    func presentMessage(for result: Result<Bool>)
    func presentError(error: Error?)
}

extension KVNProgressPresenting {
    
    func presentMessage(for result: Result<Bool>) {
        if result.isFailure {
            presentError(error: result.error)
        }
    }
    
    func presentError(error: Error?) {
        if let error = error as? APIManagerError {
            KVNProgress.showError(withStatus: error.localizedTitle)
        } else {
            KVNProgress.showError(withStatus: .localized(.error404))
        }
    }
}
