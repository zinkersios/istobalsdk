//
//  StatusFormIcons.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 19/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

protocol StatusFormIcons {}

extension StatusFormIcons where Self: UIButton {
    
    func updateState(with status: FormStatus, feedback: Bool = false) {
        switch status {
        case .error:
            self.setImage(Constants.Image.close, for: .normal)
            self.tintColor = Color.red.value
            
            if feedback {
                TapticEngine.notification.feedback(.warning)
            }
        case .success:
            self.setImage(Constants.Image.check, for: .normal)
            self.tintColor = Color.green.value
        }
    }
}
