//
//  SignOff.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 26/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

protocol SignOff {
    func forceLogOut()
}

extension SignOff where Self: UIViewController {
    
    func forceLogOut() {
//        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {        
//            UIApplication.shared.statusBarStyle = .default
//            appDelegate.changeRootViewController(.unknown)
//        }
    }
}
