//
//  Router+Protocol.swift
//  istobal
//
//  Created by Shanti Rodríguez on 3/5/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

protocol AppRouter {
    associatedtype Segue
    func perform(_ segue: Segue, from source: UIViewController, parameters: Any?)
}
