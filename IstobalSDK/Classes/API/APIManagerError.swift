//
//  APIManagerError.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

protocol ErrorProtocol: Error {
    var localizedTitle: String { get }
    var localizedDescription: String { get }
    var code: Int { get }
}

struct APIManagerError: ErrorProtocol {
    var localizedTitle: String
    var localizedDescription: String
    var code: Int
    
    init(localizedTitle: String, localizedDescription: String = "Error", code: Int = 0) {
        self.localizedTitle = localizedTitle
        self.localizedDescription = localizedDescription
        self.code = code
    }
    
    init(code: Int) {
        self.localizedTitle = ""
        self.localizedDescription = ""
        self.code = code
    }
    
    init() {
        self.localizedTitle = ""
        self.localizedDescription = ""
        self.code = 0
    }
}

enum Type: Error {
    case network(error: Error)
    case apiProvidedError(reason: String)
    case objectSerialization(reason: String)
}
