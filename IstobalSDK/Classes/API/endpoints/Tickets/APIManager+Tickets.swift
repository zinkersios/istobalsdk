//
//  APIManager+Tickets.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Alamofire

extension APIManager {
    
    /// Get My Tickets
    func fetchMyTickets(_ completionHandler: @escaping (Result<TicketDataSource>) -> Void) {
        Alamofire.request(Router.getTickets()).responseJSON { response in
            // The access token provided has expired
            if let statusCode = response.response?.statusCode, statusCode == Constants.HTTPCode.unauthorised {
                completionHandler(.failure(APIManagerError.init(code: statusCode)))
                return
            }
            
            // error parse JSON
            guard let json = response.result.value as? JSONArray else {
                completionHandler(.failure( self.parseError(response.result.error) ))                
                return
            }
            
            let allArray = json.compactMap { Ticket(json: $0) }
            let activeArray = json.compactMap({ Ticket(json: $0) }).filter({$0.status == .available})
            let comsumedArray = json.compactMap({ Ticket(json: $0) }).filter({$0.status != .available})
            
            let all = TicketsDataSource(tickets: allArray)
            let active = TicketsDataSource(tickets: activeArray)
            let comsumed = TicketsDataSource(tickets: comsumedArray)
            let dataSource = TicketDataSource(all: all, active: active, comsumed: comsumed)
            
            completionHandler(.success(dataSource))
        }
    }
    
    /// Get ticket
    func fetchTicket(with id: String, completionHandler: @escaping (Result<Ticket>) -> Void) {
        Alamofire.request(Router.getTicket(id)).responseJSON { response in
            
            guard let json = response.result.value as? JSONObject,
                let ticket = Ticket(json: json)
                else {
                    completionHandler(.failure( self.parseError(response.result.error) ))
                    return
            }
            completionHandler(.success(ticket))
        }
    }
    
    /// Update ticket vehicle
    func updateTicketVehicle(with ticket: Ticket, completionHandler: @escaping (Result<Ticket>) -> Void) {
        Alamofire.request(Router.editTicketVehicle(ticket)).responseJSON { response in
            
            guard let json = response.result.value as? JSONObject,
                let ticket = Ticket(json: json)
                else {
                    completionHandler(.failure( self.parseError(response.result.error) ))
                    return
            }
            completionHandler(.success(ticket))
        }
    }
    
    /// Start wash
    func fetchStartWash(with codeId: String, completionHandler: @escaping (Result<Bool>) -> Void) {
        Alamofire.request(Router.startWash(codeId)).responseJSON { response in
            if let statusCode = response.response?.statusCode, statusCode == Constants.HTTPCode.success {
                completionHandler(.success(true))
                return
            }
            
            guard response.result.error == nil,
                let json = response.result.value as? JSONObject else {
                    completionHandler(.failure( self.parseError(response.result.error) ))
                    return
            }
            
            if let error = self.parseErrorResponse(json) {
                debugPrint("error: \(json)")
                completionHandler(.failure(error))
                return
            }
            
            completionHandler(.failure( self.parseError(response.result.error) ))
        }
    }
    
    /// Stop wash
    func fetchStopWash(with codeId: String, completionHandler: @escaping (Result<Ticket>) -> Void) {                
        Alamofire.request(Router.stopWash(codeId)).responseJSON { response in
            
            guard let json = response.result.value as? JSONObject,
                let ticket = Ticket(json: json)
                else {
                    completionHandler(.failure( self.parseError(response.result.error) ))
                    return
            }
            
            completionHandler(.success(ticket))
        }
    }
}
