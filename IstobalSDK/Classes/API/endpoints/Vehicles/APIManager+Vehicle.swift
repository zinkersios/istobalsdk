//
//  APIManager+Vehicle.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 6/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Alamofire

extension APIManager {
    
    /// Save a new vehicle
    func fetchNewVehicle(with params: JSONObject, completionHandler: @escaping (Result<[Vehicle]>) -> Void) {
        
        Alamofire.request(Router.newVehicle(params)).responseJSON { response in
            guard response.result.error == nil,
                let result = response.result.value as? JSONObject else {
                completionHandler(.failure( self.parseError(response.result.error) ))
                return
            }
            
            guard let statusCode = response.response?.statusCode,
                let json = result[Constants.JSONKey.vehicles] as? [JSONObject],
                let vehicle = result[Constants.JSONKey.vehicle] as? JSONObject else {
                return
            }
            
            if statusCode == Constants.HTTPCode.badRequest, let error = self.parseErrorResponse(vehicle) {
                completionHandler(.failure(error))
                return
            }
            
            guard statusCode == Constants.HTTPCode.created else {
                let error = APIManagerError.init(localizedTitle: .localized(.error404))
                completionHandler(.failure(error))
                return
            }
            
            /// Update User
            if let user = result["user"] as? JSONObject, self.parseUserResponse(user) {
//                log.info("updated user data")
                debugPrint("updated user data")
            }
                        
            let vehiclesArray = json.compactMap { Vehicle(json: $0) }
            completionHandler(.success(vehiclesArray))
        }
    }
    
    /// Retrieves the collection of Vehicle resources
    func fetchVehicles(_ completionHandler: @escaping (Result<[Vehicle]>) -> Void) {
        Alamofire.request(Router.getVehicles()).responseJSON { response in
            
            // error parse JSON
            guard let json = response.result.value as? JSONArray else {
                completionHandler(.failure( self.parseError(response.result.error) ))
                return
            }
            
            // The access token provided has expired
            if let statusCode = response.response?.statusCode, statusCode == Constants.HTTPCode.unauthorised {
                completionHandler(.failure(APIManagerError.init(code: statusCode)))
                return
            }
            
            // Save my Vehicles
            APIHelper.saveMyVehicles(with: json)
            
            let vehiclesArray = json.compactMap { Vehicle(json: $0) }
            completionHandler(.success(vehiclesArray))
        }
    }

    /// edit vehicle name
    func fetchEditVehicle(with vehicle: Vehicle, completionHandler: @escaping (Result<Bool>) -> Void) {
        Alamofire.request(Router.editVehicle(vehicle)).responseJSON { response in
            
            guard let statusCode = response.response?.statusCode,
                statusCode == Constants.HTTPCode.success || statusCode == Constants.HTTPCode.created else {
                completionHandler(.failure(APIManagerError.init()))
                return
            }
            
            guard let json = response.result.value as? JSONObject else {
                completionHandler(.failure(APIManagerError.init()))
                return
            }
            
            /// Update User
            if let user = json["user"] as? JSONObject, self.parseUserResponse(user) {
//                log.info("updated user data")
                debugPrint("updated user data")
            }            
            completionHandler(.success(true))
        }
    }
    
    // delete vehicle
    func fetchDeleteVehicle(with id: String, completionHandler: @escaping (Result<[Vehicle]>) -> Void) {
        Alamofire.request(Router.deleteVehicle(id)).responseJSON { response in
            
            guard let statusCode = response.response?.statusCode,
                statusCode == Constants.HTTPCode.success,
                let result = response.result.value as? JSONObject,
                let json = result[Constants.JSONKey.vehicles] as? [JSONObject]
                else {
                    completionHandler(.failure(APIManagerError.init()))
                    return
            }
            /// Update User
            if let user = result["user"] as? JSONObject, self.parseUserResponse(user) {
//                log.info("updated user data")
                debugPrint("updated user data")
            }
            /// Update vehicles
            let vehiclesArray = json.compactMap { Vehicle(json: $0) }
            completionHandler(.success(vehiclesArray))
        }
    }
}
