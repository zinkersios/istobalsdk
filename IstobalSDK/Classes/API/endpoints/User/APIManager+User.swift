//
//  APIManager+User.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 20/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import Alamofire

extension APIManager {

    /// Get me
    func fetchMe(_ completionHandler: @escaping (Result<Bool>) -> Void) {
        Alamofire.request(Router.getMe()).responseJSON { response in
            
            debugPrint("me value: \(response.result.value)")
            
            guard let json = response.result.value as? JSONObject else {
                completionHandler(.failure( self.parseError(response.result.error) ))
                return
            }
            
            if let error = self.parseErrorResponse(json) {
                completionHandler(.failure(error))
                return
            }
            
            // The access token provided has expired
            if let statusCode = response.response?.statusCode, statusCode == Constants.HTTPCode.unauthorised {
                completionHandler(.failure(APIManagerError.init(code: statusCode)))
                return
            }
            
            // Save user dic
            if self.parseUserResponse(json) {
                completionHandler(.success(true))
            } else {
                let error = APIManagerError.init(localizedTitle: .localized(.error404))
                completionHandler(.failure(error))
            }
        }
    }
    
}
