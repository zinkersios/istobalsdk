//
//  APIManager+Login.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import Alamofire

extension APIManager {
    
    /// Login with email & password
    public func fetchLogIn(with userId: String, hash: String, completionHandler: @escaping (Result<Bool>) -> Void) {
        
        let stringParams: String = "grant_type=https://istobal.com/smartwash" + "&user_id=" + userId + "&hash=" + hash + "&company_id=" + Constants.API.companyId + "&client_id=" + Constants.API.clientID + "&client_secret=" + Constants.API.clientSecret
        
        Alamofire.request(Router.login(stringParams)).responseJSON { response in
            debugPrint("response: \(response.value)")
            
            guard response.result.error == nil, let json = response.result.value as? JSONObject else {
                completionHandler(.failure( self.parseError(response.result.error) ))
                return
            }
            
            if let error = self.parseErrorResponse(json) {
                debugPrint("error json: \(json)")
                debugPrint("error: \(error.localizedDescription)")
                completionHandler(.failure(error))
                return
            }
            
            // Parse token
            if self.parseLoginResponse(json) {
                completionHandler(.success(true))
            } else {
                let error = APIManagerError.init(localizedTitle: .localized(.error404)) // Cannot parse data to JSON format
                completionHandler(.failure(error))
            }
        }
    }

    /// Register with email & password
    public func fetchRegister(with userId: String, hash: String, completionHandler: @escaping (Result<Bool>) -> Void) {
        let params = ["user_id": userId, "hash": hash]
        
        Alamofire.request(Router.registry(params)).responseJSON { response in
            
            debugPrint("status code: \(response.response?.statusCode)") 
            
            guard response.result.error == nil, let json = response.result.value as? JSONObject else {
                completionHandler(.failure( self.parseError(response.result.error) ))
                return
            }

            // The access token provided has expired
            if let statusCode = response.response?.statusCode, statusCode == Constants.HTTPCode.created {
                completionHandler(.success(true))
            } else if let statusCode = response.response?.statusCode, statusCode == 409{
                 completionHandler(.success(true))
            } else {
                let error = APIManagerError.init(localizedTitle: .localized(.error404))
                completionHandler(.failure(error))
            }
        }
    }
    

}
