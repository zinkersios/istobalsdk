//
//  APIManager+Installation.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 31/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Alamofire

extension APIManager {   
    /// Get installation
    func fetchInstallation(with id: String, completionHandler: @escaping (Result<[TypeOfService]>) -> Void) {
        Alamofire.request(Router.getInstallation(id)).responseJSON { response in
            
            debugPrint("response: \(response)")
            
            var services:[TypeOfService] = []
            
            if let jsonArray = response.result.value as? JSONArray{
                for json in jsonArray{
                    if let service = TypeOfService(json: json){
                        services.append(service)
                    }
                }
                completionHandler(.success(services))
            }else{
                completionHandler(.failure( self.parseError(response.result.error) ))
            }
            
//            guard let json = response.result.value as? JSONObject,
//                let installation = Installation(json: json)
//                else {
//                    debugPrint("error: \(response.result.error?.localizedDescription)")
//                completionHandler(.failure( self.parseError(response.result.error) ))
//                return
//            }
            
//            completionHandler(.success(installation))
        }
    }
    
    /// Rate Installation
    func fetchRate(with valoration: Valoration, completionHandler: @escaping (Result<Ticket>) -> Void) {
        Alamofire.request(Router.rate(valoration)).responseJSON { response in
            
            guard let json = response.result.value as? JSONObject,
                let ticket = Ticket(json: json)
                else {
                    completionHandler(.failure( self.parseError(response.result.error) ))
                    return
            }
            
            completionHandler(.success(ticket))
        }
    }
    
    /// Get valorations of an installation
    func fetchValorations(with params: JSONObject, completionHandler: @escaping (Result<ValorationsDataSource>) -> Void) {
        
        Alamofire.request(Router.getRatings(params)).responseJSON { response in
            // error parse JSON?
            guard let json = response.result.value as? JSONArray else {
                completionHandler(.failure( self.parseError(response.result.error) ))
                return
            }
            
            let valorationsArray = json.compactMap { Valoration(json: $0) }
            let dataSource = ValorationsDataSource(valorations: valorationsArray)
            completionHandler(.success(dataSource))
        }
    }
    
    /// Report
    func fetchReport(with report: Report, completionHandler: @escaping (Bool) -> Void) {
        Alamofire.request(Router.report(report))
            .validate(statusCode: 200 ..< 300)
            .response { response in
                guard response.error == nil else {
                    completionHandler(false)
                    return
                }
                completionHandler(true)
        }
    }
}
