//
//  APIManager+Pay.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Alamofire

extension APIManager {
    /// Pay for services
    public func payServices(totalPaid: Double, installationId: String, selectedService: Service, selectedAddons: [Addon], completionHandler: @escaping (Result<[String: Any]>) -> Void) {
        let params = self.getParamsForBuyService(installationId: installationId, selectedService: selectedService, selectedAddons: selectedAddons, totalPaid: totalPaid)
        
        Alamofire.request(Router.buyService(params)).responseJSON { response in
            guard response.result.error == nil, let json = response.result.value as? JSONObject else {
                completionHandler(.failure( self.parseError(response.result.error) ))
                return
            }
            
            if let error = self.parseErrorResponse(json) {
                completionHandler(.failure(error))
                return
            }
            completionHandler(.success(json))
        }
    }
    
    func getParamsForBuyService(installationId: String, selectedService: Service, selectedAddons: [Addon], totalPaid: Double)->[String: Any]{
        let parameters: [String : Any] = ["type": "sdk_external_tpv",
                                          "total_paid": totalPaid,
                                          "installation": installationId,
                                          "services": [self.getServiceParams(selectedService: selectedService, selectedAddons: selectedAddons)]]
        
        return parameters
    }
    
    func getServiceParams(selectedService: Service, selectedAddons: [Addon])->[String: Any]{
        
        var addonIds:[Int] = []
        
        for addon in selectedAddons{
            addonIds.append(addon.id)
        }
        
        let parameters: [String : Any] = ["service": selectedService.id,
                                          "addons": addonIds]
        
        return parameters
    }
}
