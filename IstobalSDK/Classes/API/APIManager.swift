//
//  APIManager.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import Alamofire
import Locksmith

public class APIManager {
    public static let sharedInstance = APIManager()
    init() {}
    
    /// Access Token
    var accessToken: String? {
        set {
            guard let newValue = newValue else {
                _ = try? Locksmith.deleteDataForUserAccount(userAccount: Constants.Locksmith.accessToken)
                return
            }
            //swiftlint:disable:next unused_optional_binding
            guard let _ = try? Locksmith.updateData(data: [Constants.JSONKey.accessToken: newValue], forUserAccount: Constants.Locksmith.accessToken) else {
                _ = try? Locksmith.deleteDataForUserAccount(userAccount: Constants.Locksmith.accessToken)
                return
            }
        }
        get {
            let dictionary = Locksmith.loadDataForUserAccount(userAccount: Constants.Locksmith.accessToken)
            return dictionary?[Constants.JSONKey.accessToken] as? String
        }
    }
    
    /// User ID
    var userID: String? {
        set {
            guard let newValue = newValue else {
                _ = try? Locksmith.deleteDataForUserAccount(userAccount: Constants.Locksmith.userID)
                return
            }
            //swiftlint:disable:next unused_optional_binding
            guard let _ = try? Locksmith.updateData(data: [Constants.JSONKey.id: newValue], forUserAccount: Constants.Locksmith.userID) else {
                _ = try? Locksmith.deleteDataForUserAccount(userAccount: Constants.Locksmith.userID)
                return
            }
        }
        get {
            let dictionary = Locksmith.loadDataForUserAccount(userAccount: Constants.Locksmith.userID)
            return dictionary?[Constants.JSONKey.id] as? String
        }
    }
    
    // MARK: - API Helper
    
    /// Parse login response
    func parseLoginResponse(_ json: JSONObject, saveProfile: Bool = true) -> Bool {
        
        if let token = json[Constants.JSONKey.accessToken] as? String, let type = json[Constants.JSONKey.tokenType] as? String {
            accessToken = type + " " + token
            return true
        }
        return false
    }
    
    /// Parse user dictionary response
    func parseUserResponse(_ json: JSONObject) -> Bool {
        var mutableJSON = json
        
        if let uID = mutableJSON[Constants.JSONKey.id] as? NSNumber {
            userID = uID.stringValue
            mutableJSON[Constants.JSONKey.id] = "" // Delete user ID before saving
            saveUserProfile(mutableJSON)
            return true
        }
        return false
    }
    
    /// save user profile
    func saveUserProfile(_ json: JSONObject) {
        let data = NSKeyedArchiver.archivedData(withRootObject: json)
        UserDefaults.User.set(data, forKey: .user)
        UserDefaults.Account.set(LoginState.logged.rawValue, forKey: .loginState)
        
        if let cards = json["cards"] {
            APIHelper.saveCreditCards(with: cards)
        }
        
        if let vehicles = json["vehicles"] {
            APIHelper.saveMyVehicles(with: vehicles)
        }
        
        if let favorites = json["favouriteInstallations"] {
            APIHelper.saveMyFavorites(with: favorites)
        }
    }
    
    /// parse Error?
    func parseError(_ error: Error?) -> APIManagerError {
        let message = error?.localizedDescription ?? .localized(.error404)
        return APIManagerError.init(localizedTitle: message)
    }
    
    /// Parse error response
    //swiftlint:disable:next cyclomatic_complexity
    func parseErrorResponse(_ json: JSONObject) -> APIManagerError? {
        /// Error description
        if let errorDescription = json["error_description"] as? String {
            debugPrint("localized error: \(errorDescription)")
            return APIManagerError.init(localizedTitle: errorDescription)
        }
        
        /// Error code
        if let errorCode = json["errorCode"] as? Int {
            switch errorCode {
            case 101:
                return APIManagerError.init(localizedTitle: .localized(.error101))
            case 102:
                return APIManagerError.init(localizedTitle: .localized(.error102))
            case 103:
                return APIManagerError.init(localizedTitle: .localized(.error103))
            case 104:
                return APIManagerError.init(localizedTitle: .localized(.error104))
            case 105:
                return APIManagerError.init(localizedTitle: .localized(.error105))
            case 106:
                return APIManagerError.init(localizedTitle: .localized(.error106))
            case 107:
                return APIManagerError.init(localizedTitle: .localized(.error107))
            case 108:
                return APIManagerError.init(localizedTitle: .localized(.error108))
            case 202:
                return APIManagerError.init(localizedTitle: .localized(.error202))
            case 203:
                return APIManagerError.init(localizedTitle: .localized(.error203))
            case 204:
                return APIManagerError.init(localizedTitle: .localized(.error204))
            case 205:
                return APIManagerError.init(localizedTitle: .localized(.error205))
            case 301:
                return APIManagerError.init(localizedTitle: .localized(.error301))
            case 302:
                return APIManagerError.init(localizedTitle: .localized(.error302))
            case 303:
                return APIManagerError.init(localizedTitle: .localized(.error303))
            case 401:
                return APIManagerError.init(localizedTitle: .localized(.error401))
            default:
//                log.error("errorCode: \(errorCode)")
                debugPrint("errorCode: \(errorCode)")
                return APIManagerError.init(localizedTitle: .localized(.error404))
            }
        }
        
        /// error
        if let error = json["error"] as? String {
            return APIManagerError.init(localizedTitle: error)
        }
        
        /// violations
        if let violations = json["violations"] as? [[String: Any]] {
            let array = violations.compactMap { ErrorMessage(json: $0) }
            if array.count > 0 {
                let message = array.compactMap { String(describing: $0.description) }.joined(separator: " ")
                return APIManagerError.init(localizedTitle: message)
            }
        }
        
        return nil
    }
}
