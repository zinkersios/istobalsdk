//
//  Router.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    case logout(JSONObject)
    case checkVersion()
    case unsubscribeUser()
    case getVehicles()
    case getMe()
    case getTickets()
    case getNotifications()
    case getInstallations(JSONObject)
    case getNearbyFavInstallations(JSONObject)
    case getFavorites(JSONObject)
    case newVehicle(JSONObject)
    case registry(JSONObject)
    case editVehicle(Vehicle)
    case editTicketVehicle(Ticket)
    case deleteVehicle(String)
    case editUser(JSONObject)
    case createBilling(JSONObject)
    case assignBilling(JSONObject)
    case deleteBilling(JSONObject)
    case getBilling(JSONObject)
    case beacon(JSONObject)
    case newCreditCard(JSONObject)
    case pay(JSONObject)
    case loginWithFb(String)
    case loginWithGoogle(String)
    case login(String)
    case addFav(String)
    case deleteFav(String)
    case deleteCreditCard(String)
    case getInstallation(String)
    case getTicket(String)
    case startWash(String)
    case stopWash(String)
    case rate(Valoration)
    case getRatings(JSONObject)
    case registerDevice(JSONObject)
    case resetPassword(JSONObject)
    case redeemCode(JSONObject)
    case redeemOffer(JSONObject)
    case redeemGift(JSONObject)
    case sendGift(JSONObject)
    case report(Report)
    case buyService(JSONObject)
    
    //swiftlint:disable:next cyclomatic_complexity function_body_length
    func asURLRequest() throws -> URLRequest {
        // Add method
        var method: HTTPMethod {
            switch self {
            case .getMe, .getVehicles, .getTickets, .getTicket, .getRatings, .getBilling, .getNotifications:
                return .get
            case .getInstallations, .getFavorites, .getNearbyFavInstallations, .getInstallation:
                return .get
            case .checkVersion, .startWash, .stopWash, .redeemOffer:
                return .get
            case .login, .registry, .logout, .loginWithFb, .loginWithGoogle, .registerDevice, .resetPassword, .unsubscribeUser:
                return .post
            case .addFav, .newVehicle, .beacon, .newCreditCard, .pay, .rate, .redeemCode, .redeemGift, .sendGift, .report:
                return .post
            case .editVehicle, .editUser, .editTicketVehicle, .createBilling, .assignBilling, .deleteBilling:
                return .put
            case .deleteFav, .deleteCreditCard, .deleteVehicle:
                return .delete
            case .buyService:
                return .post
            }
        }
        
        // Add relative path
        let url: URL = {
            var url = URL(string: Constants.API.base)!
            let relativePath: String
            
            switch self {
            case .checkVersion:
                relativePath = "check-version"
            case .getMe:
                relativePath = "me"
            case .getTickets:
                relativePath = "tickets"
            case .getNotifications:
                relativePath = "notifications"
            case .getVehicles, .newVehicle:
                relativePath = "vehicles"
            case .editVehicle(let vehicle):
                relativePath = "vehicles/\(vehicle.id)"
            case .editTicketVehicle(let ticket):
                relativePath = "tickets/\(ticket.id)"
            case .deleteVehicle(let id):
                relativePath = "vehicles/\(id)"
            case .editUser, .createBilling, .assignBilling, .deleteBilling:
                let uID = APIManager.sharedInstance.userID ?? ""
                relativePath = "users/\(uID)"
            case .getInstallations:
                relativePath = "installations"
            case .getNearbyFavInstallations:
                relativePath = "installations/near-favourite"
            case .getFavorites:
                relativePath = "favourites/"
            case .getInstallation(let id):
                relativePath = "installation/\(id)/services"
            case .getTicket(let id):
                relativePath = "tickets/\(id)"
            case .getBilling:
                relativePath = "invoice_companies"
            case .registry:
                relativePath = "user/new"
            case .addFav(let id), .deleteFav(let id):
                relativePath = "installations/\(id)/favourite"
            case .logout:
                relativePath = "logout"
            case .login, .loginWithFb, .loginWithGoogle:
                url = URL(string: Constants.API.server)!
                relativePath = "smartwash/login"
            case .beacon:
                relativePath = "user-position"
            case .newCreditCard:
                relativePath = "cards"
            case .deleteCreditCard(let id):
                relativePath = "cards/\(id)"
            case .pay:
                relativePath = "pay"
            case .startWash(let id):
                relativePath = "tickets/\(id)/start"
            case .stopWash(let id):
                relativePath = "tickets/\(id)/stop"
            case .rate(let valoration):
                relativePath = "installations/\(valoration.installationID)/rate"
            case .getRatings:
                relativePath = "installation_ratings"
            case .registerDevice:
                relativePath = "register-device"
            case .resetPassword:
                relativePath = "reset-password"
            case .unsubscribeUser:
                relativePath = "unsubscribe"
            case .redeemCode:
                relativePath = "1/redeem-code"
            case .redeemOffer:
                relativePath = "check-offer"
            case .redeemGift:
                relativePath = "redeem-gift"
            case .sendGift:
                relativePath = "send-gift"
            case .report(let report):
                relativePath = "installations/\(report.installationID)/report"
            case .buyService:
                relativePath = "buy"
            }
            
            url.appendPathComponent(relativePath)
            return url
        }()
        
        // Add Parameters
        var params: (JSONObject?) = {
            switch self {
            case .checkVersion, .login, .loginWithFb, .loginWithGoogle,  .addFav, .startWash, .stopWash, .unsubscribeUser:
                return nil
            case .getMe, .getVehicles, .getRatings, .getNotifications:
                return nil
            case .deleteFav, .deleteCreditCard, .deleteVehicle:
                return nil
            case .getInstallations, .getFavorites, .getInstallation, .getNearbyFavInstallations, .redeemOffer, .getBilling:
                return nil
            case .getTicket, .getTickets:
                return nil
            case .newVehicle(let params), .beacon(let params), .newCreditCard(let params), .pay(let params):
                return (params)
            case .registry(let params), .resetPassword(let params), .registerDevice(let params):
                return (params)
            case .editUser(let params), .logout(let params), .createBilling(let params), .assignBilling(let params), .deleteBilling(let params):
                return (params)
            case .redeemCode(let params), .redeemGift(let params), .sendGift(let params):
                return (params)
            case .editVehicle(let vehicle):
                return vehicle.convertToParamaters()
            case .editTicketVehicle(let ticket):
                return ticket.paramatersForEditVehicle()
            case .rate(let valoration):
                return valoration.convertToDictionary()
            case .report(let report):
                return report.paramaters()
            case .buyService(let params):
                return (params)
            }
        }()
        
        // check if login is with social network?
        var loginType = Backend.normal
        
        // Add String Parameters
        let stringParams: (String?) = {
            switch self {
            case .login(let params):
                return (params)
            case .loginWithFb(let params):
                loginType = .facebook
                return (params)
            case .loginWithGoogle(let params):
                loginType = .google
                return (params)
            default: return nil
            }
        }()
        
        /// URL Request
        var urlRequest = URLRequest(url: url)
        switch self {
        case .getInstallations(let params), .getFavorites(let params), .getNearbyFavInstallations(let params),
             .getRatings(let params), .redeemOffer(let params), .getBilling(let params):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: params)
        default:()
        }
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json", forHTTPHeaderField: "accept")
        urlRequest.setValue(Bundle.main.releaseVersionNumberPretty, forHTTPHeaderField: "x-app-version")
        urlRequest.setValue(Constants.Device.platform, forHTTPHeaderField: "x-os")
        urlRequest.setValue(Locale.current.languageCode, forHTTPHeaderField: "Accept-Language")
        urlRequest.setValue(Constants.API.companyId, forHTTPHeaderField: "x-client-id")
        
        /// Set token if we have one
        if let token = APIManager.sharedInstance.accessToken {
            urlRequest.setValue("\(token)", forHTTPHeaderField: Constants.Key.authorization)
        }
        
        /// set string parameters (only for login)
        if let stringParams = stringParams {
            var credentialData = "\(Constants.API.clientID):\(Constants.API.clientSecret)".data(using: String.Encoding.utf8)!
            
            switch loginType {
            case .facebook:
                credentialData = "\(Constants.API.fbClientID):\(Constants.API.fbClientSecret)".data(using: String.Encoding.utf8)!
            case .google:
                credentialData = "\(Constants.API.googleClientID):\(Constants.API.googleClientSecret)".data(using: String.Encoding.utf8)!
            default:()
            }
            
            let base64Credentials = credentialData.base64EncodedString()
            urlRequest.setValue("Basic \(base64Credentials)", forHTTPHeaderField: Constants.Key.authorization)
            urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
            let data = stringParams.data(using:String.Encoding.ascii, allowLossyConversion: false)
            urlRequest.httpBody = data
            params = nil
        }
        
        let encoding = JSONEncoding.prettyPrinted
        return try encoding.encode(urlRequest, with: params)
    }
}
