//
//  MyVehicleCollectionViewCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 13/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

class MyVehicleCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlets
    @IBOutlet var ring: UICircularProgressRingView!
    @IBOutlet var viewRegistrationNumber: UIView!
    @IBOutlet var avatarView: AvatarView!
    
    @IBOutlet var lblDirty: UILabel!
    @IBOutlet var lblDays: UILabel!
    @IBOutlet var lblPercentage: UILabel!
    @IBOutlet var lblVehicleName: UILabel!
    @IBOutlet var lblRegistrationNumber: UILabel!
    @IBOutlet var lblNumberWashes: UILabel!
    @IBOutlet weak var lblTotalWashes: UILabel!
    @IBOutlet weak var lblValueOfTotalWashes: UILabel!
    @IBOutlet weak var lblWashes: UILabel!
    
    // Constraints
    @IBOutlet var topVehicleConstraint: NSLayoutConstraint!
    @IBOutlet var leadingVehicleConstraint: NSLayoutConstraint!
    @IBOutlet var bottomVehicleConstraint: NSLayoutConstraint!
    @IBOutlet var trailingVehicleConstraint: NSLayoutConstraint!
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        var imagePadding = CGFloat(11)

       self.setUpColors()
        viewRegistrationNumber.layer.cornerRadius = 2
        
        if Device.size() == .screen4Inch {
            imagePadding = CGFloat(10)
        }
        
        if Device.size() == .screen4Inch || Device.size() == .screen4_7Inch {
            leadingVehicleConstraint.constant = imagePadding
            trailingVehicleConstraint.constant = imagePadding
            topVehicleConstraint.constant = imagePadding
            bottomVehicleConstraint.constant = imagePadding
        }
        
        lblDirty.text = .localized(.lastWash)
        lblDays.text = .localized(.ago)
        lblTotalWashes.text = .localized(.totalWashes)
        // set fonts
        lblDirty.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance        
        lblRegistrationNumber.font = Font(.installed(.helveticaNeueMedium), size: .standard(.h2_5)).instance
        
        avatarView.setImage(UIImage(named: "av_smart", in: SDKNavigation.getBundle(), compatibleWith: nil))
        avatarView.enableShadow = true
    }
    
    func setUpColors(){
        viewRegistrationNumber.backgroundColor = SDKColor.navigatioBarBackground
        lblRegistrationNumber.textColor = SDKColor.navigationBarTint
        
        lblTotalWashes.textColor = SDKColor.secondaryLabel
        lblWashes.textColor = SDKColor.primaryLabel
        
        lblDirty.textColor = SDKColor.secondaryLabel
        lblDays.textColor = SDKColor.primaryLabel
        
        lblVehicleName.textColor = SDKColor.primaryLabel
        lblNumberWashes.textColor = SDKColor.secondaryLabel
        


    }
    
    func setUp(with vehicle: Vehicle?) {
        guard let vehicle = vehicle else {
            return
        }
        
        let viewModel = VehicleViewModel(model: vehicle)
        
        lblVehicleName.text = vehicle.name
        lblRegistrationNumber.text = vehicle.licenseNumber
        lblPercentage.text = viewModel.dirtDays
        lblNumberWashes.text = viewModel.users
        lblValueOfTotalWashes.text = viewModel.washes
        lblWashes.text = viewModel.washesText
        setUpProgress(for: vehicle)
    }
    
    private func setUpProgress(for vehicle: Vehicle) {
        if vehicle.dirtIndex >= 100 {
            ring.value = 100
            ring.ringStyle = .ontop
            ring.innerRingColor = Color.blueSecondary.value
        } else {
            ring.gradientColors = [Color.blue.value, Color.green.value]
            ring.ringStyle = .gradient
            ring.value = CGFloat(vehicle.dirtIndex)
        }
    }
}
