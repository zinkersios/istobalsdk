//
//  FinalizePurchaseView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 9/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class FinalizePurchaseView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var gradientView: UIView!    
    @IBOutlet var lblMakePayment: UILabel!
    @IBOutlet var lblPrice: UILabel!    
    
    // Properties
    var onButtonTap:(() -> Void)?    
    
    /// for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    // for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        SDKNavigation.getBundle().loadNibNamed("FinalizePurchaseView", owner: self, options: [:])
        addSubview(contentView)
        
        /// Setup constraints
        contentView.translatesAutoresizingMaskIntoConstraints = false                
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        /// set gradient
        let frame = CGRect(x: 0, y: 0, width: Constants.Size.screenWidth, height: self.frame.size.height)
        let gradient = Gradient.get(with: .blue, size: frame)
        contentView.layer.insertSublayer(gradient, at: 0)
        
        /// set styles
        lblMakePayment.text = Utils.capitalisedText(with: .localized(.makePayment))
        lblMakePayment.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        lblPrice.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
    }
    
    // MARK: - IBActions
    @IBAction func makePayment(_ sender: Any) {
        onButtonTap?()
    }
}

extension FinalizePurchaseView {
    func update(price: Double, offer: Offer) {
        if offer.type == .coupon {
            let precio = PriceUtils.priceToCurrency(with: price)
            let discount = PriceUtils.calculatePercent(ciento: Double(offer.discount), quantity: price)
            let discountedPrice = PriceUtils.priceToCurrency(with: (price - discount)) + "    "
            
            let font = Font(.installed(.titilliumBold), size: .standard(.h3)).instance
            let attrs1 = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.white]
            let attrs2 = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.red]
            
            let attributedString1 = NSMutableAttributedString(string: discountedPrice, attributes: attrs1)
            let attributedString2 = NSMutableAttributedString(string: precio, attributes: attrs2)
            // Set underline
            attributedString2.addAttribute(NSAttributedStringKey.strikethroughStyle,
                                           value: NSNumber(value: NSUnderlineStyle.styleThick.rawValue),
                                           range: NSMakeRange(0, attributedString2.length))
            
            attributedString1.append(attributedString2)
            lblPrice.attributedText = attributedString1
        } else {
            lblPrice.text = PriceUtils.priceToCurrency(with: price)
        }
    }
}
