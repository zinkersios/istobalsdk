//
//  CenteredTableView.swift
//  istobal
//
//  Created by Shanti Rodríguez on 19/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class CenteredTableView: UITableView {

    override func reloadData() {
        super.reloadData()
        centerTableContentsIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        centerTableContentsIfNeeded()
    }
    
    func centerTableContentsIfNeeded() {
        let totalHeight = bounds.height
        let contentHeight = contentSize.height
        let contentCanBeCentered = contentHeight < totalHeight
        
        if contentCanBeCentered {
            contentInset = UIEdgeInsets(top: ceil(totalHeight/2 - contentHeight/2), left: 0, bottom: 0, right: 0)
        } else {
            contentInset = .zero
        }
    }
}
