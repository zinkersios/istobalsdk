//
//  EmptyStateView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 6/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

enum EmptyStateViewType {
    case myVehicles
    case installation
    case ticket
}

class EmptyStateView: UIView {

    // MARK: - IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblError: UILabel!
    @IBOutlet var btnTryAgain: UIButton!
    @IBOutlet var stackViewError: UIStackView!
    @IBOutlet var imgBackground: UIImageView!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    // MARK: - Properties
    var onButtonTap:(() -> Void)?
    
    // MARK: - Set Up
    /// for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    // for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        SDKNavigation.getBundle().loadNibNamed("EmptyStateView", owner: self, options: [:])
        addSubview(contentView)
        
        /// Set up constraints
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        lblError.text = .localized(.errorConnection)
        btnTryAgain.setTitle(.localized(.tryAgain), for: .normal)
        spinner.color = SDKColor.navigationBarTint
        lblTitle.textColor = SDKColor.navigationBarTint
    }
    
    private func setUpByDefault() {
//        lblTitle.textColor = SDKColor.navigationBarTint
////        spinner.activityIndicatorViewStyle = .gray
//        spinner.color = SDKColor.navigationBarTint
////        spinner.backgroundColor = SDKColor.navigationBarTint
        spinner.startAnimating()
    }
    
    fileprivate func setUpVehicles() {
//        lblTitle.textColor = .white
        lblTitle.text = .localized(.gettingVehicles)
        spinner.startAnimating()
        imgBackground.isHidden = false
    }
    
    fileprivate func setUpInstallation() {
        setUpByDefault()
        contentView.backgroundColor = SDKColor.appBackground
        lblTitle.text = .localized(.loadingWait)
    }
    
    fileprivate func setUpForTicket() {
        setUpByDefault()
        contentView.layer.cornerRadius = StyleConstants.CornerRadius.ticket
        contentView.backgroundColor = SDKColor.appBackground
        lblTitle.text = .localized(.loadingTicket)
        self.isHidden = false
    }
    
    fileprivate func setUpView(with state: APIState, type: EmptyStateViewType) {
        spinner.stopAnimating()
        lblTitle.text = ""
        
        switch state {
        case .noResults:
            stackViewError.isHidden = true
            setUpNoResults(with: type)
        case .noInternet:
            stackViewError.isHidden = false
        default:
            stackViewError.isHidden = true
            lblTitle.text = (type == .ticket) ? .localized(.errorGettingTicket) : .localized(.errorGeneral)
        }
    }
}

// MARK: - Public
extension EmptyStateView {
    func setUp(for type: EmptyStateViewType) {
        stackViewError.isHidden = true
        
        switch type {
        case .myVehicles:
            setUpVehicles()
        case .installation:
            setUpInstallation()
        case .ticket:
            setUpForTicket()
        }
    }
    
    func handleError(_ error: Error?, type: EmptyStateViewType) {
        if let error = error as? APIManagerError, error.code == Constants.HTTPCode.unauthorised {
//            log.error("forceLogOut")
            debugPrint("forceLogOut")
            setUpView(with: .error, type: type)
        } else if !ReachabilityManager.shared.isNetworkAvailable {
            setUpView(with: .noInternet, type: type)
        } else {
            setUpView(with: .error, type: type)
        }
    }
    
    func setUpNoResults(with type: EmptyStateViewType) {
        switch type {
        case .myVehicles:
            lblTitle.text = .localized(.emptyVehicle)
        case .installation:
            lblTitle.text = .localized(.errorGeneral)
        case .ticket:
            lblTitle.text = .localized(.errorGettingTicket)
        }
        
        spinner.stopAnimating()
    }
}

// MARK: - IBActions
extension EmptyStateView {
    @IBAction func buttonTap(_ sender: Any) {
        onButtonTap?()
    }
}
