//
//  ConcaveCornersView.swift
//  istobal
//
//  Created by Shanti Rodríguez on 4/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class ConcaveCornersView: UIView {
    
    // MARK: - Public
    var corners = [UIRectCorner]()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.mask = concaveCorners()
    }
    
    // MARK: - Private Methos
    /// reverse UIBezierPath for any sided corners
    private func concaveCorners() -> CAShapeLayer {
        let mask: CAShapeLayer = CAShapeLayer()
        mask.frame = self.layer.bounds
        
        let rect = mask.bounds
        let cornerR = cornerRadius(cornerRadius: StyleConstants.CornerRadius.main, rect: rect)
        let path = UIBezierPath()
        
        /// Top Left
        let topLeft = CGPoint(x: rect.minX, y: rect.minY)
        
        if corners.contains(.topLeft) {
            path.move(to: CGPoint(x: topLeft.x, y: topLeft.y + cornerR))
            path.addArc(withCenter: topLeft, radius: cornerR, startAngle: CGFloat(Double.pi/2), endAngle: 0, clockwise: false)
        } else {
            path.move(to: topLeft)
        }
        
        /// Top Right
        let topRight = CGPoint(x: rect.maxX, y: rect.minY)
        if corners.contains(.topRight) {
            path.addLine(to: CGPoint(x: topRight.x - cornerR, y: topRight.y))
            path.addArc(withCenter: topRight, radius: cornerR, startAngle: CGFloat(Double.pi), endAngle: CGFloat(Double.pi/2), clockwise: false)
        } else {
            path.addLine(to: topRight)
        }
        
        /// Bottom Right
        let bottomRight = CGPoint(x: rect.maxX, y: rect.maxY)
        if corners.contains(.bottomRight) {
            path.addLine(to: CGPoint(x: bottomRight.x, y: bottomRight.y - cornerR))
            path.addArc(withCenter: bottomRight, radius: cornerR, startAngle: CGFloat((Double.pi/2) * 3), endAngle: CGFloat(Double.pi), clockwise: false)
        } else {
            path.addLine(to: bottomRight)
        }
        
        /// Bottom Left
        let bottomLeft = CGPoint(x: rect.minX, y: rect.maxY)
        if corners.contains(.bottomLeft) {
            path.addLine(to: CGPoint(x: bottomLeft.x + cornerR, y: bottomLeft.y))
            path.addArc(withCenter: bottomLeft, radius: cornerR, startAngle: 0, endAngle: CGFloat((Double.pi/2) * 3), clockwise: false)
        } else {
            path.addLine(to: bottomLeft)
        }
        
        path.close()
        mask.path = path.cgPath
        return mask
    }
    
    private func cornerRadius(cornerRadius: CGFloat, rect: CGRect) -> CGFloat {
        let halfWidth = rect.width / 2
        let halfHeight = rect.height / 2
        var cornerR = cornerRadius
        
        if cornerRadius > halfWidth || cornerRadius > halfHeight {
            cornerR = min(halfHeight, halfWidth)
        }
        return cornerR
    }
}
