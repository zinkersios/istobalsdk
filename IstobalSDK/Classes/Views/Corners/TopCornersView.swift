//
//  TopCornersView.swift
//  EMS
//
//  Created by Santiago Sánchez Rodríguez on 4/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class TopCornersView: UIView {

    var radius: CGFloat = StyleConstants.CornerRadius.main
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.round(corners: [.topLeft, .topRight], radius: radius)
    }
}
