//
//  ServiceCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 4/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class InstallationServiceCell: UITableViewCell {
    // MARK: - IBOutlets
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    // MARK: - Properties
    var onButtonTap:((_ action: InstallationAction) -> Void)?
    private var typeOfService: TypeOfService?
    private var installationID = 0
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let padding = CGFloat(16) // top and bottom
        containerHeightConstraint.constant = heightForServiceCell() - padding
        containerView.layer.cornerRadius = StyleConstants.CornerRadius.main
        containerView.addDropShadow(.cell)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Actions
    @IBAction func selectedService(_ sender: Any) {
        guard let typeOfService = typeOfService else {
            return
        }
        
        #if !DDEVELOPMENT
        if installationID == Constants.InstallationID.ShowroomAlcudia {
            if typeOfService.name == Constants.String.BookAppointment {
                onButtonTap?(.bookAppointment)
            } else {
                onButtonTap?(.alert(title: typeOfService.name, message: .localized(.emptyServices)))
            }
            return
        }
        #endif
        
        guard let services = typeOfService.services else {
            onButtonTap?(.alert(title: typeOfService.name, message: .localized(.emptyServices)))
            return
        }
        
        if typeOfService.hasBalance {
            onButtonTap?(.buyService(service: typeOfService))
        } else {
            onButtonTap?(.buyWash(services: services))
        }
    }
}

// MARK: - Public Methods
extension InstallationServiceCell {
    func setUp(with typeOfService: TypeOfService, installationID: Int) {
        self.typeOfService = typeOfService
        self.installationID = installationID
        lblTitle.text = typeOfService.name
        lblSubtitle.text = typeOfService.description
        
        // set image
        if let url = typeOfService.image {
            imgIcon.kf.setImage(with: url)
        }
        
        if typeOfService.services == nil {
            containerView.backgroundColor = Color.gray6.value
        }
        
        #if !DDEVELOPMENT
        if installationID == Constants.InstallationID.ShowroomAlcudia {
            if typeOfService.name == Constants.String.BookAppointment {
                containerView.backgroundColor = .white
            } else {
                containerView.backgroundColor = Color.gray6.value
            }
        }
        #endif
    }
    
    func setUp(with data: Any?, installationID: Int) {
        guard let typeOfService = data as? TypeOfService else {
            return
        }
        
        setUp(with: typeOfService, installationID: installationID)
    }
}
