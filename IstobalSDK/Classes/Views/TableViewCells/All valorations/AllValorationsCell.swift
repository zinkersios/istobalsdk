//
//  AllValorationsCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 3/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class AllValorationsCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var btnShowAll: UIButton!
    @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    // MARK: - Properties
    var onButtonTap:((_ action: InstallationAction) -> Void)?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        btnShowAll.backgroundColor = Color.gray5.value
        btnShowAll.setTitle(Utils.capitalisedText(with: .localized(.allRatings)), for: .normal)
        btnHeightConstraint.constant = defaultButtonHeight()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Actions
    @IBAction func showAll(_ sender: Any) {
        onButtonTap?(.valorations)
    }
}
