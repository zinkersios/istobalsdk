//
//  YourTicketsCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 4/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class YourTicketsCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var gradientView: TopCornersView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTicketNumber: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnStatus: UIButton!
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let cornerRadius = CGFloat(6)
        containerView.layer.cornerRadius = cornerRadius
        containerView.addDropShadow(.cell)
 
        gradientView.radius = cornerRadius
        
        // add gradient
        let frame = CGRect(x: 0, y: 0, width: gradientView.bounds.width, height: gradientView.bounds.size.height)
        let gradient = Gradient.get(with: .blue, size: frame)
        gradientView.layer.addSublayer(gradient)
        
        btnStatus.setTitle(.localized(.ticketActive), for: .normal)        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
