//
//  TicketCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 10/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class TicketCell: UITableViewCell {
    
    @IBOutlet var imgLogo: RoundedImageView!
    @IBOutlet var avatarView: AvatarView!
    @IBOutlet var lblService: UILabel!
    @IBOutlet var lblInstallationName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var viewStatus: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarView.enableShadow = true
        avatarView.setImage(Constants.Image.placeholderInstallation)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setAvatarImage(url: URL?) {
        if let link = url {
            avatarView.imageView.kf.setImage(with: link, placeholder: Constants.Image.placeholderInstallation)
        } else {
            avatarView.imageView.image = Constants.Image.placeholderInstallation
        }
    }
}

// MARK: - Public Methods
extension TicketCell {
    func setUp(with ticket: Ticket) {
        let vm = TicketViewModel(model: ticket)
        
        lblService.text = .localized(.ticket) + " " + vm.income
        lblDate.text = vm.purchasedOn
        
        // set image
        viewStatus.backgroundColor = ticket.status.color
        if ticket.status == .redeemed {
            lblInstallationName.text = ticket.fidelityCardName
            setAvatarImage(url: ticket.fidelityCardLogo)
        } else {
            lblInstallationName.text = ticket.installation?.companyName
            setAvatarImage(url: ticket.installation?.image)
        }
    }
}

