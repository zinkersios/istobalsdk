//
//  ContactDetailNameCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 7/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

class ContactDetailNameCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        lblName.font = Font(.custom(SDKFont.bold), size: .standard(.h1_5)).instance
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public
    func setUp(with data: InstallationContactData) {
        lblName.text = data.name.uppercased()
    }
}
