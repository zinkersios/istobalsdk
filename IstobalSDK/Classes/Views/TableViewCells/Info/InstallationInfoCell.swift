//
//  InstallationInfoCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 3/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class InstallationInfoCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet var btnViewRoute: UIButton!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var btnPhone: UIButton!
    @IBOutlet var btnKM: UIButton!
    // MARK: - Properties
    private var installation: Installation?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - Methods
extension InstallationInfoCell {
    func setUp(with viewModel: InstallationViewModel, userHasLocation: Bool) {
        installation = viewModel.installation
        lblLocation.text = viewModel.address
        lblTime.text = viewModel.schedule
        btnPhone.setTitle(viewModel.phone, for: .normal)
        
        if userHasLocation, viewModel.distanceHumanReadable.count > 1 {
            btnKM.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h4)).instance
            btnKM.setTitle("  " + viewModel.distanceHumanReadable + "  ", for: .normal)
            btnKM.alpha = 1.0
        }
    }
}

// MARK: - IBActions
extension InstallationInfoCell {
    @IBAction func viewRoute(_ sender: Any) {
        Maps.openMaps(with: installation)
    }
    
    @IBAction func call(_ sender: Any) {
        Utils.callNumber(with: installation?.phone)
    }
}
