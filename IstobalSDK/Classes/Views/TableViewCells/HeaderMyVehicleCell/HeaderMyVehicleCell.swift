//
//  HeaderMyVehicleCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

class HeaderMyVehicleCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet var btnBuy: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var grayView: UIView!
    @IBOutlet var shadowForButton: UIView!
    @IBOutlet var topView: UIView!
            
    override func awakeFromNib() {
        super.awakeFromNib()
        grayView.backgroundColor = Color.whiteSmoke.value
        lblTitle.text = .localized(.lastWashes)
        
        btnBuy.setTitle( Utils.capitalisedText(with: .localized(.buyWash)), for: .normal)
        btnBuy.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h1_5)).instance
        btnBuy.backgroundColor = Color.green.value
        btnBuy.setBackgroundColor(color: UIColor.black.withAlphaComponent(0.4), forState: .highlighted)
        btnBuy.layer.cornerRadius = 33
        
        if Device.size() == .screen4_7Inch {
            btnBuy.layer.cornerRadius = 31
        } else if Device.size() == .screen4Inch {
            btnBuy.layer.cornerRadius = 28
        }
        
        // set shadow under btnBuy
        shadowForButton.layer.cornerRadius = btnBuy.layer.cornerRadius
        shadowForButton.layer.masksToBounds = false
        shadowForButton.backgroundColor = Color.green.value
        shadowForButton.addDropShadow(.byDefault)        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
