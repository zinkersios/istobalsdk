//
//  BuyWashCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 7/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class BuyWashCell: UITableViewCell {

    @IBOutlet var containerView: UIView!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel!    
    @IBOutlet var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblPrice.textColor = Color.blue.value
        lblPrice.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        containerView.layer.cornerRadius = 44
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setAttributedText(with title: String, subtitle: String) {
        let boldText = title + "\n"
        let attrs = [NSAttributedStringKey.font: Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance,
                     NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        
        let attributedString = NSMutableAttributedString(string: boldText, attributes: attrs)
        let normalString = NSMutableAttributedString(string: subtitle)
        
        attributedString.append(normalString)
        lblTitle.attributedText = attributedString
    }
    
    // MARK: - Public
    func setUp(with wash: Wash) {
        setAttributedText(with: wash.name, subtitle: wash.description)
        lblPrice.text = wash.price
        imgIcon.image = UIImage(named: wash.imageName, in: SDKNavigation.getBundle(), compatibleWith: nil)
    }
}
