//
//  DuringWashingCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 21/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class DuringWashingCell: UITableViewCell {

    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnPrice: PriceButton!
    @IBOutlet var lblOnly: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblOnly.text = .localized(.only)
        btnPrice.setPrice(with: "1")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
