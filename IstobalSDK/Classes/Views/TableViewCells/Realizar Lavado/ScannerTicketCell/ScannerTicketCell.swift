//
//  ScannerTicketCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class ScannerTicketCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet var containerView: UIView!
    @IBOutlet var gradientView: TopCornersView!
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var lblTicketNumber: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnStatus: UIButton!
    @IBOutlet var btnStart: UIButton!
    
    // MARK: - Properties
    var onButtonTap:(() -> Void)?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let cornerRadius = CGFloat(6)
        containerView.layer.cornerRadius = cornerRadius
        
        // add gradient
        let frame = CGRect(x: 0, y: 0, width: gradientView.bounds.width, height: gradientView.bounds.size.height)
        let gradient = Gradient.get(with: .blue, size: frame)
        gradientView.radius = cornerRadius
        gradientView.layer.addSublayer(gradient)
        
        // styles
        lblTicketNumber.font = Font(.custom(SDKFont.bold), size: .standard(.h4_5)).instance
        lblTicketNumber.textColor = UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.5)
        
        btnStatus.setTitle( Utils.capitalisedText(with: .localized(.ticketActive)), for: .normal)
        btnStart.setTitle( Utils.capitalisedText(with: .localized(.startWash)), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func startWash(_ sender: Any) {
        onButtonTap?()
    }
}
