//
//  TerminalWithViewCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 9/11/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class TerminalWithViewCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet weak var viewCode: UIView!
    @IBOutlet weak var btnShowQR: UIButton!
    
    var onButtonTap:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.textColor = Color.blue.value
        lblTitle.font = Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance
        
        lblSubtitle.textColor = Color.mirage.value
        lblSubtitle.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
        
        lblCode.textColor = UIColor(red:0.15, green:0.33, blue:0.43, alpha:1)
        lblCode.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
        
        viewCode.backgroundColor = Color.gray7.value
        viewCode.layer.cornerRadius = 4
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with instruction: TerminalInstruction, typeOfService: TypeOfService?) {
        lblTitle.text = instruction.title
        lblSubtitle.text = instruction.body
        imgLogo.image = UIImage(named: instruction.image, in: SDKNavigation.getBundle(), compatibleWith: nil)
        
        if let typeOfService = typeOfService, let code = typeOfService.code {
            lblCode.text = code
        } else {
            lblCode.text = ""                        
        }
    }
    
    @IBAction func showQR(_ sender: Any) {
        onButtonTap?()
    }
}
