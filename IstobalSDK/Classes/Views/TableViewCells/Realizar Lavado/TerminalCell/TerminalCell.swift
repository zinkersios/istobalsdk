//
//  TerminalCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class TerminalCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var imgLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.textColor = Color.blue.value
        lblTitle.font = Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance
        
        lblSubtitle.textColor = Color.mirage.value
        lblSubtitle.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with instruction: TerminalInstruction) {
        lblTitle.text = instruction.title
        lblSubtitle.text = instruction.body
        imgLogo.image = UIImage(named: instruction.image, in: SDKNavigation.getBundle(), compatibleWith: nil)
    }
}
