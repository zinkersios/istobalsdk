//
//  InstructionCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class InstructionCell: UITableViewCell {

    @IBOutlet var btnStep: RoundedButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnStep.backgroundColor = Color.blue.value
        btnStep.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        
        lblTitle.textColor = Color.astral.value
        lblTitle.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        
        lblSubtitle.textColor = Color.atomic.value
        lblSubtitle.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with instruction: Instruction) {
        btnStep.setTitle(instruction.step, for: .normal)
        lblTitle.text = instruction.title
        lblSubtitle.text = instruction.body
    }
}
