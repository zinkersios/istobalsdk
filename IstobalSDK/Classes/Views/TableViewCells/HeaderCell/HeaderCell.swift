//
//  HeaderCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblBigTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Color.gray7.value
        
        lblBigTitle.textColor = Color.gray3.value
        lblBigTitle.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(_ section: PurchaseSummaryTableSection) {
        switch section {
        case .services:
            lblTitle.text = .localized(.services)
        case .payment:
            lblTitle.text = .localized(.paymentMethod)
        case .coupon:
            lblTitle.text = .localized(.discountCoupon)
        default:()
        }
    }
    
    func setUpForServiceInfo(with ticket: Ticket?) {                
        if ticket?.vehicleID != nil {
            lblBigTitle.text = .localized(.serviceInfoHeader1)
        } else {
            lblBigTitle.text = .localized(.serviceInfoHeader2)
        }
        
        lblBigTitle.setLineHeight(0.88)
    }
}
