//
//  AlertViewSubtitleCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 19/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Device

final class AlertViewSubtitleCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.textColor = Color.atomic.value
        lblTitle.font = Font(.custom(SDKFont.regular), size: .standard(.h2)).instance
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public
    func setUp(type: AlertViewControllerType, extraInformation: String) {
        switch type {
        case .giftReceived:
            lblTitle.text = String(format: .localized(.giftReceivedInfo), extraInformation)
//        case .schedule, .loyaltyCardInfo:
//            lblTitle.text = extraInformation
        default:
            lblTitle.text = type.subtitle
        }
        
        if type == .billingInfo && (Device.size() == .screen4Inch || Device.size() == .screen4_7Inch) {
            bottomConstraint.constant = 15
            
            if Device.size() == .screen4Inch {
                lblTitle.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
            }
        }
    }
}
