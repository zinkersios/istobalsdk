//
//  AlertViewTextFieldCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class AlertViewTextFieldCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var textField: UITextField!
    // MARK: - Properties
//    private var type = AlertViewControllerType.selectAmount
    var onUpdatedText:((_ text: String) -> Void)?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public
    func setUp(type: AlertViewControllerType) {
//        self.type = type
    }
}

// MARK: - Actions
extension AlertViewTextFieldCell {
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        let currentText = sender.text ?? ""
        self.onUpdatedText?(currentText)
    }
}

extension AlertViewTextFieldCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if type != .selectAmount {
//            return true
//        }
//
//        if string.isEmpty { return true }
//
//        let currentText = textField.text ?? ""
//        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
//        return FormManager.isValidDouble(text: replacementText)
        return true
    }
}
