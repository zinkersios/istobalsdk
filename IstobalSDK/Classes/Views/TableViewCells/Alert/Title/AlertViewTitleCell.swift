//
//  AlertViewTitleCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 19/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Device

final class AlertViewTitleCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewTop: TopCornersView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public
    func setUp(type: AlertViewControllerType) {
        lblTitle.text = type.title
        viewTop.backgroundColor = type.cellColor
        
        if type == .billingInfo && (Device.size() == .screen4Inch || Device.size() == .screen4_7Inch) {
            topConstraint.constant = 15
            bottomConstraint.constant = 15
        }
        
//        if type == .selectAmount {
//            lblTitle.textColor = .white
//        }
    }
}
