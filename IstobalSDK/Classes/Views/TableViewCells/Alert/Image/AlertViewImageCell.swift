//
//  AlertViewImageCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class AlertViewImageCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public
    func setUp(type: AlertViewControllerType) {
        lblTitle.text = type.title
        
//        if type == .schedule {
//            imgIcon.image = UIImage(named: "img_horarios")
//        }
    }
}
