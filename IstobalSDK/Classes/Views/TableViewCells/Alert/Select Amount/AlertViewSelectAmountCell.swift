//
//  AlertViewSelectAmountCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 6/8/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class AlertViewSelectAmountCell: UITableViewCell {
    // MARK: - IBOutlets
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var lblDot: UILabel!
    @IBOutlet weak var lblComma: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var lblDecimals: UILabel!
    @IBOutlet weak var lblHundredths: UILabel!
    @IBOutlet weak var lblThousands: UILabel!
    // MARK: - Properties
    public private(set) var decimals: Double = 0
    public private(set) var hundredths: Double = 0
    public private(set) var thousands: Double = 0
    var onUpdatedText:((_ text: String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private Methods
    private func setUpView() {
        let bgColor = UIColor(red:0, green:0, blue:0, alpha:0.15)
        view1.backgroundColor = bgColor
        view2.backgroundColor = bgColor
        view3.backgroundColor = bgColor
        view1.layer.cornerRadius = StyleConstants.CornerRadius.main
        view2.layer.cornerRadius = StyleConstants.CornerRadius.main
        view3.layer.cornerRadius = StyleConstants.CornerRadius.main
        
        lblDot.font = Font(.custom(SDKFont.light), size: .custom(70)).instance
        lblComma.font = Font(.custom(SDKFont.light), size: .custom(70)).instance
        lblCurrency.font = Font(.custom(SDKFont.light), size: .standard(.header)).instance
        
        lblThousands.font = Font(.custom(SDKFont.light), size: .custom(50)).instance
        lblHundredths.font = Font(.custom(SDKFont.light), size: .custom(50)).instance
        lblDecimals.font = Font(.custom(SDKFont.light), size: .custom(50)).instance
    }
    
    private func updateDecimals() {
        lblDecimals.text = convertToString(value: decimals)
        sendTotal()
    }
    
    private func updateHundredths() {
        let str = String(format: "%.2f", hundredths)
        lblHundredths.text = str.replacingOccurrences(of: ".", with: "")
        sendTotal()
    }
    
    private func updateThousands() {
        lblThousands.text = convertToString(value: thousands)
        sendTotal()
    }
    
    private func convertToString(value: Double) -> String {
        let str = String(format: "%.1f", value)
        return str.replacingOccurrences(of: ".", with: "")
    }
    
    private func sendTotal() {
        if thousands > 0 {
            let total = lblThousands.text! + lblHundredths.text! + "," + lblDecimals.text!
            onUpdatedText?(total)
        } else {
            let total = lblHundredths.text! + "," + lblDecimals.text!
            onUpdatedText?(total)
        }
    }
}

// MARK: - Actions
extension AlertViewSelectAmountCell {
    // MARK: Decimals
    @IBAction func increaseDecimals(_ sender: Any) {
        decimals += 0.5
        updateDecimals()
    }
    
    @IBAction func decreaseDecimals(_ sender: Any) {
        if decimals == 0 { return }
        decimals -= 0.5
        updateDecimals()
    }
    
    // MARK: Hundredths
    @IBAction func increaseHundredths(_ sender: Any) {
        hundredths += 0.01
        updateHundredths()
    }
    
    @IBAction func decreaseHundredths(_ sender: Any) {
        if hundredths == 0 { return }
        hundredths -= 0.01
        updateHundredths()
    }
    
    // MARK: Thousands
    @IBAction func increaseThousands(_ sender: Any) {
        thousands += 0.1
        updateThousands()
    }
    
    @IBAction func decreaseThousands(_ sender: Any) {
        if thousands == 0 { return }
        thousands -= 0.1
        updateThousands()
    }
}

// MARK: - Public
extension AlertViewSelectAmountCell {
    func setUp(type: AlertViewControllerType) {
        backgroundColor = type.cellColor
    }
}
