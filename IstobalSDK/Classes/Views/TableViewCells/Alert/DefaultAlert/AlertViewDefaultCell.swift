//
//  AlertViewDefaultCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 19/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class AlertViewDefaultCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var btnDefault: UIButton!
    @IBOutlet weak var buttonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: BottomCornersView!
    // MARK: - Properties
    var onButtonTap:((_ action: AlertAction) -> Void)?
    var cellType: AlertViewCell?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonHeightConstraint.constant = defaultButtonHeight()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private
    private func setUpCancel(type: AlertViewControllerType) {
        btnDefault.setTitle( Utils.capitalisedText(with: type.cancelMessage), for: .normal)
        btnDefault.gy_styleName = StyleConstants.Button.alert
        bottomView.isHidden = false
    }
    
    private func setUpAccept(type: AlertViewControllerType) {
        btnDefault.setTitle( Utils.capitalisedText(with: type.acceptMessage), for: .normal)
        btnDefault.gy_styleName = StyleConstants.Button.primary
        self.backgroundColor = type.cellColor
        buttonBottomConstraint.constant = 15
    }
    
    // MARK: - Actions
    @IBAction func pressed(_ sender: Any) {
        if let type = cellType, type == .accept {
            onButtonTap?(.accept)
        } else {
            onButtonTap?(.cancel)
        }
    }
    
    // MARK: - Public
    func setUp(type: AlertViewControllerType, cell: AlertViewCell) {
        if cell == .accept {
            setUpAccept(type: type)
        } else {
            setUpCancel(type: type)
        }
        
        cellType = cell
        
//        if type == .selectAmount {
//            bottomView.backgroundColor = type.cellColor
//        }
    }
}
