//
//  MyTicketFooterCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class MyTicketFooterCell: UITableViewCell {

    @IBOutlet weak var viewBackground: ConcaveCornersView!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var istobalLogoImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpGradient()
        viewBackground.corners = [.topLeft, .topRight]
        self.istobalLogoImg.isHidden = SDKOptions.hideByIstobal
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private
    private func setUpGradient() {
        let height = defaultButtonHeight()
        imgHeightConstraint.constant = height
        
        let frame = CGRect(x: 0, y: 0, width: Constants.Size.ticketCell, height: height)
        let gradient = Gradient.get(with: .ticket, size: frame, cornerRadius: StyleConstants.CornerRadius.main)
        viewBackground.layer.addSublayer(gradient)
    }
}
