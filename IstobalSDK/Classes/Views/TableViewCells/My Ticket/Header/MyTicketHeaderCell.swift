//
//  MyTicketHeaderCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Device

final class MyTicketHeaderCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblPurchasedOn: UILabel!
    @IBOutlet weak var lblValidUntil: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTotalPaid: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblPurchasedOnDate: UILabel!
    @IBOutlet weak var lblValidUntilDate: UILabel!
    @IBOutlet weak var imgVehicle: RoundedImageView!
    @IBOutlet weak var btnVehicleName: UIButton!
    @IBOutlet weak var viewBackground: ConcaveCornersView!
    @IBOutlet weak var backgroundHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.isUserInteractionEnabled = true
        
        lblTitle.textColor = Color.gray1.value
        lblSubtitle.textColor = Color.gray1.value
        
        lblTitle.font = Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance
        lblSubtitle.font = Font(.custom(SDKFont.bold), size: .standard(.h5_5)).instance
        btnVehicleName.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
        btnVehicleName.titleLabel?.lineBreakMode = .byWordWrapping
        btnVehicleName.titleLabel?.numberOfLines = 2        
        
        setUpGradient()
        viewBackground.corners = [.bottomLeft, .bottomRight]
        
        lblPurchasedOn.text = .localized(.purchasedOn)
        lblValidUntil.text = .localized(.validUntil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private Methods
    private func setUpGradient() {
        let height = getHeight()
        backgroundHeightConstraint.constant = height
        
        let frame = CGRect(x: 0, y: 0, width: Constants.Size.ticketCell, height: height)
        let gradient = Gradient.get(with: .ticket, size: frame)
        viewBackground.layer.insertSublayer(gradient, at: 0)
    }
    
    private func getHeight() -> CGFloat {
        switch Device.size() {
        case .screen3_5Inch, .screen4Inch:
            return 106
        case .screen4_7Inch:
            return 113
        default:
            return 120
        }
    }
    
    private func setVehicleImage(url: URL?) {
        if let link = url {
            imgVehicle.kf.setImage(with: link, placeholder: Constants.Image.placeholderVehicle)
        }
    }
    
    private func setUpRedeem(ticket: Ticket) {
        lblTitle.text = .localized(.debitBalance)
        btnVehicleName.setTitle(ticket.fidelityCardName, for: .normal)
        btnVehicleName.isUserInteractionEnabled = false
        btnVehicleName.setImage(nil, for: .normal)
        setVehicleImage(url: ticket.fidelityCardLogo)
        lblValidUntil.text = ""
    }
    
    private func setUpTotalPrice(_ totalPaid: String?) {
        guard let text = totalPaid else { return }
        
        let attributedText = NSAttributedString(string: text,
                                                attributes:[.strikethroughStyle: NSUnderlineStyle.styleThick.rawValue])
        lblTotalPrice.attributedText = attributedText
        lblTotalPrice.textColor = Color.red.value
    }
}

// MARK: - Public
extension MyTicketHeaderCell {
    func setUp(with vm: TicketViewModel) {
        lblStatus.text = vm.translatedStatus
        lblPurchasedOnDate.text = vm.purchasedOn
        lblValidUntilDate.text = vm.validUntil
        lblTotalPaid.text = vm.totalPaid
        setUpTotalPrice(vm.totalPrice)
        
        if vm.ticket.status == .redeemed {
            setUpRedeem(ticket: vm.ticket)
        } else {
            let name = vm.ticket.vehicleName ?? .localized(.noCarAssigned)
            btnVehicleName.setTitle("\(name) ", for: .normal)
            lblTitle.text = vm.ticket.installation?.companyName
            lblSubtitle.text = vm.navTitle
        }
    }
}
