//
//  MyTicketServiceCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Device

final class MyTicketServiceCell: UITableViewCell {
    // MARK: - IBOutlets
    @IBOutlet weak var viewBackground: ConcaveCornersView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var viewInfo: ButtonBoxView!
    @IBOutlet weak var viewQR: ButtonBoxView!
    @IBOutlet weak var viewStart: ButtonBoxView!
    @IBOutlet weak var viewSeparator: UIView!
    // MARK: - Properties
    private var typeOfService: TypeOfService?
    var onAction: ((TicketAction) -> Void)?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBackground.corners = [.topLeft, .topRight, .bottomLeft, .bottomRight]
        setUpStyles()
        lblPrice.alpha = 0
        viewInfo.type = .serviceInfo
        viewInfo.onButtonTap = { [unowned self] in
            self.onAction?(.showInfo(typeOfService: self.typeOfService))
        }
        
        viewQR.type = .seeTicketQR
        viewQR.onButtonTap = { [unowned self] in
            self.onAction?(.showQR(typeOfService: self.typeOfService))
        }
        
        viewStart.onButtonTap = { [unowned self] in
            self.onAction?(.startWash(typeOfService: self.typeOfService))
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Private Methods
    private func setUpStyles() {
        viewSeparator.backgroundColor = Color.separator.value
        lblPrice.textColor = Color.gray1.value
        lblPrice.font = Font(.custom(SDKFont.regular), size: .custom(21)).instance
    }
    
    private func updateStatus(_ status: TicketStatus) {
        lblStatus.text = status.humanReadable.capitalized
        lblStatus.textColor = status.color
        
        switch status {
        case .available:
            viewStart.type = .startServiceIsActive
        case .redeemed:
            viewStart.alpha = 0
            viewQR.alpha = 0
            lblStatus.textColor = Color.green.value
        default:
            viewStart.type = .startServiceIsNotActive
        }
    }
}

// MARK: - Public
extension MyTicketServiceCell {
    func setUp(for typeOfService: TypeOfService, ticketLine: TicketLine, ticket: Ticket) {
        self.typeOfService = typeOfService
        
        if let url = typeOfService.image {
            imgService.kf.setImage(with: url, placeholder: Constants.Image.puente)
        } else {
            imgService.image = Constants.Image.puente
        }
        
        guard let service = typeOfService.service else { return }
        
        if ticket.status == .redeemed {
            lblTitle.text = typeOfService.name
            lblSubtitle.text = typeOfService.name
            lblPrice.text = PriceUtils.priceToCurrency(with: ticket.totalPrice)
            updateStatus(ticket.status)
        } else {
            lblTitle.setBigText(with: typeOfService.name, smallText: " x\(ticketLine.washesLeft)", type: .ticketService)
            updateStatus(ticketLine.status)
            
            let serviceVM = ServiceViewModel(model: service)
            lblSubtitle.text = serviceVM.description
            lblPrice.text = serviceVM.price
        }
    }
}
