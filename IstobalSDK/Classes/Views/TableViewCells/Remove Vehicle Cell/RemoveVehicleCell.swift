//
//  RemoveVehicleCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 25/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class RemoveVehicleCell: UITableViewCell {

    @IBOutlet weak var btnRemove: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .clear
        btnRemove.setTitleColor(Color.red.value, for: .normal)
        btnRemove.setTitle( Utils.capitalisedText(with: .localized(.removeVehicle)), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
