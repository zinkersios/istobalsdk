//
//  VehicleDataCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 15/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class VehicleDataCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView(true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setUpView(_ isEditable: Bool) {
        accessoryType = isEditable ? .disclosureIndicator : .none
        selectionStyle = isEditable ? .default : .none
    }
    
    func setUp(with data: Any?, vehicle: Vehicle?) {
        guard let type = data as? EditVehicleCellType,
            let vehicle = vehicle
            else {
            return
        }
        
        switch type {
        case .name:
            lblTitle.text = .localized(.vehicleNameCell)
            lblSubtitle.text = vehicle.name
        case .brand:
            lblTitle.text = .localized(.vehicleBrand)
            lblSubtitle.text = vehicle.brand
        case .model:
            lblTitle.text = .localized(.vehicleModel)
            lblSubtitle.text = vehicle.model
        }
    }
}
