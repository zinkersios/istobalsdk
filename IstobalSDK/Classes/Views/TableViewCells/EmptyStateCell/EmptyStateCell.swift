//
//  EmptyStateCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 26/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class EmptyStateCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblError: UILabel!
    @IBOutlet var btnTryAgain: UIButton!
    @IBOutlet var stackViewError: UIStackView!
    
    // MARK: - Properties
    var onButtonTap:(() -> Void)?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        lblError.text = .localized(.errorConnection)
        btnTryAgain.setTitle(.localized(.tryAgain), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUpView(with state: APIState, type: CellType) {
        switch state {
        case .noResults:
            stackViewError.isHidden = true
            setUpNoResults(with: type)
        case .noInternet:
            stackViewError.isHidden = false
        default:
            stackViewError.isHidden = true
            lblTitle.text = .localized(.errorGeneral)
        }
    }
    
    private func setUpNoResults(with type: CellType) {
        switch type {
        case .installations, .searchWashCenters:
            lblTitle.text = .localized(.emptyInstallations)
        case .favorites:
            lblTitle.text = .localized(.emptyFavorites)
        case .allTickets:
            lblTitle.text = .localized(.emptyTickets)
        case .activeTickets:
            lblTitle.text = .localized(.emptyActiveTickets)
        case .ticketsConsumed:
            lblTitle.text = .localized(.emptyTicketsConsumed)
        case .ticketsForMyVehicles:
            lblTitle.text = .localized(.emptyTicketsForMyVehicles)
        case .valorations:
            lblTitle.text = .localized(.emptyValorations)
        case .notifications:
            lblTitle.text = .localized(.emptyNotifications)
        default:
            lblTitle.text = ""
        }
    }

    // MARK: - IBActions
    @IBAction func buttonTap(_ sender: Any) {
        onButtonTap?()
    }
}
