//
//  LastValorationCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 13/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class LastValorationCell: UITableViewCell {

    @IBOutlet var imgAvatar: RoundedImageView!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet var lblBody: UILabel!
    @IBOutlet var ratingView: FloatRatingView!
    @IBOutlet weak var viewSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.halfRatings = true
        ratingView.floatRatings = true
        viewSeparator.backgroundColor = Color.separator.value
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with viewModel: ValorationViewModel) {
        lblUsername.text = viewModel.fullName
        lblBody.text = viewModel.valoration.comment
        ratingView.rating = Float(viewModel.rating)
        
        if let image = viewModel.valoration.image, let url = URL(string: image) {
            imgAvatar.kf.setImage(with: url, placeholder: Constants.Image.placeholderUser)
        } else {
            imgAvatar.image = Constants.Image.placeholderUser
        }
    }
    
    func setUp(with data: Any?) {
        guard let valoration = data as? Valoration else {
            return
        }
        
        let vm = ValorationViewModel(model: valoration)
        setUp(with: vm)
    }
}
