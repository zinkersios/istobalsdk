//
//  OptionMenuBigCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 7/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class OptionMenuBigCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public    
    func setUp(with item: OptionMenu) {
        lblName.text = item.name
    }
}
