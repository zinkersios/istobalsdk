//
//  OptionMenuCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 7/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit
import Device

final class OptionMenuCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var nameLeadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        lblName.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        nameLeadingConstraint.constant = leadingForOptionMenuCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public
    func setUp(with item: OptionMenu) {
        lblName.text = item.name
    }
}
