//
//  ValorationCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 19/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class ValorationCell: UITableViewCell {
    
    @IBOutlet var imgAvatar: RoundedImageView!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet var lblBody: UILabel!
    @IBOutlet var ratingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.halfRatings = true
        ratingView.floatRatings = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with valoration: Valoration?) {
        guard let valoration = valoration else {
            return
        }
        
        let viewModel = ValorationViewModel.init(model: valoration)
        lblUsername.text = viewModel.fullName
        lblBody.text = viewModel.valoration.comment
        ratingView.rating = Float(viewModel.rating)
        
        if let image = valoration.image, let url = URL(string: image) {
            imgAvatar.kf.setImage(with: url, placeholder: Constants.Image.placeholderUser)
        } else {
            imgAvatar.image = Constants.Image.placeholderUser
        }
    }
}
