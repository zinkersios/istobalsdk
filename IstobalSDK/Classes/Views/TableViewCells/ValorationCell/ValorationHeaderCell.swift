//
//  ValorationHeaderCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 19/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class ValorationHeaderCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = Color.whiteSmoke.value
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with valorations: [Valoration]?, installation: Installation?) {
        guard let valorations = valorations,
            let installation = installation
            else {
            return
        }
        
        // set total
        let viewModel = InstallationViewModel(model: installation)
        lblTotal.text = "\(viewModel.valoration)"
        
        // set title
        let string: String = (valorations.count == 1) ? .localized(.valuation) : .localized(.valuations)
        lblTitle.text = "\(valorations.count) " + string
    }
}
