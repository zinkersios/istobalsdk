//
//  AddonCell.swift
//  istobal
//
//  Created by Shanti Rodríguez on 2/10/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class AddonCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet var sideView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnPrice: UIButton!
    @IBOutlet var checkbox: VKCheckbox!
    
    // MARK: - Properties
    private var price = ""
    private var addon: Addon?
    var onCheckboxTap:((_ isOn: Bool) -> Void)?
    var onInfoButtonTap:((_ addon: Addon) -> Void)?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        sideView.backgroundColor = Color.green.value
        
        /// set checkbox style
        checkbox.bgColorSelected = Color.green.value
        checkbox.bgColor         = .white
        checkbox.color           = .white
        checkbox.borderColor     = .lightGray
        
        // Handle
        checkbox.checkboxPressBlock = { [unowned self] in
            self.onCheckboxTap?(self.checkbox.isOn())
            self.checkbox.borderColor = self.checkbox.isOn() ? Color.green.value : .lightGray
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setStyleForPrice(_ isOn: Bool) {
        checkbox.setOn(isOn)
        checkbox.borderColor = isOn ? Color.green.value : .lightGray
        btnPrice.backgroundColor = isOn ? Color.blue2.value : .clear
        btnPrice.tintColor = isOn ? Color.blue.value : .lightGray
        sideView.isHidden = !isOn
        setPrice(with: self.price)
    }
    
    private func setPrice(with text: String) {
        let boldText = "  " + text + "  "
        btnPrice.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        btnPrice.setTitle(boldText, for: .normal)
    }
    
    func setUp(with addon: Addon, selectedAddons: [Addon]) {
        price = PriceUtils.priceToCurrency(with: addon.price, showSymbolPlus: false)
        
        if let descript = addon.longDescription, descript.count > 0 {
            self.addon = addon
            let smallText = "  " + .localized(.serviceMoreInfo)
            lblTitle.setBigText(with: addon.name, smallText: smallText)
        } else {
            lblTitle.text = addon.name
        }
        
        lblSubtitle.text = addon.description
        
        if selectedAddons.contains(addon) {
            setStyleForPrice(true)
        } else {
            setStyleForPrice(false)
        }
    }
    
    // MARK: - Actions
    @IBAction func showInfo(_ sender: UIButton) {
        if let addon = addon {
            onInfoButtonTap?(addon)
        }
    }
}
