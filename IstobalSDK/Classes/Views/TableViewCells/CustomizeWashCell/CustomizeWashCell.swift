//
//  CustomizeWashCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 9/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class CustomizeWashCell: UITableViewCell {

    // MARK: - IBOutlets    
    @IBOutlet var sideView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var btnPrice: UIButton!
    @IBOutlet var checkbox: VKCheckbox!
    
    // MARK: - Properties
    private var price = ""
    private var service: Service?
    var onCheckboxTap:(() -> Void)?
    var onInfoButtonTap:((_ service: Service) -> Void)?
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        sideView.backgroundColor = Color.green.value
        
        /// set checkbox style
        checkbox.bgColorSelected = Color.green.value
        checkbox.bgColor         = .white
        checkbox.color           = .white
        checkbox.borderColor     = .lightGray
        
        // Handle
        checkbox.checkboxPressBlock = { [unowned self] in
            self.onCheckboxTap?()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setStyleForPrice(_ isOn: Bool) {
        checkbox.setOn(isOn)
        btnPrice.backgroundColor = isOn ? Color.blue2.value : .clear
        btnPrice.tintColor = isOn ? Color.blue.value : .lightGray
        sideView.isHidden = !isOn
        checkbox.borderColor = isOn ? Color.green.value : .lightGray
        setPrice(with: self.price)
    }
    
    private func setPrice(with text: String) {
        let boldText = "  " + text + "  "
        btnPrice.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        btnPrice.setTitle(boldText, for: .normal)
    }
    
    func setUp(with service: Service, selectedService: Service?) {
        price = PriceUtils.priceToCurrency(with: service.price, showSymbolPlus: false)
        
        if let descript = service.longDescription, descript.count > 0 {
            self.service = service
            let smallText = "  " + .localized(.serviceMoreInfo)
            lblTitle.setBigText(with: service.name, smallText: smallText)
        } else {
            lblTitle.text = service.name
        }
        
        lblSubtitle.text = service.description
        
        if service == selectedService {
            setStyleForPrice(true)
        } else {
            setStyleForPrice(false)
        }
    }
    
    // MARK: - Actions
    @IBAction func showInfo(_ sender: UIButton) {
        if let service = service {
            onInfoButtonTap?(service)
        }
    }
}
