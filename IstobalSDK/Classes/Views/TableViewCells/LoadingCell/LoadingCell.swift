//
//  LoadingCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 26/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class LoadingCell: UITableViewCell {

    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(for type: CellType) {
        switch type {
        case .installations, .searchWashCenters:
            lblTitle.text = .localized(.gettingInstallations)
        case .favorites:
            lblTitle.text = .localized(.gettingFavorites)
        case .allTickets, .activeTickets, .ticketsConsumed, .ticketsForMyVehicles:
            lblTitle.text = .localized(.gettingTickets)
        case .valorations:
            lblTitle.text = .localized(.gettingValorations)
        case .notifications:
            lblTitle.text = .localized(.gettingNotifications)
        default: break
        }
        spinner.startAnimating()
    }
}
