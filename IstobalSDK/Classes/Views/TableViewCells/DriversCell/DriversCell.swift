//
//  DriversCell.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 15/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class DriversCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var imgAvatar: RoundedImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUp(with data: Any?) {
        guard let owner = data as? Owners else {
            return
        }
        
        let vm = OwnerViewModel.init(model: owner)
        lblTitle.text = vm.fullName
    }
}
