//
//  StopWashingView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 21/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Device

class StopWashingView: UIView {

    // MARK: - IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet var upView: UIView!
    @IBOutlet var downView: UIView!
    @IBOutlet var upHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblScrollDown: UILabel!
    @IBOutlet var lblScrollUp: UILabel!
    @IBOutlet var lblClose: UILabel!
    @IBOutlet var lblStop: UILabel!
    @IBOutlet var lblUpStop: UILabel!
    @IBOutlet var lblOfEmergency: UILabel!
    @IBOutlet var lblQuestion: UILabel!
    @IBOutlet var lblDisclaimer: UILabel!
    @IBOutlet var lblDisclaimerDescrip: UILabel!
    @IBOutlet weak var lblInfoStop: UILabel!    
    @IBOutlet var btnNot: UIButton!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var stopTopConstraint: NSLayoutConstraint!
    @IBOutlet var arrowDownHeightConstraint: NSLayoutConstraint!
    @IBOutlet var arrowUpHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblDisclaimerBottomConstraint: NSLayoutConstraint!
    @IBOutlet var downViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var onButtonTap:((_ isMachineStopped: Bool) -> Void)?
    var onFullScreenTap:(() -> Void)?
    
    // MARK: - interface
    var isUp: Bool = false {
        didSet {
            setUpHeader()
        }
    }
    
    // MARK: - Set Up
    /// for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    // for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        SDKNavigation.getBundle().loadNibNamed("StopWashingView", owner: self, options: [:])
        addSubview(contentView)
        
        /// Set up constraints
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        setStyles()
    }
    
    private func setStyles() {
        // set colors
        contentView.backgroundColor = Color.red2.value
        upView.backgroundColor = Color.whiteSmoke.value
        downView.backgroundColor = Color.red2.value
        setStyleButton(for: btnNot)
        setStyleButton(for: btnYes)
        
        // set fonts
        lblScrollUp.font = Font(.custom(SDKFont.bold), size: .standard(.h6_5)).instance
        lblUpStop.font = Font(.custom(SDKFont.bold), size: .custom(26)).instance
        lblScrollDown.font = lblScrollUp.font
        lblClose.font = Font(.custom(SDKFont.bold), size: .custom(23)).instance
        lblStop.font = Font(.custom(SDKFont.bold), size: .custom(72)).instance
        lblOfEmergency.font = Font(.custom(SDKFont.regular), size: .custom(36)).instance
        lblQuestion.font = Font(.custom(SDKFont.regular), size: .custom(30)).instance
        lblDisclaimer.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        lblDisclaimerDescrip.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        lblInfoStop.font = Font(.custom(SDKFont.light), size: .standard(.h4_5)).instance
        
        // set locations
        lblScrollUp.text = Utils.capitalisedText(with: .localized(.slideUpToStop1))
        lblUpStop.text = Utils.capitalisedText(with: .localized(.slideUpToStop2))
        lblScrollDown.text = Utils.capitalisedText(with: .localized(.scrollDownToClose1))
        lblClose.text = Utils.capitalisedText(with: .localized(.scrollDownToClose2))
        lblStop.text = Utils.capitalisedText(with: .localized(.emergencyStop1))
        lblOfEmergency.text = Utils.capitalisedText(with: .localized(.emergencyStop2))
        lblQuestion.text = .localized(.doYouWantStopMachine)
        lblQuestion.setLineHeight()
        btnNot.setTitle( Utils.capitalisedText(with: .localized(.not)), for: .normal)
        btnYes.setTitle( Utils.capitalisedText(with: .localized(.yes)), for: .normal)
        lblDisclaimerDescrip.text = .localized(.emergencyStopInformation)
        lblDisclaimerDescrip.setLineHeight()
        lblInfoStop.text = .localized(.infoStopWash)
        
        if Device.size() == .screen4Inch {
            arrowDownHeightConstraint.constant = 18
            arrowUpHeightConstraint.constant = 8
            stopTopConstraint.constant = 3
            stackViewTopConstraint.constant = 30
        } else if Device.size() == .screen4_7Inch {
            stopTopConstraint.constant = 30
        } else if Device.size() == .screen5_8Inch {
            lblDisclaimerBottomConstraint.constant = 40
            downViewTopConstraint.constant = 14
        }
    }
    
    private func setStyleButton(for button: UIButton) {
        var radius = CGFloat(29)
        
        if Device.size() == .screen4Inch {
            radius = CGFloat(26)
        } else if Device.size() == .screen4_7Inch {
            radius = CGFloat(27)
        }
        
        button.layer.cornerRadius = radius
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 4
        button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.header)).instance
    }
    
    private func setUpHeader() {
        upView.isHidden = isUp
        downView.isHidden = !isUp
//        Utils.registerEvent(.emergencyStop)
        
        if isUp {
//            Utils.registerScreen(.emergencyStop)
        }
    }
    
    // MARK: - IBActions
    @IBAction func selectedOption(_ sender: UIButton) {
        let isMachineStopped = (sender.currentTitle == Utils.capitalisedText(with: .localized(.yes))) ? true : false
        onButtonTap?(isMachineStopped)
    }
    
    @IBAction func showFullScreen(_ sender: Any) {
        onFullScreenTap?()
    }
}
