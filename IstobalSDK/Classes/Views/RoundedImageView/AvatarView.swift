//
//  AvatarView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 16/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Kingfisher

class AvatarView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var imageView: RoundedImageView!
    
    var enableShadow: Bool = false {
        didSet {
            self.setUpShadow()
        }
    }
    
    /// for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    // for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let bundle = SDKNavigation.getBundle()
        bundle.loadNibNamed("AvatarView", owner: self, options: [:])
        addSubview(contentView)
        
        /// Setup constraints
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        // rounded
        contentView.layer.cornerRadius = contentView.bounds.size.width / 2.0
        contentView.backgroundColor = .clear
    }
    
    private func setUpShadow() {
        contentView.addDropShadow()
    }
    
    func setImage(_ image: UIImage?) {
        if let image = image {
            imageView.image = image
        }
    }
}
