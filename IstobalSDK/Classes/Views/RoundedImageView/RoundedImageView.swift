//
//  RoundedImageView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 20/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.clipsToBounds = true
        let radius = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
    }
}
