//
//  RoundedButton.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.clipsToBounds = true
        let radius = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
    }
}
