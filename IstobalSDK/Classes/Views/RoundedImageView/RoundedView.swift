//
//  RoundedView.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 19/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    var isShadowEnable = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.clipsToBounds = true
        let radius = self.bounds.size.width / 2.0
        self.layer.cornerRadius = radius
        
        if isShadowEnable {
            self.addDropShadow()
        }
    }
}
