//
//  ButtonBoxView.swift
//  istobal
//
//  Created by Shanti Rodríguez on 9/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class ButtonBoxView: UIView {
    // MARK: - IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    // MARK: - Properties
    var onButtonTap:(() -> Void)?
    var type: ButtonBoxType = .serviceInfo {
        didSet {
            setUpView()
        }
    }
    
    // MARK: - Init
    /// for using in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    // for using in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        SDKNavigation.getBundle().loadNibNamed(String(describing: ButtonBoxView.self), owner: self, options: [:])
        addSubview(contentView)
        
        /// Setup constraints
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    private func setUpView() {
        contentView.backgroundColor = type.backgroundColor
        contentView.layer.cornerRadius = type.cornerRadius
        lblTitle.text = type.title
        lblTitle.font = type.font
        lblTitle.textColor = type.tintColor
        imageView.image = UIImage(named: type.imageName, in: SDKNavigation.getBundle(), compatibleWith: nil)
        
        if type.addShadow {
            contentView.addDropShadow(with: CGSize(width: 0, height: 3),
                                      radius: 7,
                                      opacity: StyleConstants.Opacity.full,
                                      shadowColor: UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.2))
        }
        
        if type == .serviceInfo {
            imageView.image!.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = Color.blue.value
        }
    }
}

// MARK: - IBActions
private extension ButtonBoxView {
    @IBAction func action(_ sender: Any) {
        if type.isEnabled {
            onButtonTap?()
        }
    }
}
