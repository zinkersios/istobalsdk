//
//  SDKAppearance.swift
//  Alamofire
//
//  Created by Hernán Darío Villamil on 25/07/2018.
//

import Foundation

public class SDKAppearance{
    
    public static var singleton = SDKAppearance()
    public static var shared: SDKAppearance {
        return singleton
    }
    
    func registerFonts(){
        let bundle = SDKNavigation.getBundle()
        
        UIFont.registerFont(bundle: bundle, fontName: "TitilliumWeb-Italic", fontExtension: ".ttf")
        UIFont.registerFont(bundle: bundle, fontName: "TitilliumWeb-Light", fontExtension: ".ttf")
        UIFont.registerFont(bundle: bundle, fontName: "TitilliumWeb-Bold", fontExtension: ".ttf")
        UIFont.registerFont(bundle: bundle, fontName: "TitilliumWeb-Regular", fontExtension: ".ttf")
        UIFont.registerFont(bundle: bundle, fontName: "TitilliumWeb-SemiBold", fontExtension: ".ttf")
    }
    
    func registerFont(_ name: String, fontExtension: String){
        UIFont.registerFont(bundle: SDKNavigation.getBundle(), fontName: name, fontExtension: fontExtension)
    }
    
    public func getAvailabeFonts()->[SDKTypography]{
        self.registerFonts()
        var fonts:[SDKTypography] = []
        
        for familyRegistered in SDKFamilyFonts.allCases{
            let font = SDKTypography()
            font.familyName = familyRegistered.rawValue

            for name in UIFont.fontNames(forFamilyName: familyRegistered.rawValue){
                    font.appendNewFont(name)
            }
            fonts.append(font)

        }

        return fonts
    }
    
    public func printAllFonts(){
        self.registerFonts()

        for family in UIFont.familyNames {
            let sName: String = family as String
            debugPrint("family: \(sName)")
            for name in UIFont.fontNames(forFamilyName: sName) {
                debugPrint("name: \(name as String)")
            }
        }
    }
    
    public func setAppearance(primaryButtonBackground: UIColor? = nil, primaryButtonLabel: UIColor? = nil, primarTitle: UIColor? = nil, primaryLabel: UIColor? = nil, secondaryLabel: UIColor? = nil, specialLabel: UIColor? = nil, appBackground: UIColor? = nil, navigatioBarBackground: UIColor? = nil, navigationBarTint: UIColor? = nil, cellBackground: UIColor? = nil, cellLabelPrimary: UIColor? = nil, cellLabelSecondary: UIColor? = nil, hideByIstobal: Bool?, font: SDKTypography?){
  
        if hideByIstobal != nil{ SDKOptions.hideByIstobal = hideByIstobal! }
        self.setColors(primaryButtonBackground: primaryButtonBackground, primaryButtonLabel: primaryButtonLabel, primarTitle: primarTitle, primaryLabel: primaryLabel, secondaryLabel: secondaryLabel, specialLabel: specialLabel, appBackground: appBackground, navigatioBarBackground: navigatioBarBackground, navigationBarTint: navigationBarTint, cellBackground: cellBackground, cellLabelPrimary: cellLabelPrimary, cellLabelSecondary: cellLabelSecondary)
        
        self.registerFonts()
        self.setFont(font: font)
       
        self.registerStyles()
    }
    
    func setFont(font: SDKTypography?){
        if let newFont = font{
            SDKFont.light = newFont.getFontFor(SDKFontSufix.light)
            SDKFont.regular = newFont.getFontFor(SDKFontSufix.regular)
            SDKFont.bold = newFont.getFontFor(SDKFontSufix.bold)
            SDKFont.semiBold = newFont.getFontFor(SDKFontSufix.semiBold)
            SDKFont.italic = newFont.getFontFor(SDKFontSufix.italic)
        }

    }
    
    func setColors(primaryButtonBackground: UIColor? = nil, primaryButtonLabel: UIColor? = nil, primarTitle: UIColor? = nil, primaryLabel: UIColor? = nil, secondaryLabel: UIColor? = nil, specialLabel: UIColor? = nil, appBackground: UIColor? = nil, navigatioBarBackground: UIColor? = nil, navigationBarTint: UIColor? = nil, cellBackground: UIColor? = nil, cellLabelPrimary: UIColor? = nil, cellLabelSecondary: UIColor? = nil){
        
        if let color = primaryButtonBackground { SDKColor.primaryButtonBackground = color }
        if let color = primaryButtonLabel{ SDKColor.primaryButtonLabel = color }
        if let color = primarTitle{ SDKColor.primarTitle = color }
        if let color = primaryLabel { SDKColor.primaryLabel = color }
        if let color = secondaryLabel{ SDKColor.secondaryLabel = color }
        if let color = specialLabel{ SDKColor.specialLabel = color }
        if let color = appBackground { SDKColor.appBackground = color }
        if let color = navigatioBarBackground{ SDKColor.navigatioBarBackground = color }
        if let color = navigationBarTint{ SDKColor.navigationBarTint = color }
        if let color = cellBackground { SDKColor.cellBackground = color }
        if let color = cellLabelPrimary{ SDKColor.cellLabelPrimary = color }
        if let color = cellLabelSecondary{ SDKColor.cellLabelSecondary = color }
    }
    
    public func setFonts(){
        self.registerFonts()
    }

    func registerStyles(){
        LabelStyle.registerStyles()
        ButtonStyle.registerStyles()
        TextFieldStyle.registerStyles()
        ViewStyle.registerStyles()
    }
  
    
}
