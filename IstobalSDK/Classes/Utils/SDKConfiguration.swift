//
//  SDKConfiguration.swift
//  Alamofire
//
//  Created by Hernán Darío Villamil on 26/07/2018.
//

import Foundation

public class SDKConfiguration{
    
    public static var singleton = SDKConfiguration()
    public static var shared: SDKConfiguration {
        return singleton
    }
    
    public func setConfiguration(clientId: String, clientSecret: String, companyId: String){
        Constants.API.clientID = clientId
        Constants.API.clientSecret = clientSecret
        Constants.API.companyId = companyId
    }
    
}
