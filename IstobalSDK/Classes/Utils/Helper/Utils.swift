//
//  Utils.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 1/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Foundation
import Device

class Utils {
    // Spinner
    class func splashSpinner() -> UIActivityIndicatorView {
        let padding: CGFloat = Device.size() == .screen5_8Inch ? 40 : 20
        let ai = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        ai.hidesWhenStopped = true
        ai.frame = CGRect(x: Constants.Size.screenWidth/2, y: Constants.Size.screenHeight - padding, width: 0, height: 0)
        return ai
    }
    
    // Create UIImage with the gradient specified
    class func generateGradientImage(direction: Direction, startColor: UIColor, endColor: UIColor) -> UIImage {
        let gradientLayer = CAGradientLayer()
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let navBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        gradientLayer.frame = navBarFrame
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        
        if direction == .horizontal {
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.15)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.47)
        }
        
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage!
    }
    
    // Create UIImage with color specified
    class func getImageWithColor(color: UIColor) -> UIImage {
        let size = CGSize(width:  UIScreen.main.bounds.size.height * 2, height: 64)
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    // Get Current View Controller
    class func currentViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        guard let viewController = viewController else { return nil }
        
        if let viewController = viewController as? UINavigationController {
            if let viewController = viewController.visibleViewController {
                return currentViewController(viewController)
            } else {
                return currentViewController(viewController.topViewController)
            }
        } else if let viewController = viewController as? UITabBarController {
            if let viewControllers = viewController.viewControllers, viewControllers.count > 5, viewController.selectedIndex >= 4 {
                return currentViewController(viewController.moreNavigationController)
            } else {
                return currentViewController(viewController.selectedViewController)
            }
        } else if let viewController = viewController.presentedViewController {
            return viewController
        } else if viewController.childViewControllers.count > 0 {
            return viewController.childViewControllers[0]
        } else {
            return viewController
        }
    }
    
    // MARK: - Set translated status
    class func statusHumanReadable(with status: TicketStatus) -> String {
        switch status {
        case .available:
            return .localized(.ticketActive)
        case .canceled:
            return .localized(.cancelled)
        case .consumed:
            return .localized(.ticketConsumed)
        case .expired:
            return .localized(.ticketExpired)
        case .pending:
            return .localized(.ticketPending)
        default: return ""
        }
       
    }
    
    // MARK: - seconds to Time String
    class func timeString(time: TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    // seconds to human readable
    class func secondsToHumanReadable(time: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .short
        
        guard let formattedString = formatter.string(from: TimeInterval(time)) else {
            return self.timeString(time: time)
        }
        return formattedString
    }
    
    // MARK: - Generate QR Image
    class func generateQR(with id: String) -> UIImage? {
        let data = id.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("Q", forKey: "inputCorrectionLevel")
            
            guard let qrCodeImage = filter.outputImage else {
                return nil
            }
            
            let size = Constants.Size.screenWidth
            let scaleX = size / qrCodeImage.extent.size.width
            let scaleY = size / qrCodeImage.extent.size.height
            let transformedImage = qrCodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
            return UIImage(ciImage: transformedImage)
        }
        
        return nil
    }
    
    // MARK: - String Helper
    class func capitalisedText(with text: String) -> String {
        let string: String = text
        return string.uppercased()
    }
    
    class func lowercaseText(with text: String) -> String {
        let string: String = text
        return string.lowercased()
    }
    
    // MARK: - Analytics
//    class func registerEvent(_ event: AnalyticsEvent) {
//        let analytics = AnalyticsManager.init()
//        analytics.log(event)
//    }
//    
//    class func registerScreen(_ screen: AnalyticsScreen) {
//        let analytics = AnalyticsManager.init()
//        analytics.screen(screen)
//    }
    
    // MARK: - Call Number
    class func callNumber(with phone: String?) {
        guard let string = phone,
            let phoneCallURL = URL(string: "tel://\(string)") else {
            return
        }
        
        let application = UIApplication.shared
        
        if application.canOpenURL(phoneCallURL) {
            application.open(phoneCallURL, options: [:], completionHandler: nil)
        }
    }
    
    class func openAppInStore() {
        guard let url = URL(string: "itms-apps://itunes.apple.com/app/id1209416535") else {
            return
        }
        
        let application = UIApplication.shared
        
        if application.canOpenURL(url) {
            application.open(url, options: [:], completionHandler: nil)
        }
    }
    
    class func openMail(with email: String?) {
        guard let email = email else { return }
        
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
    }
}
