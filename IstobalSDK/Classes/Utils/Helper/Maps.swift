//
//  Maps.swift
//  istobal
//
//  Created by Shanti Rodríguez on 30/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import MapKit

import Foundation

final class Maps {
    /// open installation in maps
    class func openMaps(with installation: Installation?) {
        guard let installation = installation else {
            return
        }
        self.openMaps(with: installation.location.latitude, long: installation.location.longitude, name: installation.companyName)
    }
    
    class func openMaps(with installation: BasicInstallation?) {
        guard let installation = installation else {
            return
        }
        
        let location = Location(latitude: installation.latitude, longitude: installation.longitude)
        self.openMaps(with: location.latitude, long: location.longitude, name: installation.companyName)
    }
    
    private class func openMaps(with lat: Double, long: Double, name: String) {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = long
        
        let regionDistance: CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        
        let options: [String: Any] = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span),
            MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
        ]
        
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
}
