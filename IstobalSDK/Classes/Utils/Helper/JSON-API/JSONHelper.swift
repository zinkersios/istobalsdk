//
//  JSONHelper.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 20/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

//
// MARK: - JSON Utilities
//
/// Adopted by a type that can be instantiated from JSON data.
protocol JSONParselable {
    /// Attempts to configure a new instance of the conforming type with values from a JSON dictionary.
    init?(json: JSONObject)
}

extension JSONParselable {
    /// Attempts to configure a new instance using a JSON dictionary selected by the `key` argument.
    init?(json: JSONObject, key: String) {
        guard let jsonDictionary = json[key] as? JSONObject else { return nil }
        self.init(json: jsonDictionary)
    }
    
    /// Attempts to produce an array of instances of the conforming type based on an array in the JSON dictionary.
    /// - Returns: `nil` if the JSON array is missing or if there is an invalid/null element in the JSON array.
    static func createRequiredInstances(from json: [String: Any], arrayKey: String) -> [Self]? {
        guard let jsonDictionaries = json[arrayKey] as? [[String: Any]] else { return nil }
        return createRequiredInstances(from: jsonDictionaries)
    }
    
    /// Attempts to produce an array of instances of the conforming type based on an array of JSON dictionaries.
    /// - Returns: `nil` if there is an invalid/null element in the JSON array.
    static func createRequiredInstances(from jsonDictionaries: [[String: Any]]) -> [Self]? {
        var array = [Self]()
        for jsonDictionary in jsonDictionaries {
            guard let instance = Self.init(json: jsonDictionary) else { return nil }
            array.append(instance)
        }
        return array
    }
}

extension Double {
    init?(json: JSONObject, key: String) {
        // Explicitly unboxing the number allows an integer to be converted to a double,
        // which is needed when a JSON attribute value can have either representation.
        guard let nsNumber = json[key] as? NSNumber else { return nil }
        self.init(_: nsNumber.doubleValue)
    }
}

extension Array where Element: CustomStringConvertible {    
    func toURLArray() -> [URL]? {
        var urlArray = [URL]()
        for string in self {
            guard let url = URL(string: String(describing: string)) else { return nil }
            urlArray.append(url)
        }
        return urlArray
    }
}
