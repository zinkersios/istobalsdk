//
//  NCenterUtils.swift
//  istobal
//
//  Created by Shanti Rodríguez on 16/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

final class NCenterUtils {
    class func updateSideMenu(with type: TypeController) {
        NotificationCenter.default.post(name: Constants.NCenter.updateSideMenu,
                                        object: nil,
                                        userInfo: [Constants.JSONKey.item: type.rawValue])
    }
}
