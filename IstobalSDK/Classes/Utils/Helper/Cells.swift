//
//  Cells.swift
//  istobal
//
//  Created by Shanti Rodríguez on 13/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation
import UIKit

final class Cells {
    static func byDefault() -> [UITableViewCell.Type] {
        return [LoadingCell.self, EmptyStateCell.self]
    }
    
    // MARK: - Installation
    
    class func forInstallationDetail() -> [UITableViewCell.Type] {
        return [/*InstallationHeaderCell.self,*/
                InstallationInfoCell.self,
                InstallationServiceCell.self,
//                InstallationSectionHeader.self,
                LastValorationCell.self,
                AllValorationsCell.self]
    }
    
    class func forPurchaseSummary() -> [UITableViewCell.Type] {
        return [HeaderCell.self
//                PurchaseSummaryCenterCell.self,
//                PurchaseSummaryServicesCell.self,
//                PurchaseSummaryCreditCell.self,
//                PurchaseSummaryCouponCell.self,
//                PurchaseSummaryPaymentCell.self
        ]
    }
    
//    class func forContactDetails() -> [UITableViewCell.Type] {
//        return [ContactDetailNameCell.self, ContactDetailCell.self]
//    }
    
    // MARK: - Notifications
//    class func forNotifications() -> [UITableViewCell.Type] {
//        var cells = byDefault()
//        cells.append(NotificationCell.self)
//        return cells
//    }
    
    // MARK: - My Tickets
    class func forMyTicket() -> [UITableViewCell.Type] {
        var cells = byDefault()
        cells.append(MyTicketHeaderCell.self)
        cells.append(MyTicketServiceCell.self)
        cells.append(MyTicketFooterCell.self)
        return cells
    }
    
    class func forMyTickets() -> [UITableViewCell.Type] {
        var cells = byDefault()
        cells.append(TicketCell.self)
        return cells
    }
    
    class func forEditVehicle() -> [UITableViewCell.Type] {
        return [HeaderCell.self,
                VehicleDataCell.self,
                DriversCell.self,
                RemoveVehicleCell.self]
    }
    
    class func forContactDetails() -> [UITableViewCell.Type] {
        return [OptionMenuBigCell.self,
                OptionMenuCell.self]
    }
}
