//
//  SRAlert.swift
//  istobal
//
//  Created by Shanti Rodríguez on 12/2/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

enum AlertAction {
    case cancel
    case accept
    case add
    case selected(Any)
}

final class SRAlert {
    // MARK: - Vehicles
    class func showVehicles(in controller: UIViewController, completion:@escaping (AlertAction) -> Void) {
        guard let coches = BasicVehicle.getMyVehiclesWithNoAssignedCar(), coches.count > 0 else {
            return
        }
        
        var vehicles = coches
        // New Vehicle
        let addVehicle = BasicVehicle(id: 0, name: .localized(.addNewVehicle), isAssignedVehicle: false)
        vehicles.append(addVehicle)
        
        let alertController = UIAlertController(title: .localized(.selectVehicle), message: nil, preferredStyle: .actionSheet)
    
        /// add vehicles
        for vehicle in vehicles {
            let style: UIAlertActionStyle = (vehicle == addVehicle) ? .destructive :  .default
            
            let action = UIAlertAction(title: vehicle.name, style: style) { (action) in
                if let alertIndex = alertController.actions.index(of: action), let vehicle = vehicles.elementAt(index: alertIndex) {
                    if vehicle == addVehicle {
                        completion(.add)
                    } else {
                        completion(.selected(vehicle))
                    }
                }
            }
            alertController.addAction(action)
        }
        // cancel
        let cancelAction = UIAlertAction(title: .localized(.cancel), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true)
    }
    
    // MARK: - Credit Card
//    class func showCreditCards(in controller: UIViewController, installationId: Int, completion:@escaping (AlertAction) -> Void) {
//        var cards: [Any] = LoyaltyCard.getCards(installationId: installationId)
//        cards.append(contentsOf: CreditCard.creditCardsForPurchaseSummary())
//
//        let alertController = UIAlertController(title: .localized(.paymentMethods), message: nil, preferredStyle: .actionSheet)
//
//        /// add credit cards
//        for card in cards {
//            var name = ""
//
//            if let creditCard = card as? CreditCard {
//                name = creditCard.number
//            } else if let loyaltyCard = card as? LoyaltyCard {
//                name = loyaltyCard.name
//            }
//
//            let action = UIAlertAction(title: name, style: .default) { (action) in
//                if let alertIndex = alertController.actions.index(of: action), let card = cards.elementAt(index: alertIndex) {
//                    completion(.selected(card))
//                }
//            }
//            alertController.addAction(action)
//        }
//        // cancel
//        let cancelAction = UIAlertAction(title: .localized(.cancel), style: .cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        controller.present(alertController, animated: true)
//    }
    
    // MARK: - Credit Card
    class func showCommentTypes(in controller: UIViewController, completion:@escaping (AlertAction) -> Void) {
        let alertController = UIAlertController(title: .localized(.type), message: nil, preferredStyle: .actionSheet)
        let types = CommentType.allValues
        
        for type in types {
            let action = UIAlertAction(title: type.detail, style: .default) { (action) in
                if let alertIndex = alertController.actions.index(of: action), let object = types.elementAt(index: alertIndex) {
                    completion(.selected(object))
                }
            }
            alertController.addAction(action)
        }
        // cancel
        let cancelAction = UIAlertAction(title: .localized(.cancel), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true)
    }
    
    // MARK: - Vehicles
//    class func showOffers(with offers: [Offer]?,
//                          service: Service?,
//                          controller: UIViewController,
//                          installationID: Int,
//                          completion:@escaping (AlertAction) -> Void) {
//        guard let coches = BasicVehicle.getMyVehiclesWithNoAssignedCar(), coches.count > 0 else {
//            return
//        }
//        // Set Up offers
//        var array = [Offer]()
//        array.append(Offer.noUseCoupon())
//        
//        if let ofertas = APIHelper.getServiceOffers(with: offers, selected: service) {
//            array += ofertas
//        }
//        array.append(contentsOf: LoyaltyCard.getOffers(installationId: installationID))
//        
//        let addCoupon = Offer.addCoupon()
//        array.append(addCoupon)
//        
//        // AlertController
//        let alertController = UIAlertController(title: .localized(.discountCoupon), message: nil, preferredStyle: .actionSheet)
//        
//        for offer in array {
//            let style: UIAlertActionStyle = (offer == addCoupon) ? .destructive :  .default
//            
//            let action = UIAlertAction(title: offer.name, style: style) { (action) in
//                if let alertIndex = alertController.actions.index(of: action), let oferta = array.elementAt(index: alertIndex) {
//                    if oferta == addCoupon {
//                        completion(.add)
//                    } else {
//                        completion(.selected(oferta))
//                    }
//                }
//            }
//            alertController.addAction(action)
//        }
//        // cancel
//        let cancelAction = UIAlertAction(title: .localized(.cancel), style: .cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        controller.present(alertController, animated: true)
//    }
    
    // MARK: - Show
    class func show(with title: String?, message: String?) {
        guard let vc = Utils.currentViewController() else {
            return
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: .localized(.accept), style: .default, handler: nil)
        
        alertController.addAction(dismissAction)
        vc.present(alertController, animated: true)
    }
    
    // MARK: - New version available. The user must be informed.
    class func showUpdateInformation() {
        let vc = SRAlertViewController.storyboardViewController()
        vc.type = .update
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(vc, animated: true, completion: nil)
    }
}
