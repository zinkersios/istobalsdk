//
//  FrameHelper.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 24/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import Device

final class FrameHelper {
    class func is4Inch() -> Bool {
        return Device.size() == .screen3_5Inch || Device.size() == .screen4Inch
    }
}

/// Height for cell (Side Menu)
func heightSideMenuCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 50
    case .screen4_7Inch:
        return 56
    default:
        return 60
    }
}

/// Height for header (Side Menu)
func heightForHeaderInSideMenu() -> CGFloat {
    switch Device.size() {
    case .screen5_8Inch:
        return 136
    default:
        return 100
    }
}

/// Height for header with weather (Side Menu)
func heightForHeaderWithWeatherInSideMenu() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 258
    case .screen4_7Inch:
        return 268
    default:
        return 278
    }
}

/// Height for header (HomeViewController)
func heightForHeaderHome() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 320
    case .screen4_7Inch:
        return 340
    default:
        return 380
    }
}

/// Height for header (Assistant step 1)
func heightForHeaderAssistant() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 150
    case .screen4_7Inch:
        return 164
    default:
        return 174
    }
}

/// Height for header (MyProfileViewController)
func heightForHeaderMyProfile() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 180
    case .screen4_7Inch:
        return 210
    default:
        return 220
    }
}

/// Height for header (MyCardHeaderViewController)
func heightForHeaderMyCard() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 215
    case .screen4_7Inch:
        return 225
    default:
        return 235
    }
}

/// Height for segmentedView (SJSegmentedViewController)
func heightForSegmentView() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 48
    case .screen4_7Inch:
        return 48
    default:
        return 50
    }
}

/// Height for installation cell
func heightForInstallationCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 100
    case .screen4_7Inch:
        return 110
    default:
        return 120
    }
}

/// Height for service cell
func heightForServiceCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 74
    case .screen4_7Inch:
        return 78
    default:
        return 80
    }
}

func heightForOtherServicesContainer(items: [TypeOfService], config: ButtonDataSourceConfig) -> CGFloat {
    var rows = CGFloat(0)
    
    for index in 0..<items.count where index % 2 == 0 {
        rows += 1
    }
    
    let heightWithoutPadding = config.cellHeight * rows
    let padding = rows * config.padding
    let result = heightWithoutPadding + padding
    return (rows > 0) ? result : config.cellHeight
}

/// Height for your ticket cell
func heightForYourTicketCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 96
    case .screen4_7Inch:
        return 100
    default:
        return 104
    }
}

/// Height for promotions cell
func heightForPromotionsCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 115
    case .screen4_7Inch:
        return 125
    default:
        return 130
    }
}

/// Height for last valoration cell
func heightForLastValorationCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 118
    case .screen4_7Inch:
        return 118
    default:
        return 118
    }
}

/// Height for default header cell
func heightForHeaderCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 28
    case .screen4_7Inch:
        return 28
    default:
        return 28
    }
}

/// Height for promotions header cell
func heightForPromotionsHeaderCell() -> CGFloat {
    let padding = CGFloat(16)
    
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 28 + padding
    case .screen4_7Inch:
        return 28 + padding
    default:
        return 28 + padding
    }
}

// MARK: - Corner Radius

/// StartWashViewController
func cornerRadiusForStartWash() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 43
    case .screen4_7Inch:
        return 50
    default:
        return 56
    }
}

// MARK: - Button
/// Used in Main, Login, Register, RecoverPass
func defaultButtonHeight() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 46
    case .screen4_7Inch:
        return 55
    default:
        return 60
    }
}

/// Used in TutorialViewController, AssistantStep2ViewController
func heightForFooter() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 62
    case .screen4_7Inch:
        return 72
    default:
        return 80
    }
}

/// Height for navigation bar
func heightForNavBar() -> CGFloat {
    switch Device.size() {
    case .screen5_8Inch:
        return 88
    default:
        return 64
    }
}

func leadingForOptionMenuCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 40
    case .screen4_7Inch:
        return 45
    case .screen5_5Inch:
        return 55
    default:
        return 40
    }
}

// MARK: - Buttons
func heightForButtonCollectionCell() -> CGFloat {
    switch Device.size() {
    case .screen3_5Inch, .screen4Inch:
        return 52
    case .screen4_7Inch:
        return 54
    default:
        return 56
    }
}

func heightForButtonContainer<T>(items: [T], config: ButtonDataSourceConfig) -> CGFloat {
    var rows = CGFloat(0)
    
    for index in 0..<items.count where index % 2 == 0 {
        rows += 1
    }
    
    let heightWithoutPadding = config.cellHeight * rows
    let padding = rows * config.padding
    let result = heightWithoutPadding + padding
    return (rows > 0) ? result : config.cellHeight
}
