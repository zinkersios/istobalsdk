//
//  APIHelper.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

final class APIHelper {
    class func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else {
            return nil
        }
        
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    /// Save credit Cards
    class func saveCreditCards(with cards: Any) {
        let dataCards = NSKeyedArchiver.archivedData(withRootObject: cards)
        UserDefaults.User.set(dataCards, forKey: .creditCards)
    }
    
    /// Save my vehicles
    class func saveMyVehicles(with vehicles: Any) {
        let vehicles = NSKeyedArchiver.archivedData(withRootObject: vehicles)
        UserDefaults.User.set(vehicles, forKey: .myVehicles)
    }
    
    /// Save my vehicles
    class func saveMyFavorites(with favorites: Any) {
        let fav = NSKeyedArchiver.archivedData(withRootObject: favorites)
        UserDefaults.User.set(fav, forKey: .favorites)
    }
    
    /// JSON to String
    class func jsonToString(json: AnyObject) -> String {
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
            let convertedString = String(data: data, encoding: String.Encoding.utf8)
            return convertedString ?? ""
        } catch let error {
            debugPrint(error)
//            log.error(error)
            return ""
        }
    }
    
    // MARK: - Offers
    /*
     * Si "service" es null hay que mostrar esa oferta, porque significa que es general, por lo que puedo aplicarlo si quiero.
     *
     * Cuando "service" no es null, miro si el ID de ese servicio, es el mismo que he seleccionado yo,
     * si es asi, quiere decir que es una oferta de ese servicio y lo muestro.
     * Si es una ID distinta quiere decir que es la oferta de otro servicio, entonces no hay que mostrarlo en el listado.
     */
    class func getServiceOffers(with offers: [Offer]?, selected service: Service?) -> [Offer]? {
        guard let offers = offers else { return nil }
        var array = [Offer]()
        
        for offer in offers {
            if offer.service == nil {
                array.append(offer)
            } else if let offerService = offer.service, offerService.id == service?.id {
                array.append(offer)
            }
        }
        return array
    }
    
    // Get the best offer
    class func getTheBestOffer(with offers: [Offer]?, selected service: Service?) -> Offer? {
        guard let array = APIHelper.getServiceOffers(with: offers, selected: service) else {
            return nil
        }
        guard let result = minimumMaximum(array) else {
            return nil
        }
        return result.maximum
    }
    
    class func parametersForReedemOffer(code: String, data: AddDiscountCode?) -> JSONObject {
        var dic = JSONObject()
        dic["code"] = code
        
        if let datos = data {
            dic["installation"] = datos.installationID
            dic["service"] = datos.serviceID
        }
        return dic
    }
    
    // MARK: - Comparable
    /*
     Finds the maximum and minimum value in an array in O(n) time.
     */
    class func minimumMaximum<T: Comparable>(_ array: [T]) -> (minimum: T, maximum: T)? {
        guard var minimum = array.first else { return nil }
        var maximum = minimum
        
        // if 'array' has an odd number of items, let 'minimum' or 'maximum' deal with the leftover
        let start = array.count % 2 // 1 if odd, skipping the first element
        for index in stride(from: start, to: array.count, by: 2) {
            let pair = (array[index], array[index+1])
            
            if pair.0 > pair.1 {
                if pair.0 > maximum {
                    maximum = pair.0
                }
                if pair.1 < minimum {
                    minimum = pair.1
                }
            } else {
                if pair.1 > maximum {
                    maximum = pair.1
                }
                if pair.0 < minimum {
                    minimum = pair.0
                }
            }
        }
        
        return (minimum, maximum)
    }
}
