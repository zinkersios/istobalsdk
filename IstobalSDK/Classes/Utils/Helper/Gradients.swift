//
//  Gradients.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 8/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class Gradient {
    public enum GradientType {
        case blue, green, white, wash, installationDetail, myCardsCell, myCards2Cell, button, ticket
    }
    
    class func get(with type: GradientType, size: CGRect, cornerRadius: CGFloat = 0) -> CAGradientLayer {
        switch type {
        case .blue:
            return self.blue(size)
        case .green:
            return self.green(size)
        case .white:
            return self.white(size)
        case .wash:
            return self.wash(size)
        case .installationDetail:
            return self.installationDetail(size)
        case .myCardsCell:
            return self.myCard(size)
        case .myCards2Cell:
            return self.myCard2(size)
        case .button:
            return self.button(size)
        case .ticket:
            return self.ticket(size, cornerRadius: cornerRadius)
        }
    }
    
    // MARK: - Private Methods
    private class func blue(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:0.07, green:0.64, blue:0.89, alpha:1).cgColor,
            UIColor(red:0.21, green:0.74, blue:0.97, alpha:1).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 1, y: 1)
        gradient.endPoint = CGPoint(x: 0, y: 0.1)
        return gradient
    }
    
    private class func green(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:0.07, green:0.89, blue:0.49, alpha:0.15).cgColor,
            UIColor(red:0.07, green:0.89, blue:0.49, alpha:0).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.68, y: 0.5)
        return gradient
    }
    
    private class func white(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:1, green:1, blue:1, alpha:0).cgColor,
            UIColor.white.cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0.5, y: -0.13)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.3)
        return gradient
    }
    
    private class func wash(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:0.19, green:0.41, blue:0.5, alpha:1).cgColor,
            UIColor(red:0.07, green:0.89, blue:0.49, alpha:1).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.cornerRadius = cornerRadiusForStartWash()
        return gradient
    }
    
    private class func installationDetail(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:0, green:0, blue:0, alpha:0.5).cgColor,
            UIColor.clear.cgColor
        ]
        return gradient
    }
    
    private class func myCard(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:0.03, green:0.13, blue:0.17, alpha:0).cgColor,
            UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.16).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0.5, y: 0)
        gradient.endPoint = CGPoint(x: 0.5, y: 1)
        return gradient
    }
    
    private class func myCard2(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.2).cgColor,
            UIColor(red:0.03, green:0.13, blue:0.17, alpha:0).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0.5, y: 1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.02)
        return gradient
    }
    
    private class func button(_ size: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradient.colors = [
            UIColor(red:0.07, green:0.64, blue:0.89, alpha:1).cgColor,
            UIColor(red:0.21, green:0.74, blue:0.97, alpha:1).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 1, y: 1)
        gradient.endPoint = CGPoint(x: 0, y: 0.1)
        gradient.cornerRadius = StyleConstants.CornerRadius.main
        return gradient
    }
    
    private class func ticket(_ size: CGRect, cornerRadius: CGFloat) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = size
        gradient.colors = [
            UIColor(red:0, green:0.5, blue:0.8, alpha:1).cgColor,
            UIColor(red:0.21, green:0.74, blue:0.97, alpha:1).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 1, y: 1)
        gradient.endPoint = CGPoint.zero
        
        if cornerRadius > 0 {
            gradient.cornerRadius = cornerRadius
        }
        return gradient
    }
}
