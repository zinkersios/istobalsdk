//
//  Meter.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 28/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

struct Meter {
    var value: Double
    
    init(_ value: Double) {
        self.value = value
    }
    
    var toString: String {
        var unitLength = " " + UnitLength.kilometers.rawValue
        var newValue = value
        
        if value < 1.0 {
            newValue = value * 1000
            unitLength = " " + UnitLength.meters.rawValue
        }
        
        let number = NSNumber(value: newValue)
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        
        guard let resultString = formatter.string(from: number) else {
            return String(format:"%.0f%@", newValue, unitLength)
        }
        
        return resultString + unitLength
    }
    
    var toFloat: Float {
        var newValue = value
        
        if value < 1.0 {
            newValue = value * 1000
        }
        
        let number = NSNumber(value: newValue)
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        
        return number.floatValue
    }
}
