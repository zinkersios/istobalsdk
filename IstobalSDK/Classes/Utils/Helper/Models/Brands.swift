//
//  Brands.swift
//  istobal
//
//  Created by Shanti Rodríguez on 6/2/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

class Brands {
    class func show(in controller: UIViewController, completion:@escaping (String?) -> Void) {
        let brands = getBrands()
        
        let alertController = UIAlertController(title: .localized(.vehicleBrand),
                                                message: nil,
                                                preferredStyle: .actionSheet)
        // Add brands
        for brand in brands {
            let action = UIAlertAction(title: brand, style: .default, handler: { _ in
                completion(brand)
            })
            alertController.addAction(action)
        }
        
        // Add Cancel
        let cancelAction = UIAlertAction(title: .localized(.cancel), style: .cancel) { _ in
            completion(nil)
        }
        alertController.addAction(cancelAction)
        // Show
        controller.present(alertController, animated: true, completion: nil)
    }
    
    //swiftlint:disable:next function_body_length
    class func getBrands() -> [String] {
        return ["Abarth",
                "Alfa Romeo",
                "Alpina",
                "Ariel",
                "Arrinera",
                "Artega",
                "Aston Martin",
                "Audi",
                "Belumbury",
                "Bentley",
                "BMW",
                "Bugatti",
                "BYD",
                "Cadillac",
                "Carver",
                "Caterham",
                "Chevrolet",
                "Chrysler",
                "Citroën",
                "Corvette",
                "Dacia",
                "Daihatsu",
                "Dodge",
                "DS",
                "Ferrari",
                "Fiat",
                "Fisker",
                "Ford",
                "Fornasari",
                "GTA",
                "Hennessey",
                "Honda",
                "Hummer",
                "Hurtan",
                "Hyundai",
                "Infiniti",
                "Isuzu",
                "Jaguar",
                "Jeep",
                "Kia",
                "Koenigsegg",
                "KTM",
                "LADA",
                "Lamborghini",
                "Lancia",
                "Land Rover",
                "Lexus",
                "Lotus",
                "Mahindra",
                "Maserati",
                "Maybach",
                "Mazda",
                "McLaren",
                "Melkus",
                "Mercedes-Benz",
                "MINI",
                "Mitsubishi",
                "Morgan",
                "Nissan",
                "Opel",
                "Pagani",
                "Peugeot",
                "PGO",
                "Porsche",
                "Renault",
                "Roding",
                "Rolls Royce",
                "Saab",
                "Santana",
                "SEAT",
                "Skoda",
                "Smart",
                "Spyker",
                "SsangYong",
                "Subaru",
                "Suzuki",
                "Tata",
                "Tazzari",
                "Tesla",
                "Think",
                "Toyota",
                "Volkswagen",
                "Volvo",
                "Wiesmann",
                "W-motors",
                "Zenvo"]
    }
}
