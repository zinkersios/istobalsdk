//
//  PriceUtils.swift
//  istobal
//
//  Created by Shanti Rodríguez on 13/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

final class PriceUtils {
    static func currencySymbol() -> String {
        return "€"
    }
    
    class func priceToCurrency(with price: Double, showSymbolPlus: Bool = false) -> String {
        let symbol = showSymbolPlus ? "+" : ""
        let newPrice = NSNumber(value: price)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = currencySymbol()
        
        if let result = formatter.string(from: newPrice) {
            return symbol + result // "123,44"
        }
        
        return String(format: "%@%.02f", symbol, price)
    }
    
    class func stringToNumber(text: String) -> NSNumber? {
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        formatter.decimalSeparator = ","
        
        guard let result = formatter.number(from: text) else {
            return nil
        }
        return result
    }
    
    // calculate the percent. Ex: (20*40)/100
    class func calculatePercent(ciento: Double, quantity: Double) -> Double {
        return (ciento * quantity)/100
    }
    
    // Apply price discount
    class func applyPriceDiscountAsString(discount: Double, price: Double) -> String {
        let percent = PriceUtils.calculatePercent(ciento: Double(discount), quantity: price)
        let discountedPrice = PriceUtils.priceToCurrency(with: (price - percent))
        return discountedPrice
    }
    
    class func applyPriceDiscount(discount: Double, price: Double) -> Double {
        let percent = PriceUtils.calculatePercent(ciento: Double(discount), quantity: price)
        return price - percent
    }
}
