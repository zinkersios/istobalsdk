//
//  Router+Enum.swift
//  istobal
//
//  Created by Shanti Rodríguez on 3/5/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum LoginSegue {
    case home
    case tutorial
}

enum AppSegue {
    case addVehicle
    case installation
    case profile
    case webView
    case reportProblem
    case valorations
    case customizeWash
    case offers
    case services
    case legalTexts
    case alertView
}

enum ProfileSegue {
    case personalData
    case paymentMethod
    case billingInformation
}

enum TicketSegue {
    case serviceInfo
}
