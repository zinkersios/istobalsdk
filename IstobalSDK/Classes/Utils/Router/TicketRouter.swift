//
//  TicketRouter.swift
//  istobal
//
//  Created by Shanti Rodríguez on 9/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import UIKit

final class TicketRouter: AppRouter {

    func perform(_ segue: TicketSegue, from source: UIViewController, parameters: Any? = nil) {
        switch segue {
        case .serviceInfo:
            source.present( TicketRouter.makeServiceInfo(parameters), animated:true)
        }
    }
}

// MARK: - Helpers

private extension TicketRouter {
    static func makeServiceInfo(_ parameters: Any?) -> UIViewController {
        let vc = TicketServiceInfoViewController.storyboardViewController()
        
        if let data = parameters as? TicketServiceRoute {
            vc.ticket = data.ticket
            vc.typeOfService = data.typeOfService
            vc.modalPresentationStyle = .overFullScreen
        }
        return vc
    }
}
