//
//  AppRouter.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 23/9/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

final class DefaultAppRouter: AppRouter {
    
    func perform(_ segue: AppSegue, from source: UIViewController, parameters: Any? = nil) {
        switch segue {
        case .addVehicle:
            source.present( DefaultAppRouter.makeAddVehicle(), animated:true)
//        case .installation:
//            let vc = DefaultAppRouter.makeInstallationDetailController(parameters)
//            source.navigationController?.pushViewController(vc, animated: true)
//        case .profile:
//            source.present( DefaultAppRouter.makeProfileController(), animated:true)
        case .webView:
            let vc = DefaultAppRouter.makeWebViewController(parameters)
            source.navigationController?.pushViewController(vc, animated: true)
//        case .reportProblem:
//            let vc = DefaultAppRouter.makeReportProblem(parameters)
//            source.navigationController?.pushViewController(vc, animated: true)
        case .valorations:
            let vc = DefaultAppRouter.makeValorations(parameters)
            source.navigationController?.pushViewController(vc, animated: true)
        case .customizeWash:
            let vc = DefaultAppRouter.makeCustomizeWash(parameters)
            source.navigationController?.pushViewController(vc, animated: true)
//        case .offers:
//            let vc = DefaultAppRouter.makeOffers(parameters)
//            source.navigationController?.pushViewController(vc, animated: true)
//        case .services:
//            let vc = DefaultAppRouter.makeServices(parameters)
//            source.navigationController?.pushViewController(vc, animated: true)
//        case .legalTexts:
//            let vc = DefaultAppRouter.makeLegalTexts(parameters)
//            source.navigationController?.pushViewController(vc, animated: true)
        default: break
        }
    }
}

// MARK: - Helpers

private extension DefaultAppRouter {
    /// Add Vehicle
    static func makeAddVehicle() -> UINavigationController {
        let vc = IntroAddVehicleViewController.storyboardViewController()
        return UINavigationController(rootViewController: vc)
    }
    
    /// My Profile
//    static func makeProfileController() -> UINavigationController {
////        Utils.registerEvent(.myProfile)
//        
//        let vc = MyProfileViewController.storyboardViewController()
//        return UINavigationController(rootViewController: vc)
//    }
    
    /// Webview
    static func makeWebViewController(_ parameters: Any?) -> UIViewController {
        let vc = AboutUsViewController.storyboardViewController()
        
        if let type = parameters as? WebViewType {
            vc.type = type
        }
        
        return vc
    }
    
    // MARK: Installation detail
//    static func makeInstallationDetailController(_ parameters: Any?) -> InstallationViewController {
//        let vc = InstallationViewController.storyboardViewController()
//
//        if let data = parameters as? InstallationRoute {
//            vc.userLocation = data.location
//            vc.installation = data.installation
//        }
//
//        return vc
//    }
//    static func makeReportProblem(_ parameters: Any?) -> UIViewController {
//        let vc = ReportProblemViewController.storyboardViewController()
//
//        if let installation = parameters as? Installation {
//            vc.installation = installation
//        }
//        return vc
//    }
    
    // Valorations
    static func makeValorations(_ parameters: Any?) -> UIViewController {
        let vc = ValorationsViewController.storyboardViewController()
        
        if let installation = parameters as? Installation {
            vc.installation = installation
        }
        return vc
    }
    
    // Customize Wash
    static func makeCustomizeWash(_ parameters: Any?) -> UIViewController {
        let vc = CustomizeWashViewController.storyboardViewController()

        if let data = parameters as? CustomizeWashRoute {
            let orderedServices = data.services.sorted {$0.price > $1.price}
            vc.installation = data.installation
            vc.services = orderedServices
            vc.offer = data.offer
            vc.delegate = data.delegate
        }
        return vc
    }
    
    // Offers
//    static func makeOffers(_ parameters: Any?) -> UIViewController {
//        let vc = OffersViewController.storyboardViewController(with: String(describing: InstallationViewController.self))
//
//        if let installation = parameters as? Installation {
//            vc.installation = installation
//        }
//        return vc
//    }
    
    // Services
//    static func makeServices(_ parameters: Any?) -> UIViewController {
//        let vc = ServicesViewController.storyboardViewController(with: String(describing: InstallationViewController.self))
//        
//        if let data = parameters as? ServicesRoute {
//            vc.installation = data.installation
//            vc.offer = data.offer
//        }
//        return vc
//    }
    
    // Legal Texts
//    static func makeLegalTexts(_ parameters: Any?) -> UIViewController {
//        let vc = LegalTextsViewController.storyboardViewController()
//
//        if let type = parameters as? LegalText {
//            vc.type = type
//        }
//        return vc
//    }
}
