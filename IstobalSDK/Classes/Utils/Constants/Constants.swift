//
//  Constants.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit

struct Constants {
    struct Image {
        static let check = UIImage(named: "ic_check_18", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let close = UIImage(named: "ic_close_28_grey", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let pin   = UIImage(named: "ic_swash_red", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let placeholderUser = UIImage(named: "placeholder_user", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let smallPlaceholderUser = UIImage(named: "placeholder_user_small", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let placeholderInstallation = UIImage(named: "placeholder_centro", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let placeholderVehicle = UIImage(named: "placeholder_vehiculo", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let navBarLogo = UIImage(named: "logo_istobal_header", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let favRed = UIImage(named: "ic_fav_red_28", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let favGray = UIImage(named: "ic_fav_28", in: SDKNavigation.getBundle(), compatibleWith: nil)
        static let puente = UIImage(named: "ic_puente_46", in: SDKNavigation.getBundle(), compatibleWith: nil)
    }
    
    struct Size {
        static let screenWidth = CGFloat(UIScreen.main.bounds.size.width)
        static let screenHeight = CGFloat(UIScreen.main.bounds.size.height)
        static let ticketCell = Constants.Size.screenWidth - 20 // 10 para cada lado. Si se cambia el pading de la celda MyTicketHeaderCell o MyTicketFooterCell, aquí también se debe cambiar.
    }
    
    struct NCenter {
        static let addOrRemoveFavorite = Notification.Name("addOrRemoveFavoriteNotification")
        static let addOrRemoveInstallation = Notification.Name("addOrRemoveInstallationNotification")
        static let updateUserHeader = Notification.Name("updateUserHeaderNotification")
        static let updateUserAvatarInHome = Notification.Name("updateUserAvatarInHomeNotification")
        static let updateUserFromSideMenu = Notification.Name("updateUserFromSideMenuNotification")
        static let updateSideMenu = Notification.Name("updateSideMenuNotification")
        static let updateWeather = Notification.Name("updateWeatherNotification")
        static let reDownloadMyTickets = Notification.Name("reDownloadMyTicketsNotification")
    }
    
    struct EstimatedRowHeight {
        static let installation =  CGFloat(120)
    }
    
    struct SectionHeaderHeight {
        static let byDefault =  CGFloat(54)
    }
    
    struct SectionFooterHeight {
        static let byDefault =  CGFloat(11)
    }
    
    struct Tag {
        static let hairLine = 6969
    }
    
    struct Color {
        static let bigStone = "#314550"
        static let shadow = UIColor(red:0, green:0, blue:0, alpha:0.2)
        static let overlay = UIColor.black.withAlphaComponent(0.8)
    }
    
    struct Map {
        static let visiblePinName = "VisiblePinsMapPin"
    }
    
    struct TableViewCellIdentifiers {
        static let header = "HeaderCell"
        static let headerMyVehicle = "HeaderMyVehicleCell"
    }
    
    struct Device {
        static let iosVersion = NSString(string: UIDevice.current.systemVersion).doubleValue
        static let isGreaterThaniOS11 = iosVersion >= 11 // iOS 11
        static let isSmallerThaniOS11 = iosVersion < 11 // iOS 10
        static let platform = "ios"
    }
    
    struct String {
        static let BookAppointment = "Reservar Cita"
    }
}
