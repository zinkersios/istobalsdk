//
//  WeatherConstants.swift
//  istobal
//
//  Created by Shanti Rodríguez on 14/2/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

struct WeatherConstants {
    //Lluvia
    static let showers1 = 11
    static let showers2 = 12
    //Nubes
    static let mostlyCloudyNight = 27
    static let mostlyCloudyDay = 28
    static let partlyCloudyNight = 29
    static let partlyCloudyDay = 30
    //Despejado
    static let clear = 31
    static let sunny = 32
    static let fairNight = 33
    static let fairDay = 34
    //Relampagos
    static let isolatedThunderstorms = 37
    static let scatteredThunderstorms1 = 38
    static let scatteredThunderstorms2 = 39
    static let severeThunderstorms = 3
    static let thunderstorms = 4
    //Nieve
    static let snowShowers = 46
    static let scatteredSnowShowers = 42
    static let heavySnow1 = 41
    static let heavySnow2 = 43
    
    // Days
    static let noOneCondition = -2
    static let muchBadDays = -1
    static let today = 0
    static let tomorrow = 1
    static let thirdDay = 2
    static let fourthDay = 3
    static let fifthDay = 4
}
