//
//  Typealias.swift
//  istobal
//
//  Created by Shanti Rodríguez on 11/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation
import CoreLocation

typealias JSONObject = [String: Any]
typealias JSONKeyJSON =  [String: [String:Any]]
typealias JSONArray = [[String: Any]]
typealias DicInstallation = [String: Installation]
typealias InstallationRoute = (installation: Installation?, location: CLLocation?)
typealias ServicesRoute = (installation: Installation?, offer: Offer)
typealias CustomizeWashRoute = (installation: Installation?, services: [Service], offer: Offer?, delegate: ServicesViewDelegate)
typealias AddDiscountCode = (type: CodeReadingType, installationID: Int, serviceID: Int)
typealias AlertViewRoute = (type: AlertViewControllerType, extraInformation: String?)
typealias TicketServiceRoute = (ticket: Ticket?, typeOfService: TypeOfService?)
