//
//  StyleConstants.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import UIKit

struct StyleConstants {
    
    struct Label {
        static let title = "titleLabelStyle"
        static let titleBlue = "titleBlueLabelStyle"
        static let titleWhite = "titleWhiteLabelStyle"
        static let belowTextField = "belowTextFieldStyle"
        static let loading = "loadingLabelStyle"
        static let titleCell = "titleCellLabelStyle"
        static let sectionHeader = "sectionHeaderLabelStyle"
        static let sectionHeader2 = "sectionHeader2LabelStyle"
        static let header = "headerLabelStyle"
        static let header2 = "header2LabelStyle"
        static let ratingCell = "ratingCellLabelStyle"
        static let totalWashes = "totalWashesLabelStyle"
        static let smallDarkGray = "smallDarkGrayLabelStyle"
        static let smallGray3 = "smallGray3LabelStyle"
        static let titleAlertView = "titleAlertViewStyle"
        static let boldH2Dot5 = "boldH2Dot5LabelStyle"
        static let boldH1Dot5 = "boldH1Dot5LabelStyle"
        static let regularH6Dot5 = "regularH6Dot5LabelStyle"
        static let blueRegularH2Dot5 = "blueRegularH2Dot5LabelStyle"
        static let grayBold25 = "grayBold25LabelStyle"
        static let lightGrayRegH2Dot5 = "lightGrayRegH2Dot5LabelStyle"
    }
    
    struct LoginLabel {
        static let welcome = "welcomeStyle"
        static let terms = "loginTermsStyle"
        static let header = "headerLogInStyle"
        static let input = "inputLogInStyle"
    }
    
    struct SideMenu {
        static let slogan = "sloganLabelStyle"
        static let username = "usernameSideMenuLabelStyle"
    }
    
    struct AddVehicleLabel {
        static let titleIntro = "titleIntroAVLabelStyle"
        static let subtitleIntro = "subtitleIntroAVLabelStyle"
        static let numberOfPages = "numberOfPagesAVLabelStyle"
        static let input = "inputAVLabelStyle"
        static let totalWashes = "valueOfTotalWashesLabelStyle"
        static let washes = "washesLabelStyle"
    }
    
    struct InstallationDetailLabel {
        static let locationTime = "locationTimeLabelStyle"
        static let ticketHeader = "ticketHeaderLabelStyle"
        static let ticketNumber = "ticketNumberLabelStyle"
        static let small = "smallLabelStyle"
        static let seeOffers = "seeOffersLabelStyle"
    }
    
    struct PurchaseSummaryLabel {
        static let centerCell = "centerCellLabelStyle"
        static let servicesCell = "servicesCellLabelStyle"
    }
    
    struct WashLabel {
        static let info = "infoWashLabelStyle"
        static let infoScan = "infoScanLabelStyle"
        static let installationNameBlue = "installationNameBlueLabelStyle"
    }
    
    struct ProfileLabel {
        static let statisticsGray3 = "statisticsGray3LabelStyle"
        static let statisticsRed2 = "statisticsRed2LabelStyle"
    }
    
    struct TutorialLabel {
        static let title = "tutorialTitleLabelStyle"
    }
    
    struct AddVehicleButton {
        static let border = "blueBorderButtonStyle"
        static let next = "nextPageButtonStyle"
    }

    struct Button {
        static let primary = "primaryButtonStyle"
        static let primaryXL = "primaryXLButtonStyle"
        static let secondary = "secondaryButtonStyle"
        static let link = "linkButtonStyle"
        static let linkLogin = "linkLoginButtonStyle"
        static let viewPassword = "viewPasswordButtonStyle"
        static let price = "priceButtonStyle"
        static let ticketStatus = "ticketStatusButtonStyle"
        static let stepsInstructions = "stepsInstructionsButtonStyle"
        static let priceForAddon = "priceForAddonButtonStyle"
        static let showQRInTicket = "showQRInTicketButtonStyle"
        static let seeAllTickets = "seeAllTicketsButtonStyle"
        static let alert = "alertButtonStyle"
        static let edit = "editButtonStyle"
    }
    
    struct InstallationButton {
        static let phone = "phoneButtonStyle"
        static let viewRoute = "viewRouteButtonStyle"
        static let option = "optionButtonStyle"
        static let dismissOptions = "dismissOptionsButtonStyle"
    }
    
    struct AssistantButton {
        static let assistantNormal = "assistantNormalButtonStyle"
        static let assistantSelected = "assistantSelectedButtonStyle"
        static let nextStep = "nextStepButtonStyle"
    }
    
    struct Checkbox {
        static let filters = "filtersCheckboxStyle"
        static let wash = "washCheckboxStyle"
    }

    struct TextField {
        static let primary = "primaryTextFieldStyle"
        static let logIn = "loginTextFieldStyle"
        static let ticket = "ticketTextFieldStyle"
    }
    
    struct TextView {
        static let primary = "defaultTextViewStyle"
        static let terms = "termsTextViewStyle"
    }
    
    struct NavBar {
        static let astral = "astralNavBarStyle"
    }
    
    struct View {
        static let credit = "creditViewStyle"
        static let prices = "pricesViewStyle"
    }
    
    struct TableView {
        static let main = "mainTableViewStyle"
        static let offers = "offersTableViewStyle"
        static let purchaseSummary = "purchaseSummaryTableViewStyle"
    }
    
    struct Offset {
        static let primary = CGSize(width: 4, height: 4)
        static let secondary =  CGSize(width: 0, height: 2)
    }
    
    struct Opacity {
        static let main = Float(0.3)
        static let full = Float(1.0)
    }
    
    struct BorderWidth {
        static let main = CGFloat(1.0)
    }
    
    struct CornerRadius {
        static let main = CGFloat(8)
        static let assistance = CGFloat(4)
        static let ticket = CGFloat(16)
    }
    
    struct Multiplier {
        static let footer = CGFloat(0.16) // used in iPhone X
    }
}
