//
//  APIConstants.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

extension Constants {
    struct HTTPCode {
        static let success = 200 // Success or OK
        static let created = 201 // Created
        static let noContent = 204 // Success
        static let unauthorised = 401 // Client Error
        static let badRequest = 400 // Bad Request
    }        
    
    struct API {
//        #if DEVELOPMENT
//        static let server = "http://devapisw.istobal.com/"
//        static let base = Constants.API.server + "api/"
//        static let clientID = "1_220q6t1pm1b4gcs0s40wkwo0ckckg4cg8c0og4g0wwws84wo08"
//        static let clientSecret = "35v44d2x9wkk0c4cswok08kok08kockog0w8cg4w4c0ckk888w"
//        static let AddonPaymentProducer = "https://preapisw.istobal.com/api/card-json"
//        static let AddonPaymentURL = "https://hpp.sandbox.addonpayments.com/pay"
//        static let AddonPaymentConsumer = "https://preapisw.istobal.com/api/1/obtain-details"
//        #elseif PREPRODUCTION
        static let server = "https://preapisw.istobal.com/"
        static let base = Constants.API.server + "smartwash/"
        static var clientID = ""
        static var clientSecret = ""
        static let AddonPaymentProducer = "https://preapisw.istobal.com/api/card-json"
        static let AddonPaymentURL = "https://hpp.sandbox.addonpayments.com/pay"
        static let AddonPaymentConsumer = "https://preapisw.istobal.com/api/1/obtain-details"
        static var companyId = ""
//        #else /// PRODUCTION
//        static let server = "https://apisw.istobal.com/"
//        static let base = Constants.API.server + "api/"
//        static var clientID = "4_5y8l395o72osckwkggk4s0owgoggswsc444c4gowwo0ossskc"
//        static var clientSecret = "4pp74gbmreo0kok0ocgw0ws8c84gw4wc4w4oswsocokwsokc80"
//        static let AddonPaymentProducer = "https://apisw.istobal.com/api/card-json"
//        static let AddonPaymentURL = "https://hpp.addonpayments.com/pay"
//        static let AddonPaymentConsumer = "https://apisw.istobal.com/api/1/obtain-details"
//        #endif
        
        static let fbClientID = "2_3t6pcylhbiaswscsowgkk84gsc0css88c4ogocsgggo4cs48w"
        static let fbClientSecret = "38gjhnqe5fc4o8ks4k0sw0ck8408c44g488gs0go44so8cws0w"
        static let googleClientID = "3_4hu99fhftwcg0o8gwo48844044s4k0gscoww8gg4w00s8k8ggk"
        static let googleClientSecret = "8qbjy0nwc90ks88sc4c0ckw4sc404g80o0s0gswwws4w80ckw"
    }
    
    struct Locksmith {
        static let accessToken = "accessToken"
        static let userID = "userID"
    }
    
    struct Key {
        static let grantType = "grant_type"
        static let clientId = "client_id"
        static let clientSecret = "client_secret"
        static let authorization = "Authorization"
    }
    
    struct JSONKey {
        static let accessToken = "access_token"
        static let tokenType = "token_type"
        static let id = "id"
        static let results = "results"
        static let installation = "installation"
        static let offset = "offset"
        static let next = "next" // next page
        static let item = "item" //
        static let vehicle = "vehicle"
        static let vehicles = "vehicles"
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let invoiceCompany = "invoiceCompany"
        static let taxID = "taxId"
        static let companyName = "companyName"
        static let address = "address"
        static let email = "email"
        static let city = "city"
        static let postalCode = "postalCode"
        static let state = "state"
        static let country = "country"
        static let translations = "translations"
    }
    
    struct InstallationID {
        static let ShowroomAlcudia = 26
    }
    
    struct URL {
        static let aboutUs = "http://www.istobal.com/es/"
        static let bookAppointment = "https://istobal-wordpress.zinkers.net"
    }
    
    struct Host {
        static let istobal = "www.istobal.com"
    }
}
