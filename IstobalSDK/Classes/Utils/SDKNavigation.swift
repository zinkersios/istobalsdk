//
//  SDKNavigation.swift
//  IstobalSDK
//
//  Created by Hernán Darío Villamil on 24/07/2018.
//

import Foundation
import Alamofire

public protocol ServicesViewDelegate{
    func onServiceReadyToBuy(installationId: String, selectedService: Service, selectedAddons: [Addon])
}

public class SDKNavigation{
    
    public static var singleton = SDKNavigation()
    public static var shared: SDKNavigation {
        return singleton
    }
    
    static func getBundle()->Bundle{
        let podBundle = Bundle(for: self)
        let bundleURL = podBundle.url(forResource: "IstobalSDK", withExtension: "bundle")
        return Bundle(url: bundleURL!)!
    }
    
    public func checkCredentialas()->Bool{
        if Constants.API.clientID == ""{return false}
        if Constants.API.clientSecret == ""{return false}
        if Constants.API.companyId == ""{return false}
        return true
    }
    
    public func perfomSegueToMyVehicles(_ caller: UIViewController){
        if !checkCredentialas(){
            debugPrint("please check your credentials before perform the segue to vehicles")
            return
        }
        let storyboard = UIStoryboard(name: "MyVehiclesViewController", bundle: SDKNavigation.getBundle())
        let vc = storyboard.instantiateInitialViewController()!
        caller.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    public func perfomSegueToMyTickets(_ caller: UIViewController){
        if !checkCredentialas(){
            debugPrint("please check your credentials before perform the segue to tickets")
            return
        }
        let storyboard = UIStoryboard(name: "MyTicketsViewController", bundle: SDKNavigation.getBundle())
        let vc = storyboard.instantiateInitialViewController()!
        caller.navigationController?.pushViewController(vc, animated: true)
    }

    public func perfomSegueToInstallationServices(_ caller: UIViewController, installationId: String, delegate: ServicesViewDelegate){
        if !checkCredentialas(){
            debugPrint("please check your credentials before perform the segue to services")
            return
        }
        self.openInstallationServicesWith(caller, installation: Installation(id: Int(installationId) ?? 0), delegate: delegate)
    }
    
    func openInstallationServicesWith(_ caller: UIViewController, installation: Installation, delegate: ServicesViewDelegate){
        debugPrint("Installation info: \(installation.companyName)")
        let storyboard = UIStoryboard(name: "ServicesViewController", bundle: SDKNavigation.getBundle())
        if let vc = storyboard.instantiateInitialViewController()! as? ServicesViewController{
            vc.installation = installation
            vc.serviceViewDelegate = delegate
            caller.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
