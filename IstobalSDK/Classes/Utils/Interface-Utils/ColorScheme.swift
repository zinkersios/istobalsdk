//
//  ColorScheme.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 13/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import UIKit
//201/42/54

enum SDKColor{
    static var primaryButtonBackground: UIColor = UIColor(red: 201.0/255.0, green: 45.0/255.0, blue: 54.0/255.0, alpha: 1.0)
    static var primaryButtonLabel: UIColor = UIColor(red: 254.0/255.0, green: 74.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    static var primarTitle: UIColor = UIColor(red: 201.0/255.0, green: 45.0/255.0, blue: 54.0/255.0, alpha: 1.0)
    static var primaryLabel: UIColor = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
    static var secondaryLabel: UIColor = UIColor(red: 205.0/255.0, green: 205.0/255.0, blue: 205.0/255.0, alpha: 1.0)
    static var specialLabel: UIColor = UIColor(red: 254.0/255.0, green: 74.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    static var appBackground: UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static var navigatioBarBackground: UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static var navigationBarTint: UIColor = UIColor(red: 201.0/255.0, green: 45.0/255.0, blue: 54.0/255.0, alpha: 1.0)
    static var cellBackground: UIColor = UIColor(red: 254.0/255.0, green: 74.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    static var cellLabelPrimary: UIColor = UIColor(red: 254.0/255.0, green: 74.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    static var cellLabelSecondary: UIColor = UIColor(red: 254.0/255.0, green: 74.0/255.0, blue: 81.0/255.0, alpha: 1.0)
}

enum SDKOptions{
    static var hideByIstobal: Bool = false
}

enum Color {
    
    case red
    case red2
    
    case blue
    case blue2
    case blueLight
    case blueUltraLight
    case blueSecondary
    
    case skyBlue
    case skyBlueLight
    case skyBlueUltraLight
    
    case green
    case green2
    case green3
    case yellow
    case yellow2
    
    case gray1
    case gray2
    case gray3
    case gray4
    case gray5
    case gray6
    case gray7
    case gray8
    
    case black1
    case black2
    case black3
    case black4
    case black5
    case black6
    case black7
    case black8
    case black9
    case backgroundBlack
    
    case whiteSmoke
    case separator
    case elephant
    case astral
    case atomic
    case solitude
    case nileBlue
    case mirage
    case matisse
    case daintree
    
    case custom(hexString: String, alpha: Double)
    
    func withAlpha(_ alpha: Double) -> UIColor {
        return self.value.withAlphaComponent(CGFloat(alpha))
    }
}

extension Color {
    
    var value: UIColor {
        var instanceColor = UIColor.clear
        
        switch self {
        case .red:
            instanceColor = UIColor(hexString: "#DB002E")
        case .red2:
            instanceColor = UIColor(red:0.86, green:0, blue:0.18, alpha:1)
        
        case .blue:
            instanceColor = UIColor(hexString: "#13A2E2") // UIColor(red:0.07, green:0.64, blue:0.89, alpha:1)
        case .blue2:
            instanceColor = UIColor(red:0.07, green:0.64, blue:0.89, alpha:0.1)
        case .blueLight:
            instanceColor = UIColor(hexString: "#42AEE3")
        case .blueUltraLight:
            instanceColor = UIColor(hexString: "#7AC5E9")
        case .blueSecondary:
            instanceColor = UIColor(hexString: "#316880")
        
        case .skyBlue:
            instanceColor = UIColor(hexString: "#B6DEF0")
        case .skyBlueLight:
            instanceColor = UIColor(hexString: "#DAEAF2")
        case .skyBlueUltraLight:
            instanceColor = UIColor(hexString: "#E8F0F3")
        
        case .green:
            instanceColor = UIColor(hexString: "#13E27E") // UIColor(red:0.07, green:0.89, blue:0.49, alpha:1)
        case .green2:
            instanceColor = UIColor(red:0.07, green:0.89, blue:0.49, alpha:0.1) // usado en el botón de aplicar filtro y el botón de tus tickets
        case .green3:
            instanceColor = UIColor(hexString: "#08BC65") // UIColor(red:0.03, green:0.74, blue:0.4, alpha:1)
        case .yellow:
            instanceColor = UIColor(hexString: "#ECD61E")
        case .yellow2:
            instanceColor = UIColor(red:0.93, green:0.84, blue:0.12, alpha:1)
            
        case .gray1:
            instanceColor = UIColor(hexString: "#333333") // UIColor(red:0.2, green:0.2, blue:0.2, alpha:1)
        case .gray2:
            instanceColor = UIColor(hexString: "#444444") // UIColor.darkGray
        case .gray3:
            instanceColor = UIColor(hexString: "#666666") // UIColor(red:0.4, green:0.4, blue:0.4, alpha:1)
        case .gray4:
            instanceColor = UIColor(hexString: "#B0B0B0") // UIColor.lightGray
        case .gray5:
            instanceColor = UIColor(hexString: "#CCCCCC") // UIColor(red:0.8, green:0.8, blue:0.8, alpha:1)
        case .gray6:
            instanceColor = UIColor(hexString: "#ECECEC")
        case .gray7:
            instanceColor = UIColor(hexString: "#F7F7F7")
        case .gray8:
            instanceColor = UIColor(red:1, green:1, blue:1, alpha:0.75)
            
        case .black1:
            instanceColor = UIColor(hexString: "#07202B")
        case .black2:
            instanceColor = UIColor(hexString: "#253740")
        case .black3:
            instanceColor = UIColor(hexString: "#45545C")
        case .black4:
            instanceColor = UIColor(hexString: "#748086")
        case .black5:
            instanceColor = UIColor(hexString: "#98A1A6")
        case .black6:
            instanceColor = UIColor(hexString: "#BFC4C7")
        case .black7:
            instanceColor = UIColor(hexString: "#D9DBDD")
        case .black8:
            instanceColor = UIColor(hexString: "#E0E2E2")
        case .black9:
            instanceColor = UIColor(red:0, green:0, blue:0, alpha:0.9) // usado en comprar lavado
        case .backgroundBlack:
            instanceColor = UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.8)
            
        case .whiteSmoke:
            instanceColor = UIColor(hexString: "#F6F6F6")
        case .separator:
            instanceColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1)
        case .elephant:
            instanceColor = UIColor(red:0.2, green:0.2, blue:0.2, alpha:0.35)
        case .astral:
            instanceColor = UIColor(red:0.19, green:0.41, blue:0.5, alpha:1)
        case .atomic:
            instanceColor = UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.75)
        case .solitude:
            instanceColor = UIColor(red:0.19, green:0.41, blue:0.5, alpha:0.1)
        case .nileBlue:
            instanceColor = UIColor(red:0.07, green:0.64, blue:0.89, alpha:0.2)
        case .mirage:
            instanceColor = UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.7)
        case .matisse:
            instanceColor = UIColor(red:0.07, green:0.64, blue:0.89, alpha:0.25)
        case .daintree:
            instanceColor = UIColor(red:0.03, green:0.13, blue:0.17, alpha:1.0)
            
        case .custom(let hexValue, let opacity):
            instanceColor = UIColor(hexString: hexValue).withAlphaComponent(CGFloat(opacity))
        }
        return instanceColor
    }
}

extension UIColor {
    /**
     Creates an UIColor from HEX String in "#363636" format
     
     - parameter hexString: HEX String in "#363636" format
     
     - returns: UIColor from HexString
     */
    convenience init(hexString: String) {
        
        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner          = Scanner(string: hexString as String)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let rrr = Int(color >> 16) & mask
        let ggg = Int(color >> 8) & mask
        let bbb = Int(color) & mask
        
        let red   = CGFloat(rrr) / 255.0
        let green = CGFloat(ggg) / 255.0
        let blue  = CGFloat(bbb) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}
