//
//  TextFieldStyle.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Goya

class TextFieldStyle {
    static func registerStyles() {
        registerStylesForTextView()
        /// Login Style
        let loginTextField = Style { (textField: UITextField) -> Void in
            textField.font = Font(.custom(SDKFont.regular), size: .standard(.h2)).instance
            textField.textColor = Color.gray2.value
            textField.tintColor = Color.green.value
            textField.setBottomBorder()
        }
        loginTextField.register(withName: StyleConstants.TextField.logIn)
        
        /// TicketReadingVC
        let ticket = Style { (textField: UITextField) -> Void in
            textField.font = Font(.custom(SDKFont.regular), size: .standard(.title)).instance
            textField.textColor = .white
            textField.tintColor = .white
            textField.backgroundColor = .clear
        }
        ticket.register(withName: StyleConstants.TextField.ticket)
        
        /// primary
        let primary = Style { (textField: UITextField) -> Void in
            textField.textColor = .darkGray
            textField.tintColor = Color.red.value
            textField.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
        }
        primary.register(withName: StyleConstants.TextField.primary)
    }
    
    private static func registerStylesForTextView() {
        /// default
        let primary = Style { (textView: PlaceholderTextView) -> Void in
            textView.placeholder = Utils.lowercaseText(with: .localized(.addComment))
            textView.typingAttributes = [NSAttributedStringKey.font.rawValue: Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance,
                                         NSAttributedStringKey.foregroundColor.rawValue: UIColor.darkGray]
        }
        primary.register(withName: StyleConstants.TextView.primary)
        
        /// Terms (Main)
        let terms = Style { (textView: UITextView) -> Void in
            let text: String = .localized(.infoTerms)
            let font = Font(.custom(SDKFont.regular), size: .standard(.h4)).instance
            let bold = Font(.custom(SDKFont.semiBold), size: .standard(.h4)).instance
            
            let attributedString = NSMutableAttributedString(string: text)
            var linkRange = attributedString.mutableString.range(of: text)
            attributedString.addAttribute(.font, value: font, range: linkRange)
            attributedString.addAttribute(.foregroundColor, value: UIColor.lightGray, range: linkRange)
            
            // General Conditions
            linkRange = attributedString.mutableString.range(of: NSLocalizedString("info_terms_general_conditions", comment: ""))
            attributedString.addAttribute(.link, value: LegalText.generalConditions.url, range: linkRange)
            attributedString.addAttribute(.font, value: bold, range: linkRange)
            
            // Conditions of use
            linkRange = attributedString.mutableString.range(of: NSLocalizedString("info_terms_conditions_use", comment: ""))
            attributedString.addAttribute(.link, value: LegalText.legalTerms.url, range: linkRange)
            attributedString.addAttribute(.font, value: bold, range: linkRange)
            
            // Privacy policy
            linkRange = attributedString.mutableString.range(of: NSLocalizedString("info_terms_privacy_policy", comment: ""))
            attributedString.addAttribute(.link, value: LegalText.privacyPolicy.url, range: linkRange)
            attributedString.addAttribute(.font, value: bold, range: linkRange)
            
            textView.attributedText = attributedString
            textView.tintColor = .lightGray
        }
        terms.register(withName: StyleConstants.TextView.terms)
    }
}
