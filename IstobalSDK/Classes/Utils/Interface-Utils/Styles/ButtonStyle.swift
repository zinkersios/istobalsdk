//
//  ButtonStyle.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Goya

class ButtonStyle {
    private static let borderWidth = CGFloat(2.0)
    
    // MARK: - Main
    static func registerStyles() {
        /// primary button style
        let primaryButton = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
            button.backgroundColor = Color.green.value
            button.setTitleColor(.white, for: .normal)
            button.tintColor = .white
            button.clipsToBounds = true
            button.layer.cornerRadius = StyleConstants.CornerRadius.main
            button.setBackgroundColor(color: UIColor.black.withAlphaComponent(0.8), forState: .highlighted)
        }
        primaryButton.register(withName: StyleConstants.Button.primary)
        
        /// primary XL style
        let primaryXL = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
            button.backgroundColor = Color.green.value
            button.setTitleColor(.white, for: .normal)
            button.tintColor = .white
            button.clipsToBounds = true
            button.layer.cornerRadius = StyleConstants.CornerRadius.main
            button.setBackgroundColor(color: UIColor.black.withAlphaComponent(0.8), forState: .highlighted)
        }
        primaryXL.register(withName: StyleConstants.Button.primaryXL)
        
        /// secondary button style
        let secondaryButton = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
            button.backgroundColor = Color.gray6.value
            button.setTitleColor( Color.gray3.value, for: .normal)
            button.tintColor = Color.gray3.value
            button.clipsToBounds = true
            button.layer.cornerRadius = StyleConstants.CornerRadius.main
        }
        secondaryButton.register(withName: StyleConstants.Button.secondary)
        
        /// link button style
        let linkButton = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
            button.setTitleColor( Color.green3.value, for: .normal)
        }
        linkButton.register(withName: StyleConstants.Button.link)
        
        /// link gray button style
        let linkLoginButton = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
            button.setTitleColor( Color.gray4.value, for: .normal)
        }
        linkLoginButton.register(withName: StyleConstants.Button.linkLogin)
        
        // MARK: Log In
        let viewPassButton = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
            button.setTitleColor( Color.gray4.value, for: .normal)
            button.tintColor = Color.gray4.value
        }
        viewPassButton.register(withName: StyleConstants.Button.viewPassword)
        
        // MARK: AlertController
        
        /// alert button style
        let alertButton = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
            button.backgroundColor = Color.green.value
            button.setTitleColor(Color.gray3.value, for: .normal)
            button.tintColor = .white
            button.clipsToBounds = true
            button.layer.cornerRadius = StyleConstants.CornerRadius.main
            button.backgroundColor = Color.separator.value
        }
        alertButton.register(withName: StyleConstants.Button.alert)
        
        registerGeneral()
        registerSmartWash()
        registerCheckbox()
        registerInstallationDetail()
        registerAddVehicle()
    }
    
    // MARK: - General
    static private func registerGeneral() {
        /// price
        let price = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
            button.tintColor = Color.blue.value
        }
        price.register(withName: StyleConstants.Button.price)
        
        /// steps instructions
        let stepsInstructions = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h1_5)).instance
            button.tintColor = .white
            button.backgroundColor = Color.blue.value
        }
        stepsInstructions.register(withName: StyleConstants.Button.stepsInstructions)
        
        /// price for addon
        let priceForAddon = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h5_5)).instance
            button.tintColor = .lightGray
            button.layer.cornerRadius = 18
        }
        priceForAddon.register(withName: StyleConstants.Button.priceForAddon)
        
        // showQR button
        let showQR = Style { (button: UIButton) -> Void in
            button.backgroundColor = Color.blue.value
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
            button.tintColor = .white
            button.contentEdgeInsets = UIEdgeInsetsMake(1, 4, 1, 4)
            button.setTitle("   " + .localized(.showQR), for: .normal)
            
            // Shadow & cornerRadius
            button.clipsToBounds = true
            button.layer.cornerRadius = 4
            button.addDropShadow(with: StyleConstants.Offset.secondary,
                                 radius: 4,
                                 opacity: StyleConstants.Opacity.full,
                                 shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.16))
        }
        showQR.register(withName: StyleConstants.Button.showQRInTicket)
        
        /// Edit
        let edit = Style { (button: UIButton) -> Void in
            button.setTitle( Utils.capitalisedText(with: .localized(.delete)), for: .normal)
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h4)).instance
            button.tintColor = Color.red.value
        }
        edit.register(withName: StyleConstants.Button.edit)
    }
    
    // MARK: - Smartwash
    static private func registerSmartWash() {
        /// Assistant normal
        let assistant = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
            button.tintColor = .darkGray
            button.backgroundColor = .white
            button.titleLabel?.lineBreakMode = .byWordWrapping
        }
        assistant.register(withName: StyleConstants.AssistantButton.assistantNormal)
        
        /// Assistant selected
        let assistantSelected = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
            button.tintColor = .white
            button.backgroundColor = Color.green.value
            button.titleLabel?.lineBreakMode = .byWordWrapping
        }
        assistantSelected.register(withName: StyleConstants.AssistantButton.assistantSelected)
        
        /// next step
        let nextStep = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
            button.tintColor = .white
            button.backgroundColor = Color.green.value
            button.setBackgroundColor(color: UIColor.black.withAlphaComponent(0.8), forState: .highlighted)
        }
        nextStep.register(withName: StyleConstants.AssistantButton.nextStep)
    }
    
    static private func registerCheckbox() {
        // MARK: - Filters
        let filterCheckbox = Style { (checkbox: VKCheckbox) -> Void in
            checkbox.line             = .thin
            checkbox.bgColorSelected  = .white
            checkbox.bgColor          = Color.red.value
            checkbox.color            = Color.red.value
            checkbox.borderColor      = UIColor.white
            checkbox.borderWidth      = 2
            checkbox.cornerRadius     = checkbox.frame.height / 2
        }
        filterCheckbox.register(withName: StyleConstants.Checkbox.filters)
        
        // MARK: - CustomizeWashCell & AssistantStep4ViewController
        let washCheckbox = Style { (checkbox: VKCheckbox) -> Void in
            checkbox.bgColorSelected = Color.green.value
            checkbox.bgColor         = Color.elephant.value
            checkbox.color           = .white
            checkbox.borderColor     = .lightGray
            checkbox.borderWidth     = 2
            checkbox.cornerRadius    = checkbox.frame.height / 2
        }
        washCheckbox.register(withName: StyleConstants.Checkbox.wash)
    }
    
    // MARK: - Installation detail
    static private func registerInstallationDetail() {
        /// ticket status
        let ticketStatus = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h4)).instance
            button.backgroundColor = Color.green2.value
            button.setTitleColor( Color.green3.value, for: .normal)
            button.layer.cornerRadius = 15
        }
        ticketStatus.register(withName: StyleConstants.Button.ticketStatus)
        
        /// Phone
        let phone = Style { (button: UIButton) -> Void in
            button.setTitleColor(.darkGray, for: .normal)
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h4)).instance
        }
        phone.register(withName: StyleConstants.InstallationButton.phone)
        
        /// View Route button
        let viewRoute = Style { (button: UIButton) -> Void in
            button.setTitle(.localized(.viewRoute), for: .normal)
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
            button.setTitleColor(Color.gray3.value, for: .normal)
            button.layer.cornerRadius = 15
            button.backgroundColor = Color.gray6.value
            button.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15)
        }
        viewRoute.register(withName: StyleConstants.InstallationButton.viewRoute)
        
        /// options
        let moreOptions = Style { (button: UIButton) -> Void in
            button.tintColor = .white
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        }
        moreOptions.register(withName: StyleConstants.InstallationButton.option)
        
        /// dismiss options
        let dismissOptions = Style { (button: UIButton) -> Void in
            button.tintColor = .white
            button.titleLabel?.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
            button.setTitle(.localized(.cancel), for: .normal)
        }
        dismissOptions.register(withName: StyleConstants.InstallationButton.dismissOptions)
    }
    
    // MARK: - Add Vehicle
    static private func registerAddVehicle() {
        
        /// button with blue border style
        let buttonWithBorder = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h2)).instance
            button.setTitleColor( Color.blue.value, for: .normal)
            button.tintColor = Color.blue.value
            button.clipsToBounds = true
            button.layer.cornerRadius = StyleConstants.CornerRadius.main
            button.layer.borderWidth = borderWidth
            button.layer.borderColor = Color.blue.value.cgColor
        }
        buttonWithBorder.register(withName: StyleConstants.AddVehicleButton.border)
        
        /// next button
        let nextPage = Style { (button: UIButton) -> Void in
            button.backgroundColor = Color.green.value
            button.tintColor = .white
            button.clipsToBounds = true
            button.layer.cornerRadius = StyleConstants.CornerRadius.main
            // Shadow
            button.addDropShadow(with: StyleConstants.Offset.secondary,
                                 radius: 8,
                                 opacity: StyleConstants.Opacity.full,
                                 shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.28))
        }
        nextPage.register(withName: StyleConstants.AddVehicleButton.next)
        
        // See all tickets
        /// link button style
        let seeAllTickets = Style { (button: UIButton) -> Void in
            button.titleLabel?.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
            button.setTitleColor( Color.green.value, for: .normal)
            button.backgroundColor = Color.whiteSmoke.value
            button.setTitle(Utils.capitalisedText(with: .localized(.seeAllTickets)), for: .normal)
        }
        seeAllTickets.register(withName: StyleConstants.Button.seeAllTickets)
    }
}
