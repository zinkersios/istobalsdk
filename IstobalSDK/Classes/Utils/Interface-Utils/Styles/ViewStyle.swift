//
//  ViewStyle.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 18/8/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Goya

final class ViewStyle {
    static func registerStyles() {
        
        /// nav bar used in: LegalNoticeViewController
        let navBar = Style { (navigationBar: UINavigationBar) -> Void in
            navigationBar.tintColor = Color.astral.value
            navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Color.astral.value,
                                                 NSAttributedStringKey.font: Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance]
        }
        navBar.register(withName: StyleConstants.NavBar.astral)
        
        /// Credit View. Used in InstallationHeaderCell
        let credit = Style { (view: UIView) -> Void in
            view.layer.cornerRadius = StyleConstants.CornerRadius.main
            view.addDropShadow()
        }
        credit.register(withName: StyleConstants.View.credit)
        
        /// Prices
        let prices = Style { (view: UIView) -> Void in
            view.layer.cornerRadius = StyleConstants.CornerRadius.main
            view.addDropShadow(with: StyleConstants.Offset.secondary,
                               radius: 10,
                               opacity: StyleConstants.Opacity.full,
                               shadowColor: UIColor(red:0, green:0, blue:0, alpha:0.2))
        }
        prices.register(withName: StyleConstants.View.prices)
        registerForTableView()
    }
    
    private static func registerForTableView() {
        /// Main TableView
        let main = Style { (tableView: UITableView) -> Void in
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 60.0
            tableView.rowHeight = UITableViewAutomaticDimension
        }
        main.register(withName: StyleConstants.TableView.main)
        
        /// Offer TableView
        let offerTableView = Style { (tableView: UITableView) -> Void in
            tableView.backgroundColor = Color.gray7.value
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 100.0
            tableView.rowHeight = UITableViewAutomaticDimension
        }
        offerTableView.register(withName: StyleConstants.TableView.offers)
        
        /// Purchase Summary TableView
        let purchaseSummaryTableView = Style { (tableView: UITableView) -> Void in
            tableView.backgroundColor = Color.gray7.value
            tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 50))
            tableView.separatorColor = Color.separator.value
            tableView.estimatedRowHeight = 60.0
            tableView.rowHeight = UITableViewAutomaticDimension
        }
        purchaseSummaryTableView.register(withName: StyleConstants.TableView.purchaseSummary)
    }
}
