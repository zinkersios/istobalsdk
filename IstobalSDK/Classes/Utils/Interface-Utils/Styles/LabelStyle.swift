//
//  LabelStyle.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import UIKit
import Goya

final class LabelStyle {
    
    static func registerStyles() {
        /// welcome
        let welcome = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.secondaryLabel
            label.font = Font(.custom(SDKFont.regular), size: .standard(.header)).instance
        }
        welcome.register(withName: StyleConstants.LoginLabel.welcome)
        
        /// Terms && HeaderTicketDetail
        let terms = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.secondaryLabel
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h4)).instance
        }
        terms.register(withName: StyleConstants.LoginLabel.terms)
        
        // MARK: - Log In
        
        /// Header
        let headerLogIn = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.primaryLabel
            label.font = Font(.custom(SDKFont.regular), size: .standard(.header)).instance
        }
        headerLogIn.register(withName: StyleConstants.LoginLabel.header)
        
        /// Label Input
        let inputLogIn = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.secondaryLabel
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        }
        inputLogIn.register(withName: StyleConstants.LoginLabel.input)
        
        // MARK: - Side Menu
        
        /// Slogan
        let slogan = Style { (label: UILabel) -> Void in
            label.textColor = UIColor.white.withAlphaComponent(0.75)
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        }
        slogan.register(withName: StyleConstants.SideMenu.slogan)
        
        let usernameSideMenu = Style { (label: UILabel) -> Void in
            label.textColor = .white
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h1)).instance
        }
        usernameSideMenu.register(withName: StyleConstants.SideMenu.username)
        
        // MARK: - Filters (FilterCentersViewController) - ScanRegistrationNumberViewController
        let filterLabel = Style { (label: UILabel) -> Void in
            label.textColor = .white
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
        }
        filterLabel.register(withName: StyleConstants.Label.titleCell)
        
        // MARK: - AlertView
        let titleAlertView = Style { (label: UILabel) -> Void in
            label.textColor = Color.astral.value
            label.font = Font(.custom(SDKFont.bold), size: .standard(.title)).instance
        }
        titleAlertView.register(withName: StyleConstants.Label.titleAlertView)
        
        registerInstallationDetail()
        registerAddVehicle()
        registerForGeneral()
        registerForWash()
        registerForProfile()
        registerForTutorial()
        registerForTickets()
    }
    
    // MARK: - Installation detail
    static private func registerInstallationDetail() {
        /// used in ScannerTicketCell, YourTicketsCell, CardView
        let ticketHeader = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.primaryLabel
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h3_5)).instance
        }
        ticketHeader.register(withName: StyleConstants.InstallationDetailLabel.ticketHeader)
        
        /// used in YourTicketsCell, ScannerTicketCell, InstallationHeaderCell, CardView
        let ticketNumber = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.primaryLabel
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        }
        ticketNumber.register(withName: StyleConstants.InstallationDetailLabel.ticketNumber)
        
        /// used in PurchaseSummaryCenterCell, ReportProblemViewController
        let small = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.primaryLabel
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h6_5)).instance
        }
        small.register(withName: StyleConstants.InstallationDetailLabel.small)
        
        /// see offers
        let seeOffers = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.primaryLabel
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h6)).instance
        }
        seeOffers.register(withName: StyleConstants.InstallationDetailLabel.seeOffers)
        
        /// Bold with size h2_5
        let boldH1Dot5 = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.primaryLabel
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h1_5)).instance
        }
        boldH1Dot5.register(withName: StyleConstants.Label.boldH1Dot5)
    }
    
    // MARK: - Add Vehicle
    static private func registerAddVehicle() {
        /// title intro
        let titleIntro = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.light), size: .standard(.header)).instance
        }
        titleIntro.register(withName: StyleConstants.AddVehicleLabel.titleIntro)
        
        /// subtitle intro
        let subtitleIntro = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray4.value
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        }
        subtitleIntro.register(withName: StyleConstants.AddVehicleLabel.subtitleIntro)
        
        /// number of pages
        let numberOfPages = Style { (label: UILabel) -> Void in
            label.textColor = SDKColor.primarTitle
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        }
        numberOfPages.register(withName: StyleConstants.AddVehicleLabel.numberOfPages)
        
        /// Label below TextField
        let labelBelowTextField = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray4.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        }
        labelBelowTextField.register(withName: StyleConstants.Label.belowTextField)
        
        /// Label Input
        let inputLabel = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h3)).instance
        }
        inputLabel.register(withName: StyleConstants.AddVehicleLabel.input)
        
        /// Label total
        let totalWashes = Style { (label: UILabel) -> Void in
            label.textColor = Color.green.value
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h4_5)).instance
        }
        totalWashes.register(withName: StyleConstants.AddVehicleLabel.totalWashes)
        
        // Washes
        let days = Style { (label: UILabel) -> Void in
            label.textColor = .white
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h4_5)).instance
        }
        days.register(withName: StyleConstants.AddVehicleLabel.washes)
    }
    
    // MARK: - General
    //swiftlint:disable:next function_body_length
    static private func registerForGeneral() {
        /// Loading
        let loadingLabel = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray4.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
            label.textAlignment = .center
        }
        loadingLabel.register(withName: StyleConstants.Label.loading)
        
        /// default section header
        let sectionHeader = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray3.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h3)).instance
        }
        sectionHeader.register(withName: StyleConstants.Label.sectionHeader)
        
        /// title label
        let title = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.bold), size: .standard(.title)).instance
        }
        title.register(withName: StyleConstants.Label.title)
        
        /// title blue label
        let titleBlue = Style { (label: UILabel) -> Void in
            label.textColor = Color.astral.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.title)).instance
        }
        titleBlue.register(withName: StyleConstants.Label.titleBlue)
        
        /// title white label
        let titleWhite = Style { (label: UILabel) -> Void in
            label.textColor = .white
            label.font = Font(.custom(SDKFont.bold), size: .standard(.title)).instance
        }
        titleWhite.register(withName: StyleConstants.Label.titleWhite)
        
        /// rating label, ServiceCollectionCell
        let ratingCell = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h5_5)).instance
        }
        ratingCell.register(withName: StyleConstants.Label.ratingCell)
        
        /// header type 1
        let headerLabel = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        }
        headerLabel.register(withName: StyleConstants.Label.header)
        
        /// header type 2
        let header2 = Style { (label: UILabel) -> Void in
            label.textColor = .lightGray
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        }
        header2.register(withName: StyleConstants.Label.header2)
        
        /// header section label (YourTicketsCell, PromotionsCell, HeaderMyVehicleCell)
        let sectionHeader2 = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray3.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
            label.textAlignment = .center
        }
        sectionHeader2.register(withName: StyleConstants.Label.sectionHeader2)
        
        /// title label for PurchaseSummaryCenterCell && YourTicketsCell - DuringWashingCell - FinishedWashVC - WashingScoreVC
        let titleWashCenterCell = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
        }
        titleWashCenterCell.register(withName: StyleConstants.PurchaseSummaryLabel.centerCell)
        
        /// title label for PurchaseSummaryServicesCell - VehicleDataCell - RepeatAnotherWashCell - PersonalInfoViewController
        let titleServicesCell = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
        }
        titleServicesCell.register(withName: StyleConstants.PurchaseSummaryLabel.servicesCell)
        
        /// total washes: ProfileHeaderViewController - ProfileHeaderViewController
        let totalWashes = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray8.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        }
        totalWashes.register(withName: StyleConstants.Label.totalWashes)
        
        /// PurchaseSummaryPaymentCell, DuringWashingCell, BuyServiceCardCell
        let smallLabel = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray3.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        }
        smallLabel.register(withName: StyleConstants.Label.smallGray3)
        
        /// label small with darkGray color
        let smallDarkGray = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        }
        smallDarkGray.register(withName: StyleConstants.Label.smallDarkGray)
        
        /// Bold with size h2_5
        let boldH2Dot5 = Style { (label: UILabel) -> Void in
            label.textColor = .white
            label.font = Font(.custom(SDKFont.bold), size: .standard(.h2_5)).instance
        }
        boldH2Dot5.register(withName: StyleConstants.Label.boldH2Dot5)
        
        /// Regultar with size h2_5 and blue
        let blueRegularH2Dot5 = Style { (label: UILabel) -> Void in
            label.textColor = Color.blue.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
        }
        blueRegularH2Dot5.register(withName: StyleConstants.Label.blueRegularH2Dot5)
        
        /// Dark Gray. Bold with size 25
        let grayBold25 = Style { (label: UILabel) -> Void in
            label.textColor = .darkGray
            label.font = Font(.custom(SDKFont.bold), size: .custom(25)).instance
        }
        grayBold25.register(withName: StyleConstants.Label.grayBold25)
        
        /// Light Gray. Regultar with H3_5
        let lightGrayRegultarH3Dot5 = Style { (label: UILabel) -> Void in
            label.textColor = .lightGray // or .gray4
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
        }
        lightGrayRegultarH3Dot5.register(withName: StyleConstants.Label.lightGrayRegH2Dot5)
    }
    
    // MARK: - Wash
    static private func registerForWash() {
        /// scan ticket: ScanRegistrationNumberViewController - TicketReadingViewController
        let scanTicket = Style { (label: UILabel) -> Void in
            label.textColor = .white
            label.font = Font(.custom(SDKFont.regular), size: .custom(22)).instance
        }
        scanTicket.register(withName: StyleConstants.WashLabel.infoScan)
        
        /// total washes: StartWashViewController
        let info = Style { (label: UILabel) -> Void in
            label.textColor = Color.atomic.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
        }
        info.register(withName: StyleConstants.WashLabel.info)
        
        /// installation name (blue)
        let installationNameBlue = Style { (label: UILabel) -> Void in
            label.textColor = Color.blue.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h4_5)).instance
        }
        installationNameBlue.register(withName: StyleConstants.WashLabel.installationNameBlue)
    }
    
    // MARK: - Profile
    static private func registerForProfile() {
        /// Statistics gray 3
        let statisticsGray3 = Style { (label: UILabel) -> Void in
            label.textColor = Color.gray3.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h3_5)).instance
        }
        statisticsGray3.register(withName: StyleConstants.ProfileLabel.statisticsGray3)
        
        /// Statistics red 2
        let statisticsRed2 = Style { (label: UILabel) -> Void in
            label.textColor = Color.red2.value
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h2_5)).instance
        }
        statisticsRed2.register(withName: StyleConstants.ProfileLabel.statisticsRed2)
    }
    
    static private func registerForTutorial() {
        /// title white label
        let titleWhite = Style { (label: UILabel) -> Void in
            label.textColor = Color.red.value
            label.font = Font(.custom(SDKFont.bold), size: .standard(.title)).instance
        }
        titleWhite.register(withName: StyleConstants.TutorialLabel.title)
    }
    
    static private func registerForTickets() {
        /// Regular with size h2_5
        let regularH6Dot5 = Style { (label: UILabel) -> Void in
            label.textColor = .white
            label.font = Font(.custom(SDKFont.regular), size: .standard(.h6_5)).instance
        }
        regularH6Dot5.register(withName: StyleConstants.Label.regularH6Dot5)
    }
}
