//
//  FontHandling.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 13/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation
import UIKit
import Device

public enum SDKFamilyFonts: String, CaseIterable{
    case titilliumWeb = "Titillium Web"
}

public enum SDKFont{
    public static var light: String = "TitilliumWeb-Light"
    public static var regular: String = "TitilliumWeb-Regular"
    public static var bold: String = "TitilliumWeb-Bold"
    public static var semiBold: String = "TitilliumWeb-SemiBold"
    public static var italic: String = "TitilliumWeb-Italic"
}

public enum SDKFontSufix{
    public static let light: String = "Light"
    public static let regular: String = "Regular"
    public static let bold: String = "Bold"
    public static let semiBold: String = "SemiBold"
    public static let italic: String = "Italic"
}

struct Font {
    
    enum FontType {
        case installed(FontName)
        case custom(String)
        case system
        case systemBold
        case systemItatic
        case systemWeighted(weight: Double)
        case monoSpacedDigit(size: Double, weight: Double)
    }
    
    enum FontSize {
        case standard(StandardSize)
        case custom(Double)
        var value: Double {
            switch self {
            case .standard(let size):
                return Font.responsiveSize(with: size.rawValue)
            case .custom(let customSize):
                return Font.responsiveSize(with: customSize)
            }
        }
    }
    
    enum FontName: String {
        case titilliumLight      = "TitilliumWeb-Light"
        case titilliumRegular    = "TitilliumWeb-Regular"
        case titilliumBold       = "TitilliumWeb-Bold"
        case titilliumSemiBold   = "TitilliumWeb-SemiBold"
        case titilliumItalic     = "TitilliumWeb-Italic"
        case helveticaNeue       = "HelveticaNeue"
        case helveticaNeueMedium = "HelveticaNeue-Medium"
    }
    
    enum StandardSize: Double {
        case header = 29.0
        case title = 25
        //swiftlint:disable:next identifier_name
        case h1_5 = 21.0
        case h1 = 20.0
        //swiftlint:disable:next identifier_name
        case h2_5 = 19.0
        case h2 = 18.0
        //swiftlint:disable:next identifier_name
        case h3_5 = 17.0
        case h3 = 16.0
        //swiftlint:disable:next identifier_name
        case h4_5 = 15.0
        case h4 = 14.0
        //swiftlint:disable:next identifier_name
        case h5_5 = 13.0
        case h5 = 12.0
        //swiftlint:disable:next identifier_name
        case h6_5 = 11.0
        case h6 = 10.0
    }
    
    var type: FontType
    var size: FontSize
    init(_ type: FontType, size: FontSize) {
        self.type = type
        self.size = size
    }
    
    private static func responsiveSize(with size: Double) -> Double {
        switch Device.size() {
        case .screen3_5Inch, .screen4Inch:
            return size - 2
        case .screen4_7Inch:
            return size - 1
        default:
            return size
        }
    }
}

extension Font {
    
    var instance: UIFont {
        
        var instanceFont: UIFont!
        switch type {
        case .custom(let fontName):
            guard let font =  UIFont(name: fontName, size: CGFloat(size.value)) else {
                fatalError("\(fontName) font is not installed, make sure it added in Info.plist and logged with Utility.logAllAvailableFonts()")
            }
            instanceFont = font
        case .installed(let fontName):
            guard let font =  UIFont(name: fontName.rawValue, size: CGFloat(size.value)) else {
                fatalError("\(fontName.rawValue) font is not installed, make sure it added in Info.plist and logged with Utility.logAllAvailableFonts()")
            }
            instanceFont = font
        case .system:
            instanceFont = UIFont.systemFont(ofSize: CGFloat(size.value))
        case .systemBold:
            instanceFont = UIFont.boldSystemFont(ofSize: CGFloat(size.value))
        case .systemItatic:
            instanceFont = UIFont.italicSystemFont(ofSize: CGFloat(size.value))
        case .systemWeighted(let weight):
            instanceFont = UIFont.systemFont(ofSize: CGFloat(size.value),
                                             weight: UIFont.Weight(rawValue: CGFloat(weight)))
        case .monoSpacedDigit(let size, let weight):
            instanceFont = UIFont.monospacedDigitSystemFont(ofSize: CGFloat(size),
                                                            weight: UIFont.Weight(rawValue: CGFloat(weight)))
        }
        return instanceFont
    }
}

#if !swift(>=4.2)
public protocol CaseIterable {
    associatedtype AllCases: Collection where AllCases.Element == Self
    static var allCases: AllCases { get }
}
extension CaseIterable where Self: Hashable {
    public static var allCases: [Self] {
        return [Self](AnySequence { () -> AnyIterator<Self> in
            var raw = 0
            var first: Self?
            return AnyIterator {
                let current = withUnsafeBytes(of: &raw) { $0.load(as: Self.self) }
                if raw == 0 {
                    first = current
                } else if current == first {
                    return nil
                }
                raw += 1
                return current
            }
        })
    }
}
#endif


