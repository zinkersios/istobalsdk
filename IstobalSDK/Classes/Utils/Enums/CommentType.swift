//
//  CommentType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 8/3/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum CommentType: String, EnumCollection {
    case suggest = "suggestion"
    case problem = "problem"
}

extension CommentType {
    var detail: String {
        switch self {
        case .suggest:
            return .localized(.makeSuggestion)
        case .problem:
            return .localized(.issueReport)
        }
    }
}
