//
//  TicketEnum.swift
//  istobal
//
//  Created by Shanti Rodríguez on 9/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum TicketAction {
    case selectVehicle
    case showInfo(typeOfService: TypeOfService?)
    case showQR(typeOfService: TypeOfService?)
    case startWash(typeOfService: TypeOfService?)
    case showMessage(title: String?, message: String?)
    case showInfoTerminal(typeOfService: TypeOfService?, error: Error?)
    case showLegalNotice(typeOfService: TypeOfService?)
}
