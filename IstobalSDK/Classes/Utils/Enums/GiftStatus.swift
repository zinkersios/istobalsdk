//
//  GiftStatus.swift
//  istobal
//
//  Created by Shanti Rodríguez on 26/2/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum GiftStatus: String {
    case none = "gift_none"
    case sent = "gift_sent"
    case received = "gift_received"
}

extension GiftStatus {
    var alpha: Float {
        switch self {
        case .none:
            return 0.0
        default:
            return 1.0
        }
    }
    
    var iconName: String {
        switch self {
        case .sent:
            return "ic_gift_out2"
        default:
            return "ic_gift_out"
        }
    }
}
