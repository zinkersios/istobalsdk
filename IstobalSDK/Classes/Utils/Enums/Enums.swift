//
//  Enums.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 17/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

// MARK: - Menu
enum TypeController: String {
    case home          = "showCenterController"
    case smartwash     = "showSmartWashViewController"
    case myVehicles    = "showMyVehiclesViewController"
    case myTickets     = "showMyTicketsViewController"
    case notifications = "showNotificationsViewController"
    case ticketReading = "showTicketReadingViewController"
    case logIn         = "showLogInViewController"
    case registry      = "showRegistryViewController"
    case aboutUs       = "showAboutUsViewController"
    case support       = "support"
    case myCards       = "showMyCardsViewController"
}

enum LoginState: Int {
    case unknown = 0
    case guest
    case logged
}

enum MenuTableSection: Int {
    case user = 0, aboutUs
}

// MARK: - API
enum APIState {
    case loading
    case results
    case noResults
    case noInternet
    case error
}

enum ItemsState {
    case none, getting
}

enum Backend: String {
    case facebook = "https://acme.com/facebook"
    case google = "https://acme.com/google"
    case normal = ""
}

enum FormStatus {
    case error
    case success
}

enum DropShadowType {
    case byDefault
    case round
    case cell
}

enum ChangeRootVC: Int {
    case unknown
    case logIn
    case register
}

enum NavBarStyle {
    case byDefault
    case myProfile
    case installationDetail
    case buyWash
}

// MARK: - Customize Wash
enum CustomizeWashTableSection: Int {
    case services = 0, addons
}

// MARK: SJSegmentedType
public enum SegmentedType {
    case byDefault
    case myTickets
    case myCard
    case addCard
    case installations
}

// MARK: - My Vehicles
enum EditVehicleTableSection: Int {
    case vehicleData = 0, drivers, remove
}

enum EditVehicleCellType {
    case name
    case brand
    case model
}

// MARK: - SmartWash
enum SmartWashTableSection: Int {
    case repeatWash = 0, assistant, saved
}

enum LevelOfDirt: Int {
    case level1 = 0, level2, level3
}

enum DirtyArea: Int {
    case any = 0, frontal, rear, sides, bottom, wheel, all
}

// MARK: - Profile
enum TypePersonalInformation {
    case name, surnames, email, password, phone, pin
}

enum ChosenAction {
    case accept, cancel, data(Any)
}

// MARK: - Washing...
/// tipo de parada de emergencia. Normal(el usuario lo activa). background(Cuando la app estuve cerrada y se carga desde el home la notificación).
enum TypeOfStop {
    case normal, foreground, background
}

// MARK: AboutUsViewController
enum WebViewType {
    case aboutUs, bookAppointment
}

// MARK: - Label
enum BigSmallText {
    case byDefault, buyService, ticketService
}

// MARK: - General
enum IsFrom {
    case none
    case purchaseSummary
}
