//
//  CardType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 18/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation
import UIKit.UIColor
import Device

enum LoyaltyCardType: String {
    case none
    case balance = "tarjeta_saldo"
    case abono = "tarjeta_abono"
}

extension LoyaltyCardType {
    var backgroundColor: UIColor {
        switch self {
        case .none:
            return .white
        default:
            return Color.red2.value
        }
    }
    
    static var height: CGFloat {
        switch Device.size() {
        case .screen3_5Inch, .screen4Inch:
            return 180
        case .screen4_7Inch:
            return 190
        default:
            return 200
        }
    }
    
    static var width: CGFloat {
        switch Device.size() {
        case .screen3_5Inch, .screen4Inch:
            return Constants.Size.screenWidth - 30
        default:
            return Constants.Size.screenWidth - 60
        }
    }
}

// MARK: Actions
enum CardAction {
    case seeQR, chargeBalance
}

enum CreateCardFrom {
    case myCards, installation
}

// MARK: - Transactions
enum TransactionType: String {
    case income = "ingreso"
    case expenses = "gasto"
}

extension TransactionType {
    var color: UIColor {
        switch self {
        case .income:
            return Color.green.value
        case .expenses:
            return Color.red.value
        }
    }
    
    var symbol: String {
        switch self {
        case .income:
            return "+"
        case .expenses:
            return "-"
        }
    }
}

// MARK: - Type of payment method
enum PaymentMethod: Int {
    case card = 1
    case fidelity = 2
}

// MARK: - Add Card
enum AddCardStatus {
    case normal, cardType
}

extension AddCardStatus {
    var question: String {
        switch self {
        case .normal:
            return .localized(.loyaltyCardQuestion)
        case .cardType:
            return .localized(.typeOfCardToAdd)
        }
    }
    
    var firstText: String {
        switch self {
        case .normal:
            return .localized(.haveCard)
        case .cardType:
            return .localized(.balanceCard)
        }
    }
    
    var secondText: String {
        switch self {
        case .normal:
            return .localized(.createCard)
        case .cardType:
            return .localized(.abonoCard)
        }
    }
}

// Do I have a card?
enum HaveAcard {
    case yes, no
}
