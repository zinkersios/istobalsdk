//
//  RedeemCodeType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 13/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: Ticket Reading

enum CodeRedeemedType {
    case ticket
    case installation // offer with installation
    case offer
    case none
}

// Code reading type
enum CodeReadingType {
    case ticket
    case offer
}

extension CodeReadingType {
    var title: String {
        switch self {
        case .ticket:
            return .localized(.ticketReading)
        case .offer:
            return .localized(.addDiscountCoupon)
        }
    }
    
    var enterCode: String {
        switch self {
        case .ticket:
            return .localized(.enterTicketCode)
        case .offer:
            return .localized(.enterCode)
        }
    }
    
    var messageForManual: String {
        switch self {
        case .ticket:
            return .localized(.enterCodeManuallyDetail)
        case .offer:
            return .localized(.enterOfferCodeManually)
        }
    }
    
    var messageForQR: String {
        switch self {
        case .ticket:
            return .localized(.scanYourTicketInfo)
        case .offer:
            return .localized(.scanYourOffer)
        }
    }
}
