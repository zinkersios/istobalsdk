//
//  CellType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 21/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum CellType {
    case installations
    case favorites
    case searchWashCenters
    case allTickets
    case activeTickets
    case ticketsConsumed
    case ticketsForMyVehicles
    case valorations
    case notifications
    case myCards
    case washingBoxes
    case transactions
}

extension CellType {
    var loading: String {
        switch self {
        case .installations, .searchWashCenters:
            return .localized(.gettingInstallations)
        case .favorites:
            return .localized(.gettingFavorites)
        case .allTickets, .activeTickets, .ticketsConsumed, .ticketsForMyVehicles:
            return .localized(.gettingTickets)
        case .valorations:
            return .localized(.gettingValorations)
        case .notifications:
            return .localized(.gettingNotifications)
        case .myCards:
            return .localized(.gettingMyCards)
        case .transactions:
            return .localized(.gettingTransactions)
        default:
            return ""
        }
    }
    
    var noResults: String {
        switch self {
        case .installations, .searchWashCenters:
            return .localized(.emptyInstallations)
        case .favorites:
            return .localized(.emptyFavorites)
        case .allTickets:
            return .localized(.emptyTickets)
        case .activeTickets:
            return .localized(.emptyActiveTickets)
        case .ticketsConsumed:
            return .localized(.emptyTicketsConsumed)
        case .ticketsForMyVehicles:
            return .localized(.emptyTicketsForMyVehicles)
        case .valorations:
            return .localized(.emptyValorations)
        case .notifications:
            return .localized(.emptyNotifications)
        case .myCards:
            return .localized(.emptyMyCards)
        case .transactions:
            return .localized(.emptyTransactions)
        default:
            return ""
        }
    }
}
