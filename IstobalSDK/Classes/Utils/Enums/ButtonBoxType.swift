//
//  ButtonBoxType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 9/7/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation
import UIKit.UIColor

enum ButtonBoxType {
    case serviceInfo
    case seeTicketQR
    case startServiceIsActive
    case startServiceIsNotActive
}

extension ButtonBoxType {
    var title: String {
        switch self {
        case .serviceInfo:
            return .localized(.info)
        case .seeTicketQR:
            return Utils.capitalisedText(with: .localized(.showQR))
        case .startServiceIsActive, .startServiceIsNotActive:
            return Utils.capitalisedText(with: .localized(.start))
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .serviceInfo:
            return .clear
        case .seeTicketQR:
            return .white
        case .startServiceIsActive:
            return Color.green.value
        case .startServiceIsNotActive:
            return Color.separator.value
        }
    }
    
    var tintColor: UIColor {
        switch self {
        case .serviceInfo:
            return Color.blue.value
        case .seeTicketQR:
            return Color.gray1.value
        case .startServiceIsActive, .startServiceIsNotActive:
            return .white
        }
    }
    
    var cornerRadius: CGFloat {
        switch self {
        case .serviceInfo:
            return CGFloat(0)
        case .seeTicketQR, .startServiceIsActive, .startServiceIsNotActive:
            return StyleConstants.CornerRadius.main
        }
    }
    
    var font: UIFont {
        switch self {
        case .serviceInfo:
            return Font(.custom(SDKFont.regular), size: .standard(.h5_5)).instance
        case .seeTicketQR, .startServiceIsActive, .startServiceIsNotActive:
            return Font(.custom(SDKFont.bold), size: .standard(.h5_5)).instance
        }
    }
    
    var addShadow: Bool {
        switch self {
        case .serviceInfo, .startServiceIsNotActive:
            return false
        case .seeTicketQR, .startServiceIsActive:
            return true
        }
    }
    
    var isEnabled: Bool {
        switch self {
        case .startServiceIsNotActive:
            return false
        default:
            return true
        }
    }
    
    var imageName: String {
        switch self {
        case .serviceInfo:
            return "ic_masinfo"
        case .seeTicketQR:
            return "ic_qr_code"
        case .startServiceIsActive, .startServiceIsNotActive:
            return "ic_play"
        }
    }
}
