//
//  BillingDataType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 3/5/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum BillingDataStatus {
    case none    // user has no billing
    case filled  // user already has all the data filled in
    case filling // user is filling in the data
}

enum BillingAPI {
    case delete // Delete billing assigned to the user
    case create // Create new billing
    case assign // Assign existing billing to the user
}

// MARK: - BillingData
enum BillingData {
    case taxID
    case companyName
    case address
    case city
    case state
    case country
    case postalCode
    case email
}

extension BillingData {
    static var noData: [BillingData] {
        return [.taxID]
    }
    
    static var data: [BillingData] {
        return [.taxID, .companyName, .address, .city, .state, .country, .postalCode, .email]
    }
    
    var brokenRuleMessage: String {
        switch self {
        case .taxID:
            return .localized(.ruleNIF)
        case .companyName:
            return .localized(.ruleCompanyName)
        case .address:
            return .localized(.ruleAddress)
        case .city:
            return .localized(.ruleCity)
        case .state:
            return .localized(.ruleProvince)
        case .country:
            return .localized(.selectCountry)
        case .postalCode:
            return .localized(.ruleZipCode)
        case .email:
            return .localized(.ruleEmail)
        }
    }
}
