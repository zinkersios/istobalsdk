//
//  TicketStatus.swift
//  istobal
//
//  Created by Shanti Rodríguez on 26/2/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation
import UIKit.UIColor

enum TicketStatus: String, Codable {
    case available
    case consumed
    case canceled
    case expired
    case pending
    case redeemed = "canjeado"
}

extension TicketStatus {
    var message: String {
        switch self {
        case .consumed:
            return .localized(.serviceStatusConsumed)
        case .canceled:
            return .localized(.serviceStatusCanceled)
        case .expired:
            return .localized(.serviceStatusExpired)
        default:
            return String(describing: self)
        }
    }
    
    var color: UIColor {
        switch self {
        case .available:
            return Color.green.value
        case .consumed:
            return Color.red.value
        default:
            return Color.yellow2.value
        }
    }
    
    var humanReadable: String {
        switch self {
        case .available:
            return .localized(.ticketActive)
        case .canceled:
            return .localized(.cancelled)
        case .consumed:
            return .localized(.ticketConsumed)
        case .expired:
            return .localized(.ticketExpired)
        case .pending:
            return .localized(.ticketPending)
        case .redeemed:
            return .localized(.redeemed)
        }
    }
}
