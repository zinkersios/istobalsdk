//
//  AlertViewControllerType.swift
//  istobal
//
//  Created by Shanti Rodríguez on 24/2/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation
import UIKit.UIColor

enum AlertViewCell {
    case title, subtitle, accept, cancel, imageAndTitle, textField, selectAmount
}

// MARK: - Type
enum AlertViewControllerType {
    case logOut
    case removeVehicle
    case unsubscribeMe
    case withoutVehicle
    case withoutVehicleInTicket
    case giftReceived
    case update
    case billing
    case billingInfo
//    case schedule
//    case deleteLoyaltyCard
//    case loyaltyCardInfo
//    case selectAmount
}

extension AlertViewControllerType {
    var title: String {
        switch self {
        case .logOut:
            return .localized(.logOut)
        case .removeVehicle:
            return .localized(.removeVehicle)
        case .unsubscribeMe:
            return .localized(.unsubscribeMe)
        case .withoutVehicle:
            return .localized(.vehicle)
        case .withoutVehicleInTicket:
            return .localized(.vehicle)
        case .giftReceived:
            return .localized(.giftReceived)
        case .update:
            return .localized(.checkVersionTitle)
        case .billing:
            return .localized(.billingInformation)
        case .billingInfo:
            return .localized(.billingInfoTitle)
//        case .schedule:
//            return .localized(.schedules)
//        case .selectAmount:
//            return .localized(.selectAnAmount)
//        case .deleteLoyaltyCard:
//            return .localized(.deleteLoyaltyCard)
//        case .loyaltyCardInfo:
//            return .localized(.information)
        }
    }
    
    var subtitle: String {
        switch self {
        case .logOut:
            return .localized(.dialogLogOut)
        case .removeVehicle:
            return .localized(.removeVehicleDetail)
        case .unsubscribeMe:
            return .localized(.unsubscribeMeInfo)
        case .withoutVehicle:
            return .localized(.withoutVehicleInfo)
        case .withoutVehicleInTicket:
            return .localized(.ticketNoVehicle)
        case .giftReceived:
            return .localized(.giftReceivedInfo)
        case .update:
            return .localized(.checkVersionMessage)
        case .billing:
            return .localized(.removeBilling)
        case .billingInfo:
            return .localized(.billingInfoMessage)
//        case.deleteLoyaltyCard:
//            return .localized(.deleteLoyaltyCardInfo)
        default:
            return ""
        }
    }
    
    var acceptMessage: String {
        switch self {
        case .logOut:
            return .localized(.yesCloseSession)
        case .removeVehicle:
            return .localized(.confirmRemoveVehicle)
        case .unsubscribeMe:
            return .localized(.confirmUnsubscribeMe)
        case .withoutVehicle:
            return .localized(.addNewVehicle)
        case .withoutVehicleInTicket:
            return .localized(.ticketAddVehicle)
        case .update:
            return .localized(.checkVersionOk)
//        case .billing, .deleteLoyaltyCard:
//            return .localized(.yesRemove)
//        case .billingInfo, .selectAmount:
//            return .localized(.accept)
        default:
            return ""
        }
    }
    
    var cancelMessage: String {
        switch self {
        case .withoutVehicle:
            return .localized(.buyWithoutAddingVehicle)
        case .withoutVehicleInTicket:
            return .localized(.ticketContinue)
        case .giftReceived, .billingInfo/*, .schedule, .loyaltyCardInfo*/:
            return .localized(.accept)
        case .update:
            return .localized(.checkVersionCancel)
//        case .deleteLoyaltyCard, .selectAmount:
//            return .localized(.cancel)
        default:
            return .localized(.noTakeMeBack)
        }
    }
    
    var cells: [AlertViewCell] {
        switch self {
        case .giftReceived, .billingInfo:
            return [.title, .subtitle, .cancel]
//        case .schedule:
//            return [.imageAndTitle, .subtitle, .cancel]
//        case .selectAmount:
//            return [.title, .selectAmount, .accept, .cancel]
//        case .loyaltyCardInfo:
//            return [.title, .subtitle, .cancel]
        default:
            return [.title, .subtitle, .accept, .cancel]
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
//        case .selectAmount:
//            return UIColor(red:0.03, green:0.13, blue:0.17, alpha:0.7)
        default:
            return Color.backgroundBlack.value
        }
    }
    
    var cellColor: UIColor {
        switch self {
//        case .selectAmount:
//            return UIColor(red:0.07, green:0.21, blue:0.27, alpha:1.00)
        default:
            return .white
        }
    }
}
