//
//  PurchaseSummaryEnum.swift
//  istobal
//
//  Created by Shanti Rodríguez on 13/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum PayKey: String {
    case installation
    case vehicle
    case services
    case user
    case coupon
    case card
    case body
    case handler
    case paymentId
    case fidelityCard
    case amount
    case fidelityDiscount
    case washes
    case packDiscount
    case addBalance
}

enum EditServiceCell {
    case submit, title, titleWithPrice, multiply, header, footer, pack(discount: DiscountPack)
}

// MARK: - Actions
enum PurchaseSummaryAction {
    case showMyTickets
    case showVehicles
    case showCreditCards
    case showCoupons
    case selectedVehicle(vehicle: BasicVehicle)
    case pay
    case selectedOffer(offer: Offer)
    case redeemedCoupon(offer: Offer)
    case selectedService(service: Service)
    case addMoreServices
    case backToLoyaltyCards
    case alert(title: String?, message: String?)
}

// MARK: - Type
enum PurchaseSummaryType {
    case services
    case loyaltyCard
}

// MARK: - Edit Service
enum EditServiceAction {
    case decrease
    case increase
    case update
    case discount(pack: DiscountPack)
}

// MARK: - Purchase summary cell type

enum CenterCellType {
    case vehicle
    case washCenter
    case loyaltyCard
}

enum PurchaseSummaryTableSection: Int {
    case center = 0, services, coupon, payment
}

extension PurchaseSummaryTableSection {
    var title: String {
        switch self {
        case .services:
            return .localized(.services)
        case .payment:
            return .localized(.paymentMethod)
        case .coupon:
            return .localized(.discountCoupon)
        default:
            return ""
        }
    }
    
    var titleOfEdit: String {
        switch self {
        default:
            return Utils.capitalisedText(with: .localized(.change))
        }
    }
    
    var editIsHidden: Bool {
        switch self {
        case .coupon, .payment:
            return false
        default:
            return true
        }
    }
}

// MARK: - Offers
enum OfferType: String, Codable {
    case noCoupon, coupon, loyaltyCard, addCoupon
}

extension OfferType {
    var isCoupon: Bool {
        switch self {
        case .coupon, .loyaltyCard:
            return true
        default:
            return false
        }
    }
}
