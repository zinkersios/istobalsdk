//
//  LegalTexts.swift
//  istobal
//
//  Created by Shanti Rodríguez on 16/4/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Legal Texts

enum LegalText {
    case generalConditions
    case legalTerms
    case privacyPolicy
    
    init?(url: String) {
        switch url {
        case LegalText.generalConditions.url:
            self = .generalConditions
        case LegalText.legalTerms.url:
            self = .legalTerms
        case LegalText.privacyPolicy.url:
            self = .privacyPolicy
        default:
            return nil
        }
    }
}

extension LegalText {
    var title: String {
        switch self {
        case .generalConditions:
            return .localized(.generalConditions)
        case .legalTerms:
            return .localized(.legalTermsAndConditions)
        case .privacyPolicy:
            return .localized(.privacyPolicy)
        }
    }
    
    var url: String {
        switch self {
        case .generalConditions:
            return "https://www.istobal.com/general-conditions"
        case .legalTerms:
            return "https://www.istobal.com/legal-terms"
        case .privacyPolicy:
            return "https://www.istobal.com/privacy-policy"
        }
    }
    
    var pdfName: String {
        switch self {
        case .generalConditions:
            return "general_conditions"
        case .legalTerms:
            return "legal_conditions"
        case .privacyPolicy:
            return "privacity"
        }
    }
}
