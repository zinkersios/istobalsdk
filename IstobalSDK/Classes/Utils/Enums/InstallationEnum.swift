//
//  InstallationEnum.swift
//  istobal
//
//  Created by Shanti Rodríguez on 18/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

enum UnitLength: String {
    case meters = "m"
    case kilometers = "km"
}

enum InstallationOption {
    case reportProblem
}

enum InstallationAction {
    case offers
    case buyWash(services: [Service])
    case bookAppointment
    case alert(title: String?, message: String?)
    case valorations
    case schedules
//    case card(card: LoyaltyCard?)
    case buyService(service: TypeOfService)
}

enum ContactDataType {
    case name, address, phone, email
}

enum InstallationExtraType {
    case offers, tickets
}

// MARK: - cell type
enum InstallationTableSection: Int {
    case header = 0, extra, buyableServices, otherServices, valorations, allValorations
}

extension InstallationTableSection {
    var headerName: String {
        switch self {
        case .buyableServices:
            return .localized(.purchaseServices)
        case .valorations:
            return .localized(.valuations)
        case .otherServices:
            return .localized(.otherServices)
        default:
            return ""
        }
    }
}
