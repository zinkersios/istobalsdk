//
//  Localizable+Wash.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Preparing Wash - During Washing
extension Localizable {
    static let preparingWash = Localizable(NSLocalizedString("preparing_wash", bundle: SDKNavigation.getBundle(), comment: "Nav bar title"))
    static let preparingWashQuestion = Localizable(NSLocalizedString("preparing_wash_question", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let outsideVehicle = Localizable(NSLocalizedString("outside_vehicle", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let insideVehicle = Localizable(NSLocalizedString("inside_vehicle", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let legalNotice = Localizable(NSLocalizedString("ticket_legal", bundle: SDKNavigation.getBundle(), comment: "Nav bar title"))
    static let instructions = Localizable(NSLocalizedString("wash_instructions", bundle: SDKNavigation.getBundle(), comment: "Nav bar title"))
    static let backToTheInstructions = Localizable(NSLocalizedString("wash_return_instruction", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let startWashingFromInside = Localizable(NSLocalizedString("start_washing_from_inside", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let startWashingFromInsideInfo = Localizable(NSLocalizedString("start_washing_from_inside_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let startWashingFromOutside = Localizable(NSLocalizedString("start_washing_from_outside", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let startWashingFromOutsideInfo = Localizable(NSLocalizedString("start_washing_from_outside_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let start = Localizable(NSLocalizedString("wash_start", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let applying = Localizable(NSLocalizedString("wash_proccess", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let didYouKnow = Localizable(NSLocalizedString("did_you_know", bundle: SDKNavigation.getBundle(), comment: "Info Button"))
    static let only = Localizable(NSLocalizedString("only", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let slideUpToStop1 = Localizable(NSLocalizedString("slide_up_to_stop_1", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let slideUpToStop2 = Localizable(NSLocalizedString("wash_stop", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let scrollDownToClose1 = Localizable(NSLocalizedString("scroll_down_to_close_1", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let scrollDownToClose2 = Localizable(NSLocalizedString("wash_close", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let emergencyStop1 = Localizable(NSLocalizedString("wash_stop_title", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let emergencyStop2 = Localizable(NSLocalizedString("wash_emergency_title", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let doYouWantStopMachine = Localizable(NSLocalizedString("do_you_want_stop_machine", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let terminalWashTitle = Localizable(NSLocalizedString("wash_problem_title", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let terminalWashSubTitle = Localizable(NSLocalizedString("wash_problem_instructions", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let legalNoticeTitle = Localizable(NSLocalizedString("ticket_legal_title", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let legalNoticeBody = Localizable(NSLocalizedString("ticket_legal_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let washing = Localizable(NSLocalizedString("washing", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let washed = Localizable(NSLocalizedString("washed", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let machineStoppedTitle = Localizable(NSLocalizedString("wash_stop_success", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let machineStoppedDetail = Localizable(NSLocalizedString("wash_stop_information", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let goTicket = Localizable(NSLocalizedString("go_ticket", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let waitForTheWashToStart = Localizable(NSLocalizedString("wait_for_the_wash_to_start", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let emergencyStopInformation = Localizable(NSLocalizedString("emergency_stop_information", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let infoStopWash = Localizable(NSLocalizedString("wash_stop_extra_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
}

// MARK: - Customize wash
extension Localizable {
    static let customizeWash = Localizable(NSLocalizedString("wash_personal", bundle: SDKNavigation.getBundle(), comment: "NavBar Title"))
    static let customizeWashOption1 = Localizable(NSLocalizedString("customize_wash_option_1", bundle: SDKNavigation.getBundle(), comment: "Washing option 1"))
    static let customizeWashOption2 = Localizable(NSLocalizedString("customize_wash_option_2", bundle: SDKNavigation.getBundle(), comment: "Washing option 2"))
    static let chooseAprogram = Localizable(NSLocalizedString("choose_a_program", bundle: SDKNavigation.getBundle(), comment: "info"))
    static let serviceMoreInfo = Localizable(NSLocalizedString("wash_more_info", bundle: SDKNavigation.getBundle(), comment: "info"))
    static let withoutVehicleInfo = Localizable(NSLocalizedString("purchase_no_vehicle_ios", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let buyWithoutAddingVehicle = Localizable(NSLocalizedString("purchase_continue", bundle: SDKNavigation.getBundle(), comment: "button"))
    static let newPaymentMethod = Localizable(NSLocalizedString("add_new_payment_method", bundle: SDKNavigation.getBundle(), comment: "Label"))
}
