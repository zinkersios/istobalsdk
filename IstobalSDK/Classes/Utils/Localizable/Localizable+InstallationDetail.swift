//
//  Localizable+InstallationDetail.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Installation Detail
extension Localizable {
    static let credit = Localizable(NSLocalizedString("credit", bundle: SDKNavigation.getBundle(), comment: "Money Credit"))
    static let viewRoute = Localizable(NSLocalizedString("show_route", bundle: SDKNavigation.getBundle(), comment: "View Route - Button"))
    static let yourActiveTickets = Localizable(NSLocalizedString("tickets_active", bundle: SDKNavigation.getBundle(), comment: "Your active tickets - Section header"))
    static let ticketActive = Localizable(NSLocalizedString("ticket_status_available", bundle: SDKNavigation.getBundle(), comment: "ticket active - button"))
    static let ticketConsumed = Localizable(NSLocalizedString("ticket_status_consumed", bundle: SDKNavigation.getBundle(), comment: "ticket consumed - button"))
    static let promotions = Localizable(NSLocalizedString("promotions", bundle: SDKNavigation.getBundle(), comment: "promotions - section header"))
    static let buyAwash = Localizable(NSLocalizedString("buy_a_wash", bundle: SDKNavigation.getBundle(), comment: "Buy a wash - NavBar Title"))
    static let chooseServices = Localizable(NSLocalizedString("choose_services", bundle: SDKNavigation.getBundle(), comment: "NavBar Title"))
    static let servicesBody = Localizable(NSLocalizedString("wash_programs_dec", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let emptyServices = Localizable(NSLocalizedString("empty_services_type", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let smartwashBody = Localizable(NSLocalizedString("wash_assitance_desc", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let timetableNotAvailable = Localizable(NSLocalizedString("timetable_not_available", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let noMoreRatings = Localizable(NSLocalizedString("no_more_ratings", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let seeGallery = Localizable(NSLocalizedString("installation_options_gallery", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let installationWithoutPhotos = Localizable(NSLocalizedString("installation_empty_gallery", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let reportSuggestion = Localizable(NSLocalizedString("installation_options_report", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let sendComment = Localizable(NSLocalizedString("installation_report_title", bundle: SDKNavigation.getBundle(), comment: "Nav Bar Title"))
    static let makeSuggestion = Localizable(NSLocalizedString("installation_report_type_suggestion", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let issueReport = Localizable(NSLocalizedString("installation_report_type_report", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let type = Localizable(NSLocalizedString("installation_report_type", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let message = Localizable(NSLocalizedString("installation_report_message", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let offers = Localizable(NSLocalizedString("offers_title", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let showOffers = Localizable(NSLocalizedString("offers_show", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let contactDetails = Localizable(NSLocalizedString("installation_contact", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let bookAppointment = Localizable(NSLocalizedString("booking_wash", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let callIn = Localizable(NSLocalizedString("installation_call", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let schedules = Localizable(NSLocalizedString("installation_schedule", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let purchaseServices = Localizable(NSLocalizedString("installation_buy_service", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let seeAvailableOffers = Localizable(NSLocalizedString("offers_show_all", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let otherServices = Localizable(NSLocalizedString("installation_other_service", bundle: SDKNavigation.getBundle(), comment: "Header name"))
}
