//
//  Localizable+MyProfile.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - My Profile
extension Localizable {
    static let myProfile = Localizable(NSLocalizedString("my_profile", bundle: SDKNavigation.getBundle(), comment: "My Profile - Title NavBar"))
    static let logOut = Localizable(NSLocalizedString("logout", bundle: SDKNavigation.getBundle(), comment: "Log out button"))
    static let myPersonalInfo = Localizable(NSLocalizedString("profile_personal_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let paymentBilling = Localizable(NSLocalizedString("payment_billing", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let preferences = Localizable(NSLocalizedString("profile_preferences", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let personalInfo = Localizable(NSLocalizedString("profile_personal", bundle: SDKNavigation.getBundle(), comment: "NavBar Title"))
    static let changePhoto = Localizable(NSLocalizedString("profile_image", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let username = Localizable(NSLocalizedString("profile_name", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let surnames = Localizable(NSLocalizedString("profile_surname", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let email = Localizable(NSLocalizedString("profile_email", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let password = Localizable(NSLocalizedString("profile_pass", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let phone = Localizable(NSLocalizedString("profile_phone", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let takePhoto = Localizable(NSLocalizedString("take_photo", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let selectFromLibrary = Localizable(NSLocalizedString("select_from_library", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let washes = Localizable(NSLocalizedString("washes", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let averageConsumption = Localizable(NSLocalizedString("stats_consume", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let frequency = Localizable(NSLocalizedString("stats_freq", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let dialogLogOut = Localizable(NSLocalizedString("logout_sure", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let infoPassword = Localizable(NSLocalizedString("info_password", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let differentPasswords = Localizable(NSLocalizedString("pass_diferents", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let yesCloseSession = Localizable(NSLocalizedString("logout_accept", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let noTakeMeBack = Localizable(NSLocalizedString("no_take_me_back", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let generalConditions = Localizable(NSLocalizedString("conditions_general", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let privacyPolicy = Localizable(NSLocalizedString("conditions_privacity", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let legalTermsAndConditions = Localizable(NSLocalizedString("conditions_legal", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let newPassword = Localizable(NSLocalizedString("hint_new_pass", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let unsubscribeMe = Localizable(NSLocalizedString("unsubscribe", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let unsubscribeMeInfo = Localizable(NSLocalizedString("unsubscribe_sure", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let confirmUnsubscribeMe = Localizable(NSLocalizedString("unsubscribe_yes", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let updatePin = Localizable(NSLocalizedString("update_pin", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let mismatchPin = Localizable(NSLocalizedString("pin_diferents", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let minCharactersPin = Localizable(NSLocalizedString("pin_bad_format", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let accessPin = Localizable(NSLocalizedString("profile_pin", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let newPin = Localizable(NSLocalizedString("hint_new_pin", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let repeatPin = Localizable(NSLocalizedString("hint_new_pin_repeat", bundle: SDKNavigation.getBundle(), comment: "Label"))
}
