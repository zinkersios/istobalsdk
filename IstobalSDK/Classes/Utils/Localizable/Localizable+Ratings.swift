//
//  Localizable+Ratings.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Ratings
extension Localizable {
    static let rateLastWash = Localizable(NSLocalizedString("rate_last_wash", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let addComment = Localizable(NSLocalizedString("valoration_add_comment", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let score1 = Localizable(NSLocalizedString("valoration_bad", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let score2 = Localizable(NSLocalizedString("valoration_regular", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let score3 = Localizable(NSLocalizedString("valoration_improvable", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let score4 = Localizable(NSLocalizedString("valoration_good", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let score5 = Localizable(NSLocalizedString("valoration_perfect", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let allRatings = Localizable(NSLocalizedString("btn_show_all_valorations", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let valuations = Localizable(NSLocalizedString("valorations_number_multiple", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let valuation = Localizable(NSLocalizedString("valorations_number_single", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let valuationWithoutName = Localizable(NSLocalizedString("valoration_user_default", bundle: SDKNavigation.getBundle(), comment: "valuation without name"))
}

// MARK: - Rate App
extension Localizable {
    static let rateTitle = Localizable(NSLocalizedString("rate_app_title", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let rateMessage = Localizable(NSLocalizedString("rate_app_info", bundle: SDKNavigation.getBundle(), comment: "Message"))
    static let rateNow = Localizable(NSLocalizedString("rate_now", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let rateLater = Localizable(NSLocalizedString("rate_later", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let rateNot = Localizable(NSLocalizedString("rate_not", bundle: SDKNavigation.getBundle(), comment: "Button"))
}
