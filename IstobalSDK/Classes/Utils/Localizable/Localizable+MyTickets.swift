//
//  Localizable+MyTickets.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - My Tickets
extension Localizable {
    static let code = Localizable(NSLocalizedString("ticket_code", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let purchasedOn = Localizable(NSLocalizedString("ticket_buyed_date", bundle: SDKNavigation.getBundle(), comment: "purchased on - date"))
    static let validUntil = Localizable(NSLocalizedString("ticket_valid_date", bundle: SDKNavigation.getBundle(), comment: "valid until - date"))
    static let purchaseInformation = Localizable(NSLocalizedString("purchase_information", bundle: SDKNavigation.getBundle(), comment: "purchase information - Button"))
    static let reportProblem = Localizable(NSLocalizedString("report_a_problem", bundle: SDKNavigation.getBundle(), comment: "report a problem - Button"))
    static let servicesPurchased = Localizable(NSLocalizedString("ticket_services", bundle: SDKNavigation.getBundle(), comment: "services purchased - section header"))
    static let cancelled = Localizable(NSLocalizedString("ticket_status_canceled", bundle: SDKNavigation.getBundle(), comment: "ticket status"))
    static let ticketExpired = Localizable(NSLocalizedString("ticket_status_expired", bundle: SDKNavigation.getBundle(), comment: "ticket status"))
    static let ticketPending = Localizable(NSLocalizedString("ticket_status_pending", bundle: SDKNavigation.getBundle(), comment: "ticket status"))
    static let useTheTerminal = Localizable(NSLocalizedString("wash_problem_instruction_1_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let useTheTerminalDescrip = Localizable(NSLocalizedString("wash_problem_instruction_1_text", bundle: SDKNavigation.getBundle(), comment: "Subtitle Label"))
    static let terminalTryAgain = Localizable(NSLocalizedString("wash_problem_instruction_2_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let terminalTryAgainDescrip = Localizable(NSLocalizedString("terminal_try_again_Description", bundle: SDKNavigation.getBundle(), comment: "Subtitle Label"))
    static let ticket = Localizable(NSLocalizedString("ticket", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let ticketInstruction1Title = Localizable(NSLocalizedString("info_service_option_1_title", bundle: SDKNavigation.getBundle(), comment: "Ticket title instruction"))
    static let ticketInstruction2Title = Localizable(NSLocalizedString("info_service_option_2_title", bundle: SDKNavigation.getBundle(), comment: "Ticket title instruction"))
    static let ticketInstruction1Body = Localizable(NSLocalizedString("info_service_option_1_description", bundle: SDKNavigation.getBundle(), comment: "Ticket detail instruction"))
    static let ticketInstruction2Body = Localizable(NSLocalizedString("info_service_option_2_description", bundle: SDKNavigation.getBundle(), comment: "Ticket detail instruction"))
    static let startWashing = Localizable(NSLocalizedString("start_washing", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let serviceStatusCanceled = Localizable(NSLocalizedString("service_status_canceled", bundle: SDKNavigation.getBundle(), comment: "AlertView message"))
    static let serviceStatusConsumed = Localizable(NSLocalizedString("service_status_consumed", bundle: SDKNavigation.getBundle(), comment: "AlertView message"))
    static let serviceStatusExpired = Localizable(NSLocalizedString("service_status_expired", bundle: SDKNavigation.getBundle(), comment: "AlertView message"))
    static let showQR = Localizable(NSLocalizedString("show_qr", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let generatingQR = Localizable(NSLocalizedString("generating_qr", bundle: SDKNavigation.getBundle(), comment: "Loading"))
    static let washingTime = Localizable(NSLocalizedString("wash_time", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let serviceInfoHeader1 = Localizable(NSLocalizedString("info_service_options", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let serviceInfoHeader2 = Localizable(NSLocalizedString("info_service_options_short", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let ticketNoVehicle = Localizable(NSLocalizedString("ticket_no_vehicle", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let ticketAddVehicle = Localizable(NSLocalizedString("ticket_add_vehicle", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let ticketContinue = Localizable(NSLocalizedString("ticket_continue", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let info = Localizable(NSLocalizedString("general_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let redeemed = Localizable(NSLocalizedString("redeemed", bundle: SDKNavigation.getBundle(), comment: "Label"))
}
