//
//  Localizable+Weather.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Weather
extension Localizable {
    static let today = Localizable(NSLocalizedString("weather_today", bundle: SDKNavigation.getBundle(), comment: "today"))
    static let shortMonday = Localizable(NSLocalizedString("weather_day_monday", bundle: SDKNavigation.getBundle(), comment: "Monday"))
    static let shortTuesday = Localizable(NSLocalizedString("weather_day_tuesday", bundle: SDKNavigation.getBundle(), comment: "Tuesday"))
    static let shortWednesday = Localizable(NSLocalizedString("weather_day_wednesday", bundle: SDKNavigation.getBundle(), comment: "wednesday"))
    static let shortThursday = Localizable(NSLocalizedString("weather_day_thursday", bundle: SDKNavigation.getBundle(), comment: "thursday"))
    static let shortFriday = Localizable(NSLocalizedString("weather_day_friday", bundle: SDKNavigation.getBundle(), comment: "friday"))
    static let shortSaturday = Localizable(NSLocalizedString("weather_day_saturday", bundle: SDKNavigation.getBundle(), comment: "saturday"))
    static let shortSunday = Localizable(NSLocalizedString("weather_day_sunday", bundle: SDKNavigation.getBundle(), comment: "sunday"))
    static let monday = Localizable(NSLocalizedString("weather_monday", bundle: SDKNavigation.getBundle(), comment: "Monday"))
    static let tuesday = Localizable(NSLocalizedString("weather_tuesday", bundle: SDKNavigation.getBundle(), comment: "Tuesday"))
    static let wednesday = Localizable(NSLocalizedString("weather_wednesday", bundle: SDKNavigation.getBundle(), comment: "wednesday"))
    static let thursday = Localizable(NSLocalizedString("weather_thursday", bundle: SDKNavigation.getBundle(), comment: "thursday"))
    static let friday = Localizable(NSLocalizedString("weather_friday", bundle: SDKNavigation.getBundle(), comment: "friday"))
    static let saturday = Localizable(NSLocalizedString("weather_saturday", bundle: SDKNavigation.getBundle(), comment: "saturday"))
    static let sunday = Localizable(NSLocalizedString("weather_sunday", bundle: SDKNavigation.getBundle(), comment: "sunday"))
    static let washToday = Localizable(NSLocalizedString("weather_wash_today", bundle: SDKNavigation.getBundle(), comment: "wash day"))
    static let washTomorrow = Localizable(NSLocalizedString("weather_wash_tomorrow", bundle: SDKNavigation.getBundle(), comment: "wash day"))
    static let washNoDay = Localizable(NSLocalizedString("weather_wash_no_day", bundle: SDKNavigation.getBundle(), comment: "wash day"))
    static let washDay = Localizable(NSLocalizedString("weather_wash_day_ios", bundle: SDKNavigation.getBundle(), comment: "wash day"))
    static let washPrevision = Localizable(NSLocalizedString("weather_wash_prevision", bundle: SDKNavigation.getBundle(), comment: "wash day"))
}
