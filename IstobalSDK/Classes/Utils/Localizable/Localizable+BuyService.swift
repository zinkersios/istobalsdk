//
//  Localizable+BuyService.swift
//  istobal
//
//  Created by Shanti Rodríguez on 26/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Buy Service
extension Localizable {
    static let buyXservice = Localizable(NSLocalizedString("buy_x_service", bundle: SDKNavigation.getBundle(), comment: "NavBar Title"))
    static let buyServiceTicket = Localizable(NSLocalizedString("card_program_of", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let buyServiceTicketInfo = Localizable(NSLocalizedString("card_program_info", bundle: SDKNavigation.getBundle(), comment: "Subtitle"))
    static let consumeYourCardBalance1 = Localizable(NSLocalizedString("card_balance_first", bundle: SDKNavigation.getBundle(), comment: "Subtitle"))
    static let consumeYourCardBalance2 = Localizable(NSLocalizedString("card_balance_third", bundle: SDKNavigation.getBundle(), comment: "Subtitle"))
    static let startNow = Localizable(NSLocalizedString("card_start_now", bundle: SDKNavigation.getBundle(), comment: "Button"))
}
