//
//  Localizable.swift
//  istobal
//
//  Created by Santiago Sánchez Rodríguez on 14/7/17.
//  Copyright © 2017 Istobal. All rights reserved.
//

import Foundation

// MARK: - General
extension Localizable {
    static let accept = Localizable(NSLocalizedString("accept", bundle: SDKNavigation.getBundle(), comment: "Accept button"))
    static let cancel = Localizable(NSLocalizedString("cancel", bundle: SDKNavigation.getBundle(), comment: "Cancel button"))
    static let tryAgain = Localizable(NSLocalizedString("try_again", bundle: SDKNavigation.getBundle(), comment: "Try Again - Button"))
    static let delete = Localizable(NSLocalizedString("delete", bundle: SDKNavigation.getBundle(), comment: "Delete - Button"))
    static let edit = Localizable(NSLocalizedString("edit", bundle: SDKNavigation.getBundle(), comment: "Edit - Button"))
    static let continuar = Localizable(NSLocalizedString("continuous", bundle: SDKNavigation.getBundle(), comment: "continue - button"))
    static let not = Localizable(NSLocalizedString("no", bundle: SDKNavigation.getBundle(), comment: "Not - button"))
    static let yes = Localizable(NSLocalizedString("yes", bundle: SDKNavigation.getBundle(), comment: "Yes - button"))
    static let save = Localizable(NSLocalizedString("save", bundle: SDKNavigation.getBundle(), comment: "button"))
    static let send = Localizable(NSLocalizedString("btn_send", bundle: SDKNavigation.getBundle(), comment: "button"))
    static let understood = Localizable(NSLocalizedString("understood", bundle: SDKNavigation.getBundle(), comment: "button"))
    static let select = Localizable(NSLocalizedString("select", bundle: SDKNavigation.getBundle(), comment: "button"))
    static let close = Localizable(NSLocalizedString("close", bundle: SDKNavigation.getBundle(), comment: "button"))
    static let change = Localizable(NSLocalizedString("change", bundle: SDKNavigation.getBundle(), comment: "button"))
}

// MARK: - Permissions
extension Localizable {
    static let permissionsLocationTitle = Localizable(NSLocalizedString("permissions_location_title", bundle: SDKNavigation.getBundle(), comment: "¿Where are you?"))
    static let permissionsLocationSubtitle = Localizable(NSLocalizedString("permissions_location_subtitle", bundle: SDKNavigation.getBundle(), comment: "Permissions location subtitle"))
    static let permissionsLocationInfo = Localizable(NSLocalizedString("permissions_location_info", bundle: SDKNavigation.getBundle(), comment: "Permissions location info"))
    static let noPermissionsVideo = Localizable(NSLocalizedString("no_permissions_video", bundle: SDKNavigation.getBundle(), comment: "Permissions QR info"))
}

// MARK: - Tabs
extension Localizable {
    static let tabNearbyCenters = Localizable(NSLocalizedString("close_centers", bundle: SDKNavigation.getBundle(), comment: "Nearby centers TAB"))
    static let tabFavorites = Localizable(NSLocalizedString("favorite_centers", bundle: SDKNavigation.getBundle(), comment: "Favorites TAB"))
    static let tabStatistics = Localizable(NSLocalizedString("profile_stats", bundle: SDKNavigation.getBundle(), comment: "statistics TAB"))
    static let tabEditProfile = Localizable(NSLocalizedString("profile_edit", bundle: SDKNavigation.getBundle(), comment: "Edit Profile TAB"))
    static let tabAll = Localizable(NSLocalizedString("tickets_all", bundle: SDKNavigation.getBundle(), comment: "all tickets TAB"))
    static let tabActive = Localizable(NSLocalizedString("tickets_actived", bundle: SDKNavigation.getBundle(), comment: "active tickets TAB"))
    static let tabConsumed = Localizable(NSLocalizedString("tickets_consumed", bundle: SDKNavigation.getBundle(), comment: " tickets consumed"))
    static let tabTransactions = Localizable(NSLocalizedString("card_operations", bundle: SDKNavigation.getBundle(), comment: "Transactions"))
}

// MARK: - Form
extension Localizable {
    static let enterOwnerName = Localizable(NSLocalizedString("enter_owner_name", bundle: SDKNavigation.getBundle(), comment: "missing field fill"))
    static let enterCardNumber = Localizable(NSLocalizedString("enter_card_number", bundle: SDKNavigation.getBundle(), comment: "missing field fill"))
    static let selectExpirationDate = Localizable(NSLocalizedString("select_expiration_date", bundle: SDKNavigation.getBundle(), comment: "missing field fill"))
    static let enterCCV = Localizable(NSLocalizedString("enter_ccv", bundle: SDKNavigation.getBundle(), comment: "missing field fill"))
}

// MARK: - Time
extension Localizable {
    static let prepTime = Localizable(NSLocalizedString("prep_time", bundle: SDKNavigation.getBundle(), comment: "16 de 09 de 2017"))
}
