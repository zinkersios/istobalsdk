//
//  Localizable+Home.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Home
extension Localizable {
    static let searchCenter = Localizable(NSLocalizedString("map_search", bundle: SDKNavigation.getBundle(), comment: "Search for a center - Button"))
    static let connectToSaveFav = Localizable(NSLocalizedString("connect_to_save_fav", bundle: SDKNavigation.getBundle(), comment: "Sign in or Register to save a favorite"))
}

// MARK: - Search Center
extension Localizable {
    static let placeholderSearchWashCenter = Localizable(NSLocalizedString("search_filter", bundle: SDKNavigation.getBundle(), comment: "placeholder for searchBar"))
    static let applyFilters = Localizable(NSLocalizedString("apply_filters", bundle: SDKNavigation.getBundle(), comment: "apply filters - button"))
    static let filterByServices = Localizable(NSLocalizedString("filter_by_services", bundle: SDKNavigation.getBundle(), comment: "filter by services - header section"))
    static let sortedByProximity = Localizable(NSLocalizedString("sorted_by_proximity", bundle: SDKNavigation.getBundle(), comment: "Sorted by proximity - header section"))
    static let services = Localizable(NSLocalizedString("wash_programs", bundle: SDKNavigation.getBundle(), comment: "services - header section"))
    static let creditIn = Localizable(NSLocalizedString("credit_in", bundle: SDKNavigation.getBundle(), comment: "credit in wash center"))
    static let paymentMethod = Localizable(NSLocalizedString("profile_payment_method", bundle: SDKNavigation.getBundle(), comment: "payment method - header section"))
    static let emptyPaymentMethodDescription = Localizable(NSLocalizedString("empty_payment_method_description", bundle: SDKNavigation.getBundle(), comment: "AlertView message"))
    static let emptyVehicleDescription = Localizable(NSLocalizedString("empty_vehycle_description", bundle: SDKNavigation.getBundle(), comment: "AlertView message"))
    static let noCarAssigned = Localizable(NSLocalizedString("empty_vehycle", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let selectVehicle = Localizable(NSLocalizedString("select_vehicle", bundle: SDKNavigation.getBundle(), comment: "AlertController title"))
    static let selectCreditCard = Localizable(NSLocalizedString("select_credit_card", bundle: SDKNavigation.getBundle(), comment: "AlertController title"))
}
