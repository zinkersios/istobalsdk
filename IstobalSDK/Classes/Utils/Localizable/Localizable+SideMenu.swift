//
//  Localizable+SideMenu.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Side Menu
extension Localizable {
    static let slogan = Localizable(NSLocalizedString("istobal_message", bundle: SDKNavigation.getBundle(), comment: "Istobal Slogan"))
    static let washingCenters = Localizable(NSLocalizedString("menu_center", bundle: SDKNavigation.getBundle(), comment: "Washing centers - side menu item"))
    static let smartWash = Localizable(NSLocalizedString("menu_smartwash", bundle: SDKNavigation.getBundle(), comment: "SmartWash - side menu item"))
    static let myVehicles = Localizable(NSLocalizedString("menu_vehicles", bundle: SDKNavigation.getBundle(), comment: "My vehicles - side menu item"))
    static let myTickets = Localizable(NSLocalizedString("menu_tickets", bundle: SDKNavigation.getBundle(), comment: "My Tickets - side menu item"))
    static let notifications = Localizable(NSLocalizedString("menu_notifications", bundle: SDKNavigation.getBundle(), comment: "Notifications - side menu item"))
    static let ticketReading = Localizable(NSLocalizedString("menu_offers", bundle: SDKNavigation.getBundle(), comment: "Ticket reading - side menu item"))
    static let registry = Localizable(NSLocalizedString("menu_register", bundle: SDKNavigation.getBundle(), comment: "registry - side menu item"))
    static let aboutUs = Localizable(NSLocalizedString("menu_who", bundle: SDKNavigation.getBundle(), comment: "about us - side menu item"))
    static let support = Localizable(NSLocalizedString("menu_support", bundle: SDKNavigation.getBundle(), comment: "Support - side menu item"))
}
