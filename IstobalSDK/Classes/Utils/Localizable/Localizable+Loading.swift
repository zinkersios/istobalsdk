//
//  Localizable+Loading.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Loading (also used in KVNProgress)
extension Localizable {
    static let loadingLoggingIn = Localizable(NSLocalizedString("dialog_logging_in", bundle: SDKNavigation.getBundle(), comment: "Logging in"))
    static let loadingWelcome = Localizable(NSLocalizedString("dialog_welcome", bundle: SDKNavigation.getBundle(), comment: "Welcome message"))
    static let loadingRegisteringUser = Localizable(NSLocalizedString("dialog_registering_user", bundle: SDKNavigation.getBundle(), comment: "Registering user message"))
    static let endingRegistration = Localizable(NSLocalizedString("dialog_ending_registration", bundle: SDKNavigation.getBundle(), comment: "Ending registration"))
    static let gettingInstallations = Localizable(NSLocalizedString("loading_getting_installations", bundle: SDKNavigation.getBundle(), comment: "Getting installations"))
    static let gettingFavorites = Localizable(NSLocalizedString("loading_getting_favorites", bundle: SDKNavigation.getBundle(), comment: "Getting favorites"))
    static let gettingTickets = Localizable(NSLocalizedString("loading_getting_tickets", bundle: SDKNavigation.getBundle(), comment: "Getting my tickets"))
    static let gettingVehicles = Localizable(NSLocalizedString("loading_getting_vehicles", bundle: SDKNavigation.getBundle(), comment: "Getting my vehicles"))
    static let gettingValorations = Localizable(NSLocalizedString("loading_getting_valorations", bundle: SDKNavigation.getBundle(), comment: "Getting my vehicles"))
    static let savingVehicle = Localizable(NSLocalizedString("edit_car_saving", bundle: SDKNavigation.getBundle(), comment: "Saving a new vehicle"))
    static let updatingData = Localizable(NSLocalizedString("loading_updating_data", bundle: SDKNavigation.getBundle(), comment: "Updating data"))
    static let updatingAvatar = Localizable(NSLocalizedString("loading_updating_avatar", bundle: SDKNavigation.getBundle(), comment: "Updating avatar"))
    static let updatedVehicleData = Localizable(NSLocalizedString("loading_updated_vehicle_data", bundle: SDKNavigation.getBundle(), comment: "Updated vehicle"))
    static let updatedProfile = Localizable(NSLocalizedString("loading_updated_profile", bundle: SDKNavigation.getBundle(), comment: "Updated profile"))
    static let updatedAvatar = Localizable(NSLocalizedString("loading_updated_avatar", bundle: SDKNavigation.getBundle(), comment: "Updated avatar"))
    static let loadingWait = Localizable(NSLocalizedString("loading_wait", bundle: SDKNavigation.getBundle(), comment: "Wait..."))
    static let makingPurchase = Localizable(NSLocalizedString("loading_making_purchase", bundle: SDKNavigation.getBundle(), comment: "Pay"))
    static let purchaseOK = Localizable(NSLocalizedString("loading_purchase_ok", bundle: SDKNavigation.getBundle(), comment: "Pay OK"))
    static let stoppingWashing = Localizable(NSLocalizedString("loading_stop_wash", bundle: SDKNavigation.getBundle(), comment: "Stopping washing"))
    static let postingReview = Localizable(NSLocalizedString("loading_posting_review", bundle: SDKNavigation.getBundle(), comment: "Posting Review"))
    static let postingReviewOK = Localizable(NSLocalizedString("valoration_success", bundle: SDKNavigation.getBundle(), comment: "Posting Review OK"))
    static let eliminatingVehicle = Localizable(NSLocalizedString("loading_eliminating_vehicle", bundle: SDKNavigation.getBundle(), comment: "Eliminating vehicle"))
    static let vehicleDeleted = Localizable(NSLocalizedString("loading_vehicle_deleted", bundle: SDKNavigation.getBundle(), comment: "vehicle deleted"))
    static let recoveryEmailSent = Localizable(NSLocalizedString("recovery_pass_success", bundle: SDKNavigation.getBundle(), comment: "Success"))
    static let emailDoesNotExists = Localizable(NSLocalizedString("loading_recover_password_fail", bundle: SDKNavigation.getBundle(), comment: "Fail"))
    static let updatingTicket = Localizable(NSLocalizedString("loading_updating_ticket", bundle: SDKNavigation.getBundle(), comment: "Updating ticket"))
    static let updatedTicket = Localizable(NSLocalizedString("loading_updated_ticket", bundle: SDKNavigation.getBundle(), comment: "updated ticket"))
    static let redeemingGift = Localizable(NSLocalizedString("loading_redeem_gift", bundle: SDKNavigation.getBundle(), comment: "redeem gift"))
    static let generatingGift = Localizable(NSLocalizedString("loading_send_gift", bundle: SDKNavigation.getBundle(), comment: "redeem gift"))
    static let sendingComment = Localizable(NSLocalizedString("loading_sending_comment", bundle: SDKNavigation.getBundle(), comment: "Comment"))
    static let commentSent = Localizable(NSLocalizedString("installation_report_sended", bundle: SDKNavigation.getBundle(), comment: "Comment"))
    static let gettingNotifications = Localizable(NSLocalizedString("loading_getting_notifications", bundle: SDKNavigation.getBundle(), comment: "Getting my notifications"))
    static let eliminating = Localizable(NSLocalizedString("loading_eliminating", bundle: SDKNavigation.getBundle(), comment: "Eliminating"))
    static let billingDeleted = Localizable(NSLocalizedString("loading_billing_deleted", bundle: SDKNavigation.getBundle(), comment: "Billing Deleted"))
    static let checkingNIF = Localizable(NSLocalizedString("checking_nif", bundle: SDKNavigation.getBundle(), comment: "Checking NIF"))
    static let completeBilling = Localizable(NSLocalizedString("complete_billing_info", bundle: SDKNavigation.getBundle(), comment: "Billing"))
    static let loadedExistingBilling = Localizable(NSLocalizedString("loaded_existing_billing", bundle: SDKNavigation.getBundle(), comment: "Billing"))
    static let savingBilling = Localizable(NSLocalizedString("loading_saving_billing", bundle: SDKNavigation.getBundle(), comment: "Billing"))
    static let savedData = Localizable(NSLocalizedString("loading_saved_data", bundle: SDKNavigation.getBundle(), comment: "Billing"))
    static let loadingTicket = Localizable(NSLocalizedString("loading_ticket", bundle: SDKNavigation.getBundle(), comment: "Loading..."))
    static let errorGettingTicket = Localizable(NSLocalizedString("get_ticket_error", bundle: SDKNavigation.getBundle(), comment: "error getting a ticket"))
    static let gettingMyCards = Localizable(NSLocalizedString("loading_getting_my_cards", bundle: SDKNavigation.getBundle(), comment: "Getting my cards"))
    static let gettingTransactions = Localizable(NSLocalizedString("loading_getting_transactions", bundle: SDKNavigation.getBundle(), comment: "Getting Transactions"))
    static let deletingCard = Localizable(NSLocalizedString("loading_deleting_card", bundle: SDKNavigation.getBundle(), comment: "Deleting card"))
    static let deletedCard = Localizable(NSLocalizedString("loading_deleted_card", bundle: SDKNavigation.getBundle(), comment: "Deleted card"))
    static let creatingCard = Localizable(NSLocalizedString("loading_creating_a_card", bundle: SDKNavigation.getBundle(), comment: "Creating a card"))
    static let createdCard = Localizable(NSLocalizedString("loading_card_created", bundle: SDKNavigation.getBundle(), comment: "Created card"))
    static let addingCard = Localizable(NSLocalizedString("loading_adding_card", bundle: SDKNavigation.getBundle(), comment: "Adding card"))
    static let addedCard = Localizable(NSLocalizedString("loading_added_card", bundle: SDKNavigation.getBundle(), comment: "Added card"))
}
