//
//  Localizable+LogIn.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Log In Scene
extension Localizable {
    static let yourEmail = Localizable(NSLocalizedString("hint_email", bundle: SDKNavigation.getBundle(), comment: "your e-mail"))
    static let yourPassword = Localizable(NSLocalizedString("hint_pass", bundle: SDKNavigation.getBundle(), comment: "your password"))
    static let viewPassword = Localizable(NSLocalizedString("show_pass", bundle: SDKNavigation.getBundle(), comment: "View password button"))
    static let hidePassword = Localizable(NSLocalizedString("hide_pass", bundle: SDKNavigation.getBundle(), comment: "Hide password button"))
    static let signInWithFacebook = Localizable(NSLocalizedString("sign_in_with_facebook", bundle: SDKNavigation.getBundle(), comment: "Sign in with facebook - button"))
    static let signInWithGoogle = Localizable(NSLocalizedString("sign_in_with_google", bundle: SDKNavigation.getBundle(), comment: "Sign in with Google - button"))
    static let btnNotHaveAccount = Localizable(NSLocalizedString("have_no_account", bundle: SDKNavigation.getBundle(), comment: "I do not have an account - button"))
    static let iForgotMyPassword = Localizable(NSLocalizedString("btn_recovery_pass", bundle: SDKNavigation.getBundle(), comment: "Nav Bar title"))
    static let youForgetYourPassword = Localizable(NSLocalizedString("recovery_pass_title", bundle: SDKNavigation.getBundle(), comment: "Section title"))
    static let enterYourEmailForAccount = Localizable(NSLocalizedString("enter_your_email_for_account", bundle: SDKNavigation.getBundle(), comment: "section info"))
}

// MARK: - Register Scene
extension Localizable {
    static let registerWithYourEmail = Localizable(NSLocalizedString("register_messge", bundle: SDKNavigation.getBundle(), comment: "Register with your email"))
    static let createPassword = Localizable(NSLocalizedString("hint_create_pass", bundle: SDKNavigation.getBundle(), comment: "Create a password - Label"))
    static let repeatPassword = Localizable(NSLocalizedString("hint_repeat_pass", bundle: SDKNavigation.getBundle(), comment: "Repeat a password - Label"))
    static let completeRegistration = Localizable(NSLocalizedString("btn_finish_register", bundle: SDKNavigation.getBundle(), comment: "Complete registration - Label"))
    static let haveAnAccount = Localizable(NSLocalizedString("have_account", bundle: SDKNavigation.getBundle(), comment: "I already have an account - Label"))
}
