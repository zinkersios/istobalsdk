//
//  Localizable+TicketReading.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Ticket Reading
extension Localizable {
    static let scanYourTicketInfo = Localizable(NSLocalizedString("scan_your_ticket_info", bundle: SDKNavigation.getBundle(), comment: "Info Scan"))
    static let enterCodeManually = Localizable(NSLocalizedString("offers_code_add_manual", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let enterCodeManuallyDetail = Localizable(NSLocalizedString("offers_add_code", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let useQR = Localizable(NSLocalizedString("offers_code_scan", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let enterCode = Localizable(NSLocalizedString("offers_code_empty", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let enterTicketCode = Localizable(NSLocalizedString("enter_ticket_code", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let checkingCode = Localizable(NSLocalizedString("checking_code", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let scanYourOffer = Localizable(NSLocalizedString("coupon_add_code_instruction", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let enterOfferCodeManually = Localizable(NSLocalizedString("coupon_add_code", bundle: SDKNavigation.getBundle(), comment: "Label"))
}

// MARK: - Redeem Gift
extension Localizable {
    static let giveThisTicket = Localizable(NSLocalizedString("ticket_option_gift", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let giftMessage = Localizable(NSLocalizedString("ticket_gift_share_message", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let giftReceived = Localizable(NSLocalizedString("ticket_gift_redeem", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let giftReceivedInfo = Localizable(NSLocalizedString("ticket_gift_info_ios", bundle: SDKNavigation.getBundle(), comment: "Message"))
    static let youHaveGift = Localizable(NSLocalizedString("you_have_a_gift", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let youHaveGiftInfo = Localizable(NSLocalizedString("you_have_a_gift_info", bundle: SDKNavigation.getBundle(), comment: "Message"))
    static let shareConsumed = Localizable(NSLocalizedString("share_consumed", bundle: SDKNavigation.getBundle(), comment: "Message"))
}
