//
//  Localizable+MainScene.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Main Scene
extension Localizable {
    static let infoWelcome = Localizable(NSLocalizedString("info_welcome", bundle: SDKNavigation.getBundle(), comment: "Info welcome"))
    static let infoTerms = Localizable(NSLocalizedString("info_terms", bundle: SDKNavigation.getBundle(), comment: "Information on terms and conditions"))
    static let logIn = Localizable(NSLocalizedString("login_message", bundle: SDKNavigation.getBundle(), comment: "Show log in view"))
    static let btnFacebook = Localizable(NSLocalizedString("btn_facebook", bundle: SDKNavigation.getBundle(), comment: "Sign in with facebook"))
    static let btnGoogle = Localizable(NSLocalizedString("btn_google", bundle: SDKNavigation.getBundle(), comment: "Sign in with facebook"))
    static let btnCreateAccount = Localizable(NSLocalizedString("btn_register", bundle: SDKNavigation.getBundle(), comment: "Create Account"))
    static let btnTryWithoutRegistration = Localizable(NSLocalizedString("without_register", bundle: SDKNavigation.getBundle(), comment: "Try without registration"))
    static let btnBackToTutorial = Localizable(NSLocalizedString("go_tutorial", bundle: SDKNavigation.getBundle(), comment: "Back to tutorial"))
    static let checkVersionTitle = Localizable(NSLocalizedString("check_version_new", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let checkVersionMessage = Localizable(NSLocalizedString("check_version_info", bundle: SDKNavigation.getBundle(), comment: "Message"))
    static let checkVersionOk = Localizable(NSLocalizedString("check_version_yes", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let checkVersionCancel = Localizable(NSLocalizedString("check_version_no", bundle: SDKNavigation.getBundle(), comment: "Button"))
}
