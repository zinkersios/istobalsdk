//
//  Localizable+Cards.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Cards
extension Localizable {
    static let myCards = Localizable(NSLocalizedString("cards", bundle: SDKNavigation.getBundle(), comment: "NavBar Title"))
    static let addBalance = Localizable(NSLocalizedString("card_add_credit", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let addBalanceSubtitle = Localizable(NSLocalizedString("card_add_credit_info", bundle: SDKNavigation.getBundle(), comment: "Subtitle"))
    static let addsCorrectBalance = Localizable(NSLocalizedString("card_add_credit_bad", bundle: SDKNavigation.getBundle(), comment: "Subtitle"))
    static let addBalanceQuestion = Localizable(NSLocalizedString("card_add_credit_value", bundle: SDKNavigation.getBundle(), comment: "Subtitle"))
    static let otherValue = Localizable(NSLocalizedString("card_add_other_value", bundle: SDKNavigation.getBundle(), comment: "Subtitle"))
    static let loyaltyCardQuestion = Localizable(NSLocalizedString("card_fidelity_question", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let haveCard = Localizable(NSLocalizedString("btn_have_card", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let createCard = Localizable(NSLocalizedString("btn_create_card", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let enterYourCardNumber = Localizable(NSLocalizedString("card_fidelity_add_number", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let cardNumberDigits = Localizable(NSLocalizedString("card_fidelity_number", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let incorrectCardNumber = Localizable(NSLocalizedString("incorrect_card_number", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let only8Digits = Localizable(NSLocalizedString("only_8_digits", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let selectCenterToCreateCard = Localizable(NSLocalizedString("select_center_to_create_card", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let xPercentSavings = Localizable(NSLocalizedString("perc_discount", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let deleteLoyaltyCard = Localizable(NSLocalizedString("card_remove_card", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let loyaltyCard = Localizable(NSLocalizedString("card_fidelity", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let multistation = Localizable(NSLocalizedString("fidelity_multiple", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let balanceX = Localizable(NSLocalizedString("balance_x", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let increaseCardBalance = Localizable(NSLocalizedString("card_balance_charge_desc", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let debitBalance = Localizable(NSLocalizedString("card_balance_charge", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let information = Localizable(NSLocalizedString("dialog_fidelity_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let balanceCard = Localizable(NSLocalizedString("btn_type_balance", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let abonoCard = Localizable(NSLocalizedString("btn_type_subscription", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let typeOfCardToAdd = Localizable(NSLocalizedString("card_fidelity_type", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let loyaltyCardWithoutNumber = Localizable(NSLocalizedString("card_fidelity_no_number", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let addAbonoBalance = Localizable(NSLocalizedString("card_add_subscription", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let deleteLoyaltyCardInfo = Localizable(NSLocalizedString("card_fidelity_delete_sure", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let selectAnAmount = Localizable(NSLocalizedString("card_select_credit", bundle: SDKNavigation.getBundle(), comment: "Label"))
}
