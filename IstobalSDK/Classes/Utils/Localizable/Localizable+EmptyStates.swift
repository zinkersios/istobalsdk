//
//  Localizable+EmptyStates.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Empty States
extension Localizable {
    static let emptyInstallations = Localizable(NSLocalizedString("empty_state_installations", bundle: SDKNavigation.getBundle(), comment: "Empty State for Installations"))
    static let emptyFavorites = Localizable(NSLocalizedString("empty_state_favorites", bundle: SDKNavigation.getBundle(), comment: "Empty State for Installations"))
    static let emptyTickets = Localizable(NSLocalizedString("empty_state_tickets", bundle: SDKNavigation.getBundle(), comment: "Empty State for all tickets"))
    static let emptyActiveTickets = Localizable(NSLocalizedString("empty_state_active_tickets", bundle: SDKNavigation.getBundle(), comment: "Empty State for active tickets"))
    static let emptyTicketsConsumed = Localizable(NSLocalizedString("empty_state_tickets_consumed", bundle: SDKNavigation.getBundle(), comment: "Empty State for tickets consumed"))
    static let emptyVehicle = Localizable(NSLocalizedString("empty_state_vehicles", bundle: SDKNavigation.getBundle(), comment: "Empty State for my vehicles"))
    static let emptyTicketsForMyVehicles = Localizable(NSLocalizedString("empty_state_tickets_my_vehicles", bundle: SDKNavigation.getBundle(), comment: "Empty State for tickets"))
    static let emptyValorations = Localizable(NSLocalizedString("empty_state_valorations", bundle: SDKNavigation.getBundle(), comment: "Empty State for valorations"))
    static let emptyNotifications = Localizable(NSLocalizedString("empty_state_notifications", bundle: SDKNavigation.getBundle(), comment: "Empty State for notifications"))
    static let emptyMyCards = Localizable(NSLocalizedString("empty_state_my_cards", bundle: SDKNavigation.getBundle(), comment: "Empty State for My Cards"))
    static let emptyTransactions = Localizable(NSLocalizedString("empty_operations", bundle: SDKNavigation.getBundle(), comment: "Empty State for My Transactions"))
}
