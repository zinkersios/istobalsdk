//
//  Localizable+SmartWash.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - SmartWash
extension Localizable {
    static let repeatAnotherWash = Localizable(NSLocalizedString("repeat_another_wash", bundle: SDKNavigation.getBundle(), comment: "cell"))
    static let washingAssistant = Localizable(NSLocalizedString("wash_assitance", bundle: SDKNavigation.getBundle(), comment: "cell title"))
    static let createsCustomWash = Localizable(NSLocalizedString("creates_a_custom_wash", bundle: SDKNavigation.getBundle(), comment: "cell subtitle"))
    static let myWashesSaved = Localizable(NSLocalizedString("wash_saved", bundle: SDKNavigation.getBundle(), comment: "section header"))
    static let questionStep1 = Localizable(NSLocalizedString("question_step_1", bundle: SDKNavigation.getBundle(), comment: "Question from step 1"))
    static let questionStep2 = Localizable(NSLocalizedString("question_step_2", bundle: SDKNavigation.getBundle(), comment: "Question from step 2"))
    static let questionStep3 = Localizable(NSLocalizedString("question_step_3", bundle: SDKNavigation.getBundle(), comment: "Question from step 3"))
    static let questionStep4 = Localizable(NSLocalizedString("add_smartwash_4", bundle: SDKNavigation.getBundle(), comment: "Question from step 4"))
    static let levelOfDirt = Localizable(NSLocalizedString("level_of_dirt", bundle: SDKNavigation.getBundle(), comment: "Level of dirt"))
    static let levelOfDirt1 = Localizable(NSLocalizedString("smartwashe_dirt_low", bundle: SDKNavigation.getBundle(), comment: "Level of dirt 1 - button"))
    static let levelOfDirt2 = Localizable(NSLocalizedString("smartwashe_dirt_normal", bundle: SDKNavigation.getBundle(), comment: "Level of dirt 2 - button"))
    static let levelOfDirt3 = Localizable(NSLocalizedString("smartwashe_dirt_high", bundle: SDKNavigation.getBundle(), comment: "Level of dirt 3 - button"))
    static let dirtierAreas = Localizable(NSLocalizedString("smartwashe_zone", bundle: SDKNavigation.getBundle(), comment: "Dirtier Areas"))
    static let dirtyAreaAny = Localizable(NSLocalizedString("dirty_area_any", bundle: SDKNavigation.getBundle(), comment: "Any - Button"))
    static let dirtyAreaFrontal = Localizable(NSLocalizedString("smartwashe_zone_frontal", bundle: SDKNavigation.getBundle(), comment: "Frontal - Button"))
    static let dirtyAreaRear = Localizable(NSLocalizedString("smartwashe_zone_trasera", bundle: SDKNavigation.getBundle(), comment: "Rear - Button"))
    static let dirtyAreaSides = Localizable(NSLocalizedString("smartwashe_zone_lateral", bundle: SDKNavigation.getBundle(), comment: "Sides - Button"))
    static let dirtyAreaBottom = Localizable(NSLocalizedString("smartwashe_zone_bajos", bundle: SDKNavigation.getBundle(), comment: "Bottom - Button"))
    static let dirtyAreaWheel = Localizable(NSLocalizedString("smartwashe_zone_ruedas", bundle: SDKNavigation.getBundle(), comment: "Wheel - Button"))
    static let dirtyAreaAll = Localizable(NSLocalizedString("smartwashe_zone_all", bundle: SDKNavigation.getBundle(), comment: "All - Button"))
    static let extras = Localizable(NSLocalizedString("smartwashe_extras", bundle: SDKNavigation.getBundle(), comment: "Extras"))
    static let finalizePurchase = Localizable(NSLocalizedString("finish_personal", bundle: SDKNavigation.getBundle(), comment: "Finalize purchase - Button"))
    static let saveThisWash = Localizable(NSLocalizedString("smartwash_save", bundle: SDKNavigation.getBundle(), comment: "Save this wash - Button"))
    static let nameYourWash = Localizable(NSLocalizedString("smartwash_add_name", bundle: SDKNavigation.getBundle(), comment: "Name your wash"))
}
