//
//  Localizable+Instructions.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Instructions
extension Localizable {
    static let instruction1 = Localizable(NSLocalizedString("wash_instructions_1_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let instruction2 = Localizable(NSLocalizedString("wash_instructions_2_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let instruction3 = Localizable(NSLocalizedString("wash_instructions_3_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let instruction3Outside = Localizable(NSLocalizedString("instruction_3_outside", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let instruction1Body = Localizable(NSLocalizedString("wash_instructions_1_description", bundle: SDKNavigation.getBundle(), comment: "Description Label"))
    static let instruction2Body = Localizable(NSLocalizedString("wash_instructions_2_description", bundle: SDKNavigation.getBundle(), comment: "Description Label"))
    static let instruction3Body = Localizable(NSLocalizedString("wash_instructions_3_description", bundle: SDKNavigation.getBundle(), comment: "Description Label"))
    static let instruction3OutsideBody = Localizable(NSLocalizedString("instruction_3_outside_body", bundle: SDKNavigation.getBundle(), comment: "Description Label"))
    static let instructionDetail1Title = Localizable(NSLocalizedString("instruction_detail_1_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let instructionDetail3Title = Localizable(NSLocalizedString("instruction_detail_3_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let instructionDetail3OutsideTitle = Localizable(NSLocalizedString("instruction_detail_3_outside_title", bundle: SDKNavigation.getBundle(), comment: "Title Label"))
    static let stepA = Localizable(NSLocalizedString("step_a", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let stepB = Localizable(NSLocalizedString("step_b", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let stepC = Localizable(NSLocalizedString("step_c", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let stepD = Localizable(NSLocalizedString("step_d", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let stepABody = Localizable(NSLocalizedString("instructions_one_a", bundle: SDKNavigation.getBundle(), comment: "Step description"))
    static let stepBBody = Localizable(NSLocalizedString("instructions_one_b", bundle: SDKNavigation.getBundle(), comment: "Step description"))
    static let stepCBody = Localizable(NSLocalizedString("instructions_one_c", bundle: SDKNavigation.getBundle(), comment: "Step description"))
    static let stepDBody = Localizable(NSLocalizedString("instructions_one_d", bundle: SDKNavigation.getBundle(), comment: "Step description"))
    static let instructionDetail2Body = Localizable(NSLocalizedString("instruction_detail_2_body", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let instructionDetail3Body = Localizable(NSLocalizedString("instructions_description_third_inside", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let instructionDetail3OutsideBody = Localizable(NSLocalizedString("instruction_detail_3_outside_body", bundle: SDKNavigation.getBundle(), comment: "Label"))
}
