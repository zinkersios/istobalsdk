//
//  Localizable+Billing.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Billing information
extension Localizable {
    static let billingInformation = Localizable(NSLocalizedString("profile_billing", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let taxID = Localizable(NSLocalizedString("profile_bill_cif", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let companyName = Localizable(NSLocalizedString("profile_bill_social", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let billingAddress = Localizable(NSLocalizedString("profile_bill_adress", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let billingCity = Localizable(NSLocalizedString("profile_bill_city", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let billingZipCode = Localizable(NSLocalizedString("profile_bill_postal", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let billingState = Localizable(NSLocalizedString("profile_bill_province", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let billingCountry = Localizable(NSLocalizedString("profile_bill_country", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let billingEmail = Localizable(NSLocalizedString("profile_bill_email", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let selectCountry = Localizable(NSLocalizedString("select_country", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let removeBilling = Localizable(NSLocalizedString("profile_billing_sure", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let ruleNIF = Localizable(NSLocalizedString("enter_nif", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let ruleCompanyName = Localizable(NSLocalizedString("enter_company_name", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let ruleAddress = Localizable(NSLocalizedString("enter_address", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let ruleCity = Localizable(NSLocalizedString("enter_city", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let ruleZipCode = Localizable(NSLocalizedString("enter_zip_code", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let ruleProvince = Localizable(NSLocalizedString("enter_province", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let ruleEmail = Localizable(NSLocalizedString("enter_email", bundle: SDKNavigation.getBundle(), comment: "Info"))
    static let wrongFormatEmail = Localizable(NSLocalizedString("email_bad_format", bundle: SDKNavigation.getBundle(), comment: "Error"))
    static let billingInfoTitle = Localizable(NSLocalizedString("dialog_title_facturation", bundle: SDKNavigation.getBundle(), comment: "Title"))
    static let billingInfoMessage = Localizable(NSLocalizedString("dialog_message_facturation", bundle: SDKNavigation.getBundle(), comment: "Message"))
}

// MARK: - Payment methods
extension Localizable {
    static let paymentMethods = Localizable(NSLocalizedString("profile_payments", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let addNewCard = Localizable(NSLocalizedString("add_new_card", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let addCard = Localizable(NSLocalizedString("payment_methods_add", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let editCard = Localizable(NSLocalizedString("edit_card", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let titular = Localizable(NSLocalizedString("payment_titular", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let number = Localizable(NSLocalizedString("payment_number", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let expiration = Localizable(NSLocalizedString("payment_date", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let ccv = Localizable(NSLocalizedString("payment_ccv", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let infoDeleteCard = Localizable(NSLocalizedString("payment_card_delete", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let noPaymentMethods = Localizable(NSLocalizedString("payment_nodata", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let yesRemove = Localizable(NSLocalizedString("payment_delete_accept", bundle: SDKNavigation.getBundle(), comment: "Button"))
}
