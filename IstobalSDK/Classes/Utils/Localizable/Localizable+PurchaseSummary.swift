//
//  Localizable+PurchaseSummary.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Purchase Summary
extension Localizable {
    static let purchaseSummary = Localizable(NSLocalizedString("purchase_summary", bundle: SDKNavigation.getBundle(), comment: "NavBar Title"))
    static let makePayment = Localizable(NSLocalizedString("do_payment", bundle: SDKNavigation.getBundle(), comment: "make payment - button"))
    static let vehicle = Localizable(NSLocalizedString("wash_vehicle", bundle: SDKNavigation.getBundle(), comment: "vehicle - title"))
    static let washingCenter = Localizable(NSLocalizedString("wash_installation", bundle: SDKNavigation.getBundle(), comment: "washing center - title"))
    static let total = Localizable(NSLocalizedString("wash_total", bundle: SDKNavigation.getBundle(), comment: "total price"))
    static let creditAvailable = Localizable(NSLocalizedString("credit_available", bundle: SDKNavigation.getBundle(), comment: "credit available"))
    static let repeatLastWash = Localizable(NSLocalizedString("wash_repeat", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let estimatedTime = Localizable(NSLocalizedString("wash_time_total", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let addNewVehicle = Localizable(NSLocalizedString("add_new_vehicle", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let discountCoupon = Localizable(NSLocalizedString("wash_purchase_coupon", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let noDiscountCoupon = Localizable(NSLocalizedString("wash_purchase_without_coupon", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let addDiscountCoupon = Localizable(NSLocalizedString("wash_purchase_add_coupon", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let couponRedeemed = Localizable(NSLocalizedString("coupon_redeemed", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let addMoreServices = Localizable(NSLocalizedString("services_add_more", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let update = Localizable(NSLocalizedString("update", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let discountPacks = Localizable(NSLocalizedString("wash_discount_packs", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let discountPacksInfo = Localizable(NSLocalizedString("wash_discount_packs_info", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let abono = Localizable(NSLocalizedString("wash_packs_subscription", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let xDiscount = Localizable(NSLocalizedString("value_discount", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let payWithLoyaltyCardInfo = Localizable(NSLocalizedString("fidelity_offer_error", bundle: SDKNavigation.getBundle(), comment: "Label"))
}
