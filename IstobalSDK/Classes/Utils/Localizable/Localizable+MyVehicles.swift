//
//  Localizable+MyVehicles.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - My Vehicles
extension Localizable {
    static let buyWash = Localizable(NSLocalizedString("buy_wash", bundle: SDKNavigation.getBundle(), comment: "Buy wash - button"))
    static let lastWashes = Localizable(NSLocalizedString("last_tickets_vehicle", bundle: SDKNavigation.getBundle(), comment: "Last washes - section header"))
    static let vehicleNameCell = Localizable(NSLocalizedString("vehicle_name_cell", bundle: SDKNavigation.getBundle(), comment: "Vehicle name - cell"))
    static let vehicleBrand = Localizable(NSLocalizedString("vehicle_brand", bundle: SDKNavigation.getBundle(), comment: "Vehicle brand - cell"))
    static let vehicleModel = Localizable(NSLocalizedString("vehicle_model", bundle: SDKNavigation.getBundle(), comment: "Vehicle model - cell"))
    static let drivers = Localizable(NSLocalizedString("vehicle_drivers", bundle: SDKNavigation.getBundle(), comment: "Drivers - Section header"))
    static let days = Localizable(NSLocalizedString("days_number_multiple", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let day = Localizable(NSLocalizedString("days_number_single", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let aWash = Localizable(NSLocalizedString("wash_number_single", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let variousWashes = Localizable(NSLocalizedString("wash_number_multiple", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let aUser = Localizable(NSLocalizedString("vehicle_number_single", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let variousUsers = Localizable(NSLocalizedString("vehicle_number_multiple", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let unnamedVehicle = Localizable(NSLocalizedString("unnamed_vehicle", bundle: SDKNavigation.getBundle(), comment: "unnamed vehicle"))
    static let lastWash = Localizable(NSLocalizedString("last_wash", bundle: SDKNavigation.getBundle(), comment: "Last wash - Label"))
    static let ago = Localizable(NSLocalizedString("ago", bundle: SDKNavigation.getBundle(), comment: "X days ago - Label"))
    static let removeVehicle = Localizable(NSLocalizedString("vehicle_delete", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let removeVehicleDetail = Localizable(NSLocalizedString("vehicle_delete_confirm", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let confirmRemoveVehicle = Localizable(NSLocalizedString("vehicle_delete_accept", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let seeAllTickets = Localizable(NSLocalizedString("show_all", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let totalWashes = Localizable(NSLocalizedString("wash_number", bundle: SDKNavigation.getBundle(), comment: "Label"))
}

// MARK: - Add Vehicle Scene
extension Localizable {
    static let introAddVehicle = Localizable(NSLocalizedString("title_intro_add_vehicle", bundle: SDKNavigation.getBundle(), comment: "Title Intro - label"))
    static let subIntroAddVehicle = Localizable(NSLocalizedString("add_vehicle_now", bundle: SDKNavigation.getBundle(), comment: "Subtitle Intro - Label"))
    static let addVehicle = Localizable(NSLocalizedString("add_vehicle", bundle: SDKNavigation.getBundle(), comment: "Add vehicle - Button"))
    static let continueWithoutVehicle = Localizable(NSLocalizedString("btn_no_add_vehicle", bundle: SDKNavigation.getBundle(), comment: "continue without vehicle - Button"))
    static let addVehicleQuestion1 = Localizable(NSLocalizedString("add_vehicle_question_1", bundle: SDKNavigation.getBundle(), comment: "Question 1 to add vehicle"))
    static let addVehicleQuestion2 = Localizable(NSLocalizedString("add_vehicle_step_2", bundle: SDKNavigation.getBundle(), comment: "Question 2 to add vehicle"))
    static let registrationNumber = Localizable(NSLocalizedString("vehicle_registration", bundle: SDKNavigation.getBundle(), comment: "registration number"))
    static let vehicleName = Localizable(NSLocalizedString("vehicle_name", bundle: SDKNavigation.getBundle(), comment: "Vehicle Name"))
    static let exampleVehicleName = Localizable(NSLocalizedString("vehicle_name_example", bundle: SDKNavigation.getBundle(), comment: "Example Vehicle Name"))
    static let exampleLicensePlate = Localizable(NSLocalizedString("vehicle_registration_example", bundle: SDKNavigation.getBundle(), comment: "Example for license plate"))
    static let exampleLicensePlateError = Localizable(NSLocalizedString("example_license_plate_error", bundle: SDKNavigation.getBundle(), comment: "Example for license plate"))
    static let congratsAddedVehicle = Localizable(NSLocalizedString("add_vehicle_success", bundle: SDKNavigation.getBundle(), comment: "Congrats added vehicle"))
}

// MARK: - Registration number
extension Localizable {
    static let registrationNumberScan = Localizable(NSLocalizedString("registration_number_scan", bundle: SDKNavigation.getBundle(), comment: "NavBar Title"))
    static let registrationNumberScanInfo = Localizable(NSLocalizedString("registration_number_scan_info", bundle: SDKNavigation.getBundle(), comment: "scan info"))
    static let youHaveTickets = Localizable(NSLocalizedString("you_have_tickets", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let youDontHaveTickets = Localizable(NSLocalizedString("you_dont_have_tickets", bundle: SDKNavigation.getBundle(), comment: "Label"))
    static let buyNewWash = Localizable(NSLocalizedString("buy_new_wash", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let startWash = Localizable(NSLocalizedString("start_wash", bundle: SDKNavigation.getBundle(), comment: "Button"))
}
