//
//  Localizable+Tutorial.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Tutorial
extension Localizable {
    static let skip = Localizable(NSLocalizedString("btn_close_tutorial", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let finishTutorial = Localizable(NSLocalizedString("finish_tutorial", bundle: SDKNavigation.getBundle(), comment: "Button"))
    static let tutorialPage1Body = Localizable(NSLocalizedString("tutorial_description_1", bundle: SDKNavigation.getBundle(), comment: "body of page 1"))
    static let tutorialPage2Title = Localizable(NSLocalizedString("tutorial_title_2", bundle: SDKNavigation.getBundle(), comment: "body1 of page 2"))
    static let tutorialPage2Body1 = Localizable(NSLocalizedString("tutorial_phase_2_description_1", bundle: SDKNavigation.getBundle(), comment: "body1 of page 2"))
    static let tutorialPage2Body2 = Localizable(NSLocalizedString("tutorial_phase_2_description_2", bundle: SDKNavigation.getBundle(), comment: "body3 of page 2"))
    static let tutorialPage2Body3 = Localizable(NSLocalizedString("tutorial_phase_2_description_3", bundle: SDKNavigation.getBundle(), comment: "body3 of page 2"))
    static let tutorialPage3Title = Localizable(NSLocalizedString("tutorial_page3_title", bundle: SDKNavigation.getBundle(), comment: "title of page 3"))
    static let tutorialPage3Body = Localizable(NSLocalizedString("tutorial_description_3", bundle: SDKNavigation.getBundle(), comment: "body of page "))
    static let tutorialPage4Title = Localizable(NSLocalizedString("tutorial_page4_title", bundle: SDKNavigation.getBundle(), comment: "title of page 4"))
    static let tutorialPage4Body = Localizable(NSLocalizedString("tutorial_description_4", bundle: SDKNavigation.getBundle(), comment: "body of page 4"))
    static let tutorialPage5Title = Localizable(NSLocalizedString("tutorial_page5_title", bundle: SDKNavigation.getBundle(), comment: "title of page 5"))
    static let tutorialPage5Body = Localizable(NSLocalizedString("tutorial_description_5", bundle: SDKNavigation.getBundle(), comment: "body of page 5"))
}
