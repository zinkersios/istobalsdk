//
//  Localizable+Errors.swift
//  istobal
//
//  Created by Shanti Rodríguez on 20/6/18.
//  Copyright © 2018 Istobal. All rights reserved.
//

import Foundation

// MARK: - Errors
extension Localizable {
    static let error404 = Localizable(NSLocalizedString("error_api_404", bundle: SDKNavigation.getBundle(), comment: "Error 404"))
    static let errorGeneral = Localizable(NSLocalizedString("error_general", bundle: SDKNavigation.getBundle(), comment: "Error general"))
    static let errorFacebookPermissions = Localizable(NSLocalizedString("login_error_permissions", bundle: SDKNavigation.getBundle(), comment: "Error requesting facebook permissions"))
    static let errorRegister = Localizable(NSLocalizedString("signup_error", bundle: SDKNavigation.getBundle(), comment: "There was an error registering the user"))
    static let errorConnection = Localizable(NSLocalizedString("error_connection", bundle: SDKNavigation.getBundle(), comment: "Rhere is no Internet"))
    static let error101 = Localizable(NSLocalizedString("error_101", bundle: SDKNavigation.getBundle(), comment: "Error code 101"))
    static let error102 = Localizable(NSLocalizedString("error_102", bundle: SDKNavigation.getBundle(), comment: "Error code 102"))
    static let error103 = Localizable(NSLocalizedString("error_103", bundle: SDKNavigation.getBundle(), comment: "Error code 103"))
    static let error104 = Localizable(NSLocalizedString("error_104", bundle: SDKNavigation.getBundle(), comment: "Error code 104"))
    static let error105 = Localizable(NSLocalizedString("error_105", bundle: SDKNavigation.getBundle(), comment: "Error code 105"))
    static let error106 = Localizable(NSLocalizedString("error_106", bundle: SDKNavigation.getBundle(), comment: "Error code 106"))
    static let error107 = Localizable(NSLocalizedString("error_107", bundle: SDKNavigation.getBundle(), comment: "Error code 107"))
    static let error108 = Localizable(NSLocalizedString("error_108", bundle: SDKNavigation.getBundle(), comment: "Error code 108"))
    static let error202 = Localizable(NSLocalizedString("error_202", bundle: SDKNavigation.getBundle(), comment: "Error code 202"))
    static let error203 = Localizable(NSLocalizedString("error_203", bundle: SDKNavigation.getBundle(), comment: "Error code 203"))
    static let error204 = Localizable(NSLocalizedString("error_204", bundle: SDKNavigation.getBundle(), comment: "Error code 204"))
    static let error205 = Localizable(NSLocalizedString("error_205", bundle: SDKNavigation.getBundle(), comment: "Error code 205"))
    static let error301 = Localizable(NSLocalizedString("error_301", bundle: SDKNavigation.getBundle(), comment: "Error code 301"))
    static let error302 = Localizable(NSLocalizedString("error_302", bundle: SDKNavigation.getBundle(), comment: "Error code 302"))
    static let error303 = Localizable(NSLocalizedString("error_303", bundle: SDKNavigation.getBundle(), comment: "Error code 303"))
    static let error401 = Localizable(NSLocalizedString("error_401", bundle: SDKNavigation.getBundle(), comment: "Error code 401"))
}
